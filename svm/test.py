import numpy as np
from numpy.linalg import norm
from numpy import dot
import gzip
import cPickle
import IO
from sklearn.svm import SVC,LinearSVC

def test_mnist():

	training =  IO.unpickle(icdar_train_pkl)
	testing = IO.unpickle(icdar_test_pkl)

	training = zip(training[0],training[1])
	testing = zip(testing[0],testing[1])

	for i in xrange(10):
		for j in xrange(i+1,10):
			train = filter(lambda x:(x[1] == i or x[1] == j),training)
			test = filter(lambda x:(x[1] == i or x[1] == j),testing)

			svc = SVC(kernel='rbf')
			x,y = zip(*train)
			svc.fit(x,y)
			x,y = zip(*test)
			print i,j, svc.score(x,y)

	



def f1(a,b):
	s = 0
	limit = 1000000
	for i in xrange(limit):
		m1 = np.random.random(a.shape) > .5
		m2 = np.random.random(a.shape) > .5

		r = np.dot(m1*a,m2*b)
		s += r
	s *= 1.
	s /= limit

	print s


def f2(a,b,c,d,p):
	s = 0
	limit = 1000000
	for i in xrange(limit):
		m1 = np.random.random(a.shape) > 1-p
		m2 = np.random.random(a.shape) > 1-p

		r = (np.dot(m1*a,m2*b)+c)**d
		s += r
	s *= 1.
	s /= limit

	return s


def polynomial_kernel(x,y,c,d,p):
	s = 0
	for k in xrange(0,d+1):
		s += (p**2*np.dot(x,y))**(d-k) *c**k


def random_exp_kernel(x,y,sigma,p):
	s = 0
	limit = 1000000
	for i in xrange(limit):
		m1 = np.random.random(a.shape) > 1-p
		m2 = np.random.random(a.shape) > 1-p

		z = dot(m1*x-m2*y,m1*x-m2*y)

		r = np.exp(-z/sigma)
		s += r
	s *= 1.
	s /= limit
	return s

def closed_exp_kernel(x,y,sigma,p):
	n = len(x)
	t = 1
	for i in xrange(n):
		a = np.exp(-(x[i]-y[i])**2/sigma)
		b = np.exp(-x[i]**2/sigma)
		c = np.exp(-y[i]**2/sigma)

		t *= a*(p**2) + b*p*(1-p) + c*p*(1-p)+ (1-p)**2
	return t

def exp_kernel(x,y,sigma):
	return np.exp(-dot(x-y,x-y)/sigma)
	

if __name__=='__main__':
	a = np.array([1,2.,3])
	b = np.array([7,4.,-3])
	a = np.random.random(10)*20
	b = np.random.random(10)*56

	c = 1
	d = 3
	p = .9
	sigma = 133
	test_mnist()
	# print exp_kernel(a,b,sigma)
	# print random_exp_kernel(a,b,sigma,p)
	# print closed_exp_kernel(a,b,sigma,p)

