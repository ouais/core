""" This file contains different utility functions that are not connected
in anyway to the networks presented in the tutorials, but rather help in
processing the outputs into a more understandable way.

For example ``tile_raster_images`` helps in generating a easy to grasp
image from a set of samples or weights.
"""


import numpy
import scipy
import scipy.sparse
import pylab as plt

def downsample(rng,y,n_samples):
    classes = numpy.unique(y)
    total_candidates = []

    for c in classes:
        candidates = numpy.where(y == c)[0]
        rng.shuffle(candidates)
        total_candidates.extend(candidates[:n_samples])

    return numpy.array(total_candidates)


class batch(object):
    def __init__(self,batch_size):
        self.batch_size = batch_size

    def __call__(self,f):
        def wrapper(t,X):

            p = 0
            rem = 0            

            while p < X.shape[0]:                
                Z = X[p:min(p+self.batch_size,X.shape[0])]

                if not isinstance(Z,numpy.ndarray):
                         Z = numpy.array(Z.todense(),dtype='float32')

                if Z.shape[0] != self.batch_size:   

                    zeros = numpy.zeros((self.batch_size-Z.shape[0],) + (Z.shape[1:]))
                    rem = Z.shape[0]
                    Z = numpy.array(numpy.concatenate((Z,zeros),axis=0),dtype=X.dtype)

                    # else:
                    #     zeros = numpy.zeros((self.batch_size-Z.shape[0],X.shape[1]),dtype='float32')
                    #     rem = Z.shape[0]                        
                    #     Z = scipy.sparse.vstack((Z,X.__class__(zeros)))

                
                temp_results = f(t,Z)
                if rem != 0:
                    temp_results = temp_results[:rem]

                if p == 0:                    
                    results = numpy.zeros((X.shape[0],) + temp_results.shape[1:])
                # print Z.shape, X.shape,p, min(p+self.batch_size,X.shape[batch_dim]), temp_results.shape
                results[p:min(p+self.batch_size,X.shape[0])] = temp_results

                p += self.batch_size
            return numpy.array(results,dtype='float32')
        return wrapper

def scale_to_unit_interval(ndar, eps=1e-8):
    """ Scales all values in the ndarray ndar to be between 0 and 1 """
    ndar = ndar.copy()
    ndar -= ndar.min()
    ndar *= 1.0 / (ndar.max() + eps)
    return ndar


def tile_raster_images(X, img_shape, tile_shape, tile_spacing=(0, 0),
                       scale_rows_to_unit_interval=True,
                       output_pixel_vals=True):
    """
    Transform an array with one flattened image per row, into an array in
    which images are reshaped and layed out like tiles on a floor.

    This function is useful for visualizing datasets whose rows are images,
    and also columns of matrices for transforming those rows
    (such as the first layer of a neural net).

    :type X: a 2-D ndarray or a tuple of 4 channels, elements of which can
    be 2-D ndarrays or None;
    :param X: a 2-D array in which every row is a flattened image.

    :type img_shape: tuple; (height, width)
    :param img_shape: the original shape of each image

    :type tile_shape: tuple; (rows, cols)
    :param tile_shape: the number of images to tile (rows, cols)

    :param output_pixel_vals: if output should be pixel values (i.e. int8
    values) or floats

    :param scale_rows_to_unit_interval: if the values need to be scaled before
    being plotted to [0,1] or not


    :returns: array suitable for viewing as an image.
    (See:`PIL.Image.fromarray`.)
    :rtype: a 2-d array with same dtype as X.

    """

    assert len(img_shape) == 2
    assert len(tile_shape) == 2
    assert len(tile_spacing) == 2

    # The expression below can be re-written in a more C style as
    # follows :
    #
    # out_shape    = [0,0]
    # out_shape[0] = (img_shape[0]+tile_spacing[0])*tile_shape[0] -
    #                tile_spacing[0]
    # out_shape[1] = (img_shape[1]+tile_spacing[1])*tile_shape[1] -
    #                tile_spacing[1]
    out_shape = [(ishp + tsp) * tshp - tsp for ishp, tshp, tsp
                        in zip(img_shape, tile_shape, tile_spacing)]

    if isinstance(X, tuple):
        assert len(X) == 4
        # Create an output numpy ndarray to store the image
        if output_pixel_vals:
            out_array = numpy.zeros((out_shape[0], out_shape[1], 4),
                                    dtype='uint8')
        else:
            out_array = numpy.zeros((out_shape[0], out_shape[1], 4),
                                    dtype=X.dtype)

        #colors default to 0, alpha defaults to 1 (opaque)
        if output_pixel_vals:
            channel_defaults = [0, 0, 0, 255]
        else:
            channel_defaults = [0., 0., 0., 1.]

        for i in xrange(4):
            if X[i] is None:
                # if channel is None, fill it with zeros of the correct
                # dtype
                dt = out_array.dtype
                if output_pixel_vals:
                    dt = 'uint8'
                out_array[:, :, i] = numpy.zeros(out_shape,
                        dtype=dt) + channel_defaults[i]
            else:
                # use a recurrent call to compute the channel and store it
                # in the output
                out_array[:, :, i] = tile_raster_images(
                    X[i], img_shape, tile_shape, tile_spacing,
                    scale_rows_to_unit_interval, output_pixel_vals)
        return out_array

    else:
        # if we are dealing with only one channel
        H, W = img_shape
        Hs, Ws = tile_spacing

        # generate a matrix to store the output
        dt = X.dtype
        if output_pixel_vals:
            dt = 'uint8'
        out_array = numpy.zeros(out_shape, dtype=dt)

        for tile_row in xrange(tile_shape[0]):
            for tile_col in xrange(tile_shape[1]):
                if tile_row * tile_shape[1] + tile_col < X.shape[0]:
                    this_x = X[tile_row * tile_shape[1] + tile_col]
                    if scale_rows_to_unit_interval:
                        # if we should scale values to be between 0 and 1
                        # do this by calling the `scale_to_unit_interval`
                        # function
                        this_img = scale_to_unit_interval(
                            this_x.reshape(img_shape))
                    else:
                        this_img = this_x.reshape(img_shape)
                    # add the slice to the corresponding position in the
                    # output array
                    c = 1
                    if output_pixel_vals:
                        c = 255
                    out_array[
                        tile_row * (H + Hs): tile_row * (H + Hs) + H,
                        tile_col * (W + Ws): tile_col * (W + Ws) + W
                        ] = this_img * c
        return out_array


def plot_histograms(firings, N):
    
    N = int(numpy.ceil(numpy.sqrt(N)))
    plt.figure(figsize=(N,N))

    axisNum = 0
    for row in range(N):
        for col in range(N):
            axisNum += 1
            ax = plt.subplot(N, N, axisNum)      
            ax.set_xticklabels([])
            ax.set_yticklabels([])  
            plt.hist(firings[:,row*N+col],bins=50)

    plt.show()