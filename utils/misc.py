import numpy as np
import Image
from utils import tile_raster_images
import collections
import re


__stat_file = "stats.txt"
data_dir = "/home/oalsha1/data/"

dictionary_dir = data_dir+"dictionary.pkl"

icdar_train_pkl  = data_dir + "icdar/char/icdar_train.pkl"
icdar_test_pkl  = data_dir + "icdar/char/icdar_test.pkl"

words_test_dir = data_dir+'words/test/'
words_test_pkl = data_dir+'words/test/test_words.pkl'

words_train_dir = data_dir+'words/train/'
words_train_pkl = data_dir+'words/train/train_words.pkl'

hmm_maxout_pkl = data_dir + 'hmm_maxout_training_set.pkl'

synthetic_words_pkl = data_dir+'synthetic_words.pkl'
natural_words_pkl = data_dir+'natural_words.pkl'
synthetic_chars_pkl = data_dir+'synthetic_chars.pkl'
extended_train_pkl = data_dir + 'extended_train.pkl'

icdar_scene_train_pkl = data_dir+'icdar_scene_train.pkl'
icdar_scene_test_pkl = data_dir+'icdar_scene_test.pkl'

SVT_scene_test_pkl = data_dir+ 'SVT_scene_test.pkl'

SVT_test_words_pkl = data_dir + 'SVT_test_words.pkl'
SVT_test_words_lexicons_pkl = data_dir + 'SVT_test_words_lexicons.pkl'
SVT_scene_test_stage1_pkl = data_dir + 'SVT_scene_test_stage1.pkl'
SVT_scene_test_stage2_pkl = data_dir + 'SVT_scene_test_stage2.pkl'
SVT_scene_lexicons_pkl = data_dir+'svt_scene_lexicons.pkl'

icdar_scene_test_stage1_pkl = data_dir+'icdar_scene_test_stage1.pkl'
icdar_scene_test_stage2_pkl = data_dir+'icdar_scene_test_stage2.pkl'
icdar_scene_test_stage2_0_pkl = data_dir+'icdar_scene_test_stage2_0.pkl'
icdar_scene_test_stage2_5_pkl = data_dir+'icdar_scene_test_stage2_5.pkl'
icdar_scene_test_stage2_20_pkl = data_dir+'icdar_scene_test_stage2_20.pkl'
icdar_scene_test_stage2_50_pkl = data_dir+'icdar_scene_test_stage2_50.pkl'
icdar_scene_test_stage2_full_pkl = data_dir+'icdar_scene_test_stage2_full.pkl'
icdar_scene_test_stage2_large_pkl = data_dir+'icdar_scene_test_stage2_large.pkl'

icdar_line_segmentation_pkl = data_dir + 'icdar_line_segmentation.pkl'

test_words_lex5_pkl = data_dir+'test_word_lexicons_5.pkl'
test_words_lex20_pkl = data_dir+'test_word_lexicons_20.pkl'
test_words_lex50_pkl = data_dir+'test_word_lexicons_50.pkl'

large_lexicon = data_dir + 'large_lexicon.txt'

alphabet_pkl = data_dir +'alphabet.pkl'

da_pkl = data_dir + 'da.pkl'

ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
CAPITALS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
SMALLS = 'abcdefghijklmnopqrstuvwxyz'
NUMS = '0123456789'

iALPHABET = {}
iALPHABET.update([(i, ALPHABET.index(i)) for i in ALPHABET])

ALPHABETi = {}
ALPHABETi.update([(ALPHABET.index(i),i) for i in ALPHABET])

def compute_stats(X_train,y_train,X_test,y_test):

    f = open(__stat_file,"w")
    f.write("number of training samples: {0}\n".format(X_train.shape[0]))
    f.write("number of testing samples: {0}\n".format(X_test.shape[0]))

    s = collections.Counter(y_train)

    f.write("number of classes in training set: {0}\n".format(len(s)))
    f.write("dimensionality of input: {0}\n".format(X_train.shape[1]))

    f.write("class distribution in training set: \n")
    a = map(lambda x: (chr(x), s[x], round(s[x]/float(len(y_train)),3)),s)
    a.sort()
    for x in a:
        f.write("char: {0}, count: {1}, percent: {2}\n".format(x[0],int(x[1]),x[2]))

    s = collections.Counter(y_test)

    f.write("\n")
    f.write("number of classes in testing set: {0}\n".format(len(s)))
    f.write("class distribution in testing set: \n")
    a = map(lambda x: (chr(x), s[x], round(s[x]/float(len(y_test)),3)),s)
    a.sort()
    for x in a:
        f.write("char: {0}, count: {1}, percent: {2}\n".format(x[0],int(x[1]),x[2]))

    f.close()
    


def save_dataset(pkl_path=None,X=None,image_path='dataset.png'):
    if pkl_path != None:
        X,y = IO.unpickle(pkl_path)
    
    image = Image.fromarray(tile_raster_images(X=X,img_shape=(32, 32), tile_shape=(30, 30),tile_spacing=(1, 1)))
    image.save(image_path)



if __name__=='__main__':
    clean_large_lexicon()