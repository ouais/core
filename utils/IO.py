import cPickle
import cloud

def pickle(data,outfile):
    with open(outfile, "w") as f:
        cloud.serialization.cloudpickle.dump(data,f)
    
def unpickle(path):
    return cPickle.load(open(path,"r"))