import time
import numpy as np
import gzip
import cPickle

from utils import IO
from utils.utils import *
from utils.misc import *

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from theano.tensor.signal import downsample
from theano.tensor.nnet import conv

from deepnn.NN import HiddenLayer, NeuralNetwork
from deepnn.costs import *

import preprocessing
from preprocessing import normalize_dataset,flatten

from deepnn.algorithm import Backpropagation

from pylab import *

from pylearn2.sandbox.cuda_convnet.response_norm import CrossMapNorm
from pylearn2.sandbox.cuda_convnet.filter_acts import FilterActs
from pylearn2.sandbox.cuda_convnet.pool import MaxPool
from pylearn2.sandbox.cuda_convnet.stochastic_pool import StochasticMaxPool
from pylearn2.expr.normalize import CrossChannelNormalization
from pylearn2.linear.local_c01b import Local

from theano.sandbox.cuda.basic_ops import gpu_contiguous

import warnings
warnings.simplefilter('error')


class ConvPoolLayer(object):
	def __init__(self, numpy_rng, theano_rng, input, filter_shape, pool_shape, pool_stride,dropout=0, activation = T.tanh):

		self.input = input
		self.numpy_rng = numpy_rng
		self.theano_rng = theano_rng
		self.original_dropout = dropout

		self.W = theano.shared(np.array(numpy_rng.normal(0,.01,filter_shape),dtype=theano.config.floatX), name='W',borrow=True)

		b_values = np.zeros((filter_shape[0],), dtype=theano.config.floatX)
		self.b = theano.shared(value=b_values, borrow=True)

		self.p_drop = theano.shared(theano._asarray(dropout, dtype=theano.config.floatX),borrow=True)
		self.activation_scale = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)

		noise = theano_rng.normal(size=(self.b.shape), avg=0.0, std=.01)
		self.noise_scale = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)

		input_shuffled = input.dimshuffle(1, 2, 3, 0) # bc01 to c01b
		filters_shuffled = self.W.dimshuffle(1, 2, 3, 0) # bc01 to c01b

		conv_op = FilterActs(stride=1, partial_sum= 1,pad=(filter_shape[2]-1))
		contiguous_input = gpu_contiguous(input_shuffled)
		contiguous_filters = gpu_contiguous(filters_shuffled)
		conv_out_shuffled = conv_op(contiguous_input, contiguous_filters)

		pool_op = MaxPool(ds=pool_shape, stride=pool_stride)
		pooled_out_shuffled = pool_op(conv_out_shuffled)

		normalization_op = CrossChannelNormalization( alpha = 1e-3, beta= .5, n =31)
		contiguous_input = gpu_contiguous(pooled_out_shuffled)
		pooled_out_shuffled = normalization_op(contiguous_input)

		pooled_out = pooled_out_shuffled.dimshuffle(3, 0, 1, 2) # c01b to bc01

		self.noisy_b = self.b + noise * self.noise_scale

		self.output = activation(pooled_out + self.b.dimshuffle('x', 0, 'x', 'x'))

		mask  = self.theano_rng.binomial(size = (self.output.shape) ,p = self.p_drop,dtype='float32')

		if dropout > 0:
			self.output = self.output * mask * self.activation_scale

		self.activation = activation

		self.conv_out = conv_out_shuffled
		self.pooled_out = pooled_out

		self.params = [self.W, self.b]

	def set_dropout(self,scale):
		self.p_drop.set_value(value=scale)

	def set_activation_scale(self,scale):
		self.activation_scale.set_value(value=scale)

	def set_test_mode(self):

		self.set_dropout(1)
		self.noise_scale.set_value(0)

		activation_scale = self.original_dropout if self.original_dropout > 0 else 1
		self.set_activation_scale(activation_scale)

	def set_train_mode(self):
		self.set_dropout(self.original_dropout)				
		self.set_activation_scale(1)
		self.noise_scale.set_value(1)




class CNN(NeuralNetwork):

	def __init__(self,input_size=32,kernel_size=5,batch_size=128, pool_sizes=[5,5,5], pool_strides=[2,2,2], nkerns=[64,64,64],nnodes=[],drops=[], activation=lambda x: T.maximum(x,0.0)):

		self.x = T.fmatrix('x') 
		self.y = T.ivector('y')

		self.time = time.time()

		self.input_size = input_size
		self.batch_size = batch_size
		self.nkerns = nkerns
		self.nnodes = nnodes

		self.numpy_rng = np.random.RandomState(666)
		self.theano_rng = RandomStreams(self.numpy_rng.randint(666))

		self.n_layers = len(nkerns) + len(nnodes)
		
		self.layers = []
		self.output = []
		self.layers_sizes = []

		nkerns.insert(0,3)

		layer_input = self.x

		layer_input = layer_input.reshape((batch_size,3, input_size, input_size,))

		self.input_dropout = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)
		self.input_scale = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)

		if len(drops) != len(nkerns) + len(nnodes):
			drops = [0]* (len(nkerns) + len(nnodes)+1)

		self.drops = drops

		if self.drops[0] > 0:
			self.input_dropout.set_value(value= self.drops[0])
			mask  = self.theano_rng.binomial(size = (layer_input.shape) ,p=self.input_dropout,dtype='float32')
			layer_input = layer_input * mask * self.input_scale

		# add the convolutional layers
		drops_id = 1
		for i in xrange(1,len(nkerns)):				
			# create a new convolutional layer
			h = ConvPoolLayer(self.numpy_rng , self.theano_rng, input=layer_input,
				filter_shape=(nkerns[i],nkerns[i-1], kernel_size, kernel_size), 
				pool_shape=pool_sizes[i-1],
				pool_stride=pool_strides[i-1], dropout=drops[drops_id], activation=activation)

			input_size = int(np.ceil( (input_size+kernel_size-pool_sizes[i-1]-1)/float(pool_strides[i-1]))) + 1
			layer_input = h.output

			self.layers.append(h)

			self.output.append(theano.function([self.x], h.output))

			self.layers_sizes.append(nkerns[i] * input_size * input_size)
			drops_id +=1

		layer_input = layer_input.flatten(2)
		input_size = nkerns[-1] * input_size * input_size


		for i in xrange(len(nnodes)):
			h = HiddenLayer(self.numpy_rng,self.theano_rng, input=layer_input, n_in=input_size,n_out=nnodes[i],dropout=drops[drops_id], activation=activation)
			input_size = nnodes[i]
			layer_input = h.output

			self.layers.append(h)
			self.output.append(theano.function([self.x], h.output))

			self.layers_sizes.append(input_size)
			drops_id += 1

		self.last_input = layer_input

	def __str__(self):
		name = 'cnn'
		for k in self.n_kerns:
			name += '_{}' .format(k)

		for k in self.nnodes:
			name += '_{}' .format(k)

		if np.sum(self.drops) > 0:
			name += '_dropout'

		return name

def monitor(epoch,model,algorithm,save_location,X,y,X_2,y_2):
	model.set_test_mode()


	print 'epoch: {} time: {:.2f}'.format(epoch, time.time()-model.time)
	
	if epoch % 5 == 0 :			
		print 'train score: {:2f} test score: {:2f}'.format(1-np.mean(algorithm.batch_errors),algorithm.score(X_2,y_2))
		IO.pickle(model,save_location)

	model.time = time.time()
	model.set_train_mode()
	# IO.pickle(model,'models/cnn_cifar100_temp.pkl')

def test1():
	# X_train,y_train =IO.unpickle(data_dir+"/cifar100/transfer/transfer_train.pkl")
	# X_train,y_train =IO.unpickle(data_dir+"/cifar10/cifar10_train.pkl")
	# X_train,y_train =IO.unpickle(data_dir+"/cifar10/cifar10_train_c.pkl")
	X_train,y_train =IO.unpickle(data_dir+"/cifar100/cifar100_train_c.pkl")
	#X_train,y_train =IO.unpickle(data_dir+"/stl10/stl10_train.pkl")
	# X_train,y_train =IO.unpickle(data_dir+"/icdar_train.pkl")
	# X_train, y_train = normalize_dataset(X_train,y_train)
	X_train = preprocessing.flatten(X_train)
	#indices = IO.unpickle(data_dir+'/stl10/stl10_folds.pkl')[0]
	#X_train = X_train[indices]
	#y_train = y_train[indices]

	# y_train = np.array(IO.unpickle(data_dir+'/cifar100/cifar-100-python/train')['coarse_labels'],dtype='int32')

	# X_test,y_test = IO.unpickle(data_dir+"/cifar100/transfer/transfer_test.pkl")
	# X_test,y_test = IO.unpickle(data_dir+"/cifar10/cifar10_test_c.pkl")
	X_test,y_test = IO.unpickle(data_dir+"/cifar100/cifar100_test_c.pkl")
	# X_test,y_test = IO.unpickle(data_dir+"/stl10/stl10_test.pkl")
	# X_test,y_test =IO.unpickle(data_dir+"/icdar_test.pkl")
	# X_test,y_test = normalize_dataset(X_test,y_test)
	X_test = preprocessing.flatten(X_test)

	# y_test = np.array(IO.unpickle(data_dir+'/cifar100/cifar-100-python/test')['coarse_labels'],dtype='int32')



	nn = CNN(batch_size = 128 ,pool_sizes=[5,5,5], pool_strides=[2,2,2], nkerns=[96,96,96],drops=[0,0,0,.5,.5], nnodes=[1000])
	# nn.add_layer(100, activation= lambda x: x)	
	nn.add_layer(100)	

	cost = lambda x,y: LargeMarginLoss_2(x,y,100)

	nn = IO.unpickle('models/cnn_cifar100_96_3_1k_1_dropout.pkl')
	
	#nn.set_test_mode()	
	#print nn.score(X_test,y_test)
	
	# nn.set_train_mode()
	#return 1

	#save_location = 'models/cnn_cifar100_64_3_1k_1.pkl'	
	save_location = 'models/cnn_cifar100_96_3_1k_1_dropout.pkl'
	# save_location = 'models/cnn_stl10_fold_0_dropout.pkl'
		
	bprop = Backpropagation(nn,learning_rate=.01,max_filter_norms=[4,4,4],max_row_norms=[4,4,4],epochs=10000,momentum=.9,batch_size=128)
	bprop.add_epoch_monitor(lambda x: monitor(x,nn,bprop,save_location,X_train,y_train,X_test,y_test))

	losses= bprop.fit(X_train,y_train)
	plot(losses)
	show()

	IO.pickle(nn,save_location)

	print "algorithm: %s score: %.4f" % (str(bprop), bprop.score(X_test,y_test))
	
	

		


if __name__=='__main__':
	test1()
