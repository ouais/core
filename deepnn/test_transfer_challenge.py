import numpy as np
from utils.utils import *
from utils import IO
from utils.misc import *
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from algorithm import Backpropagation
from NN import *
from ldr.test_multitask import downsample


import preprocessing

@batch(128)
def nn_map(model,X):
	return model.output[-2](X)


def test():
	rng = np.random.RandomState(666)

	cnn = IO.unpickle("models/cnn_cifar100_ldr_2.pkl")
	
	X_train,y_train = IO.unpickle(data_dir+"/transfer_challenge/transfer_train.pkl")
	X_test,y_test = IO.unpickle(data_dir+"/transfer_challenge/transfer_test.pkl")

	X_train = preprocessing.flatten(X_train)
	X_test = preprocessing.flatten(X_test)

	indices = downsample(rng,y_train,1)
	X_train = X_train[indices]
	y_train = y_train[indices]

	X_train_new = nn_map(cnn,X_train)
	X_test_new = nn_map(cnn,X_test)

	model = LogisticRegression()
	model.fit(X_train_new,y_train)

	print "score: %.4f" % (model.score(X_test_new,y_test))
	
if __name__=='__main__':
	test()