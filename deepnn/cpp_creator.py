import sys
sys.path.append("../..")
import IO


class cascade_code:
	def __init__(self,cascade):
		self.cascade = cascade

	def headers(self):
		s = """
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;
		"""
		return s

	def global_vars(self):
		sizes_ar = '{'
		for x in self.cascade.layers_sizes:
			sizes_ar+= '%d, ' % x
		sizes_ar = sizes_ar[:-2]
		sizes_ar += '}'

		s = """
int n = %d;""" % self.cascade.n_layers	+	"""
int C = 2 """ + """
int layers_sizes [] = %s;""" % sizes_ar 	+	"""
float **h = new float*[n];
float **c = new float*[n];
"""
		return s

	def initialize(self):
		s = """
void initialize()
{
for (int i =0;i<n;i++)
	h[i] = new float[layers_sizes[i]];

for (int i =0;i<n;i++)
	c[i] = new float[C];
}
		"""
		return s


	def main(self):
		s = """
int main()
{
    Mat image;
    image = imread("rl.jpg", CV_LOAD_IMAGE_COLOR);   // Read the file
   	if(! image.data )                              // Check for invalid input
   	{
        cout <<  "Could not open or find the image" << std::endl ;
       	return -1;
   	}
    
   	namedWindow( "Display window", CV_WINDOW_AUTOSIZE );// Create a window for display.
   	imshow( "Display window", image );                   // Show our image inside it.
   	waitKey(0);                                          // Wait for a keystroke in the window
   	return 0;
}"""
		return s

	def create_file(self,file_name):
		s = ''
		s += self.headers()
		s += self.global_vars()
		s += self.initialize()
		s += self.to_function()
		s += self.main()


		f = open(file_name,'wb')
		f.write(s)
		f.close()

	def to_function(self):
		nc = self.cascade
		f = """ 
int classify()
{
		"""
		for i in xrange(nc.n_layers):
				
			c = nc.classifiers[i]	
			W = c.W.get_value(borrow=True)
			b = c.b.get_value(borrow=True)
			f += '\n' + self.classifier_to_function(W,b,i)
			if i != nc.n_layers-1:
				h = nc.hiddenLayer[i]	
				W = h.W.get_value(borrow=True)
				b = h.b.get_value(borrow=True)
				f += '\n' + self.matrix_vector_to_function(W,b,i)

		f  += """
}
"""
		return f


	def classifier_to_function(self,M,b,layer_index,eps=.005):
		total = ''
		for i in xrange(M.shape[1]):
			s = '0'
			for j in xrange(M.shape[0]):
				if M[j,i] > 0 + eps:
					s += ' + %f*v[%d]' % (M[j,i],j)

			if b[i] > 0 + eps:
				s += ' + %f' % b[i]

			s = 'c[%d][%d] = %s' %(layer_index,i,s)
			total += '\n'+s

		return total


	def matrix_vector_to_function(self,M,b,layer_index,eps=.005):
		total = ''
		for i in xrange(M.shape[1]):
			s = '0'
			for j in xrange(M.shape[0]):
				if M[j,i] > 0 + eps:
					s += ' + %f*v[%d]' % (M[j,i],j)

			if b[i] > 0 + eps:
				s += ' + %f' % b[i]

			if s != '0':
				s = 'max(0,%s)' % s
				s = 'h[%d][%d] = %s' %(layer_index,i,s)	
				total += '\n'+s
		return total



if __name__ == '__main__':
	nc = IO.unpickle('NC.pkl')
	cc = cascade_code(nc)
	cc.create_file('cpp/test_1.cpp')
