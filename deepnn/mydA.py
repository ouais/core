"""
 This tutorial introduces denoising auto-encoders (dA) using Theano.

 Denoising autoencoders are the building blocks for SdA.
 They are based on auto-encoders as the ones used in Bengio et al. 2007.
 An autoencoder takes an input x and first maps it to a hidden representation
 y = f_{\theta}(x) = s(Wx+b), parameterized by \theta={W,b}. The resulting
 latent representation y is then mapped back to a "reconstructed" vector
 z \in [0,1]^d in input space z = g_{\theta'}(y) = s(W'y + b').  The weight
 matrix W' can optionally be constrained such that W' = W^T, in which case
 the autoencoder is said to have tied weights. The network is trained such
 that to minimize the reconstruction error (the error between x and z).

 For the denosing autoencoder, during training, first x is corrupted into
 \tilde{x}, where \tilde{x} is a partially destroyed version of x by means
 of a stochastic mapping. Afterwards y is computed as before (using
 \tilde{x}), y = s(W\tilde{x} + b) and z as s(W'y + b'). The reconstruction
 error is now measured between z and the uncorrupted input x, which is
 computed as the cross-entropy :
	  - \sum_{k=1}^d[ x_k \log z_k + (1-x_k) \log( 1-z_k)]


 References :
   - P. Vincent, H. Larochelle, Y. Bengio, P.A. Manzagol: Extracting and
   Composing Robust Features with Denoising Autoencoders, ICML'08, 1096-1103,
   2008
   - Y. Bengio, P. Lamblin, D. Popovici, H. Larochelle: Greedy Layer-Wise
   Training of Deep Networks, Advances in Neural Information Processing
   Systems 19, 2007

"""

import cPickle
import gzip
import os
import sys
import time
sys.path.append("../..")
import IO


import numpy

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from theano import pp

from logistic_sgd import load_data
from logistic_sgd import LogisticRegression
from utils import tile_raster_images,scale_to_unit_interval,batch
from random import shuffle

from pylab import *
from dataset import *
from misc import *
from preprocessing import normalize_sample,resize_sample,otsu,segment
import PIL.Image
from collections import Counter


class dA(object):

	def __init__(self,n_visible,n_hidden,L1_lambda=.001):
		rng = numpy.random.RandomState(123)
		theano_rng = RandomStreams(rng.randint(2 ** 30))

		x = T.fmatrix('x')

		self._init_(numpy_rng=rng, theano_rng=theano_rng, input=x,n_visible=n_visible, n_hidden=n_hidden,L1_lambda=L1_lambda)		


	def _init_(self, numpy_rng, theano_rng=None, input=None,corruption_level=.3,L1_lambda=.001,
				 n_visible=1024, n_hidden=500,W=None, bhid=None, bvis=None):

		self.n_visible = n_visible
		self.n_hidden = n_hidden
		self.corruption_level = corruption_level
		self.L1_lambda = L1_lambda

		if not theano_rng:
			theano_rng = RandomStreams(numpy_rng.randint(2 ** 30))

		if not W:
			initial_W = numpy.asarray(numpy_rng.uniform(
					  low=-4 * numpy.sqrt(6. / (n_hidden + n_visible)),
					  high=4 * numpy.sqrt(6. / (n_hidden + n_visible)),
					  size=(n_visible, n_hidden)), dtype=theano.config.floatX)
			W = theano.shared(value=initial_W, name='W', borrow=True)

		if not bvis:
			bvis = theano.shared(value=numpy.zeros(n_visible,dtype=theano.config.floatX),borrow=True)

		if not bhid:
			bhid = theano.shared(value=numpy.zeros(n_hidden,dtype=theano.config.floatX),name='b',borrow=True)

		self.W = W
		self.b = bhid
		self.b_prime = bvis
		self.W_prime = self.W.T
		self.theano_rng = theano_rng
		self.x = T.fmatrix(name='input')
		self.y = T.ivector('y') 

		self.params = [self.W, self.b, self.b_prime]

	def get_corrupted_input(self, input, corruption_level):
		return  self.theano_rng.binomial(size=input.shape, n = 1,p = 1-corruption_level,dtype=theano.config.floatX) * input

	def get_hidden_values(self, input):
		return T.nnet.sigmoid(T.dot(input, self.W) + self.b)

	def get_reconstructed_input(self, hidden):
		return  T.nnet.sigmoid(T.dot(hidden, self.W_prime) + self.b_prime)

	def get_pretrain_cost_updates(self, learning_rate):

		tilde_x = self.get_corrupted_input(self.x, self.corruption_level)
		t = self.get_hidden_values(tilde_x)
		z = self.get_reconstructed_input(t)

		L = - T.sum(self.x * T.log(z) + (1 - self.x) * T.log(1 - z), axis=1)
		cost = T.mean(L)

		gparams = T.grad(cost, self.params)
		updates = []
		
		for param, gparam in zip(self.params, gparams):
			updates.append((param, param - learning_rate * gparam))

		self.reconstruct = theano.function([self.x],self.get_reconstructions())

		return (cost, updates)

	def get_finetune_cost_prob_updates(self,learning_rate):

		index = T.lscalar()

		tilde_x = self.get_corrupted_input(self.x, self.corruption_level)
		t = self.get_hidden_values(tilde_x)    	

		c1 = LogisticRegression(input=self.x, n_in= self.n_visible,n_out=2)
		c2 = LogisticRegression(input=t, n_in= self.n_hidden,n_out=2)


		#cost = classifier.negative_log_likelihood(self.y)#+ T.sum(T.abs_(self.W))
		cost = c1.negative_log_likelihood(self.y) + c2.negative_log_likelihood(self.y)
		#+ self.L1_lambda*T.sum(T.abs_(classifier.W)) + self.L1_lambda*T.sum(T.abs_(self.W))
		# cost = -T.mean(T.log(classifier.p_y_given_x)[T.arange(self.y.shape[0]), self.y])
		prob = c2.log_likelihood()


		self.params = [self.W, self.b]
		self.params.extend(c1.params)		
		self.params.extend(c2.params)		

		updates = []
		gparams = T.grad(cost, self.params)

		for param, gparam in zip(self.params, gparams):
			updates.append((param, param - learning_rate * gparam))

		self.gparams = gparams

		return (cost,prob,updates)

	def get_reconstructions(self):

		y = self.get_hidden_values(self.x)
		z = self.get_reconstructed_input(y)
		return z

	def pretrain(self,X,learning_rate=.1,training_epochs=30,batch_size=20):
		train_set_x = shared_dataset_X(X)	

		n_train_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size
		index = T.lscalar()  

		cost, updates = self.get_pretrain_cost_updates(learning_rate=learning_rate)

		train_da = theano.function([index], cost, updates=updates,
			givens=	{self.x: train_set_x[index * batch_size:(index + 1) * batch_size]})

		for epoch in xrange(training_epochs):
			for batch_index in xrange(n_train_batches):
				train_da(batch_index)
		

	def finetune(self,X,y,learning_rate=.1,training_epochs=100,batch_size=20):
		train_set_x,train_set_y = shared_dataset(X,y)	

		n_train_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size
		index = T.lscalar()  
		self.corruption_level = 0
		cost, prob, updates = self.get_finetune_cost_prob_updates(learning_rate=learning_rate)

		self.prob_model = theano.function([self.x], prob)

		train_da = theano.function([index], cost, updates=updates,
			givens=	
			{
				self.x: train_set_x[index * batch_size:(index + 1) * batch_size],
				self.y: train_set_y[index * batch_size:(index + 1) * batch_size],
			})

		prev_loss = 0
		loss = np.inf
		presistence = 0

		for epoch in xrange(training_epochs):			
			loss = 0
			for batch_index in xrange(n_train_batches):
				loss += train_da(batch_index)
			prev_loss = loss
			# print loss
			if loss- prev_loss > .1:
				presistence += 1
			else:
				presistence = 0
			

	@batch(128)
	def predict_proba(self,X):		
		return self.prob_model(X)
	
	def predict(self,X):
		p_hat = self.predict_proba(X)
		y_hat = numpy.zeros(len(X),dtype=int)
		for i in xrange(len(p_hat)):
			y_hat[i] = p_hat[i].argmax()
		return y_hat
	
	def score(self, X,y):
		y_hat = self.predict(X)
		hits = y == y_hat
		return hits.mean()

	def fit(self,X,y,n_hidden=5):
		X,y = normalize_dataset(X,y)
		# print 'pretraining...'
		# self.pretrain(X)
		print 'finetuning...'
		self.finetune(X,y)
		
def normalize_dataset(X,y,eps=1e-3):
	new_X = []

	for i in xrange(len(X)):		
		x = X[i].flatten()
		x -= x.min()
		x /= x.max()+ eps
		new_X.append(x)

	z = zip(new_X,y)
	shuffle(z)
	new_X,y = zip(*z)

	new_X = np.array(new_X,'float32')

	return new_X,y

def create_da_pkl():
	#X,Y,y = IO.unpickle(data_dir+'icdar_pair_train.pkl')
	#X,y =IO.unpickle(data_dir+"detector/detection_set.pkl")
	X,y =IO.unpickle(data_dir+"faces/train/cbcl_faces_train.pkl")
	X,y = normalize_dataset(X,y)
	y = np.clip(y,0,1)

	da = dA(n_visible=361,n_hidden=20)

	print 'pretraining...'
	da.pretrain(X)
	print 'finetuning...'
	da.finetune(X,y)
	
	print da.score(X,y)

	IO.pickle(da,'denoising_da.pkl')
	#a,b = da.reconstruct(new_X)

def visualize_data():
	size = 19

	da = IO.unpickle('denoising_da.pkl')
	# X,y =IO.unpickle(data_dir+"detector/detection_set.pkl")
	X,y =IO.unpickle(data_dir+"faces/train/cbcl_faces_train.pkl")

	X,y = normalize_dataset(X,y)

	new_X = da.reconstruct(X)

	R = []

	for i in xrange(len(new_X)):	
		x = new_X[i]			
		x = normalize_sample(x)
		x = x.reshape(size,size)
		R.append(x)

	W = da.W.get_value(borrow=True).T

	# matshow(W)
	# hist(W.flatten())
	# show()

	# W = da.second_W[0].get_value(borrow=True).T
	# hist(W.flatten())
	# show()

	# p8 = W[0].reshape(size,size)


	image = PIL.Image.fromarray(tile_raster_images(X=da.W.get_value(borrow=True).T,
		img_shape=(size, size), tile_shape=(10, 10),tile_spacing=(1, 1)))
	image.save('weights.png')

	image = PIL.Image.fromarray(tile_raster_images(X=numpy.array(X[:100]),
		img_shape=(size, size), tile_shape=(10, 10),tile_spacing=(2, 2)))
	image.save('originals.png')

if __name__ == '__main__':
	create_da_pkl()
	visualize_data()