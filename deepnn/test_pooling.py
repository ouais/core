import numpy as np

import theano
import theano.tensor as T

def test():
	x = T.fmatrix()
	y = T.fmatrix()
	a = np.random.random((4,4))	
	pooling_size = 2
	b = np.random.random((a.shape[0]/pooling_size,a.shape[1]/pooling_size))

	for i in xrange(0,a.shape[0], pooling_size):
		for j in xrange(0, a.shape[0], pooling_size):
			y[i/pooling_size,j/pooling_size] = T.max(x[i:i+pooling_size,j:j+pooling_size])

	f = theano.function([x],y)

	print a
	
	print f(a)



if __name__ == '__main__':
	test()
