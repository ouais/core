import numpy as np

import theano
import theano.tensor as T
from logistic_sgd import LogisticRegression

def norms():
	m = T.fmatrix()
	norms = m.norm(L= 2,axis = 0)
	normalizers = T.maximum(norms,1)

	f = theano.function([m],norms)
	g = theano.function([m],m/normalizers)

	n = np.array(np.random.random((4,4)),dtype='float32')
	print n
	print np.linalg.norm(n[:,0])
	print f(n)
	print g(n)


def logistic_regression():

	index = T.lscalar() 
	x = T.fmatrix()
	y = T.ivector()

	momentum = .9
	learning_rate = .1

	X_train,y_train =IO.unpickle(data_dir+"/cifar10/cifar10_train.pkl")
	X_train,y_train = normalize_dataset(X_train,y_train)

	classifier = LogisticRegression()
	cost = classifier.negative_log_likelihood(y_train)

	params = classifier.params

	gparams = T.grad(cost, params)

		lr = theano.shared(theano._asarray(learning_rate, dtype=theano.config.floatX),name='learning rate',borrow=True)

		for param in params:			
			inc = theano.shared(value=param.get_value() * 0,borrow=True)
			if param.name != None:
				inc.name = 'inc_'+param.name
			incs.append(inc)

	for param, gparam, inc in zip(params, gparams,incs):			

		update = inc * momentum - lr * gparam
		updates.append((inc,  update))
		updates.append((param, param + update))

	train = theano.function([index], cost, updates=updates,
			givens=	
			{
				x: train_set_x[index * batch_size:(index + 1) * batch_size],
				y: train_set_y[index * batch_size:(index + 1) * batch_size]
			}))

	prev_loss = np.inf
	loss = np.inf

	iterations = 0
	while (prev_loss == np.inf) or (prev_loss - loss > .001):
		prev_loss = loss
		loss = train(iterations)		
		iterations += 1






