
import numpy

from utils.IO import *
from utils.utils import *

import theano
import theano.sparse
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from utils import *
from random import shuffle

from dataset import *
from preprocessing import normalize_dataset,normalize_dataset2
import PIL.Image
from collections import Counter

from pylab import *


def safe_softmax(x):
    """Softmax that shouldn't overflow."""
    e_x = T.exp(x - T.max(x, axis=1, keepdims=True))
    x_sm = e_x / (T.sum(e_x, axis=1, keepdims=True) + 1e-6)
    return x_sm

class RandomFourierLayer(object):
	def __init__(self, rng, input, n_in, n_out):
		W = numpy.array(numpy.sqrt(2)*numpy.random.uniform(0,numpy.pi,(n_in,n_out/2)),dtype='float32')
		self.params = []
		self.output = T.concatenate([T.cos(T.dot(input,W)), T.sin(T.dot(input,W))], axis=1)
		# self.output = T.cos(T.dot(input,W))

class HiddenLayer(object):
	def __init__(self, rng, theano_rng, input, n_in, n_out, W=None, b=None,dropout=0,activation=T.tanh):

		self.original_dropout = dropout

		if W is None:
			W_values = numpy.asarray(rng.normal(0,.01,(n_in,n_out)),dtype=theano.config.floatX)

			W = theano.shared(value=W_values, name='W', borrow=True)

		if b is None:
			b_values = np.array(rng.normal(0,1,(n_out,)),dtype=theano.config.floatX)
			b = theano.shared(value=b_values, name='b', borrow=True)

		self.W = W
		self.b = b

		self.rng = rng
		self.theano_rng = theano_rng
		self.n_in = n_in
		self.n_out = n_out		
		self.activation = activation
		self.input = input
		self.dropout = 	dropout	

		self.p_drop = theano.shared(theano._asarray(dropout, dtype=theano.config.floatX),borrow=True)
		self.activation_scale = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)		

		noise = theano_rng.normal(size=(self.b.shape), avg=0.0, std=.01)
		self.noise_scale = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)	

		if isinstance(input,theano.sparse.SparseVariable):
			lin_output = theano.sparse.dot(input, self.W ) + self.b
		else:
			lin_output = T.dot(input, self.W) + self.b			
		
		self.output = activation(lin_output)

		mask  = self.theano_rng.binomial(size = (self.output.shape) ,p=self.p_drop,dtype='float32')
		if self.dropout > 0:
			self.output = self.output * mask * self.activation_scale

		# parameters of the model		
		self.params = [self.W, self.b]

	def set_dropout(self,scale):
		self.p_drop.set_value(value=scale)

	def set_activation_scale(self,scale):
		self.activation_scale.set_value(value=scale)

	def set_test_mode(self):

		self.set_dropout(1)
		self.noise_scale.set_value(0)

		activation_scale = 1 if self.original_dropout == 0 else self.original_dropout		
		self.set_activation_scale(activation_scale)

	def set_train_mode(self):
		self.set_dropout(self.original_dropout)				
		self.set_activation_scale(1)
		self.noise_scale.set_value(1)

	def set_noise_stddev(self,noise):
		self.noise_stddev.set_value(value=noise+1e-6)

	def refresh(self):
		W_values = numpy.asarray(self.rng.normal(0,.01,(self.n_in,self.n_out)),dtype=theano.config.floatX)

		self.W.set_value(value=W_values)

		b_values = numpy.zeros((self.n_out,), dtype=theano.config.floatX)

		self.b.set_value(value=b_values)

	def randomize_permutation(self):
		# W_values = numpy.array(self.rng.normal(size=(self.n_in, self.n_out)),dtype=theano.config.floatX)
		W_values = numpy.array([])
		ints = self.rng.randint(0,self.n_in,self.n_out)
		for i in xrange(self.n_out):
			d = numpy.zeros((self.n_in,1),dtype= 'float32')
			d[ints[i]] = 1
			if len(W_values) == 0:
				W_values = d
			else:
				W_values = numpy.hstack((W_values,d))

		self.W.set_value(value=W_values)

	def randomize_radamacher(self):
		W_values = numpy.array(self.rng.uniform((self.n_in, self.n_out)),dtype=theano.config.floatX)
		W_values[numpy.where(W_values > .5)]  = 1
		W_values[numpy.where(W_values <= .5)] = -1

	def randomize_achlioptas(self):
		W_values = numpy.array(self.rng.uniform((self.n_in, self.n_out)),dtype=theano.config.floatX)
		W_values[numpy.where(W_values >= 5/6.)]  = -1
		W_values[numpy.where(W_values >= 1/6.)]  =  0
		W_values[numpy.where(W_values >  0)]     =  1
				


class NeuralNetwork(object):

	def __init__(self,layers_sizes,activations=lambda t: T.maximum(0,t),name='none',rng=None,drops=[],sparse_input = False):

		if rng is None:			
			self.numpy_rng = numpy.random.RandomState(666)
		else:
			self.numpy_rng = rng

		self.theano_rng = RandomStreams(self.numpy_rng.randint(666))

		if sparse_input:
			self.x = theano.sparse.csc_matrix('x')
		else:
			self.x = T.fmatrix('x')
		self.y = T.ivector('y') 

		self.layers_sizes = layers_sizes		
		self.n_layers = len(layers_sizes)

		self.layers = [0]*(self.n_layers-1)
		self.output = [0]*(self.n_layers-1)

		if len(drops) != len(layers_sizes):
			drops = [0]* len(layers_sizes)

		self.drops = drops

		self.hiddenlayer_params = []

		if not isinstance(activations,list):
			activations = [activations] *(self.n_layers-1)

		self.activations = activations

		
		self.input_dropout = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)
		self.input_scale = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)
		self.name = name

		layer_input = self.x

		if self.drops[0] > 0:
			self.input_dropout.set_value(value= self.drops[0])
			mask  = self.theano_rng.binomial(size = (layer_input.shape) ,p=self.input_dropout,dtype='float32')
			layer_input = layer_input * mask * self.input_scale

		for i in xrange(self.n_layers-1):

			self.layers[i] = HiddenLayer(
				rng=self.numpy_rng, theano_rng = self.theano_rng,
				input=layer_input,n_in=layers_sizes[i], n_out=layers_sizes[i+1], 
				dropout = self.drops[i+1], activation=self.activations[i])

			self.output[i] = theano.function([self.x],self.layers[i].output)
			layer_input = self.layers[i].output


		self.last_input = layer_input

	def set_dropout(self,scale):
		for layer in self.layers:
			layer.set_dropout(value=scale)

	def set_weights_scale(self,scale):
		for layer in self.layers:
			layer.set_weights_scale(value=scale)

	def set_train_mode(self):
		self.input_dropout.set_value(value=self.drops[0])
		self.input_scale.set_value(value=1)

		for layer in self.layers:
			layer.set_train_mode()

	def set_test_mode(self):
		self.input_dropout.set_value(value=1)
		self.input_scale.set_value(value=self.drops[0])

		for layer in self.layers:
			layer.set_test_mode()

	def get_masks(self,n_classes, d):
		masks = []
		stride = d/n_classes

		for c in xrange(n_classes):
			mask = numpy.zeros(d)
			mask[c*stride:(c+1)*stride] = 1
			masks.append(mask)		
		masks = numpy.array(masks,dtype='float32')
		masks = theano.shared(value=masks, borrow=True)
		return masks
		

	def add_generic_layer(self,layer):
		self.layers.append(layer)
		self.output.append(theano.function([self.x],layer.output))

	def add_layer(self,n_out,dropout=0, activation= lambda x: safe_softmax(x)):
		h = HiddenLayer(rng=self.numpy_rng, theano_rng=self.theano_rng, input=self.last_input,n_in=self.layers_sizes[-1], n_out=n_out,dropout=dropout,activation=activation)
		self.layers.append(h)
		self.output.append(theano.function([self.x],h.output))
		self.layers_sizes.append(n_out)
		self.last_input = h.output


	def temporal_outputs(self,X,layer_index):

		firings = self.output[layer_index](X)
		return firings

	@batch(128)
	def predict(self,X):

		p_hat = self.output[-1](X)
		y_hat = p_hat.argmax(axis=1)		
		return y_hat

	def predict_proba(self,X):
		return self.output[-1](X)
	
	def score(self, X,y):

		y_hat = self.predict(X)
		return np.array(y_hat==y).mean()
	
def visualize(X,nn,layer,file_name):

	W = nn.layers[layer].W.get_value(borrow=True).T

	size = int(numpy.sqrt(W.shape[1]))
	# print size, W.shape

	# hist(W.flatten(),bins=50)
	image = PIL.Image.fromarray(tile_raster_images(X=W,
		img_shape=(size, size), tile_shape=(10,W.shape[0]/10),tile_spacing=(1, 1)))
	image.save(file_name)

	
	# show_temporal_output()
	