import cPickle
import gzip
import os
import sys
import time
sys.path.append("../..")
import IO


import numpy

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from theano import pp

from logistic_sgd import load_data
from logistic_sgd import LogisticRegression
from utils import tile_raster_images,scale_to_unit_interval,batch
from random import shuffle

from pylab import *
from dataset import *
from misc import *
from preprocessing import normalize_sample,resize_sample,otsu,segment
import PIL.Image
from collections import Counter

class HiddenLayer(object):
	def __init__(self, rng, input, n_in, n_out, W=None, b=None,activation=T.tanh):

		if W is None:

			W_values = numpy.asarray(rng.uniform(
					low=-numpy.sqrt(6. / (n_in + n_out)),
					high=numpy.sqrt(6. / (n_in + n_out)),
					size=(n_in, n_out)), dtype=theano.config.floatX)

			if activation == theano.tensor.nnet.sigmoid:
				W_values *= 4

			W = theano.shared(value=W_values, name='W', borrow=True)

		if b is None:
			b_values = numpy.zeros((n_out,), dtype=theano.config.floatX)
			b = theano.shared(value=b_values, name='b', borrow=True)

		self.W = W
		self.b = b

		lin_output = T.dot(input, self.W) + self.b
		self.output = (lin_output if activation is None
					   else activation(lin_output))
		# parameters of the model
		self.params = [self.W, self.b]

class Neural_Cascade(object):

	def __init__(self,layers_sizes,L1_lambda=2.):
		rng = numpy.random.RandomState(123)
		theano_rng = RandomStreams(rng.randint(2 ** 30))

		self.x = T.fmatrix('x')
		self.y = T.ivector('y') 

		x = self.x

		self._init_(numpy_rng=rng,layers_sizes=layers_sizes, theano_rng=theano_rng, input=x,L1_lambda=L1_lambda)		


	def _init_(self, numpy_rng, layers_sizes, theano_rng=None, input=None,corruption_level=.3,L1_lambda=.001):

		self.layers_sizes = layers_sizes		
		self.n_layers = len(layers_sizes)
		self.corruption_level = corruption_level
		self.L1_lambda = L1_lambda

		self.hiddenLayer = [0]*(self.n_layers-1)
		self.classifiers = [0]*self.n_layers
		self.params = []
		
		rectifier = lambda t: T.maximum(0,t)

		for i in xrange(self.n_layers):
			layer_input = input
			if i > 0:
				layer_input = self.hiddenLayer[i-1].output

			if i < self.n_layers-1:
				self.hiddenLayer[i] = HiddenLayer(rng=numpy_rng, input=layer_input,n_in=layers_sizes[i], n_out=layers_sizes[i+1],activation=rectifier)
				self.params.extend(self.hiddenLayer[i].params)

			self.classifiers[i] = LogisticRegression(input=layer_input, n_in= self.layers_sizes[i],n_out=2)		
			self.classifiers[i].predict_proba = theano.function([input],self.classifiers[i].p_y_given_x)
			self.classifiers[i].predict = theano.function([input],self.classifiers[i].y_pred)	
			self.params.extend(self.classifiers[i].params)


	def get_finetune_cost_updates(self,learning_rate):

		cost = 0
		for i in xrange(len(self.classifiers)):			
			cost += self.classifiers[i].negative_log_likelihood(self.y)			
		
		for p in self.params:	
			cost += self.L1_lambda*T.mean(T.abs_(p))

		updates = []
		gparams = T.grad(cost, self.params)

		for param, gparam in zip(self.params, gparams):
			updates.append((param, param - learning_rate * gparam))

		self.gparams = gparams

		return (cost,updates)


	def finetune(self,X,y,learning_rate=.1,training_epochs=10,batch_size=20):
		train_set_x,train_set_y = shared_dataset(X,y)	

		n_train_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size
		index = T.lscalar()  
		self.corruption_level = 0
		cost, updates = self.get_finetune_cost_updates(learning_rate=learning_rate)

		train_da = theano.function([index], cost, updates=updates,
			givens=	
			{
				self.x: train_set_x[index * batch_size:(index + 1) * batch_size],
				self.y: train_set_y[index * batch_size:(index + 1) * batch_size],
			})

		prev_loss = 0
		loss = np.inf
		presistence = 0

		for epoch in xrange(training_epochs):			
			loss = 0
			for batch_index in xrange(n_train_batches):
				loss += train_da(batch_index)
			prev_loss = loss
			print loss
			if loss- prev_loss > .1:
				presistence += 1
			else:
				presistence = 0

	@batch(128)
	def predict(self,X):
		y_hat = numpy.ones(len(X),dtype=int)
		for classifier in self.classifiers:
			y_hat = np.logical_and(y_hat,classifier.predict(X))
		
		return y_hat
	
	def score(self, X,y):
		y_hat = self.predict(X)
		hits = y == y_hat
		return hits.mean()

	def fit(self,X,y,n_hidden=5):
		X,y = normalize_dataset(X,y)
		# print 'pretraining...'
		# self.pretrain(X)
		print 'finetuning...'
		self.finetune(X,y)
		



def normalize_dataset(X,y,eps=1e-3):
	new_X = []

	for i in xrange(len(X)):		
		x = X[i].flatten()
		x -= x.min()
		x /= x.max()+ eps
		new_X.append(x)

	z = zip(new_X,y)
	shuffle(z)
	new_X,y = zip(*z)

	new_X = np.array(new_X,'float32')

	return new_X,y

def create_nc_pkl():
	#X,Y,y = IO.unpickle(data_dir+'icdar_pair_train.pkl')
	#X,y =IO.unpickle(data_dir+"detector/detection_set.pkl")
	X,y =IO.unpickle(data_dir+"faces/train/cbcl_faces_train.pkl")
	X,y = normalize_dataset(X,y)
	y = np.clip(y,0,1)

	nc = Neural_Cascade(layers_sizes=[361,25,44,12])
	nc.fit(X,y)	
	
	print nc.score(X,y)

	IO.pickle(nc,'NC.pkl')
	
def visualize_data():
	size = 19

	nc = IO.unpickle('NC.pkl')
	cascade_to_function(nc)

	# X,y =IO.unpickle(data_dir+"detector/detection_set.pkl")
	X,y =IO.unpickle(data_dir+"faces/train/cbcl_faces_train.pkl")

	X,y = normalize_dataset(X,y)

	W = nc.hiddenLayer[0].W.get_value(borrow=True).T

	matshow(W)
	# hist(W.flatten())
	show()

	# W = nc.second_W[0].get_value(borrow=True).T
	# hist(W.flatten())
	# show()

	# p8 = W[0].reshape(size,size)


	image = PIL.Image.fromarray(tile_raster_images(X=nc.hiddenLayer[0].W.get_value(borrow=True).T,
		img_shape=(size, size), tile_shape=(10, 10),tile_spacing=(1, 1)))
	image.save('weights.png')

	image = PIL.Image.fromarray(tile_raster_images(X=numpy.array(X[:100]),
		img_shape=(size, size), tile_shape=(10, 10),tile_spacing=(2, 2)))
	image.save('originals.png')

if __name__ == '__main__':
	# create_nc_pkl()
	visualize_data()