import theano
import theano.tensor as T

from utils import IO
from utils.misc import data_dir

from algorithm import DBN_Pretrainer
from NN import NeuralNetwork
from preprocessing import normalize_dataset2

def train_rbm():
    X_train,y_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
    X_train,y_train = normalize_dataset2(X_train,y_train)

    NN = NeuralNetwork([len(X_train[0]),500],activation_functions=T.nnet.sigmoid)

    dbnp = DBN_Pretrainer(NN,batch_size=20,learning_rate=.1,training_epochs=15)

    dbnp.fit(X_train)
    IO.pickle(dbnp,'models/mnist_rbm.pkl')

def show_filters():
    X_train, y_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
    X_train, y_train = normalize_dataset2(X_train,y_train)




if __name__ == '__main__':
    train_rbm()
