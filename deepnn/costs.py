import numpy as np
import theano
import theano.tensor as T

def CrossEntropyLoss(x,y):
	return -T.mean(T.log(x)[T.arange(y.shape[0]), y])

def WeightedCrossEntropyLoss(x,y,cost_matrix):	
	theano_cost_matrix = theano.shared(value=cost_matrix,name='C', borrow=True)

	return -T.mean(T.dot(T.log(x),theano_cost_matrix)[T.arange(y.shape[0]), y])

def LargeMarginLoss_2(x,y,n_classes):
	"this expects y to be a vector of integers"
	B = x[T.arange(y.shape[0]), y]

	C = T.ones_like(x)
	ranges = T.shape_padleft(T.arange(0,n_classes), 1)
	D = T.sub(C,T.eq(T.shape_padright(y, 1),ranges))

	A = T.max(T.add(x,D),axis=1)

	# return D, T.eq(T.shape_padright(y, 1),ranges)
	
	return T.mean(A-B)


def MCL2Hinge(x, y):
	
	margin_pos = T.maximum(0.0, (1.0 - x))
	margin_neg = T.maximum(0.0, (1.0 + x))
	obs_idx = T.arange(y.shape[0])
	loss_pos = T.sum(margin_pos[obs_idx,y]**2.0)
	loss_neg = T.sum(margin_neg**2.0) - T.sum(margin_neg[obs_idx,y]**2.0)
	loss = (loss_pos + loss_neg) / y.shape[0]

	return loss


def LargeMarginLoss(x,y):
	"this expects y to be one-hot encoded"

	Z = T.mean(T.max(x+ (1-y),1)-T.sum(x*y,1))

	return  Z

def AUCLoss(x,y):

	v = x[T.eq(y, 1).nonzero()[0],1]
	q = x[T.eq(y, 0).nonzero()[0],0]

	return -T.mean(T.nnet.sigmoid(T.add(v,-q[:,None])))
	return -T.mean(T.gt(v,q[:,None]))

def RMSE(x,y):
	return T.sqrt(T.mean((x.T-y)**2))


def test_large_margin_loss():
	x = T.fmatrix()
	y1 = T.ivector()
	y2 = T.imatrix()
	oh = theano.function([x,y2],LargeMarginLoss(x,y2))
	oh2 = theano.function([x,y1],LargeMarginLoss_2(x,y1,4))

	xs = np.array(np.random.random((7,4)),dtype='float32')
	ys = np.array([1,2,3,2,1,2,1],dtype='int32')

	y_hat = np.zeros((len(ys),4))		
	y_hat[np.arange(len(ys)),ys] = 1
	ys2 = np.array(y_hat,dtype='int32')
	print ys2
	print 1-ys2
	print oh(xs, ys2)
	print oh2(xs, ys)


def test_AUCLoss():
	x = T.fmatrix()
	y = T.ivector()
	oh = theano.function([x,y],AUCLoss(x,y))

	xs = np.array(np.random.random((7,2)),dtype='float32')
	ys = np.array([1,0,0,0,1,0,1],dtype='int32')

	print xs
	print ys
	print oh(xs, ys)


if __name__=='__main__':
	test_AUCLoss()

	