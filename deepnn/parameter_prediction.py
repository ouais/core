
import numpy as np
from utils.IO import *
from NN import *
from algorithm import *


def test():
	# X_train,y_train =IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X_train,y_train =IO.unpickle(data_dir+"/cifar100/cifar100_train.pkl")
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test,y_test = IO.unpickle(data_dir+"/cifar100/cifar100_test.pkl")
	X_test,y_test = normalize_dataset(X_test,y_test)

	N = len(X_train[0])
	NN = NeuralNetwork(layers_sizes=[N,100,100,100])

	bprop = Backpropagation(NN,train_last_layer_only=False, momentum = .9,max_n_batches=10000)
	# bprop = Supervised_Pretraining(NN,n_train_batches=10000)
	bprop.train(X_train,y_train)

	print "algorithm: %s scored: %.4f" % (str(bprop), NN.score(X_test,y_test))

	visualize(X_train,NN,'weights.png')


if __name__ == '__main__':
	test()