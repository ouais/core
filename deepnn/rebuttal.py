We thank the reviewers for their comments. Before answering specific comments, we would like to clarify that the major goal of the end-to-end system is not only to provided state-of-the-art performance on a difficult AI problem (which we do), but to do so while balancing computing speed and algorithmic simplicity. Most of our design choices are motivated with this balance in mind. 


R1: "I am not a big fan of slapping HMMs together with discriminative models. It does not seem very principled."

We agree that combining HMMs and discriminative classifier in hybrid models does not produce a proper generative model. However it has the following important advantages: 
- It leads to a simple training procedure.
- It produces superior empirical results, as demonstrated by our experiments.
- It is standard practice in the speech recognition community, with state-of-the-art performance there as well.
Overall, it offers the right balance between simplicity, speed and performance.


R2: "It has however lot of similarities with existing systems (in particular LeCun, 1998)."

Graph Transformer Networks offer general models that subsume a large class of recognition pipelines. As such, the end-to-end training techniques pointed out by LeCun 1998 could be applied to many related pipelines. In that perspective, the main contribution of our work then is to:
- Propose specific algorithmic components as building blocks of this general pipeline.
- Demonstrate that these lead to state-of-the-art empirical performance on text recognition in natural images.
- Provide sufficient code and results to allow reasonably easy reproducibility and future comparisons.


R2: "While the authors do cite this paper (LeCun 1998), they omit to bring a real comparison with their approach."

We will clarify the relation between this work and ours in the discussion.  A deeper (empirical) comparison to GTNs would be interesting, but cannot be included in this paper for the following reasons:
- We are unaware of published results on GTNs for text in natural images, such as ICDAR 2003 and SVT datasets.
- We were unable to find code for GTNs in order to run the comparison ourselves.
- Re-implementing GTNs from our reading of the paper would require prohibitive efforts.


R2: "I found the title is misleading: in my opinion, end-to-end systems refer to systems which are trained in a end-to-end way for the task of interest."

The title, while possibly misleading, was chosen to point out that the paper addresses all stages of word recognition as opposed to a single stage, which is the case for many previous works.  We are open to suggestions of alternate titles.


R3: "because the pipeline involves a lot of pieces (...) it is a little tough to tell where the results are coming from. (...) is there a particular component that would benefit most from the ideas here?"

In the big picture of trading off accuracy, speed and accessibility, we try to explain the tradeoffs with existing systems and modules as much as possible. If we were to look at a single one of these factors, then comparisons with other methods would have been simple. However, due to the interaction of the modules and the multitude of evaluation criteria, it is difficult to isolate certain parts and their contributions. That said, the choice of the character recognizer and the word recognizers were made after testing various other algorithms and finding them unable to strike the desired tradeoff we seek, either because of low accuracies, time consumed or sheer complexity.


R1: "The code (hopefully training and testing?) will be a valuable resource."
R3: "I appreciate that the authors intend to post code for the system; for a pipeline of this size, that will be crucial."

We will be releasing both the training and the testing code. Running the training code will require some effort in terms of installing libraries; a detailed README will be provided.  The testing code should be very straight-forward to run.
