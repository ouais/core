import theano
import theano.tensor as T

from algorithm import *
from NN import *
from preprocessing import normalize_dataset
from utils.misc import *


def test_SDA():
	# X,y = IO.unpickle(data_dir+"/cifar10/cifar10_train.pkl")
	X,y = IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	# X,y = IO.unpickle(data_dir+"/cifar100/cifar100_train.pkl")
	X,y = normalize_dataset2(X,y)	

	N = len(X[0])	

	nn = NeuralNetwork(layers_sizes=[N,500, 500])
	algorithm = da_Pretrainer(nn,learning_rate=.02,epochs=40,momentum=0.9)

	algorithm.fit(X)

	IO.pickle(nn,'models/sda_mnist.pkl')
	visualize(X,nn,0,'plots/sda_weights0.png')
	# visualize(X,nn,1,'plots/sda_weights1.png')

def test_ULLDR():
	# X,y = IO.unpickle(data_dir+"/cifar10/cifar10_train.pkl")
	X,y = IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	# X,y = IO.unpickle(data_dir+"/cifar100/cifar100_train.pkl")
	X,y = normalize_dataset2(X,y)	

	N = len(X[0])	

	nn = NeuralNetwork(layers_sizes=[N,500])
	algorithm = dLLDR(nn,learning_rate=.1,batch_size=batch_size,sampler=sampler,max_n_batches=1000)

	algorithm.fit(X,X)

	IO.pickle(nn,'models/ldr_mnist.pkl')
	visualize(X,nn,0,'plots/ldr_weights0.png')
	# visualize(X,nn,1,'plots/sda_weights1.png')

def test_classify():
	nn = IO.unpickle('models/sda_mnist.pkl')

	X_train,y_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X_train,y_train = normalize_dataset2(X_train,y_train)	

	X_test,y_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
	X_test,y_test = normalize_dataset2(X_test,y_test)	

	nn.add_layer(10)

	bprop = Backpropagation(nn,max_n_batches=10000,learning_rate = .01,momentum=.9,batch_size=200)
	losses = bprop.fit(X_train,y_train)
	# visualize(X_train,nn,0,'plots/nn_weights_0.png')

	print bprop.score(X_test,y_test)


if __name__ =='__main__':
	test_SDA()