import numpy

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from utils.utils import tile_raster_images
from utils import IO

from NN import HiddenLayer


class dA(object):

    def __init__(self, hiddenLayer):

        self.n_in = hiddenLayer.n_in 
        self.n_out = hiddenLayer.n_out
        self.activation = hiddenLayer.activation

        self.W = hiddenLayer.W
        self.b = hiddenLayer.b
        self.b_prime = theano.shared(value=numpy.zeros(self.n_in,dtype=theano.config.floatX), borrow=True)
        self.theano_rng = RandomStreams(hiddenLayer.rng.randint(2**30))

        self.x = hiddenLayer.input

        self.params = [self.W, self.b, self.b_prime]

    def get_corrupted_input(self, input, corruption_level):
        return  self.theano_rng.binomial(size=input.shape, n=1,p=1 - corruption_level,dtype=theano.config.floatX) * input

    def get_hidden_values(self, input):
        return self.activation(T.dot(input, self.W) + self.b)

    def get_reconstructed_input(self, hidden):
        return  T.dot(hidden, self.W.T) + self.b_prime

    def get_cost_updates(self, corruption_level, learning_rate, momentum=0.5):

        tilde_x = self.get_corrupted_input(self.x, corruption_level)
        y = self.get_hidden_values(tilde_x)        
        z = self.get_reconstructed_input(y)
        # L = - T.sum(self.x * T.log(z) + (1 - self.x) * T.log(1 - z), axis=1)
        L = T.sum((self.x - z)**2, axis=1)
        cost = T.sqrt(T.mean(L))
        # cost = T.mean(L)

        self.output = y

        gparams = T.grad(cost, self.params)
        updates = []
        incs = []

        for param in self.params:            
            inc = theano.shared(value=param.get_value() * 0,borrow=True)
            if param.name != None:
                inc.name = 'inc_'+param.name
            incs.append(inc)

        for param, gparam, inc in zip(self.params, gparams,incs):            

            update = inc * momentum - learning_rate * gparam
            updates.append((inc,  update))
            updates.append((param, param + update))


        return (cost, updates)


