import numpy

from utils.IO import *
from utils.misc import *
from utils.utils import *

import time
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from utils import *
from random import shuffle

from pylab import *
from dataset import *
from preprocessing import normalize_dataset,normalize_dataset2
import PIL.Image
from collections import Counter

from sklearn.gaussian_process import GaussianProcess
from sklearn import svm
from sklearn import neighbors

from ldr.samplers import SupervisedSampler
from algorithm import Backpropagation, MaskBackpropagation, LMNNBP
from NN import *

def test_algorithms(algorithms):
	X_train,y_train =IO.unpickle(data_dir+"/mnist/mnist_train.pkl") 
	# X_train,y_train =IO.unpickle(data_dir+"/cifar10/cifar10_train.pkl")
	# X_train,y_train =IO.unpickle(data_dir+"/cifar100/cifar100_train.pkl")
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test,y_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
	# X_test, y_test = IO.unpickle(data_dir+"/cifar10/cifar10_test.pkl")
	# X_test,y_test = IO.unpickle(data_dir+"/cifar100/cifar100_test.pkl")
	X_test, y_test = normalize_dataset(X_test,y_test)

	print X_train.shape, X_test.shape

	N = len(X_train[0])
	for algorithm in algorithms:
		start = time.time()
		losses = algorithm.fit(X_train,y_train)
		end = time.time()
		plot(losses)
		show()
		print "algorithm: %s train score: %.4f, test score: %.4f, time: %.2f" % (str(algorithm), algorithm.score(X_train,y_train), algorithm.score(X_test,y_test), end-start)


def test1():
	
	mask = [True,False,True,False,True]
	NN = NeuralNetwork([784,2000,1000,2000,1000, 2000],
		activation_functions=[lambda x: x, lambda x: T.maximum(x,0), lambda x: x, lambda x:  T.maximum(x,0),lambda x: T.maximum(x,0)] )


	mask = [True,True,True]
	NN = NeuralNetwork([784,1000,1000],
		activation_functions=[lambda x: T.maximum(x,0), lambda x:  T.maximum(x,0)] )

	# NN = IO.unpickle('sda_cifar100.pkl')
	NN.add_layer(10)	

	bprop = MaskBackpropagation(NN,mask,max_n_batches=10000,learning_rate = .01, momentum=.9)

	algorithms = []
	algorithms.append(bprop)

	test_algorithms(algorithms)
	# visualize(algorithm.NN,'finetuned_weights.png')

def test2():

	NN = NeuralNetwork([1024,1000,1000],activation_functions=[lambda x: T.maximum(x,0),lambda x:  T.maximum(x,0)] )

	# NN = IO.unpickle('sda_cifar100.pkl')
	NN.add_layer(10)	

	bprop = Backpropagation(NN,max_n_batches=10000,learning_rate = .01)

	X_train,y_train =IO.unpickle(data_dir+"/cifar10/cifar10_train.pkl")
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test,y_test = IO.unpickle(data_dir+"/cifar10/cifar10_test.pkl")
	X_test,y_test = normalize_dataset(X_test,y_test)

	sampler = SupervisedSampler(NN.numpy_rng,10,y_train)

	pretrainer = LDR_Pretrainer(NN)
	losses = pretrainer.fit(X_train,y_train,sampler=sampler,learning_rate=.01,n_train_batches=5000)
	plot(losses)
	show()

	bprop = Backpropagation(NN,max_n_batches=10000,learning_rate = .01)
	losses = bprop.fit(X_train,y_train)
	plot(losses)
	show()

	print "train score: %.4f, test score: %.4f" % (bprop.score(X_train,y_train), bprop.score(X_test,y_test))

def test3():
	nn = NeuralNetwork([784,1000, 1000],activation_functions=[lambda x: T.maximum(0,x),lambda x: T.maximum(0,x)])
	nn.add_layer(10)

	X_train,y_train =IO.unpickle(data_dir+"/mnist/mnist_train.pkl") 
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test,y_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
	X_test,y_test = normalize_dataset(X_test,y_test)

	bprop = Backpropagation(nn,max_n_batches=1000,learning_rate = .01)
	losses = bprop.fit(X_train,y_train)

	firings = nn.temporal_outputs(X_train,1)
	# print np.max(firings),np.min(firings)
	plot_histograms(firings,100)
	# plot(losses)
	# show()

def monitor(e,model,X,y):
	model.set_test_mode()
	print '{} {:.2f}'.format(e,100*model.score(X,y))
	model.set_train_mode()

def test4():

	nn = NeuralNetwork([784,1200,1200],activations=[lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)],drops=[.8,0.5,0.5] )

	# NN = IO.unpickle('sda_cifar100.pkl')
	nn.add_layer(10)

	X_train,y_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl") 
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test,y_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
	X_test,y_test = normalize_dataset(X_test,y_test)	

	# nn = IO.unpickle('models/nn_dropout_10.pkl')

	# nn = IO.unpickle('models/nn_mnist_bprop.pkl')

	bprop = Backpropagation(nn,epochs=10,max_row_norms=[4,4,4],learning_rate = .01,momentum=.9, batch_size=100)
	bprop.add_epoch_monitor(lambda e: monitor(e,nn,X_test,y_test))

	losses = bprop.fit(X_train,y_train)
	# visualize(X_train,nn,0,'plots/nn_weights_0.png')
	# IO.pickle(nn,'models/nn_dropout_10.pkl')

	plot(losses)
	show()

	p_hat = nn.predict_proba(X_train)
	print	np.mean(np.max(p_hat,1))

	IO.pickle(nn,'models/nn_mnist_bprop.pkl')

	print "train score: %.4f, test score: %.4f" % (bprop.score(X_train,y_train), bprop.score(X_test,y_test))
	

def test_lmnnbp():
	k = 5
	X_train,y_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl") 
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test,y_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
	X_test,y_test = normalize_dataset(X_test,y_test)

	nn = IO.unpickle('models/nn_mnist_lmnnbp.pkl')

	X_train_new = nn.output[-2](X_train)
	X_test_new = nn.output[-2](X_test)

	model = neighbors.KNeighborsClassifier(k, weights='uniform')
	model.fit(X_train_new,y_train)
	print model.score(X_test_new,y_test)

def compute_hashed_distance(a,b):
	c = a ^ b
	return bin(c)[2:].count('1')

def create_hash_dictionary(nn,X,y):
	P = nn.output[-2](X)
	d = {}
	for i in xrange(len(P)):
		x = P[i,:]
		hashed_x = np.array(np.round(x-.5),dtype='bool')
		hashed_x = '0b'+''.join([str(int(x)) for x  in hashed_x])
		hashed_x = int(hashed_x,2)
		if not (hashed_x in d):			
			d[hashed_x] = []

		d[hashed_x].append(y[i])

	return d

def hash_dataset(nn,X):

	P = nn.output[-2](X)

	X_h = []

	for i in xrange(len(P)):
		x = P[i,:]
		hashed_x = np.array(np.round(x),dtype='bool')
		hashed_x = '0b'+''.join([str(int(x)) for x in hashed_x])
		hashed_x = int(hashed_x,2)
		X_h.append(hashed_x)

	return np.array(X_h)


def compute_knn_score(nn,X_test,y_test,d,k=1):
	X_h = hash_dataset(nn,X_test)

	keys = [x for x in d]
	y_hat = []
	for x in X_h:
		distances = np.array([compute_hashed_distance(x,y) for y in d])
		y_hat.append(keys[distances.argmin()])

	y_hat = np.array(y_hat)
	return np.mean(y_hat == y_test)

if __name__ == '__main__':
	test4()
