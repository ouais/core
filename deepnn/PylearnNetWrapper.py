import numpy as np
from collections import Counter

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from utils import IO
from utils.misc import *
from utils.utils import *
from deepnn.NN import *
from preprocessing import normalize_dataset

from algorithm import Backpropagation

from pylearn2.utils import serial
from pylearn2.models.mlp import Softmax, Linear
from pylab import *



class PylearnNetWrapper(object):
	def __init__(self,yaml=None):

		# self.x = T.ftensor4('x')		
		self.x = T.fmatrix('x')	
		self.y = T.ivector('y') 
		self.numpy_rng = np.random.RandomState(666)
		self.theano_rng = RandomStreams(self.numpy_rng.randint(666))

		self.model = serial.load_train_file(yaml)
		self.layers = []
		self.output = []

		layer_input = self.x
		self.model = self.model.model
		input_size = self.model.input_space.shape[0]

		layer_input = self.x.reshape((3, input_size, input_size,self.model.batch_size))

		# layer_input = self.x.reshape((self.model.batch_size, input_size, input_size, 1))
		self.first_layer = layer_input
		for layer in self.model.layers:	

			layer.output = layer.fprop(layer_input)		
			layer.params = layer.get_params()		
			if hasattr(layer, 'num_units'):	
				layer.n_out  = layer.num_units

			output = theano.function([self.x],layer.output)

			layer_input = layer.output

			self.output.append(output)
			self.layers.append(layer)

		self.last_input = layer_input


	def fprop_until_last(self, state_below, start=1):

		rval = self.model.layers[0].fprop(state_below)

		rlist = [rval]

		for layer in self.model.layers[1:-start]:
			rval = layer.fprop(rval)
			rlist.append(rval)

		return rval

	def add_generic_layer(self,layer):
		self.layers.append(layer)
		self.output.append(theano.function([self.x],layer.output))

	def add_layer(self,output_size, activation_function= lambda x: safe_softmax(x)):
		h = HiddenLayer(rng=self.numpy_rng, theano_rng=self.theano_rng, input=self.last_input,n_in=self.layers[-1].n_out, n_out=output_size,activation=activation_function)
		self.layers.append(h)
		self.output.append(theano.function([self.x],h.output))

	@batch(128)
	def predict_proba(self,X):				
		X = np.array(X,dtype='float32')
		return self.output[-1](X)
	
	def predict(self,X):
		p_hat = self.predict_proba(X)
		y_hat = np.zeros(len(X),dtype=int)
		for i in xrange(len(p_hat)):
			y_hat[i] = p_hat[i].argmax()
		return y_hat
	
	def score(self, X,y):
		y_hat = self.predict(X)
		hits = y == y_hat
		return hits.mean()
		
	def fit(self, X,y):
		
		self.f = theano.function([self.x],model.fprop(self.x))
		self.model = model


def test_pkl():
	model = PylearnNetWrapper(pkl='yaml/icdar_best.pkl')
	X = np.random.random([10,32,32])
	r = model.predict_proba(X)
	X = np.random.random([32,32])
	r = model.predict_proba(X)
	X = np.random.random([100,32,32])
	y = np.random.randint(0,8,100)
	IO.pickle((X,y),temp_location)

def train_yaml():
	model = PylearnNetWrapper(yaml='yaml/cifar10_no_aug.yaml')
	X_train, y_train = IO.unpickle('/home/oalsha1/data/cifar10/cifar10_train.pkl')
	# X_train, y_train = IO.unpickle('/home/ouais/data/icdar_train.pkl')
	X_train, y_train = normalize_dataset(X_train,y_train,flatten=True)

	print model.first_layer.eval({model.x:X_train[:128]}).shape
	print model.output[0](X_train[:128]).shape
	print model.output[1](X_train[:128]).shape
	print model.output[2](X_train[:128]).shape

	bprop = Backpropagation(model,learning_rate=.01, max_n_batches=30000,momentum=.5,batch_size=128)
	results = bprop.fit(X_train,y_train)

	
	plot(results)
	show()
	IO.pickle(model,'temp/my_net.pkl')

def test_yaml():
	model = IO.unpickle('temp/my_net.pkl')
	X_test, y_test = IO.unpickle('/home/oalsha1/data/cifar10/cifar10_test.pkl')
	X_test, y_test = normalize_dataset(X_test,y_test,flatten=True)

	print "score: %.4f" % model.score(X_test,y_test)


def test():
	# test_pkl()
	train_yaml()
	test_yaml()
	print "wrapper works."


if __name__ =='__main__':
	test()
