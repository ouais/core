import numpy as np
from sklearn.gaussian_process import GaussianProcess
from matplotlib import pyplot as pl
import sys
import time
sys.path.append("../..")
import IO

X,y = IO.unpickle('/home/ouais/data/mnist/mnist_tain.pkl')
gp = GaussianProcess(corr='cubic', theta0=1e-2, thetaL=1e-4, thetaU=1e-1,random_start=100)


gp.fit(X, y)