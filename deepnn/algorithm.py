import numpy as np
import theano
import theano.tensor as T
from dataset import *
from collections import Counter
from logistic_sgd import LogisticRegression, load_data
from theano.tensor.shared_randomstreams import RandomStreams
from dA import dA
from rbm import RBM

from pylab import *
from NN import HiddenLayer, visualize

from deepnn.costs import *

from deepnn.samplers import SupervisedSampler
import warnings
warnings.simplefilter('ignore')


def row_normalize(x):
	"""Normalize rows of matrix x to unit (L2) length."""
	x_normed = x / T.sqrt(T.sum(x**2.,axis=1,keepdims=1)+1e-6)
	return x_normed

def col_normalize(x):
	"""Normalize cols of matrix x to unit (L2) length."""
	x_normed = x / T.sqrt(T.sum(x**2.,axis=0,keepdims=1)+1e-6)
	return x_normed

def compute_updates_grads(cost,params,learning_rate=.1,masks=None,momentum=.95):
		

		updates = []
		incs = []

		gparams = T.grad(cost, params)

		global_learning_rate = theano.shared(theano._asarray(learning_rate, dtype=theano.config.floatX),name='learning rate',borrow=True)
		global_momentum = theano.shared(theano._asarray(momentum, dtype=theano.config.floatX),name='learning rate',borrow=True)

		for param in params:			
			inc = theano.shared(value=np.array(param.get_value() * 0,dtype='float32'),borrow=True)
			inc.name = 'inc_'+ str(param.name)
			inc.constrainable = False
			incs.append(inc)
		
		for param, gparam, inc in zip(params, gparams,incs):			

			update = inc * global_momentum - global_learning_rate * gparam
			updates.append((inc,  update))

			param.constrainable = True

			updates.append((param, param + update))


		return updates, (global_learning_rate, global_momentum), incs

def constrain_row_norms(updates, max_row_norms):
	new_updates = []
	max_norm_id = 0

	for update in updates:
		param, updated_param = update		

		if param.constrainable and param.get_value(borrow=True).ndim == 2 and max_norm_id < len(max_row_norms) and max_row_norms[max_norm_id] > 0:
			row_norms = T.sum(updated_param**2, axis=1, keepdims=1)
			scale  = T.clip(T.sqrt(max_row_norms[max_norm_id] / row_norms), 0., 1.)
			updated_param = updated_param * scale
			max_norm_id += 1

			updated_param = T.cast(updated_param,'float32')

			new_updates.append((param,updated_param))
		else:
			new_updates.append(update)

	return new_updates

def constrain_filter_norms(updates, max_filter_norms):
	new_updates = []

	max_norm_id = 0

	for update in updates:
		param, updated_param = update

		if param.constrainable and param.get_value(borrow=True).ndim == 4 and max_norm_id < len(max_filter_norms) and max_filter_norms[max_norm_id] > 0:

			filter_norms = T.sum(updated_param**2, axis=(1,2,3), keepdims=1)
			scale  = T.clip(T.sqrt(max_filter_norms[max_norm_id] / filter_norms), 0., 1.)
			updated_param = updated_param * scale.dimshuffle(0, 'x', 'x', 'x')

			updated_param = T.cast(updated_param,'float32')

			max_norm_id += 1

			new_updates.append((param,updated_param))
		else:
			new_updates.append(update)
			
	return new_updates



class TrainingAlgorithm:
	def __init__(self,NN):
		self.NN = NN
		self.x = NN.x
		self.numpy_rng = self.NN.numpy_rng
		self.theano_rng = self.NN.theano_rng
		self.n_layers = len(self.NN.layers)+1
		self.regularizers = []
		self.epoch_monitors = []
		self.batch_monitors = []

	def add_epoch_monitor(self,monitor):
		self.epoch_monitors.append(monitor)

	def add_batch_monitor(self,monitor):
		self.batch_monitors.append(monitor)

	def run_epoch_monitors(self,epoch_id):
		for m in self.epoch_monitors:
			m(epoch_id)

	def run_batch_monitors(self,batch_id):
		for m in self.batch_monitors:
			m(batch_id)

	def add_test_set(self,X_test,y_test):
		index = T.lscalar()

		test_set_x,test_set_y = shared_dataset(X_test,y_test)

		errs = T.mean(T.eq(self.NN.layers[-1].output.argmax(axis=1), self.y))

		self.test_fn = theano.function(inputs=[index], outputs= errs, givens=
			{
			self.x: test_set_x[index*self.batch_size:(1+index)*self.batch_size],
			self.y: test_set_y[index*self.batch_size:(1+index)*self.batch_size]}
			)

		self.n_test_batches = X_test.shape[0]/self.batch_size

	def compute_score_on_test(self):
		errs = []
		for i in xrange(self.n_test_batches):			
			errs.append(self.test_fn(i))
		return np.mean(errs)


class da_Pretrainer(TrainingAlgorithm):
	def __init__(self,NN,L1_lambda = 0, batch_size=20,learning_rate=.01,epochs=1,momentum=.5,corruption_levels= [.1, .2, .3]):		
		TrainingAlgorithm.__init__(self,NN)

		self.dA_layers = [0]*(NN.n_layers-1)
		self.rng = NN.numpy_rng
		self.hiddenLayer = NN.layers
		self.name = "sdA"

		self.batch_size = batch_size
		self.learning_rate = learning_rate
		self.epochs = epochs
		self.corruption_levels = corruption_levels
		self.momentum = momentum
		self.L1_lambda = L1_lambda

		for i in xrange(len(NN.layers)):
			self.dA_layers[i] = dA(NN.layers[i])


	def fit(self,X):

		v = T.ivector()
		q = T.fmatrix()

		corruption = self.corruption_levels
		
		train_set_x = shared_dataset_X(X)

		corruption_level = T.scalar('corruption')  	
		lr = T.scalar('lr') 

		n_batches = len(X)/self.batch_size

		pretraining_fns = []

		for layer_index in xrange(self.n_layers-1):
			dA = self.dA_layers[layer_index]
			cost, updates = dA.get_cost_updates(corruption_level,lr,momentum=self.momentum)

			row_sparsity = T.mean(T.abs_(col_normalize(dA.output)))
			col_sparsity = T.mean(T.abs_(row_normalize(dA.output)))
			
			cost += self.L1_lambda * (row_sparsity+col_sparsity)

			fn = theano.function(inputs=[
				v,
				theano.Param(corruption_level, default=0.2),
				theano.Param(lr, default=0.1)],
				outputs=cost,
				updates=updates,givens={self.x:  train_set_x[v]})

			pretraining_fns.append(fn)

		for i in xrange(self.n_layers-1):
			print "training layer %d" % (i)
			losses = []
			for e in xrange(self.epochs):
				for batch_index in xrange(n_batches):		
					indices = np.array(self.rng.randint(0,len(X),self.batch_size),dtype='int32')
					loss = pretraining_fns[i](indices,corruption= corruption[i],lr=self.learning_rate)				
					losses.append(loss)
				print np.mean(loss)

		return losses


class dLLDR(TrainingAlgorithm):
	def __init__(self,NN,batch_size=20,K=50,n_batches=100,learning_rate=.1,corruption_levels= [.1, .2, .3]):		
		TrainingAlgorithm.__init__(self,NN)

		self.dA_layers = [0]*(NN.n_layers-1)
		self.hiddenLayer = NN.layers
		self.name = "sdA"

		self.batch_size = batch_size
		self.learning_rate = learning_rate
		self.corruption_levels = corruption_levels

		for i in xrange(len(NN.layers)):
			self.dA_layers[i] = dA(NN.layers[i])


	def fit(self,X):

		v = T.ivector() 
		q = T.fmatrix()
		index = T.lscalar('index')
		train_set_x = shared_dataset_X(X)

		corruption_level = T.scalar('corruption')  
		lr = T.scalar('lr') 

		pretraining_fns = []
		fprops = []
		bprops = []

		for layer_index in xrange(self.n_layers-1):
			dA = self.dA_layers[layer_index]
			cost, updates = dA.get_cost_updates(corruption_level,lr)

			fn = theano.function(inputs=[index,theano.Param(corruption_level, default=0.2),theano.Param(lr, default=0.1)],outputs=cost,
				updates=updates,givens={self.x: train_set_x[index*self.batch_size:(1+index)*self.batch_size]})

			last_layer_updates, glr, incs = compute_updates_grads(cost,dA.params,self.learning_rate,momentum=0.9)
			trunk_updates, glr, incs = compute_updates_grads(cost,hiddenlayer_params,self.learning_rate,momentum=0.1)
		
			train_last_layer = theano.function([self.NN.layers[-2].output, self.y], cost, updates=last_layer_updates)
			
			fprop = theano.function([v], self.NN.layers[layer_index].output,givens={self.x: train_set_x[v]})
			bprop = theano.function([v,self.y],cost, updates=trunk_updates,givens={self.x: train_set_x[v]})

			fprops.append(fprop)
			bprops.append(bprop)
			pretraining_fns.append(fn)

		for i in xrange(self.n_layers-1):
			print "training layer %d" % (i)

			losses = []
			for batch_index in xrange(self.n_batches):

				indices = self.rng.randint(0,len(self.X),self.batch_size)
				p = fprop(indices)

				self.NN.layers[-1].refresh()

				prev_loss = np.inf
				loss = np.inf

				while iterations< 500 and (prev_loss == np.inf or prev_loss - loss > .001):
					prev_loss = loss
					loss = train_last(p,y_temp)		
					iterations += 1

				for k in xrange(self.K):
					indices = self.sampler.sample_testing_indices(problem,self.batch_size)
					y_temp  = self.relabel(y[indices])
				
					loss = bprop(indices,y_temp)					
					batch_losses.append(loss)

					loss = pretraining_fns[i](batch_index,corruption=self.corruption_levels[i],lr=self.learning_rate)

				print np.mean(batch_losses)
				losses.extend(batch_losses)	

			print np.mean(loss)

class DBN_Pretrainer(TrainingAlgorithm):
	def __init__(self,NN,batch_size=20,learning_rate=.01,training_epochs=15):
		TrainingAlgorithm.__init__(self,NN)

		self.batch_size = batch_size
		self.learning_rate = learning_rate
		self.training_epochs = training_epochs

		self.rbm_layers = [0]*(NN.n_layers-1)
		self.hiddenLayer = NN.layers

		for i in xrange(len(NN.layers)):
			self.rbm_layers[i] = RBM(NN.layers[i])


	def fit(self,X):

		v = T.ivector() 
		index = T.lscalar('index')
		train_set_x = shared_dataset_X(X)

		pretraining_fns = []
		n_train_batches = X.shape[0] / self.batch_size


		for rbm in self.rbm_layers:
			persistent_chain = theano.shared(numpy.zeros((self.batch_size, rbm.n_hidden),dtype=theano.config.floatX),borrow=True)
			
			cost, updates = rbm.get_cost_updates(lr=self.learning_rate,persistent=persistent_chain, k=15)
			
			fn = theano.function(inputs=[index], outputs=cost,
				updates=updates, givens={self.x: train_set_x[index*self.batch_size:(1+index)*self.batch_size]})

			pretraining_fns.append(fn)

		for i in xrange(self.n_layers-1):
			print "training layer %d" % (i)
			for epoch in xrange(self.training_epochs):
				cs = []
				for batch_index in xrange(n_train_batches):
					c = pretraining_fns[i](batch_index)
					cs.append(c)
				print np.mean(cs)


class Supervised_Pretraining(TrainingAlgorithm):
	def __init__(self,NN,L1_lambda=.05,L2_lambda=.05,n_train_batches=10000,):
		TrainingAlgorithm.__init__(self,NN)
		self.hiddenLayer = NN.layers
		self.y= NN.y
		self.L1_lambda = L1_lambda
		self.L2_lambda = L2_lambda
		self.n_train_batches = n_train_batches

	def __str__(self):
		return 'supervised pretraining with random projections'

	def create_mask(self,layer_index,target_index):
		n_in = self.layers_sizes[layer_index]
		n_out = self.layers_sizes[layer_index+1]

		m = np.zeros((n_in,n_out),dtype='float32')

		n_chunks = self.n_layers-layer_index
		chunk_size = self.layers_sizes[layer_index+1]/n_chunks

		m[:,(target_index-1-layer_index)*chunk_size:(target_index-layer_index)*chunk_size] = np.ones((m.shape[0],chunk_size),dtype= 'float32')

		return m


	def create_training_function(self,train_set_x,layer_index,n_classes,learning_rate):
		params = []
		masks = []

		v = T.ivector()

		for i in xrange(layer_index):
			params.extend(self.hiddenLayer[i].params)
			for param in self.hiddenLayer[i].params:
				if len(param.get_value().shape) == 1:
					mask = np.ones(param.get_value().shape,dtype='float32')
				else:
					mask = self.create_mask(i,layer_index)
				masks.append(mask)


		self.classifier = LogisticRegression(input=self.hiddenLayer[layer_index].output, n_in= self.layers_sizes[layer_index+1],n_out=n_classes)
		cost = self.classifier.negative_log_likelihood(self.y)

		params.extend(self.classifier.params)
		for param in self.classifier.params:
			masks.append(np.ones(param.get_value().shape,dtype='float32'))

		updates, grads = compute_updates_grads(cost,params,learning_rate,momentum=0.9)

		f = theano.function([v,self.y], cost,updates= updates, givens={self.x: train_set_x[v]})

		return f

	def fit(self,X,y,learning_rate=.1,batch_size=200):
		v = T.ivector()
		y = np.array(y,dtype='int32')

		train_set_x = shared_dataset_X(X)
		
		n_classes = len(np.unique(y))

		for layer_index in xrange(self.n_layers-1):			
			
			f = self.create_training_function(train_set_x,layer_index,n_classes,learning_rate)
			presistence = 0
			losses = []
			for batch_index in xrange(self.n_train_batches):

				indices = np.array(self.numpy_rng.randint(0,X.shape[0],batch_size),dtype='int32')
				loss = f(indices,y[indices])
				losses.append(loss)

			plot(losses)
			show()

		self.NN.prob_model = theano.function([self.x], self.classifier.log_likelihood())

		return losses

class LDR_Pretrainer(TrainingAlgorithm):
	def __init__(self,NN,L1_lams=None,L2_lams=None,learning_rate=.01,max_n_batches=5000,max_row_norms=None,max_filter_norms=None,
		batch_size=200, K=25,rate_decay =.998, sampler = None, cost = CrossEntropyLoss,f_momentum=.5,h_momentum=.9):
		TrainingAlgorithm.__init__(self,NN)
		self.layers = NN.layers
		self.y= NN.y
		self.L1_lams = L1_lams
		self.L2_lams = L2_lams
		self.sampler = sampler
		self.max_n_batches = max_n_batches
		self.learning_rate = learning_rate
		self.K = K
		self.batch_size = batch_size
		self.cost = cost(self.NN.layers[-1].output,self.y)
		self.rate_decay = rate_decay
		self.rate_decay_updates = 250

		self.f_momentum = f_momentum
		self.h_momentum = h_momentum

		self.max_row_norms = max_row_norms
		self.max_filter_norms = max_filter_norms

		if self.L1_lams == None:
			self.L1_lams = [0] * len(NN.layers)

		if self.L2_lams == None:
			self.L2_lams = [0] * len(NN.layers)

		if self.max_row_norms == None:
			self.max_row_norms = [0] * len(NN.layers)

		if self.max_filter_norms == None:
			self.max_filter_norms = [0] * len(NN.layers)
	
	def __str__(self):
		return 'LDR'

	def relabel(self,y):
		d = {}
		counter = Counter(y)
		d.update([(x, counter.keys().index(x)) for x in counter.keys()])

		y_new = np.array([d[x] for x in y],dtype='int32')
		return y_new

	def fit(self,X,y):

		v = T.ivector()
		train_set_x = shared_dataset_X(X)		

		y = np.array(y,dtype='int32')

		cost = self.cost

		for layer_id in xrange(len(self.NN.layers)):
			for p in self.NN.layers[layer_id].params:
				if self.L2_lams[layer_id] > 0:
					cost += self.L2_lams[layer_id]* T.sum(p**2)
				if self.L1_lams[layer_id] > 0:
					cost += self.L1_lams[layer_id]* T.sum(T.abs_(p))	
		

		hiddenlayer_params = []
		for i in xrange(len(self.NN.layers)-1):
			hiddenlayer_params.extend(self.NN.layers[i].params)
		
		last_updates, (last_lr,last_mom), incs = compute_updates_grads(cost,self.NN.layers[-1].params,self.learning_rate,momentum=self.h_momentum)
		last_updates = constrain_row_norms(last_updates,max_row_norms=[self.max_row_norms[-1]])

		trunk_updates, (trunk_lr,trunk_mom), trunk_incs = compute_updates_grads(cost,hiddenlayer_params,self.learning_rate,momentum=self.f_momentum)
		trunk_updates = constrain_filter_norms(trunk_updates,max_filter_norms=self.max_filter_norms)
		trunk_updates = constrain_row_norms(trunk_updates,max_row_norms=self.max_row_norms)
		
		train_last = theano.function([self.NN.layers[-2].output, self.y], cost, updates=last_updates)
		# errors = theano.function([self.NN.layers[-1].output, self.y], self.classifier.errors(self.y))

		fprop = theano.function([v], self.NN.layers[-2].output,givens={self.x: train_set_x[v]})
		bprop = theano.function([v,self.y],cost, updates=trunk_updates,givens={self.x: train_set_x[v]})

		self.losses = []		

		updates_counter = 0

		for batch_index in xrange(self.max_n_batches):

			problem = self.sampler.sample_problem()
			indices = self.sampler.sample_training_indices(problem,self.batch_size)
			y_temp  = self.relabel(y[indices])

			self.batch_losses = []

			prev_loss = np.inf
			loss = np.inf

			iterations = 0 	


			p = fprop(indices)	

			self.NN.layers[-1].refresh()
			# self.NN.layers[-1].set_train_mode()

			while iterations< 700 and (prev_loss == np.inf or prev_loss - loss > .001):
				prev_loss = loss
				loss = train_last(p,y_temp)		
				iterations += 1

			training_loss = loss

			for k in xrange(self.K):
				indices = self.sampler.sample_testing_indices(problem,self.batch_size)
				y_temp  = self.relabel(y[indices])
				
				loss = bprop(indices,y_temp)					
				self.batch_losses.append(loss)

				updates_counter += 1

				if updates_counter >= self.rate_decay_updates:
					updates_counter = 0
					new_learning_rate = np.array(trunk_lr.get_value()* self.rate_decay,dtype='float32')
					trunk_lr.set_value(new_learning_rate)

			print batch_index, iterations, np.mean(self.batch_losses), training_loss

			self.losses.extend(self.batch_losses)	

			#################################################
			# trunk_incs were set to 0 after each iteration for the transfer learning contest
			#################################################

			for param in trunk_incs:
				param.set_value(param.get_value()*0)

			# for param in incs:
			# 	param.set_value(param.get_value()*0)

			self.run_batch_monitors(batch_index)



		self.NN.layers[-1].refresh()	

		return self.losses


class Baxter_Pretrainer(TrainingAlgorithm):
	def __init__(self,NN,L1_lambda=0.5,L2_lambda=.001,sampler = None,learning_rate=.1,max_n_batches=1000,batch_size=200,output_size=10, stick=False,momentum=.9,cost=CrossEntropyLoss):
		TrainingAlgorithm.__init__(self,NN)
		self.layers = NN.layers
		self.y= NN.y
		self.L1_lambda = L1_lambda
		self.L2_lambda = L2_lambda
		self.sampler = sampler
		self.learning_rate = learning_rate
		self.max_n_batches = max_n_batches
		self.batch_size = batch_size
		self.stick = stick
		self.cost = cost
		self.momentum = momentum
	
	def __str__(self):
		return 'Baxter'

	def relabel(self,y):
		d = {}
		counter = Counter(y)
		d.update([(x, counter.keys().index(x)) for x in counter.keys()])

		y_new = np.array([d[x] for x in y],dtype='int32')
		return y_new

	def batch_monitor(self,n,k,loss):
		if n % k == 0:
			msg = 'loss at batch {} = {.2f}'.format((n,loss))
			print msg

	def fit(self,X,y):

		d = T.fmatrix()
		v = T.ivector()

		# self.monitors.append(self.batch_monitor())

		train_set_x = shared_dataset_X(X)
		
		y = np.array(y,dtype='int32')
		
		cost = 0

		for layer in xrange(len(self.NN.layers)-1):
			for p in self.NN.layers[layer].params:
				cost += self.L1_lambda * T.mean(T.abs_(p))
				cost += self.L2_lambda* T.sum(p**2)

		hiddenlayer_params = []
		for i in xrange(len(self.NN.layers)-1):
			hiddenlayer_params.extend(self.NN.layers[i].params)
		
		training_fns = {}
		self.predictors = {}
		temp_layers = []

		for i in xrange(len(self.sampler.problems)):

			problem = self.sampler.problems[i]
			problem_size = self.sampler.problem_size

			activation = T.nnet.softmax
			if self.sampler.regression:
				activation = lambda x:x

			temp_layer = HiddenLayer(rng=self.numpy_rng, theano_rng= self.NN.theano_rng, input=self.NN.layers[-2].output,
				n_in=self.NN.layers[-2].n_out, n_out=problem_size,activation= activation)

			problem_cost = self.cost(temp_layer.output,self.y)

			params = hiddenlayer_params[:]
			params.extend(temp_layer.params)

			updates, glr, incs = compute_updates_grads(problem_cost,params,self.learning_rate,momentum=self.momentum)
			train_fn = theano.function([v, self.y], problem_cost, updates=updates,givens={self.x: train_set_x[v]})

			p_hat = temp_layer.output
			# y_hat = p_hat.argmax(axis=1)	
			self.predictors[problem] = theano.function([self.x], p_hat)

			training_fns[problem] = train_fn
			temp_layers.append(temp_layer)
		

		losses = []		

		for batch_index in xrange(self.max_n_batches):

			problem = self.sampler.sample_problem()

			indices = self.sampler.sample_training_indices(problem,self.batch_size)
			y_temp  = self.relabel(y[indices])

			loss = training_fns[problem](indices,y_temp)

			losses.append(loss)	

			# self.run_monitors()

		if self.stick:
			self.NN.layers[-1].W.set_value(temp_layer.W.get_value())
			self.NN.layers[-1].b.set_value(temp_layer.b.get_value())

		return losses


class Backpropagation(TrainingAlgorithm):
	def __init__(self,NN,L1_lams=None,L2_lams=None,max_row_norms=None,max_filter_norms=None,
		train_last_layer_only=False,momentum = 0.9,epochs=5000,learning_rate=.01,batch_size=120,rate_decay=.998, cost = CrossEntropyLoss):

		TrainingAlgorithm.__init__(self,NN)

		self.hiddenLayer = NN.layers
		self.y= NN.y
		self.momentum = momentum
		self.train_last_layer_only = train_last_layer_only
		self.epochs = epochs
		self.score = NN.score
		self.learning_rate = learning_rate
		self.batch_size = batch_size
		self.cost = cost(self.NN.layers[-1].output,self.y)
		self.L1_lams = L1_lams
		self.L2_lams = L2_lams
		self.max_row_norms = max_row_norms
		self.max_filter_norms = max_filter_norms
		self.rate_decay = rate_decay
		self.rate_decay_updates = 250 # when is the rate decay activated

		if self.L1_lams == None:
			self.L1_lams = [0] * len(NN.layers)

		if self.L2_lams == None:
			self.L2_lams = [0] * len(NN.layers)

		if self.max_row_norms == None:
			self.max_row_norms = [0] * len(NN.layers)

		if self.max_filter_norms == None:
			self.max_filter_norms = [0] * len(NN.layers)
	
	def __str__(self):
		return 'Backpropagation'

	def set_momentum(self,momentum):
		self.global_momentum.set_value(value=momentum)

	def fit(self,X,y):

		warnings.simplefilter('ignore')
		v = T.ivector()

		self.batch_size = min(self.batch_size,len(y))
		
		train_set_x,train_set_y = shared_dataset(X,y)	

		cost = self.cost

		for layer_id in xrange(len(self.NN.layers)-1):
			layer = self.NN.layers[layer_id]
			if self.L2_lams[layer_id] > 0:
				cost += self.L2_lams[layer_id]* T.sum(layer.output**2.) / layer.output.size


		params_to_tune = self.NN.layers[-1].params[:]

		if self.train_last_layer_only is False:
			for i in xrange(len(self.NN.layers)-1):
				params_to_tune.extend(self.NN.layers[i].params)
		

		updates, pars, trsh1 = compute_updates_grads(cost, params_to_tune,self.learning_rate,momentum=self.momentum)
		updates = constrain_row_norms(updates,max_row_norms=self.max_row_norms)
		updates = constrain_filter_norms(updates,max_filter_norms=self.max_filter_norms)

		global_momentum, global_learning_rate = pars

		y_hat = T.argmax(self.NN.layers[-1].output,axis=1)
		errors = T.mean(T.neq(y_hat, self.y))

		train = theano.function([v], [cost, errors], updates=updates,givens=	
				{
					self.x: train_set_x[v],
					self.y: train_set_y[v]
				})

		max_n_batches = X.shape[0]/self.batch_size 
		data_size = X.shape[0]

		self.losses = []	
		updates = 0	

		for e in xrange(self.epochs):
			self.batch_errors = []
			for i in xrange(max_n_batches):
				indices = np.array(self.numpy_rng.randint(0,data_size,self.batch_size),dtype='int32')

				loss,errs = train(indices)

				self.batch_errors.append(errs)				

				updates += 1

				if updates >= self.rate_decay_updates:
					updates = 0
					new_learning_rate = np.array(global_learning_rate.get_value()* self.rate_decay,dtype='float32')
					global_learning_rate.set_value(new_learning_rate)

			self.losses.extend(self.batch_errors)
			self.run_epoch_monitors(e)

			
			


		return self.losses


class SparseBackpropagation(TrainingAlgorithm):
	def __init__(self,NN,L1_lams=None,L2_lams=None,max_row_norms=None,train_last_layer_only=False,momentum = 0.9,epochs=5000,learning_rate=.01,batch_size=120, cost = CrossEntropyLoss):
		TrainingAlgorithm.__init__(self,NN)

		self.hiddenLayer = NN.layers
		self.y= NN.y
		self.momentum = momentum
		self.train_last_layer_only = train_last_layer_only
		self.epochs = epochs
		self.score = NN.score
		self.learning_rate = learning_rate
		self.batch_size = batch_size
		self.cost = cost(self.NN.layers[-1].output,self.y)
		self.L1_lams = L1_lams
		self.L2_lams = L2_lams
		self.max_row_norms = max_row_norms

		if self.L1_lams == None:
			self.L1_lams = [0] * len(NN.layers)

		if self.L2_lams == None:
			self.L2_lams = [0] * len(NN.layers)

		if self.max_row_norms == None:
			self.max_row_norms = [0] * len(NN.layers)
	
	def __str__(self):
		return 'SparseBackpropagation'

	def fit(self,X,y):
		print 'Sparse Backpropagation started...'
		v = T.ivector()

		self.batch_size = min(self.batch_size,len(y))
		max_n_batches = X.shape[0]/self.batch_size 

		# train_set_x,train_set_y = shared_dataset(X,y)	

		cost = self.cost

		for layer_id in xrange(len(self.NN.layers)):
			for p in self.NN.layers[layer_id].params:
				if self.L2_lams[layer_id] > 0:
					cost += self.L2_lams[layer_id]* T.sum(p**2)

		params_to_tune = self.NN.layers[-1].params[:]

		if self.train_last_layer_only is False:
			for i in xrange(len(self.NN.layers)-1):
				params_to_tune.extend(self.NN.layers[i].params)


		updates, trsh0, trsh1 = compute_updates_grads(cost, params_to_tune,self.learning_rate,momentum=self.momentum,max_row_norms=self.max_row_norms)

		train = theano.function([self.x,self.y], cost, updates=updates)


		self.losses = []

		for e in xrange(self.epochs):
			self.batch_losses = []
			for i in xrange(max_n_batches):
				indices = np.array(self.numpy_rng.randint(0,X.shape[0],self.batch_size),dtype='int32')
				loss = train(X[indices,:].todense(),y[indices])
				self.batch_losses.append(loss)				
				print loss

			self.losses.extend(self.batch_losses)							
			self.run_epoch_monitors(e)


		return self.losses

class MaskBackpropagation(TrainingAlgorithm):
	def __init__(self,NN,mask, L1_lambda=.0,L2_lambda=.00,train_last_layer_only=False,final_persistence = 1,momentum = 0.9,max_n_batches=5000,learning_rate=.01,batch_size=120, cost = CrossEntropyLoss):
		TrainingAlgorithm.__init__(self,NN)

		self.hiddenLayer = NN.layers
		self.y= NN.y
		self.L1_lambda = L1_lambda
		self.L2_lambda = L2_lambda
		self.momentum = momentum
		self.final_persistence = final_persistence
		self.train_last_layer_only = train_last_layer_only
		self.max_n_batches = max_n_batches
		self.score = NN.score
		self.learning_rate = learning_rate
		self.batch_size = batch_size
		self.cost = cost(self.NN.layers[-1].output,self.y)
		self.mask = mask
	
	def __str__(self):
		return 'MaskBackpropagation'

	def fit(self,X,y):
		print 'Masked Backpropagation started...'
		self.batch_size = min(self.batch_size,len(y))
		batch_size = self.batch_size
		v = T.ivector()

		train_set_x,train_set_y = shared_dataset(X,y)

		cost = self.cost

		bimodal_cost = 0

		for layer in xrange(len(self.NN.layers)):
			for p in self.NN.layers[layer].params:
				cost += self.L1_lambda * T.mean(T.abs_(p))
				cost += self.L2_lambda* T.mean(p**2)

		params_to_tune = self.NN.layers[-1].params[:]

		if self.train_last_layer_only is False:
			for i in xrange(len(self.NN.layers)-1):
				if self.mask[i] == True:
					params_to_tune.extend(self.NN.layers[i].params)
				else:
					self.NN.layers[i].randomize_achlioptas()


		updates, trsh0, trsh1 = compute_updates_grads(cost, params_to_tune,self.learning_rate,momentum=self.momentum)

		train = theano.function([v], cost, updates=updates,
			givens=	
			{
				self.x: train_set_x[v],
				self.y: train_set_y[v]
			})

		losses = []

		for i in xrange(self.max_n_batches):
			indices = np.array(self.numpy_rng.randint(0,len(y),self.batch_size),dtype='int32')
			loss = train(indices)
			losses.append(loss)
			# for i in xrange(len(self.NN.layers)-1):
			# 	if self.mask[i] == False:
			# 		self.NN.layers[i].randomize_achlioptas()
			# print loss

		return losses


class LMNNBP(TrainingAlgorithm):
	def __init__(self,NN,L1_lambda=.0,L2_lambda=.00,train_last_layer_only=False,momentum = 0.9,epochs=5,learning_rate=.01,batch_size=120, cost = CrossEntropyLoss):
		TrainingAlgorithm.__init__(self,NN)

		self.hiddenLayer = NN.layers
		self.y= NN.y
		self.L1_lambda = L1_lambda
		self.L2_lambda = L2_lambda
		self.momentum = momentum
		self.train_last_layer_only = train_last_layer_only
		self.epochs = epochs
		self.score = NN.score
		self.learning_rate = learning_rate
		self.batch_size = batch_size
		self.cost = cost(self.NN.layers[-1].output,self.y)
	
	def __str__(self):
		return 'LMNNBP'

	def fit(self,X,y):
		print 'LMNNBP started...'
		self.batch_size = min(self.batch_size,len(y))
		batch_size = self.batch_size
		max_n_batches = len(X)/self.batch_size 

		v = T.ivector()

		train_set_x,train_set_y = shared_dataset(X,y)	

		cost = self.cost

		for layer in self.NN.layers:
			for p in layer.params:
				cost += self.L1_lambda * T.mean(T.abs_(p))
				cost += self.L2_lambda * T.mean(p**2)

		params_to_tune = self.NN.layers[-1].params[:]

		if self.train_last_layer_only is False:
			for i in xrange(len(self.NN.layers)-1):
				params_to_tune.extend(self.NN.layers[i].params)


		activations = self.NN.layers[-2].output.T+ 1e-6
		activations /= T.sqrt(T.sum(activations**2,axis=0))

		D = T.dot(activations.T,activations)
		A = D[:batch_size/2,:batch_size/2]
		B = D[batch_size/2:batch_size,:batch_size/2]
		C = D[:batch_size/2,batch_size/2:batch_size,]
		E = D[batch_size/2:batch_size,batch_size/2:batch_size]

		cost = 0*cost -( T.mean(A) -T.mean(B) - T.mean(C) + T.mean(E))
		
		updates, trsh0, trsh1 = compute_updates_grads(cost, params_to_tune,self.learning_rate,momentum=self.momentum)

		

		if len(X.shape) == 2:
			train = theano.function([v], [cost,errors], updates=updates,givens=	
				{
					self.x: train_set_x[v],
					self.y: train_set_y[v]
				})		
		elif len(X.shape) == 4:
			train = theano.function([v], [cost,errors], updates=updates,givens=	
				{
					self.x: train_set_x[:,:,:,v],
					self.y: train_set_y[v]
				})		



		sampler = SupervisedSampler(self.NN.numpy_rng,2,y)

		losses = []
		f = theano.function([self.x],D)

		for e in xrange(self.epochs):
			for i in xrange(max_n_batches):
				problem = sampler.sample_problem()
				indices = sampler.sample_indices(problem)
				loss = train(indices)
				losses.append(loss)				
			self.run_monitors()

		indices = sampler.sample_indices(problem)

		d = f(X[indices,:])
		print np.mean(d[:100,:100]),np.mean(d[100:200,:100]),np.mean(d[100:,100:])
		imshow(d)
		colorbar()
		show()

		return losses
