import numpy as np

class SupervisedSampler:
	def __init__(self,numpy_rng,problem_size,y,batch_size=200):
		self.rng = numpy_rng		
		self.y = y
		self.classes = np.unique(y)
		self.ilocs = {}
		self.batch_size = batch_size
		self.problem_size = problem_size

		for c in self.classes:
			self.ilocs[c] = np.array(np.where(y == c)[0],dtype='int32')

		self.compute_sizes()

	def sample_problem(self):	
		problem = self.rng.choice(len(self.classes), self.problem_size,replace=False)
		return problem		

	def compute_sizes(self):		
		rem = self.batch_size - (self.problem_size-1)*self.batch_size/self.problem_size
		self.sizes = [self.batch_size/self.problem_size]*(self.problem_size-1) + [rem]

	def sample_indices(self,problem,batch_size=None):
		for c in problem:
			self.rng.shuffle(self.ilocs[c])

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		for i in xrange(len(problem)):
			c = problem[i]
			temp = self.ilocs[c][:self.sizes[i]]
			indices = np.hstack((indices,temp))

		return indices
