
import numpy as np
from utils import IO
from utils.utils import *
from utils.misc import *

from random import shuffle

from pylab import *

from sklearn import svm

from samplers import *
from algorithm import *
from dataset import *
from preprocessing import normalize_dataset

from NN import NeuralNetwork, visualize
from experiment_core import transform_dataset, execute_test, print_results
from test_multitask import load_dataset




def test_transfer(name):
	rng = np.random.RandomState(666)

	model = IO.unpickle('materials/'+name+'_transfer.pkl')

	Xy_train = load_dataset(data_dir+"/cifar100/transfer/transfer_train.pkl")
	Xy_test = load_dataset(data_dir+"/cifar100/transfer/transfer_test.pkl")

	transform_dataset(Xy_train,model,'materials/'+name+'transfer_train.pkl')
	transform_dataset(Xy_test,model,'materials/'+name+'transfer_test.pkl')

	Xy_train = IO.unpickle('materials/'+name+'transfer_train.pkl')
	Xy_test = IO.unpickle('materials/'+name+'transfer_test.pkl')

	n_samples_list = [1,2,5,10, 100]

	results = execute_test(rng, Xy_train, Xy_test, testing_problems = [[0,1,2,3,4,5,6,7,8,9]], n_samples_list = n_samples_list, n_estimations = 1)

	print_results(name,n_samples_list,results)


def train_transfer(name):
	rng = np.random.RandomState(666)

	nn = NeuralNetwork([1024,1000,1000],activation_functions=[lambda x: T.maximum(x,0),lambda x:  T.maximum(x,0)] )

	nn.add_layer(100)	
	
	X_train,y_train =IO.unpickle(data_dir+"/cifar100/cifar100_train.pkl")
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test,y_test = IO.unpickle(data_dir+"/cifar100/cifar100_test.pkl")
	X_test,y_test = normalize_dataset(X_test,y_test)

	sampler = SupervisedSampler(rng,50,y_train)
	# bprop = Backpropagation(nn,max_n_batches=15000,learning_rate = .01, momentum=.9)
	algorithm = LDR_Pretrainer(nn,L1_lambda=0.5,L2_lambda=.001,train_last_layer_only=True,learning_rate=.1,max_n_batches=1000,batch_size=200, K=50, sampler = sampler)
		
	losses= algorithm.fit(X_train,y_train)
	plot(losses)
	show()

	IO.pickle(nn,'materials/'+name+'_transfer.pkl')


def train_test(name):
	train_transfer(name)
	test_transfer(name)

if __name__ == '__main__':
	# test_transfer()
	train_test("LDR")
	
	