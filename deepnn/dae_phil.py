import theano
import theano.tensor as T

from deepnn.NN import NeuralNetwork, visualize
from deepnn.algorithm import da_Pretrainer
from preprocessing import normalize_dataset2

from utils import IO
from utils.misc import data_dir



def train():
	nn = NeuralNetwork([784,500],activation_functions= T.nnet.sigmoid )

	X_train,y_train =IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X_train,y_train = normalize_dataset2(X_train,y_train)

	da = da_Pretrainer(nn)

	da.fit(X_train,learning_rate=.001,training_epochs=10,corruption_levels=[.3])
	visualize(X_train,nn,0,'temp/phil_dae_weights_0.png')

	IO.pickle(nn,'temp/phil_nn.pkl')


if __name__ == '__main__':
	train()

