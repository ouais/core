"""This tutorial introduces the LeNet5 neural network architecture
using Theano.  LeNet5 is a convolutional neural network, good for
classifying images. This tutorial shows how to build the architecture,
and comes with all the hyper-parameters you need to reproduce the
paper's MNIST results.


This implementation simplifies the model in the following ways:

 - LeNetConvPool doesn't implement location-specific gain and bias parameters
 - LeNetConvPool doesn't implement pooling by average, it implements pooling
   by max.
 - Digit classification is implemented with a logistic regression rather than
   an RBF network
 - LeNet5 was not fully-connected convolutions at second layer

References:
 - Y. LeCun, L. Bottou, Y. Bengio and P. Haffner:
   Gradient-Based Learning Applied to Document
   Recognition, Proceedings of the IEEE, 86(11):2278-2324, November 1998.
   http://yann.lecun.com/exdb/publis/pdf/lecun-98.pdf

"""
import warnings
warnings.simplefilter('ignore')

import os
import sys
import time
import gzip
import cPickle
import numpy

import theano
import theano.tensor as T
from theano.tensor.signal import downsample
from theano.tensor.nnet import conv

from logistic_sgd import LogisticRegression
from mlp import HiddenLayer
from dataset import *


class LeNetConvPoolLayer(object):
	"""Pool Layer of a convolutional network """

	def __init__(self, rng, input, filter_shape, image_shape, poolsize=(2, 2)):

		assert image_shape[1] == filter_shape[1]
		self.input = input

		fan_in = numpy.prod(filter_shape[1:])
		fan_out = (filter_shape[0] * numpy.prod(filter_shape[2:]) /numpy.prod(poolsize))

		W_bound = numpy.sqrt(6. / (fan_in + fan_out))

		self.W = theano.shared(numpy.asarray(rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),dtype=theano.config.floatX),borrow=True)

		b_values = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
		self.b = theano.shared(value=b_values, borrow=True)

		conv_out = conv.conv2d(input=input, filters=self.W,filter_shape=filter_shape, image_shape=image_shape)

		pooled_out = downsample.max_pool_2d(input=conv_out,ds=poolsize, ignore_border=True)

		self.output = T.tanh(pooled_out + self.b.dimshuffle('x', 0, 'x', 'x'))

		self.params = [self.W, self.b]


class CNN:

	def __init__(self,input_size=32,kernel_size=5,batch_size=128,pool_size=2,nkerns=[48,128],n_classes=62):

		self.input_size = input_size
		self.kernel_size = kernel_size
		self.batch_size = batch_size
		self.pool_size = pool_size
		self.nkerns = nkerns
		self.n_classes = n_classes

		rng = numpy.random.RandomState(23455)

		index = T.lscalar()
		x = T.matrix('x') 
		y = T.ivector('y')

		layer0_input = x.reshape((batch_size, 1, input_size, input_size))        

		layer0 = LeNetConvPoolLayer(rng, input=layer0_input,
				image_shape=(batch_size, 1, input_size, input_size),
				filter_shape=(nkerns[0], 1, kernel_size, kernel_size), poolsize=(2, 2))

		layer0_output_size = (input_size-kernel_size+1)/pool_size

		print (batch_size, 1, input_size, input_size),(nkerns[0], 1, kernel_size, kernel_size)

		layer1 = LeNetConvPoolLayer(rng, input=layer0.output,
				image_shape=(batch_size, nkerns[0], layer0_output_size, layer0_output_size),
				filter_shape=(nkerns[1], nkerns[0], kernel_size, kernel_size), poolsize=(pool_size, pool_size))

		layer1_output_size = (layer0_output_size-kernel_size+1)/pool_size

		print (batch_size, nkerns[0], layer0_output_size, layer0_output_size),(nkerns[1], nkerns[0], kernel_size, kernel_size)

		# layer2 = LeNetConvPoolLayer(rng, input=layer1.output,
		# 		image_shape=(batch_size, nkerns[1], layer1_output_size, layer1_output_size),
		# 		filter_shape=(nkerns[2], nkerns[1], kernel_size, kernel_size), poolsize=(pool_size, pool_size))

		# layer2_output_size = (layer1_output_size-kernel_size+1)/pool_size

		layer3_input = layer1.output.flatten(2)
		print nkerns[1] * layer1_output_size * layer1_output_size

		layer3 = HiddenLayer(rng, input=layer3_input, n_in=nkerns[1] * layer1_output_size * layer1_output_size,
							 n_out=240, activation=T.tanh)

		layer4 = LogisticRegression(input=layer3.output, n_in=240, n_out=n_classes)

		self.lastlayer = layer4

		self.cost = self.lastlayer.negative_log_likelihood(y)
		self.probability = self.lastlayer.log_likelihood()

		self.params = layer4.params+ layer3.params + layer1.params + layer0.params #+  layer2.params
		self.grads = T.grad(self.cost, self.params)

		self.index = index
		self.y = y
		self.x = x


	def compute_test_probabilities(self):
		batch_size = self.batch_size
		probs = numpy.array([self.prob_model(i) for i in xrange(self.n_test_batches)])
		probs = probs[0,0:batch_size-self.test_pad,:] 

		return  probs


	def create_prob_model(self,X):
		index = self.index
		x = self.x
		y = self.y
		batch_size = self.batch_size

		t = X.shape

		X,self.test_pad = self.complete_to_batch(X,batch_size)

		test_set_x = shared_dataset_X(X)		

		self.prob_model = theano.function([index], self.probability,
			givens={x:test_set_x[index * batch_size: (index + 1) * batch_size]})

		n_test_batches = self.test_set_x.get_value(borrow=True).shape[0]
		self.n_test_batches /= self.batch_size


	def complete_to_batch(self,X,batch_size):

		pad = (batch_size-(len(X)%batch_size))%batch_size
		X_pad = numpy.tile(X[-1],(pad,1))

		if len(X.shape) == 2:
			X = numpy.vstack((X,X_pad))
		elif len(X.shape) == 1:
			X = numpy.append(X,X_pad)
		return X,pad


	def score(self,X_test,y_test):

		index = self.index
		x = self.x
		y = self.y
		batch_size = self.batch_size

		X_test,pad = self.complete_to_batch(X_test,batch_size)
		y_test,pad = self.complete_to_batch(y_test,batch_size)

		test_set_x, test_set_y = shared_dataset(X_test,y_test)

		self.test_model = theano.function([index], self.lastlayer.errors_1b1(y),
			givens={
				x: test_set_x[index * batch_size: (index + 1) * batch_size],
				y: test_set_y[index * batch_size: (index + 1) * batch_size]})

		n_test_batches = test_set_x.get_value(borrow=True).shape[0]
		n_test_batches /= self.batch_size
		test_losses = numpy.array([self.test_model(i) for i in xrange(n_test_batches)])

		results = test_losses[0:n_test_batches-1].flatten()
		results = numpy.append(results,test_losses[-1][0:batch_size-pad])

		test_score = 1.0-numpy.mean(results)

		return test_score

		
	def fit(self,X_train,y_train,X_valid=None,y_valid=None,learning_rate=.1, n_epochs=10000):

		rng = numpy.random.RandomState(23455)
		index = self.index
		x = self.x
		y = self.y
		batch_size = self.batch_size

		X_train,pad = self.complete_to_batch(X_train,batch_size)
		y_train,pad = self.complete_to_batch(y_train,batch_size)

		print X_train.shape, y_train.shape

		train_set_x, train_set_y = shared_dataset(X_train,y_train)
		test_set_x = shared_dataset_X(numpy.array([[0.0]]))

		if X_valid == None:
			self.validate_model = None
		else:	
			valid_set_x, valid_set_y = shared_dataset(X_valid,y_valid)
			self.validate_model = theano.function([index], self.lastlayer.errors(y),
				givens={
					x: valid_set_x[index * batch_size: (index + 1) * batch_size],
					y: valid_set_y[index * batch_size: (index + 1) * batch_size]})

			n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
			n_valid_batches /= batch_size
			n_valid_batches += 1

		
		updates = []
		for param_i, grad_i in zip(self.params, self.grads):
			updates.append((param_i, param_i - learning_rate * grad_i))

		self.train_model = theano.function([index], self.cost, updates=updates,
			  givens={
				x: train_set_x[index * batch_size: (index + 1) * batch_size],
				y: train_set_y[index * batch_size: (index + 1) * batch_size]})

		
		# compute number of minibatches for training, validation and testing
		n_train_batches = train_set_x.get_value(borrow=True).shape[0]
		n_train_batches /= batch_size
		

		patience = 50000  # look as this many examples regardless
		patience_increase = 10  # wait this much longer when a new best is
							   # found
		improvement_threshold = 0.995  # a relative improvement of this much is considered significant

		validation_frequency = min(n_train_batches, patience / 2)

		best_params = None
		best_validation_loss = numpy.inf
		best_iter = 0
		test_score = 0.
		start_time = time.clock()

		epoch = 0
		done_looping = False

		while (epoch < n_epochs) and (not done_looping):
			epoch = epoch + 1
			for minibatch_index in xrange(n_train_batches):

				iter = (epoch - 1) * n_train_batches + minibatch_index
				if iter % 100 == 0:
					print 'training @ iter = ', iter
				#print minibatch_index,n_train_batches,X_train.shape,y_train.shape

				cost_ij = self.train_model(minibatch_index)

				if (iter + 1) % validation_frequency == 0 and self.validate_model != None:

					# compute zero-one loss on validation set
					
					this_validation_loss = self.score(X_valid,y_valid)
					print this_validation_loss					
					print('epoch %i, minibatch %i/%i, validation error %f %%' % \
						  (epoch, minibatch_index + 1, n_train_batches, \
						   this_validation_loss * 100.))

					# if we got the best validation score until now
					if this_validation_loss < best_validation_loss:

						#improve patience if loss improvement is good enough
						if this_validation_loss < best_validation_loss *  \
						   improvement_threshold:
							patience = max(patience, iter * patience_increase)

						# save best validation score and iteration number
						best_validation_loss = this_validation_loss
						best_iter = iter

				if patience <= iter:
					done_looping = True
					break

		end_time = time.clock()
		if self.validate_model != None:
			print('Best validation score of %f %% obtained at iteration %i,'\
			  	'with test performance %f %%' %
			  	(best_validation_loss * 100., best_iter + 1, test_score * 100.))


		print >> sys.stderr, ('The code for file ' +
							  os.path.split(__file__)[1] +
							  ' ran for %.2fm' % ((end_time - start_time) / 60.))



if __name__ == '__main__':
	cnn = CNN(input_size=28)
	f = gzip.open('/home/ouais/data/mnist/mnist.pkl.gz', 'rb')
	train_set, valid_set, test_set = cPickle.load(f)
	f.close()

	train_set_x, train_set_y = train_set
	valid_set_x, valid_set_y = valid_set
	test_set_x, test_set_y = test_set
	
	cnn.fit(X_train=train_set_x,y_train=train_set_y,X_valid=valid_set_x,y_valid=valid_set_y)

