
import numpy
import theano
import theano.tensor as T

def shared_dataset(X,y, borrow=True):
        """ Function that loads the dataset into shared variables

        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        """
        
        shared_x = theano.shared(numpy.asarray(X,dtype=theano.config.floatX),borrow=borrow)
        shared_y = theano.shared(numpy.asarray(y,dtype=theano.config.floatX),borrow=borrow)
        
        # When storing data on the GPU it has to be stored as floats
        # therefore we will store the labels as ``floatX`` as well
        # (``shared_y`` does exactly that). But during our computations
        # we need them as ints (we use labels as index, and if they are
        # floats it doesn't make sense) therefore instead of returning
        # ``shared_y`` we will have to cast it to int. This little hack
        # lets ous get around this issue
        return shared_x, T.cast(shared_y, 'int32')




def shared_dataset_X(X,borrow=True):
        
        shared_x = theano.shared(numpy.asarray(X,dtype=theano.config.floatX),borrow=borrow)
        return shared_x


def sparse_shared_dataset_X(X,borrow=True):
        
        shared_x = theano.shared(X,borrow=borrow)
        return shared_x
