import numpy as np
import theano
import theano.tensor as T
from deepnn.dataset import *
from collections import Counter
from theano.tensor.shared_randomstreams import RandomStreams

from pylab import *
from deepnn.NN import HiddenLayer, visualize
from deepnn.algorithm import *
from deepnn.costs import *


import warnings
warnings.simplefilter('ignore')



class SFTD(TrainingAlgorithm):
	def __init__(self,NN,L1_lams=None,L2_lams=None,max_row_norms=None,max_filter_norms=None,
		train_last_layer_only=False,momentum = 0.9,epochs=5000,learning_rate=.01,batch_size=120,rate_decay=.998, cost = CrossEntropyLoss):

		TrainingAlgorithm.__init__(self,NN)

		self.hiddenLayer = NN.layers
		self.y= NN.y
		self.momentum = momentum
		self.train_last_layer_only = train_last_layer_only
		self.epochs = epochs
		self.score = NN.score
		self.learning_rate = learning_rate
		self.batch_size = batch_size
		self.cost_1 = cost[0](self.NN.layers[-1].output,self.y)
		self.cost_2 = cost[1](self.NN.layers[-1].output,self.y)
		self.L1_lams = L1_lams
		self.L2_lams = L2_lams
		self.max_row_norms = max_row_norms
		self.max_filter_norms = max_filter_norms
		self.rate_decay = rate_decay
		self.rate_decay_updates = 250 # when is the rate decay activated

		if self.L1_lams == None:
			self.L1_lams = [0] * len(NN.layers)

		if self.L2_lams == None:
			self.L2_lams = [0] * len(NN.layers)

		if self.max_row_norms == None:
			self.max_row_norms = [0] * len(NN.layers)

		if self.max_filter_norms == None:
			self.max_filter_norms = [0] * len(NN.layers)
	
	def __str__(self):
		return 'Backpropagation'

	def set_momentum(self,momentum):
		self.global_momentum.set_value(value=momentum)

	def fit(self,X,y):

		warnings.simplefilter('ignore')
		v = T.ivector()

		# cost = self.cost

		# for layer_id in xrange(len(self.NN.layers)-1):
		# 	layer = self.NN.layers[layer_id]
		# 	if self.L2_lams[layer_id] > 0:
		# 		cost += self.L2_lams[layer_id]* T.sum(layer.output**2.) / layer.output.size


		params_to_tune = self.NN.layers[-1].params[:]

		if self.train_last_layer_only is False:
			for i in xrange(len(self.NN.layers)-1):
				params_to_tune.extend(self.NN.layers[i].params)

		# params_to_tune.append(self.x)

		cost_1 = self.cost_1
		cost_2 = self.cost_2
		

		updates_1, pars, trsh1 = compute_updates_grads(cost_1, params_to_tune,self.learning_rate,momentum=self.momentum)
		updates_2, pars, trsh1 = compute_updates_grads(cost_2, params_to_tune,self.learning_rate,momentum=self.momentum)

		# updates_1 = constrain_row_norms(updates_1,max_row_norms=self.max_row_norms)
		# updates_2 = constrain_row_norms(updates_2,max_row_norms=self.max_row_norms)

		global_momentum, global_learning_rate = pars

		y_hat = T.argmax(self.NN.layers[-1].output,axis=1)
		errors = T.mean(T.neq(y_hat, self.y))


		train = theano.function([self.x,self.y], cost_1, updates=updates_1)
		special_train = theano.function([self.x,self.y], cost_2, updates=updates_2)

		max_n_batches = len(X)

		self.losses = []	
		updates_counter = 0	

		# print X[0].shape

		for e in xrange(self.epochs):
			for i in xrange(max_n_batches):		

				if X[i][1] is None:
					mini_X = np.array([X[i][0]],dtype='float32')
					loss = special_train(mini_X,np.array([y[i]]))		
				else:
					mini_X = np.vstack((X[i][0],X[i][1]))
					mini_X = np.array(mini_X,dtype='float32')

					loss = train(mini_X,np.array([y[i]]))		

				updates_counter += 1

				self.losses.append(loss)

				if updates_counter >= self.rate_decay_updates:
					updates_counter = 0
					new_learning_rate = np.array(global_learning_rate.get_value()* self.rate_decay,dtype='float32')
					global_learning_rate.set_value(new_learning_rate)


			
			


		return self.losses