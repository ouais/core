import numpy

from utils.IO import *
from utils.misc import *
from utils.utils import *

import time
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from utils import *
from random import shuffle

from pylab import *
from preprocessing import normalize_dataset,normalize_dataset2
import PIL.Image
from collections import Counter

from sklearn.gaussian_process import GaussianProcess
from sklearn import svm
from sklearn import neighbors

from ldr.samplers import SupervisedSampler
from train_rl import SFTD
from deepnn.NN import *

def read_data():
	data= IO.unpickle('mountaincar_trajectories.data')
	X = []
	for x in data:
		X.extend(x)

	pairs = []
	y = []

	for x in X:
		if x[2] is None:
			pairs.append((x[0][:2],x[2]))
		else:
			pairs.append((x[0][:2],x[2][:2]))
		# print pairs[-1]
		y.append(x[1])

	y = np.array(y,dtype='int32')

	return pairs, y

def read_test_data():
	data= IO.unpickle('mountaincar_grid.data')
	data = np.array(data,dtype='float32')
	return data



def ClementLoss(x,y,gamma):
	return T.mean((x[0] - gamma*x[1]-y[0])**2)

def ClementLoss_2(x,y,gamma):
	return T.mean((x[0] -y[0])**2)

def test():

	rng = np.random.RandomState(555)

	nn = NeuralNetwork([2,40,1],activations=[lambda x: T.nnet.sigmoid(x),lambda x:x],drops=[0],rng=rng)

	pairs, y = read_data()

	cost_1 = lambda x,y: ClementLoss(x,y,.99)
	cost_2 = lambda x,y: ClementLoss_2(x,y,.99)

	cost = (cost_1,cost_2)

	sftd = SFTD(nn,epochs=5,max_row_norms=[1,1,1],learning_rate = .05,momentum=.0, batch_size=100,cost=cost)
	losses = sftd.fit(pairs,y)

	plot(losses)
	show()

	data = read_test_data()
	data = data[:,:2]

	y_hat = nn.predict_proba(data)

	print y_hat.shape, data.shape

	print np.mean(y_hat)

	# points = data[:,:2]
	points = data
	points = np.hstack((points,y_hat))

	import matplotlib.pyplot as plt
	from matplotlib import cm
	from mpl_toolkits.mplot3d import Axes3D

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	ax.plot_trisurf(*zip(*points), cmap=cm.jet, linewidth=0.2)

	plt.show()
	# IO.pickle(nn,'models/nn_mnist_bprop.pkl')


if __name__ == '__main__':
	test()