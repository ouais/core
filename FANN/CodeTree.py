
import numpy
import time
import operator
import sys
from utils.IO import *
from utils.misc import *
from utils.utils import *
from OrderedDropout import *
from scipy.stats import mode

try:
    pkl = "pickles/" + sys.argv[2] + ".pkl"
except IndexError:
    pkl = "pickles/nn_600.pkl"
    
nn= IO.unpickle(pkl)
nn.set_test_mode()
X_train,y_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl") 
X_train,y_train = normalize_dataset(X_train,y_train)

X_test,y_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
X_test,y_test = normalize_dataset(X_test,y_test)

	

def testingViewing():
    
    
    ordropoutLayer = findOrdropoutLayer(nn)          
    firings = nn.temporal_outputs(X_train,ordropoutLayer)
    ordropoutLayer = findOrdropoutLayer(nn)
    codeLength = 0 if nn.layers[ordropoutLayer].unitSweep.get_value() ==0 else  nn.layers[ordropoutLayer].unitSweep.get_value() -1
    print codeLength
    plot_histograms(firings,49)
    show()
    
def testing123():
    
    ordropoutLayer = findOrdropoutLayer(nn)
    codeLength = 0 if nn.layers[ordropoutLayer].unitSweep.get_value() ==0 else  nn.layers[ordropoutLayer].unitSweep.get_value() -1
    
    
    trainFirings = nn.temporal_outputs(X_train,ordropoutLayer)
    trainEncodings,threshold = generateEncodings(trainFirings,codeLength)
    
    root = generateTree(trainEncodings) 
    
    
    
    testFirings = nn.temporal_outputs(X_test,ordropoutLayer)
    testEncodings,threshold = generateEncodings(testFirings,codeLength,threshold)
        
    #check, should be the same as encodings
#    codes = hiddenLayerActivations[indices]
        
    #precision at k test    
#    numSamples = 100
#    logIndices = np.logspace(0.0,4.0,num = numSamples)
#    precision = numpy.zeros(numSamples)
#    for j in range(numSamples):
#        nSize = logIndices[j]
#        numNeighbors = 0
#        numSameClass = 0
#        for i in range(size(testEncodings,axis = 0)):
#            testClass = y_test[i]
#            neighbors = throughTreeBySize(root,example =testEncodings[i],neighborhoodSize =nSize)
#            #        neighbors = throughTreeByB(root,example =encodings[i],b= numpy.size(encodings,axis = 1) -3)
#    
#            neighborsClass =  y_train[neighbors]
#            sameClassNeighbors = testClass == neighborsClass
#            numSameClass += sameClassNeighbors.sum()
#            numNeighbors += size(neighborsClass)
#        
#        precision[j] =float(numSameClass)/numNeighbors 
#        print precision[j]
        
    #precision @ b test
#    precision = numpy.zeros(numpy.size(testEncodings,axis = 1))
#    for j in range(numpy.size(testEncodings,axis = 1)):
#        numNeighbors = 0
#        numSameClass = 0
#        for i in range(size(testEncodings,axis = 0)):
#            testClass = y_test[i]
#            neighbors = throughTreeByB(root,example =testEncodings[i],b= j+1)
#            neighborsClass =  y_train[neighbors]
#            sameClassNeighbors = testClass == neighborsClass
#            numSameClass += sameClassNeighbors.sum()
#            numNeighbors += size(neighborsClass)
#        
#        precision[j] =float(numSameClass)/numNeighbors 
#        print precision[j]
        
    #k nearest neighbors    
    numSamples = 10
    logIndices = np.logspace(0.0,2.0,num = numSamples)
    precision = numpy.zeros(numSamples)
    for j in range(numSamples):
        nSize = logIndices[j]
        numNeighbors = 0
        numSameClass = 0
        for i in range(size(testEncodings,axis = 0)):
            testClass = y_test[i]
            neighbors = throughTreeBySize(root,example =testEncodings[i],neighborhoodSize =nSize)
            
            #if you get too many neighbors, use euclid distance on the subset            
#            if len(neighbors) > nSize:
#            if size(neighbors,axis = 0) > nSize:
#                similarityDict = {}
#                testCode = testEncodings[i]
#                for k in range(size(neighbors,axis = 0 )):
#                    trainCode = trainEncodings[neighbors[k]]
#                    difference = sum(square(testCode-trainCode))
#                    similarityDict[k] = difference
                
                
                
    
            neighborsClass =  y_train[neighbors]
            
            nearestClass = scipy.stats.mode(neighborsClass,axis = None)
            
            if nearestClass[0] == testClass:
                numSameClass += 1
            numNeighbors += 1
        
        precision[j] =float(numSameClass)/numNeighbors 
        print logIndices[j]
        print precision[j]
        

    print 'hello'
    
def testingSpeed():
    nn= IO.unpickle("pickles/nn_2048.pkl")
    nn.set_test_mode()
    
    
    ordropoutLayer = findOrdropoutLayer(nn)
    lengthList = [1024]
    precision = zeros((3,5))
    trainFirings = nn.temporal_outputs(X_train,ordropoutLayer)
    testFirings = nn.temporal_outputs(X_test,ordropoutLayer)
    for k in range(len(lengthList)):
        codeLength = lengthList[k] -1    
        trainEncodings,threshold = generateEncodings(trainFirings,codeLength)
        
        root = generateTree(trainEncodings) 
        
        testEncodings,threshold = generateEncodings(testFirings,codeLength,threshold)
        
        logIndices = [2,32,512]
        
        for j in range(len(logIndices)):
            nSize = logIndices[j]
            timeSpent = 0
    
            for i in range(size(testEncodings,axis = 0)):
                start = time.clock()
                neighbors = throughTreeBySize(root,example =testEncodings[i],neighborhoodSize =nSize)
                #        neighbors = throughTreeByB(root,example =encodings[i],b= numpy.size(encodings,axis = 1) -3)
                stop = time.clock()
                timeSpent += (stop - start)
            
            precision[j,k] =float(timeSpent)/size(testEncodings,axis=0)
            print logIndices[j]
            print lengthList[k] 
            print precision[j,k]
        
        #delete the tree
        treeDeletion(root)
        
def testingEuclid():
     
    
    ordropoutLayer = findOrdropoutLayer(nn)
    codeLength = 0 if nn.layers[ordropoutLayer].unitSweep.get_value() ==0 else  nn.layers[ordropoutLayer].unitSweep.get_value() -1
    
    
    trainFirings = nn.temporal_outputs(X_train,ordropoutLayer)
    trainEncodings,threshold = generateEncodings(trainFirings,codeLength)
    
    root = generateTree(trainEncodings)
    
    
    
    testFirings = nn.temporal_outputs(X_test,ordropoutLayer)
    testEncodings,threshold = generateEncodings(testFirings,codeLength,threshold)
    
    numSamples = 10
    logIndices = np.logspace(0.0,2.0,num = numSamples)
    precision = numpy.zeros(numSamples)    
    numSameClass= numpy.zeros(numSamples)
    numTotal = numpy.zeros(numSamples)

    for j in range(numpy.size(testEncodings,axis = 0)):
        similarityDict = {}
        testCode = testEncodings[j]
        testClass = y_test[j]
        
        for i in range(numpy.size(trainEncodings,axis = 0)):
            trainCode = trainEncodings[i]
            difference = sum(square(testCode-trainCode))
            similarityDict[i] = difference
        sortedDict = sorted(similarityDict.iteritems(), key = operator.itemgetter(1))
        for d in range(numSamples):
            k = int(logIndices[d])
            nearestNeighbors = sortedDict[0:k]
            trainClass = zeros(size(nearestNeighbors,axis = 0))
            for i in range(size(nearestNeighbors,axis = 0)):
                trainIndice = nearestNeighbors[i][0]
                trainClass[i] =  y_train[trainIndice]
            nearestClass = scipy.stats.mode(trainClass,axis=None)
            if nearestClass[0] == testClass:
                numSameClass[d] += 1
            numTotal[d] += 1
                
        print j
    for d in range(numSamples):        
        precision[d] = float(numSameClass[d])/numTotal[d]
        print int(logIndices[d])
        print precision[d]
        
    print 'hello'     
    

def testingWeightedEuclid():
   
    
    ordropoutLayer = findOrdropoutLayer(nn)
    codeLength = 0 if nn.layers[ordropoutLayer].unitSweep.get_value() ==0 else  nn.layers[ordropoutLayer].unitSweep.get_value() -1
    
    
    trainFirings = nn.temporal_outputs(X_train,ordropoutLayer)
    trainEncodings,threshold = generateEncodings(trainFirings,codeLength)
    
    
    testFirings = nn.temporal_outputs(X_test,ordropoutLayer)
    testEncodings,threshold = generateEncodings(testFirings,codeLength,threshold)
    
    probs = numpy.asarray([0.97*((1-0.97)**i) for i in range(int(codeLength))],dtype = np.float32)
    
    numSamples = 10
    logIndices = np.logspace(0.0,2.0,num = numSamples)
    precision = numpy.zeros(numSamples)    
    numSameClass= numpy.zeros(numSamples)
    numTotal = numpy.zeros(numSamples)
    
    for j in range(numpy.size(testEncodings,axis = 0)):
        similarityDict = {}
        testCode = testEncodings[j]
        testClass = y_test[j]
        
        for i in range(numpy.size(trainEncodings,axis = 0)):
            trainCode = trainEncodings[i]
            difference = sum(square(testCode-trainCode)*probs)
            similarityDict[i] = difference
        sortedDict = sorted(similarityDict.iteritems(), key = operator.itemgetter(1))
        for d in range(numSamples):
            k = int(logIndices[d])
            nearestNeighbors = sortedDict[0:k]
            trainClass = zeros(size(nearestNeighbors,axis = 0))
            for i in range(size(nearestNeighbors,axis = 0)):
                trainIndice = nearestNeighbors[i][0]
                trainClass[i] =  y_train[trainIndice]
            nearestClass = scipy.stats.mode(trainClass,axis=None)
            if nearestClass[0] == testClass:
                numSameClass[d] += 1
            numTotal[d] += 1
                
        print j
        
    for d in range(numSamples):        
        precision[d] = float(numSameClass[d])/numTotal[d]
        print int(logIndices[d])
        print precision[d]
        
    print 'hello'     
    

    
            
def treeDeletion(node):
    if node.left != None:
        treeDeletion(node.left)
    if node.right != None:
        treeDeletion(node.right)
    node =None
    
def throughTreeBySize(root,example,neighborhoodSize):
    currentNode = root
    for j in range(numpy.size(example,axis=0)):
        if example[j] ==0:
            if currentNode.left ==None:
                currentNode = currentNode.right
            else:
                currentNode = currentNode.left
        elif example[j] ==1:
            if currentNode.right == None:
                currentNode = currentNode.left
            else:
                currentNode = currentNode.right
        if currentNode.marginalSize < neighborhoodSize:
            return currentNode.data
    return currentNode.data
def throughTreeByB(root,example,b):
    currentNode = root
    for j in range(b):
        if example[j] ==0:
            if currentNode.left ==None:
                return []
            currentNode = currentNode.left
        elif example[j] ==1:
            if currentNode.right == None:
                return []
            currentNode = currentNode.right
    return currentNode.data
    
def generateTree(examples):
    root = TreeNode()
    for i in range(numpy.size(examples,axis= 0)):
        currentNode = root
        currentNode.addData(i)
        for j in range(numpy.size(examples,axis=1)):
            if examples[i,j]==0:
                if currentNode.left ==None:
                    newNode = TreeNode()
#                    newNode = TreeNode(id = currentNode.id)
#                    newNode.appendId(0)
                    currentNode.left = newNode
                currentNode = currentNode.left
            elif examples[i,j]==1:
                if currentNode.right ==None:
                    newNode= TreeNode()
#                    newNode = TreeNode(id=currentNode.id)
#                    newNode.appendId(1)
                    currentNode.right = newNode
                currentNode = currentNode.right
                
            else:
                print 'error'
                
            currentNode.addData(i)
        
        
                
    return root   
    
    
class TreeNode():
#    def __init__(self,data=[],left=None,right=None,id =[],marginalSize = 0):
#        self.left = left
#        self.right = right
#        self.id = id
#        self.marginalSize = marginalSize        
#        self.data.append(data)
#    def appendId(self,newNum):
#        self.id.append(newNum)
    def __init__(self,data = None,left=None,right=None,marginalSize = 0):
        self.left = left
        self.right = right
        self.marginalSize = marginalSize
        if not data:        
            self.data = []
        else:
            self.data = data
        
    def addData(self,newData):
        self.data.append(newData)
        self.marginalSize += 1
    
def testCenter():
    try:
        print sys.argv[1]
        if sys.argv[1] == 'testing123':            
            testing123()
        elif sys.argv[1] == 'testingSpeed':
            testingSpeed()
        elif sys.argv[1] == 'testingEuclid':
            testingEuclid()
        elif sys.argv[1] == 'testingWeightedEuclid':
            testingWeightedEuclid()
        elif sys.argv[1] == 'testingViewing':
            testingViewing()    
        else:
            print 'incorrect command'
       
    except IndexError:
        testing123()
    
            
    
if __name__ == '__main__':
	testCenter()
