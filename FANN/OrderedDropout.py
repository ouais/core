# -*- coding: utf-8 -*-
"""
Created on Thu Jun 12 15:16:52 2014

@author: pnguye41
"""

import time
import numpy

from utils.IO import *
from utils.misc import *
from utils.utils import *

from deepnn.algorithm import *
from deepnn.NN import *

import theano
import theano.sparse
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from theano import function




class OrderDropoutNN(NeuralNetwork):
    def __init__(self,layers_sizes,activations=lambda t: T.maximum(0,t),name='none',rng=None,drops=[],sparse_input = False,ordrops = []):

		if rng is None:			
			self.numpy_rng = numpy.random.RandomState(666)
		else:
			self.numpy_rng = rng

		self.theano_rng = RandomStreams(self.numpy_rng.randint(666))

		if sparse_input:
			self.x = theano.sparse.csc_matrix('x')
		else:
			self.x = T.fmatrix('x')
		self.y = T.fmatrix('y') 

		self.layers_sizes = layers_sizes		
		self.n_layers = len(layers_sizes)

		self.layers = [0]*(self.n_layers-1)
		self.output = [0]*(self.n_layers-1)

		if len(drops) != len(layers_sizes):
			drops = [0]* len(layers_sizes)

		self.drops = drops

		self.hiddenlayer_params = []

		if not isinstance(activations,list):
			activations = [activations] *(self.n_layers-1)

		self.activations = activations

		
        	self.input_dropout = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)
		self.input_scale = theano.shared(theano._asarray(1, dtype=theano.config.floatX),borrow=True)
		self.name = name

		layer_input = self.x

        	if self.drops[0] > 0:
			self.input_dropout.set_value(self.drops[0])
			mask  = self.theano_rng.binomial(size = (layer_input.shape) ,p=self.input_dropout,dtype='float32')
			layer_input = layer_input * mask * self.input_scale

        	self.ordrops = ordrops
        	#if self.ordrops[0] > 0:
                #DO later, add ordered dropout for the first input layer
              
		layer_trueInput = self.x
		self.noiseAmount = 1e-1
		layer_noisyInput = self.x + self.theano_rng.normal(size=self.x.shape, std = self.noiseAmount)
        
		for i in xrange(self.n_layers-1):
        		self.layers[i] = OrderDropoutHiddenLayer(
				rng=self.numpy_rng, theano_rng = self.theano_rng,
				input=layer_input,n_in=layers_sizes[i], n_out=layers_sizes[i+1], trueInput = layer_trueInput, noisyInput= layer_noisyInput,
				dropout = self.drops[i+1], activation=self.activations[i],ordrop = self.ordrops[i+1])

			self.output[i] = theano.function([self.x],self.layers[i].output)
			layer_input = self.layers[i].output
			layer_trueInput = self.layers[i].trueOutput
			layer_noisyInput = self.layers[i].noisyOutput


		self.last_input = layer_input

#    def set_dropout(self,scale):
#    		for layer in self.layers:
#    			layer.set_dropout(value=scale)
#    
#    def set_weights_scale(self,scale):
#    		for layer in self.layers:
#    			layer.set_weights_scale(value=scale)
#    
#    def set_train_mode(self):
#            
#    		self.input_dropout.set_value(self.drops[0])
#    		self.input_scale.set_value(1)
#    
#    		for layer in self.layers:
#    			layer.set_train_mode()
#    
#    def set_test_mode(self):
#    		self.input_dropout.set_value(1)
#    		self.input_scale.set_value(self.drops[0])
#    
#    		for layer in self.layers:
#    			layer.set_test_mode()
#    
#    def get_masks(self,n_classes, d):
#    		masks = []
#    		stride = d/n_classes
#    
#    		for c in xrange(n_classes):
#    			mask = numpy.zeros(d)
#    			mask[c*stride:(c+1)*stride] = 1
#    			masks.append(mask)		
#    		masks = numpy.array(masks,dtype='float32')
#    		masks = theano.shared(value=masks, borrow=True)
#    		return masks
#    		
#    
#    def add_generic_layer(self,layer):
#    		self.layers.append(layer)
#    		self.output.append(theano.function([self.x],layer.output))
#    
#    def add_layer(self,n_out,dropout=0, activation= lambda x: safe_softmax(x)):
#    		h = HiddenLayer(rng=self.numpy_rng, theano_rng=self.theano_rng, input=self.last_input,n_in=self.layers_sizes[-1], n_out=n_out,dropout=dropout,activation=activation)
#    		self.layers.append(h)
#    		self.output.append(theano.function([self.x],h.output))
#    		self.layers_sizes.append(n_out)
#    		self.last_input = h.output
#    
#    
#    def temporal_outputs(self,X,layer_index):
#    
#    		firings = self.output[layer_index](X)
#    		return firings
    
#    @batch(128)
#    def predict(self,X):
#    
#    		p_hat = self.output[-1](X)
#    		y_hat = p_hat.argmax(axis=1)		
#    		return y_hat
    
#    def predict_proba(self,X):
#    		return self.output[-1](X)
    	
    def score(self, X,y):
    		y_hat = self.output[-1](X)
    		return numpy.sqrt(numpy.mean((y_hat-y)**2))              
    		
     
        
        
        
class OrderDropoutHiddenLayer(HiddenLayer):
    def __init__(self, rng, theano_rng, input, n_in, n_out,trueInput,noisyInput, W=None, b=None,dropout=0,activation=T.tanh,ordrop=0):
        HiddenLayer.__init__(self, rng, theano_rng, input, n_in, n_out, W=None, b=None,dropout=0,activation=T.tanh)
        
        if isinstance(trueInput,theano.sparse.SparseVariable):
            lin_trueOutput = theano.sparse.dot(trueInput, self.W ) + self.b
        else:
            lin_trueOutput = T.dot(trueInput, self.W) + self.b	
        self.trueOutput = activation(lin_trueOutput)
        
        if isinstance(noisyInput,theano.sparse.SparseVariable):
            lin_noisyInput = theano.sparse.dot(noisyInput, self.W ) + self.b
        else:
            lin_noisyInput = T.dot(noisyInput, self.W) + self.b
        self.noisyOutput= activation(lin_noisyInput)
        
        
        self.original_ordrop = ordrop
        self.ordrop = ordrop
        self.unitSweep =theano.shared(theano._asarray(0, dtype=theano.config.floatX),borrow = True)

        
        self.probs = [self.ordrop*((1-self.ordrop)**i) for i in range(self.n_out)]
        self.geopvals = theano.shared(theano._asarray(self.probs, dtype=theano.config.floatX),borrow=True)
        if self.ordrop > 0:
            #set up geometric mask
            
            geomArray = self.theano_rng.multinomial(size= self.output.mean(axis=1).shape,n=1,pvals=self.geopvals,dtype='float32')
            geomValue =T.outer((geomArray.T.nonzero()[0]),T.ones(self.n_out))
            fill = T.ones(self.output.mean(axis=1).shape)
            indexArray = theano.shared(theano._asarray(range(self.n_out),dtype = theano.config.floatX))
            indices = T.outer(indexArray,fill).T
            self.geomMask = T.ge(geomValue+self.unitSweep,indices)
            self.output = self.output * self.geomMask * self.activation_scale 
            
    def set_ordrop(self,scale):
		probs = numpy.asarray([scale*((1-scale)**i) for i in range(self.n_out)],dtype = np.float32)
		self.geopvals.set_value(probs)
            
    def set_test_mode(self):
            

		self.set_dropout(1)
		self.noise_scale.set_value(0)
		activation_scale = 1 if self.original_dropout == 0 else self.original_dropout		
		self.set_activation_scale(activation_scale)
  
  
		self.set_ordrop(0)   

    def set_train_mode(self):
		self.set_dropout(self.original_dropout)				
		self.set_activation_scale(1)
		self.noise_scale.set_value(1)
  
		self.set_ordrop(self.original_ordrop)
  
  

class AutoBackpropagation(TrainingAlgorithm):
    #BackProp for autoencoder
    #changes:
    #cost = RMSE, , yhat = ...
    

    def __init__(self,NN,L1_lams=None,L2_lams=None,max_row_norms=None,max_filter_norms=None,
                 train_last_layer_only=False,momentum = 0.9,epochs=5000,learning_rate=.01,batch_size=120,rate_decay=.998, cost = RMSE):

        TrainingAlgorithm.__init__(self,NN)

        self.hiddenLayer = NN.layers
        self.y= NN.y
        self.momentum = momentum
        self.train_last_layer_only = train_last_layer_only
        self.epochs = epochs
        self.score = NN.score
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.cost = cost(self.NN.layers[-1].output.T,self.y)
        self.L1_lams = L1_lams
        self.L2_lams = L2_lams
        self.max_row_norms = max_row_norms
        self.max_filter_norms = max_filter_norms
        self.rate_decay = rate_decay
        self.rate_decay_updates = 250 # when is the rate decay activated

        if self.L1_lams == None:
                self.L1_lams = [0] * len(NN.layers)

        if self.L2_lams == None:
                self.L2_lams = [0] * len(NN.layers)

        if self.max_row_norms == None:
                self.max_row_norms = [0] * len(NN.layers)

        if self.max_filter_norms == None:
                self.max_filter_norms = [0] * len(NN.layers)
        
        #orderedDropout
        self.odLayer = None
        for i in range(self.NN.n_layers):
            if hasattr(self.NN.layers[i],'original_ordrop') and self.NN.layers[i].original_ordrop >0:
                self.odLayer = i
                
        self.invarianceCost = invarianceRegularizer(self.NN.layers[self.odLayer].trueOutput, self.NN.layers[self.odLayer].noisyOutput, self.NN.noiseAmount)
        
	
    def __str__(self):
        return 'Backpropagation'

    def set_momentum(self,momentum):
        self.global_momentum.set_value(value=momentum)
  

      

    def fit(self,X,y):

        warnings.simplefilter('ignore')
        v = T.ivector()

        self.batch_size = min(self.batch_size,len(y))
    		         
		
        train_set_x,train_set_y = shared_dataset(X,y)	

        cost = self.cost + self.invarianceCost

        for layer_id in xrange(len(self.NN.layers)-1):
                layer = self.NN.layers[layer_id]
                if self.L2_lams[layer_id] > 0:
                    cost += self.L2_lams[layer_id]* T.sum(layer.output**2.) / layer.output.size

               
                
        params_to_tune = self.NN.layers[-1].params[:]

        if self.train_last_layer_only is False:
                for i in xrange(len(self.NN.layers)-1):
                    params_to_tune.extend(self.NN.layers[i].params)
		

        updates, pars, trsh1 = compute_updates_grads(cost, params_to_tune,self.learning_rate,momentum=self.momentum)
		          
        updates = constrain_row_norms(updates,max_row_norms=self.max_row_norms)
        updates = constrain_filter_norms(updates,max_filter_norms=self.max_filter_norms)

        global_momentum, global_learning_rate = pars

        y_hat = self.NN.layers[-1].output
        errors = T.sqrt(T.mean((y_hat-self.y)**2))

        train = theano.function([v], [cost, errors], updates=updates,givens=	
				{
					self.x: train_set_x[v],
					self.y: train_set_y[v]
				})

        max_n_batches = X.shape[0]/self.batch_size 
        data_size = X.shape[0]

        self.losses = []	
        updates = 0
        
        #HiddenLayerActivations will be used to know when to unit sweep
        self.NN.set_test_mode()   
        hiddenLayerActivations = []
        for i in range(self.NN.n_layers):
            if i ==self.odLayer :
                hiddenLayerActivations.insert(i,self.NN.output[i](X)[:,self.NN.layers[i].unitSweep.get_value()])
            else:
                hiddenLayerActivations.insert(i,[])
                
        self.NN.set_train_mode()

	                              
        #training every epoch                           
        for e in xrange(self.epochs):
#            start = time.clock()
            self.batch_errors = []
            for i in xrange(max_n_batches):
                indices = np.array(self.numpy_rng.randint(0,data_size,self.batch_size),dtype='int32')
				
                loss,errs = train(indices)
				

                self.batch_errors.append(errs)				

                updates += 1

                if updates >= self.rate_decay_updates:
                    updates = 0
                    new_learning_rate = np.array(global_learning_rate.get_value()* self.rate_decay,dtype='float32')
                    global_learning_rate.set_value(new_learning_rate)

            self.losses.extend(self.batch_errors)
            self.run_epoch_monitors(e)
			#unit sweeping
			#caution: since the layers are set up such that the input isn't considered a layer, unit sweeping will not 
			#work on the input layer. But I don't know why you would do that in the first place            
            self.NN.set_test_mode()                   
            
            RMSEdifference = sqrt(mean((self.NN.output[self.odLayer](X)[:,self.NN.layers[self.odLayer].unitSweep.get_value()] - hiddenLayerActivations[self.odLayer])**2))
            if RMSEdifference < 0.01:
                self.NN.layers[self.odLayer].unitSweep.set_value(self.NN.layers[self.odLayer].unitSweep.get_value() +1)
            hiddenLayerActivations[self.odLayer] = self.NN.output[self.odLayer](X)[:,self.NN.layers[self.odLayer].unitSweep.get_value()]
                      
               
            self.NN.set_train_mode()
#            stop = time.clock()
#            print stop - start     
        return self.losses
  
def shared_dataset(X,y, borrow=True):
    shared_x = theano.shared(numpy.asarray(X,dtype=theano.config.floatX),borrow=borrow)
    shared_y = theano.shared(numpy.asarray(y,dtype=theano.config.floatX),borrow=borrow)


    return shared_x, shared_y
  
def testOrderedDropout():
    
    
    nn = OrderDropoutNN([784,100],activations=[lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)],drops=[.8,0.0],ordrops =[0.0,0.97] )
#    nn = OrderDropoutNN([784,1200,100,1200],activations=[lambda x: x,lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)],drops=[.8,0.0,5.0,0.5],ordrops =[0.0,0.0,0.97,0.0] )
#    nn = OrderDropoutNN([784,1200,1200],activations=[lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)],drops=[.8,0.5,0.5],ordrops =[0.0,0.0,0.0] )
#    nn = OrderDropoutNN([784,2048],activations=[lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)],drops=[.8,0.0],ordrops =[0.0,0.97] )

	# NN = IO.unpickle('sda_cifar100.pkl')
#    nn.add_layer(10)
#
#    X_train,y_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl") 
#    X_train,y_train = normalize_dataset(X_train,y_train)
#
#    X_test,y_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
#    X_test,y_test = normalize_dataset(X_test,y_test)	
#
#	# nn = IO.unpickle('models/nn_dropout_10.pkl')
#
#	# nn = IO.unpickle('models/nn_mnist_bprop.pkl')
#
#    bprop = Backpropagation(nn,epochs=10,max_row_norms=[4,4,4],learning_rate = .01,momentum=.9, batch_size=100)
#    bprop.add_epoch_monitor(lambda e: monitor(e,nn,X_test,y_test))
#
#    losses = bprop.fit(X_train,y_train)

    nn.add_layer(784,activation = lambda x: x)

    X_train,y_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl") 
    X_train,y_train = normalize_dataset(X_train,y_train)

    X_test,y_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
    X_test,y_test = normalize_dataset(X_test,y_test)	

	# nn = IO.unpickle('models/nn_dropout_10.pkl')

	# nn = IO.unpickle('models/nn_mnist_bprop.pkl')

    bprop = AutoBackpropagation(nn,epochs=600,max_row_norms=[4,4,4],learning_rate = .01,momentum=.9, batch_size=100)
    bprop.add_epoch_monitor(lambda e: monitor(e,nn,X_test,X_test))

    losses = bprop.fit(X_train,X_train)
    
    nn.set_test_mode()
    
    
    ordropoutLayer = findOrdropoutLayer(nn)          
    firings = nn.temporal_outputs(X_train,ordropoutLayer)
   
    
    #get the encodings 
#    threshold= 0
#    codeLength = 0 if nn.layers[ordropoutLayer].unitSweep.get_value() ==0 else nn.layers[ordropoutLayer].unitSweep.get_value() -1
#    testFirings = nn.temporal_outputs(X_test,ordropoutLayer)
#    
#    encodings,threshold = generateEncodings(testFirings,codeLength,threshold)
                

	# visualize(X_train,nn,0,'plots/nn_weights_0.png')
	# IO.pickle(nn,'models/nn_dropout_10.pkl')
#    IO.pickle(encodings,'pickles/encoding_600_multi_vstrict.pkl')
#    IO.pickle(nn,'pickles/nn_600_multi_vstrict.pkl')
#    IO.pickle(encodings,'pickles/encoding_600_2.pkl')
    IO.pickle(nn,'pickles/nn_noise_1e-1.pkl')
    
#    plot_histograms(firings,49)
#    plot(losses)
#    show()

def invarianceRegularizer(x,y,noiseAmount):
#    return 0
    return (T.mean(((x-y)**2)/(noiseAmount**2)))
    
    
def generateEncodings(firings, codeLength, threshold= None):
    encodings = firings[:,0:codeLength]
    
    
    
#    threshold = zeros(size(encodings,axis = 1))
#    encodings[encodings > threshold] = 1
#    encodings[encodings <=threshold] = 0
    



    
    
    if threshold == None:
        threshold = zeros(size(encodings,axis = 1))
        enStd = numpy.std(encodings,axis=0)
        bQuantile = 50
        enPercentile = numpy.percentile(encodings,bQuantile,axis = 0)
        
        
        for i in range(size(encodings,axis=1)):
            if enStd[i] < 0.1:
                threshold[i] = 0 #if the variance is this low, then most likely all the values are the same
            else:
                threshold[i] = enPercentile[i]
    
    
    for i in range(size(encodings,axis = 0)):
        encodings[i][encodings[i]> threshold] = 1
        encodings[i][encodings[i]<=threshold] = 0
        

    return encodings,threshold
    

    
def findOrdropoutLayer(nn):
    ordropoutLayer = None
    for i in range(nn.n_layers):
         if hasattr(nn.layers[i],'original_ordrop') and nn.layers[i].original_ordrop >0 :
                ordropoutLayer = i
    if ordropoutLayer == None:
        print 'no nested dropout layer found'
    return ordropoutLayer
    
def monitor(e,model,X,y):
	model.set_test_mode()
	print '{} {:.2f}'.format(e,100*model.score(X,y))
     
	model.set_train_mode()

    
if __name__ == '__main__':
	testOrderedDropout()
