import numpy as np

from utils import IO
from preprocessing import normalize_dataset

from sklearn import svm
from sklearn.cluster import MiniBatchKMeans
from sklearn.decomposition import MiniBatchDictionaryLearning

from sklearn.feature_extraction.image import extract_patches_2d

from pylab import *

def create_kernel(x,patch):
	n = int(np.sqrt(len(x)))
	z = x.reshape((28,28))

	K = np.zeros((len(x),len(x)))
	print patch.shape[0]/2

	for i in xrange(patch.shape[0]/2, z.shape[0]-patch.shape[0]/2):
		for j in xrange(patch.shape[1]/2, z.shape[1]- patch.shape[1]/2):

			pos = i*z.shape[1]+j

			for k1 in xrange(-patch.shape[0]/2+1, patch.shape[0]/2+1):
				for k2 in xrange(-patch.shape[1]/2+1, patch.shape[1]/2+1):

					if k1*z.shape[1]+pos+k2 < 0:
						print k1*z.shape[1]+pos+k2
						print k1,z.shape[1],pos,k2,i , j
					K[k1*z.shape[1]+pos+k2, pos] = patch[k1+patch.shape[0]/2,k2+patch.shape[1]/2]

	return K



def test1():
	X,y = IO.unpickle('/home/ouais/data/mnist/mnist_train.pkl')
	# X,y = normalize_dataset(X,y)

	X = X[:100]

	

	patch_size = (5,5)

	rng = np.random.RandomState(666)
	new_X = []
	for x in X:
		x = x.reshape((28,28))
		patches = extract_patches_2d(x, patch_size, max_patches=50,random_state=rng)
		new_X.extend(patches)
		

	X,y = normalize_dataset(new_X,y)

	kmeans =  MiniBatchKMeans(n_clusters=81, random_state=rng)
	kmeans.fit(X)


	figure(figsize=(4.2, 4))
	for i, patch in enumerate(kmeans.cluster_centers_):
	    subplot(9, 9, i + 1)
	    imshow(patch.reshape(patch_size), cmap=cm.gray)
    	xticks(())
    	yticks(())
	show()

	kernels = []
	for center in kmeans.cluster_centers_:
		create_kernel(X[0],np.array())





if __name__ == '__main__':
	test1()