
import numpy as np
from utils import IO
from utils.misc import *
from preprocessing import normalize_dataset

from sklearn.linear_model import LogisticRegression

def downsample(rng,y,n_samples):
	classes = np.unique(y)
	total_candidates = []

	for c in classes:
		candidates = np.where(y == c)[0]
		rng.shuffle(candidates)
		total_candidates.extend(candidates[:n_samples])

	return np.array(total_candidates)

def achlioptas(rng,n_in,n_out):
	X = np.array(rng.uniform(0,1,(n_in, n_out)))
	X[np.where(X >= 5/6.)]  = -1
	X[np.where(X >= 1/6.)]  =  0
	X[np.where(X >  0)]     =  1

	return X

def test():

	projection_size = 10000
	n_trials = 10
	n_samples = 1
	rng = np.random.RandomState(666)

	X_train, y_train = IO.unpickle(data_dir + 'mnist/mnist_train.pkl')
	X_train, y_train = normalize_dataset(X_train,y_train)
	X_test , y_test  = IO.unpickle(data_dir + 'mnist/mnist_test.pkl')
	X_test, y_test   = normalize_dataset(X_test,y_test)

	projection = rng.normal(0,1,(X_train.shape[1],projection_size))
	X_test_project = np.dot(X_test,projection)
		
	scores = []

	for i in xrange(n_trials):
		indices = downsample(rng,y_train,n_samples)		

		X_temp, y_temp = X_train[indices], y_train[indices]
		X_temp = np.dot(X_temp,projection)
		# projection     = achlioptas(rng,X_temp.shape[1],projection_size)
		
		model = LogisticRegression()

		model.fit(X_temp,y_temp)
		scores.append(model.score(X_test_project,y_test))

	print np.mean(scores)

if __name__ == '__main__':
	test()