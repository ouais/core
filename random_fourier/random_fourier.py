import numpy as np
from pylab import *


def fourier_features(d,N):	
	W = np.random.normal(0,1,(d,N));
	return W


def apply_random_features(W,x):
	
	N = W.shape[1]
	b_1 = np.random.uniform(0,2*np.pi,N)
	b_2 = np.random.uniform(0,2*np.pi,N)

	# f = np.sqrt(1./(2*N))*np.hstack( (np.cos(np.dot(x,W)+b_1), np.sin(np.dot(x,W)+b_2) ) )
	f = np.sqrt(2./N)*np.cos(np.dot(x,W)+b_1)
	# print f.shape
	return f


def gaussian(x,y):
	return np.exp(-(np.sum((x-y)**2,axis=1))/(2*x.shape[1]))


def compare():
	d = 30
	N = 2048
	n = 10000
	trials = 10000

	X = np.random.random((n,d))

	W = fourier_features(d,N)

	diffs = []

	id_1 = np.random.randint(0,X.shape[0],trials)
	id_2 = np.random.randint(0,X.shape[0],trials)

	r_1 = gaussian(X[id_1,:], X[id_2,:])

	r_2 = np.sum(apply_random_features(W,X[id_1,:])* apply_random_features(W,X[id_2,:]),axis=1)

	# print apply_random_features(W,X[id_1,:])**2
	# print r_2
	print np.mean(r_1)

	diffs = r_1-r_2
	# hist(r_1)
	# show()
	# hist(r_2)
	# show()

	print np.mean(diffs), np.std(diffs), np.mean(r_2),np.mean(r_1)


	hist(diffs,bins=1000)
	show()


if __name__ == '__main__':
	compare()
