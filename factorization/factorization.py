import time
import numpy as np

import theano
import theano.tensor as T

def numpy_test(L,n_estimations):
	matrices = []
	for i in xrange(1, len(L)):
		matrices.append(np.random.random((L[i-1],L[i])))

	start = time.time()
	for i in xrange(n_estimations):

		mat = matrices[0]
		for j in xrange(1,len(matrices)):
			mat = np.dot(mat,matrices[j])

	end = time.time()

	return end-start

def theano_test(L,n_estimations):
	matrices = []
	theano_matrices = []
	for i in xrange(1, len(L)):
		matrices.append(np.array(np.random.random((L[i-1],L[i])),dtype='float32'))
		theano_matrices.append(T.fmatrix())

	mat = theano_matrices[0]
	for j in xrange(1,len(theano_matrices)):
		mat = T.dot(mat,theano_matrices[j])

	f = theano.function(theano_matrices,mat)
	start = time.time()	
	for i in xrange(n_estimations):
		f(*matrices)
	end = time.time()

	return end-start



if __name__ =='__main__':
	n_estimations = 10


	a = numpy_test([1024,1000,1000],n_estimations)
	b = numpy_test([1024,100,1000,100,1000],n_estimations)

	print "a: {0}, b: {1}".format(np.sum(a),np.sum(b))



