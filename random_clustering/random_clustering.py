print(__doc__)

import time

import numpy as np
import pylab as pl

from sklearn import cluster, datasets
from sklearn.metrics import euclidean_distances
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler
from collections import Counter
import warnings

warnings.simplefilter("ignore")

class HyperplaneSampler:
    def __init__(self,numpy_rng,proportions,K=50):
        self.rng = numpy_rng
        self.proportions = proportions
        total = sum(self.proportions)
        self.proportions = [x/total for x in self.proportions]
        self.n_classes = len(proportions)
        self.proportions.sort()
        self.K = K

    def __str__(self):
     return "random hyperplanes"

    def sample_problem(self,X):
        h = self.rng.random((self.dimensions,1))
        return h

    def predict(self,X):
        y_pred = np.array(np.zeros(len(X)),dtype='int32')
        c = len(self.planes)

        for plane,threshold,less_than in self.planes:
            if less_than:
                bools = np.squeeze(np.dot(X,plane) < threshold)
            else:
                bools = np.squeeze(np.dot(X,plane) > threshold)

            y_pred[np.where(bools)] = np.maximum(y_pred[np.where(bools)],c)
            c -= 1

        return y_pred

    def fit(self,X):
        n_dimensions = len(X[0])         
        self.planes = []
    
        while len(self.proportions) > 1:
            minimum_error = np.inf
            n_samples = float(len(X))

            for k in xrange(self.K):
                h = self.rng.normal(0,1,(n_dimensions,1))  

                y_pred =  np.squeeze(np.dot(X,h))
                y_pred.sort()

                for p in self.proportions:
                    i = int(p*n_samples)

                    threshold = (y_pred[i]+y_pred[i+1])/2.
                                
                    p0,p1 = i/n_samples, (n_samples-i)/n_samples

                    err = np.array([min(abs(x-p0),abs(x-p1)) for x in self.proportions])

                    if err.min() < minimum_error:
                        minimum_error = err.min()
                        to_remove = err.argmin()
                        best_h = h
                        best_threshold = threshold
                        best_less_than = abs(self.proportions[to_remove] - p0) < abs(self.proportions[to_remove] - p1)                
            
            self.planes.append((best_h,best_threshold,best_less_than))
            del self.proportions[(to_remove)]         
            total = sum(self.proportions)
            self.proportions = [x/total for x in self.proportions]

            if best_less_than:
                bools = np.squeeze(np.dot(X,best_h) < best_threshold)
            else:
                bools = np.squeeze(np.dot(X,best_h) > best_threshold)

            X = np.delete(X,bools,0)



np.random.seed(0)
rng = np.random.RandomState(666)

n_samples = 1500
noisy_circles = datasets.make_circles(n_samples=n_samples, factor=.5,
                                      noise=.05)
noisy_moons = datasets.make_moons(n_samples=n_samples, noise=.05)
blobs = datasets.make_blobs(n_samples=n_samples, random_state=8)
no_structure = np.random.rand(n_samples, 2), None

colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
colors = np.hstack([colors] * 20)

pl.figure(figsize=(14, 9.5))
# pl.subplots_adjust(left=.001, right=.999, bottom=.001, top=.96, wspace=.05,
#                    hspace=.01)

plot_num = 1
for i_dataset, dataset in enumerate([noisy_circles, noisy_moons, blobs,
                                     no_structure]):
    X, y = dataset
    # normalize dataset for easier parameter selection
    X = StandardScaler().fit_transform(X)

    # estimate bandwidth for mean shift
    bandwidth = cluster.estimate_bandwidth(X, quantile=0.3)

    # connectivity matrix for structured Ward
    connectivity = kneighbors_graph(X, n_neighbors=10)
    # make connectivity symmetric
    connectivity = 0.5 * (connectivity + connectivity.T)

    # Compute distances
    #distances = np.exp(-euclidean_distances(X))
    distances = euclidean_distances(X)

    # create clustering estimators
    two_means = cluster.MiniBatchKMeans(n_clusters=2)
    dbscan = cluster.DBSCAN(eps=.2)
    hpsampler = HyperplaneSampler(rng,[.3,.3,.3])

    for algorithm in [two_means, dbscan, hpsampler]:
        # predict cluster memberships
        t0 = time.time()
        algorithm.fit(X)
        t1 = time.time()
        if hasattr(algorithm, 'labels_'):
            y_pred = algorithm.labels_.astype(np.int)
        else:
            y_pred = algorithm.predict(X)

        # plot
        pl.subplot(4, 3, plot_num)
        if i_dataset == 0:
            pl.title(str(algorithm).split('(')[0], size=18)
        pl.scatter(X[:, 0], X[:, 1], color=colors[y_pred].tolist(), s=10)

        if hasattr(algorithm, 'cluster_centers_'):
            centers = algorithm.cluster_centers_
            center_colors = colors[:len(centers)]
            pl.scatter(centers[:, 0], centers[:, 1], s=100, c=center_colors)
        pl.xlim(-2, 2)
        pl.ylim(-2, 2)
        pl.xticks(())
        pl.yticks(())
        pl.text(.99, .01, ('%.2fs' % (t1 - t0)).lstrip('0'),
                transform=pl.gca().transAxes, size=15,
                horizontalalignment='right')
        plot_num += 1

pl.show()





        