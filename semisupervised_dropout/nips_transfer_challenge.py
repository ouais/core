#########################################
"""
results: SDE: 44.48, DEV: 42.63
"""

#########################################

import numpy as np
import theano
import theano.tensor as T
import theano.tensor.shared_randomstreams

from FrankeNet import SS_DEV_NET
from load_data import load_udm, load_udm_ss, load_mnist

from utils_phil import *

from load_data import load_udm, load_udm_ss, load_mnist,_shared_dataset

import preprocessing

import NetTrainers as NT

from utils import IO
from utils.utils import batch

import cPickle

def train_ss_mlp(NET, mlp_params, sgd_params, datasets):
	"""Run semisupervised DEV-regularized test."""
 

	# Tell the net that it's semisupervised, which will force it to use only
	# unlabeled examples for computing the DEV regularizer.
	NET.is_semisupervised = 1

	# Run training on the given NET
	err = NT.train_ss_mlp(NET=NET, \
		mlp_params=mlp_params, \
		sgd_params=sgd_params, \
		datasets=datasets)
	return err


def standard_run(mlp_type,rng, n_trials=1):
	"""Run multiple semisupervised learning tests."""
	# Set some reasonable sgd parameters
	sgd_params = {}
	sgd_params['start_rate'] = 0.1
	sgd_params['decay_rate'] = 1
	sgd_params['wt_norm_bound'] = 3.5
	sgd_params['epochs'] = 1000
	sgd_params['batch_size'] = 128
	sgd_params['mlp_type'] = mlp_type
	# Set some reasonable mlp parameters
	mlp_params = {}
	mlp_params['layer_sizes'] = [5*5*96, 200,200, 11]
	mlp_params['dev_clones'] = 1
	mlp_params['dev_types'] = [1,1, 2]
	mlp_params['dev_lams']  = [.1,.1, 1]
	mlp_params['dev_mix_rate'] = 0.
	mlp_params['lam_l2a'] = 1e-2
	mlp_params['use_bias'] = 1

	# Goofy symbolic sacrament to Theano    
	x_in = T.matrix('x_in')

	d = cPickle.load(open('data/transfer_challenge_data.pkl',"r"))
	X_train, y_train = d['train']
	X_test, y_test = d['test']
	X_test, y_test = X_test[:2542,:], y_test[:2542] # this is because the last 8 samples were resampled
	X_unlabel =  d['unlabelled']

	print X_train.shape, X_test.shape

	train_tuple = _shared_dataset((X_train,y_train+1))
	valid_tuple = _shared_dataset((X_test,y_test+1))
	test_tuple  = _shared_dataset((X_test, y_test+1))

	y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

	unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

	datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)

	errors = []

	for i in xrange(n_trials):
		
		NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)

		sgd_params['decay_rate'] = 1
		sgd_params['result_tag'] = 'pretrained_ss_final_{}_{}'.format('_'.join([str(s) for s in mlp_params['layer_sizes']]),sgd_params['mlp_type'])
		sgd_params['start_rate'] = 0.001
		sgd_params['epochs'] = 50
		train_ss_mlp(NET, mlp_params, sgd_params, datasets)
		sgd_params['decay_rate'] = .7
		sgd_params['epochs'] = 25
		err = train_ss_mlp(NET, mlp_params, sgd_params, datasets)

		# IO.pickle(NET,'models/'+sgd_params['result_tag'])
		errors.append(err)

	return np.mean(errors)


if __name__ == '__main__':
	rng = np.random.RandomState(666)
	sde_result = standard_run('sde',rng, n_trials=5)

	rng = np.random.RandomState(666)
	dev_result = standard_run('dev',rng, n_trials=5)

	print "SDE: {:.2f}, DEV: {:.2f}".format(sde_result, dev_result)
