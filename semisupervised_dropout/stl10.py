#########################################
# Testing scripts for MNIST experiments #
#########################################

import numpy as np
import theano
import theano.tensor as T
import theano.tensor.shared_randomstreams

from FrankeNet import SS_DEV_NET
from load_data import load_udm, load_udm_ss, load_mnist

from utils_phil import *

from deepnn.dataset import shared_dataset, shared_dataset_X
from deepnn.NN import NeuralNetwork
from deepnn.algorithm import Backpropagation
from deepnn.costs import *

from load_data import load_udm, load_udm_ss, load_mnist,_shared_dataset

import preprocessing

import NetTrainers as NT

from utils import IO
from utils.utils import batch

import cPickle

@batch(128)
def nn_map(nn,X):
    return nn.output[-3](X)

def complete_to_multiple(X,y,multiple_of = 128):

    rem = multiple_of - len(X) % multiple_of

    indices = np.random.choice(len(X), rem)

    X_new, y_new = X[indices], y[indices]

    X = np.vstack((X,X_new))
    y = np.hstack((y,y_new))

    return X,y


def load_stl10():

    X_train, y_train = IO.unpickle('/home/oalsha1/data/stl10/stl10_train.pkl')
    X_train = preprocessing.flatten(X_train)

    folds = IO.unpickle('/home/oalsha1/data/stl10/stl10_folds.pkl')
    X_train, y_train = X_train[folds[0]], y_train[folds[0]]

    X_train, y_train = complete_to_multiple(X_train,y_train,128)

    Xy_train = X_train, y_train

    X_test, y_test = IO.unpickle('/home/oalsha1/data/stl10/stl10_test.pkl')
    X_test = preprocessing.flatten(X_test)
    X_test, y_test = complete_to_multiple(X_test,y_test,128)

    Xy_test = X_test, y_test

    X_unlabel = np.load('/home/oalsha1/data/stl10/stl10_unlabelled.pkl')
    X_unlabel = X_unlabel[:100000-32] # to complete to a multiple of 128
    X_unlabel = preprocessing.flatten(X_unlabel)

    return Xy_train, Xy_test ,X_unlabel


def preprocess_stl10():

    Xy_train, Xy_test, X_unlabel = load_stl10()    
    X_train, y_train = Xy_train 
    X_test, y_test = Xy_test 

    net = IO.unpickle('models/cnn_stl10_647.pkl')
    net.set_test_mode()

    X_train = preprocessing.flatten(nn_map(net,X_train))
    X_test = preprocessing.flatten(nn_map(net,X_test))
    

    d = {}
    d['train'] = X_train, y_train
    d['test'] = X_test, y_test    

    chunk_size = 10000 - 16

    n_chunks = X_unlabel.shape[0]/chunk_size

    for i in xrange(n_chunks):
        data = X_unlabel[i*chunk_size:(i+1)*chunk_size]
        d['unlabelled_{}'.format(i)] = preprocessing.flatten(nn_map(net,data))

    with open('data/stl10_preprocessed_chunked.pkl','w') as f:
        cPickle.dump(d,f)


def train_ss_mlp(NET, mlp_params, sgd_params, datasets):
    """Run semisupervised DEV-regularized test."""
 

    # Tell the net that it's semisupervised, which will force it to use only
    # unlabeled examples for computing the DEV regularizer.
    NET.is_semisupervised = 1

    # Run training on the given NET
    NT.train_ss_mlp(NET=NET, \
        mlp_params=mlp_params, \
        sgd_params=sgd_params, \
        datasets=datasets)
    return 1


def train_dae(NET, dae_layer, mlp_params, sgd_params,datasets):
    """Run DAE training test."""

    # Run denoising autoencoder training on the given layer of NET
    NT.train_dae(NET=NET, \
        dae_layer=dae_layer, \
        mlp_params=mlp_params, \
        sgd_params=sgd_params, \
        datasets=datasets)
    return 1

def batch_test_ss_mlp_gentle(test_count=10, su_count=1000):
    """Run multiple semisupervised learning tests."""
    # Set some reasonable sgd parameters
    sgd_params = {}
    sgd_params['start_rate'] = 0.1
    sgd_params['decay_rate'] = 0.998
    sgd_params['wt_norm_bound'] = 4
    sgd_params['epochs'] = 1000
    sgd_params['batch_size'] = 128
    sgd_params['mlp_type'] = 'dev'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [9*9*96, 500, 11]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1,  5]
    mlp_params['dev_lams'] = [0.1, 0.2]
    mlp_params['dev_mix_rate'] = 0.
    mlp_params['lam_l2a'] = 1e-2
    mlp_params['use_bias'] = 1

    x_in = T.matrix('x_in')

    rng = np.random.RandomState(666)

    d = cPickle.load(open('data/stl10_preprocessed_wo_unlabel.pkl',"r"))
    X_train, y_train = d['train']
    X_test, y_test = d['test']
    X_unlabel,y_meh =  d['unlabel']

    train_tuple = _shared_dataset((X_train,y_train+1))
    valid_tuple = _shared_dataset((X_test,y_test+1))
    test_tuple  = _shared_dataset((X_test, y_test+1))

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

    datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)

    # Run tests with different sorts of regularization
    for test_num in range(test_count):
        rng_seed = test_num
        NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)
        sgd_params['result_tag'] = "ss_dev_500x500_s{0:d}_{1:d}".format(su_count,test_num)

        train_ss_mlp(NET, mlp_params, sgd_params, datasets)
        
    return 1

def monitor(e,model,X,y):
    model.set_test_mode()
    if e % 5 == 0:
        print '{} {:.2f}'.format(e,100*model.score(X,y))
    model.set_train_mode()

def test_ouais():
    d = cPickle.load(open('data/stl10_preprocessed.pkl',"r"))
    X_train, y_train = d['train']
    X_test, y_test = d['test']

    nn = NeuralNetwork([1000,500],activations=[lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)],drops=[.5,0.5,0.5] )
    nn.add_layer(10, activation=lambda x: x)

    cost = lambda x,y:LargeMarginLoss_2(x,y,10)

    bprop = Backpropagation(nn,epochs=3000,max_row_norms=[.2,.2,.2],learning_rate = .01,momentum=.9, batch_size=128,cost=cost)
    bprop.add_epoch_monitor(lambda e: monitor(e,nn,X_test,y_test))

    bprop.fit(X_train,y_train)


def centipede():
    """Run multiple semisupervised learning tests."""
    # Set some reasonable sgd parameters
    sgd_params = {}
    sgd_params['start_rate'] = 0.01
    sgd_params['decay_rate'] = .998
    sgd_params['wt_norm_bound'] = .2
    sgd_params['epochs'] = 1000
    sgd_params['batch_size'] = 128
    sgd_params['mlp_type'] = 'dev'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [96*9*9, 500, 500, 11]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1,1,  2]
    mlp_params['dev_lams'] = [0.1,.1, .2]
    mlp_params['dev_mix_rate'] = 0.0
    mlp_params['lam_l2a'] = 1e-2
    mlp_params['use_bias'] = 1

    x_in = T.matrix('x_in')

    Xy_train, Xy_test, X_unlabel = load_stl10()

    X_train, y_train = Xy_train
    X_test, y_test = Xy_test

    rng = np.random.RandomState(666)

    train_tuple = _shared_dataset((X_train,y_train+1))
    valid_tuple = _shared_dataset((X_test,y_test+1))
    test_tuple  = _shared_dataset((X_test, y_test+1))

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

    datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)

    cnn = IO.unpickle('models/cnn_stl10_fold_0_dropout.pkl')
    cnn.set_train_mode()

    extended_params = []
    for j in xrange(len(cnn.layers)-2):
        extended_params.extend(cnn.layers[j].params)

    output = cnn.layers[-3].output
    output = output.flatten(2)

    mlp_params['extended_params'] = extended_params
    mlp_params['preprocessor'] = output

    x_in = cnn.x

    # Run tests with different sorts of regularization
    NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)
    sgd_params['result_tag'] = '{}_{}'.format("centipede",sgd_params['mlp_type'])
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)
    return 1



def batch_test_ss_mlp_pt(test_count=1, su_count=1000):
    """Setup basic test for semisupervised DEV-regularized MLP."""

    # Set some reasonable sgd parameters
    sgd_params = {}
    sgd_params['start_rate'] = 0.01
    sgd_params['decay_rate'] = 0.998
    sgd_params['wt_norm_bound'] = 3.5
    sgd_params['epochs'] = 1000
    sgd_params['batch_size'] = 100
    sgd_params['mlp_type'] = 'dev'
    sgd_params['result_tag'] = 'xxx'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [1000,500,500,11]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1, 1, 5]
    mlp_params['dev_lams'] = [0.1, 0.1, 0.1]
    mlp_params['dev_mix_rate'] = 0.0
    mlp_params['lam_l2a'] = 1e-3
    mlp_params['use_bias'] = 1


    d = cPickle.load(open('data/stl10_preprocessed.pkl',"r"))
    X_train, y_train = d['train']
    X_test, y_test = d['test']
    X_unlabel =  d['unlabelled']

    train_tuple = _shared_dataset((X_train,y_train+1))
    valid_tuple = _shared_dataset((X_test,y_test+1))
    test_tuple  = _shared_dataset((X_test, y_test+1))

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

    datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)

    for test_num in range(test_count):
        rng_seed = test_num
        sgd_params['result_tag'] = "test_{0:d}".format(test_num)

        # Initialize a random number generator for this test
        rng = np.random.RandomState(rng_seed)

        # Construct the SS_DEV_NET object that we will be training
        x_in = T.matrix('x_in')
        NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)

        # Initialize biases in each net layer (except final layer) to small
        # positive constants (rather than their default zero initialization)
        for (num, layer) in enumerate(NET.mlp_layers):
            b_init = layer.b.get_value(borrow=False)
            b_const = np.zeros(b_init.shape, dtype=theano.config.floatX)
            if (num < (len(NET.mlp_layers)-1)):
                b_const = b_const + 0.0
            layer.b.set_value(b_const)

        ##########################################
        # First, pretrain each layer in the mlp. #
        ##########################################
        sgd_params['mlp_type'] = 'raw'
        sgd_params['batch_size'] = 25
        sgd_params['start_rate'] = 0.01
        sgd_params['epochs'] = 40
        for i in range(len(NET.mlp_layers)-1):
            print("==================================================")
            print("Pretraining hidden layer {0:d}".format(i+1))
            print("==================================================")
            train_dae(NET, i, mlp_params, sgd_params,datasets)

        # Run semisupervised training on the given MLP
        sgd_params['top_only'] = True
        sgd_params['mlp_type'] = 'dev'
        sgd_params['epochs'] = 10
        NET.set_dev_lams([0.005, 0.005, 0.005])
        rng = np.random.RandomState(rng_seed)
        train_ss_mlp(NET, mlp_params, sgd_params, datasets)
        sgd_params['top_only'] = False
        sgd_params['mlp_type'] = 'dev'
        sgd_params['epochs'] = 10
        NET.set_dev_lams([0.02, 0.02, 0.02])
        rng = np.random.RandomState(rng_seed)
        train_ss_mlp(NET, mlp_params, sgd_params, datasets)
        sgd_params['mlp_type'] = 'dev'
        sgd_params['epochs'] = 10
        NET.set_dev_lams([0.05, 0.05, 0.05])
        rng = np.random.RandomState(rng_seed)
        train_ss_mlp(NET, mlp_params, sgd_params, datasets)
        sgd_params['mlp_type'] = 'dev'
        sgd_params['epochs'] = 10
        NET.set_dev_lams([0.1, 0.1, 0.1])
        rng = np.random.RandomState(rng_seed)
        train_ss_mlp(NET, mlp_params, sgd_params, datasets)
        sgd_params['result_tag'] = "pt_dev_500x500_s{0:d}_{1:d}".format(su_count, test_num)
        sgd_params['mlp_type'] = 'dev'
        sgd_params['epochs'] = 500
        NET.set_dev_lams([0.1, 0.1, 0.2])
        rng = np.random.RandomState(rng_seed)
        train_ss_mlp(NET, mlp_params, sgd_params, datasets)

    return 1

if __name__ == '__main__':
    
    # preprocess_stl10()
    # batch_test_ss_mlp_gentle(test_count=1, su_count=40000)
    centipede()
    # batch_test_ss_mlp_pt()
    # test_ouais()
