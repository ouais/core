#########################################
# Testing scripts for MNIST experiments #
#########################################

import numpy as np
import theano
import theano.tensor as T
import theano.tensor.shared_randomstreams

from FrankeNet import SS_DEV_NET
from load_data import load_udm, load_udm_ss, load_mnist

from utils_phil import *

from deepnn.dataset import shared_dataset, shared_dataset_X
from deepnn.NN import NeuralNetwork
from deepnn.costs import LargeMarginLoss_2

from deepnn.algorithm import Backpropagation

from load_data import load_udm, load_udm_ss, load_mnist,_shared_dataset

import preprocessing

import NetTrainers as NT

from utils import IO
from utils.utils import batch

import cPickle

@batch(128)
def nn_map(nn,X):
    return nn.output[-3](X)

def load_cifar(rng,mlp_params):
    
    train_tuple = shared_dataset(X_train,y_train+1)
    # valid_tuple = shared_dataset(X_train[valid_indices],y_train[valid_indices]+1)
    valid_tuple = shared_dataset(X_test,y_test+1)
    test_tuple  = shared_dataset(X_test, y_test+1)

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = shared_dataset(X_unlabel,y_unlabel)

    return (train_tuple,unlabelled_data, valid_tuple, test_tuple)

def train_ss_mlp(NET, mlp_params, sgd_params, datasets):
    """Run semisupervised DEV-regularized test."""
 

    # Tell the net that it's semisupervised, which will force it to use only
    # unlabeled examples for computing the DEV regularizer.
    NET.is_semisupervised = 1

    # Run training on the given NET
    NT.train_ss_mlp(NET=NET, \
        mlp_params=mlp_params, \
        sgd_params=sgd_params, \
        datasets=datasets)
    return 1

def train_dae(NET, dae_layer, mlp_params, sgd_params,datasets):
    """Run DAE training test."""

    # Run denoising autoencoder training on the given layer of NET
    NT.train_dae(NET=NET, \
        dae_layer=dae_layer, \
        mlp_params=mlp_params, \
        sgd_params=sgd_params, \
        datasets=datasets)
    return 1

def monitor(e,model,X,y):
    model.set_test_mode()
    if e % 100 == 0:
        print '{} {:.2f}'.format(e,100*model.score(X,y))
    model.set_train_mode()

def standard_run():
    """Run multiple semisupervised learning tests."""
    # Set some reasonable sgd parameters
    sgd_params = {}
    sgd_params['start_rate'] = 0.1
    sgd_params['decay_rate'] = 1
    sgd_params['wt_norm_bound'] = 3.5
    sgd_params['epochs'] = 1000
    sgd_params['batch_size'] = 128
    sgd_params['mlp_type'] = 'dev'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [5*5*96, 200,200, 11]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1,  5]
    mlp_params['dev_lams'] = [0.1, 0.2]
    mlp_params['dev_mix_rate'] = 0.
    mlp_params['lam_l2a'] = 1e-2
    mlp_params['use_bias'] = 1

    # Goofy symbolic sacrament to Theano    
    x_in = T.matrix('x_in')

    rng = np.random.RandomState(666)

    d = cPickle.load(open('transfer_challenge_data.pkl',"r"))
    X_train, y_train = d['train']
    X_test, y_test = d['test']
    X_unlabel =  d['unlabelled']

    print X_train.shape, X_test.shape

    train_tuple = _shared_dataset((X_train,y_train+1))
    valid_tuple = _shared_dataset((X_test,y_test+1))
    test_tuple  = _shared_dataset((X_test, y_test+1))

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

    datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)

    # nn = NeuralNetwork([2400,100,100],activations=[lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)],drops=[.5,0.5,0.5] )
    # nn.add_layer(10, activation=lambda x: x)

    # cost = lambda x,y:LargeMarginLoss_2(x,y,10)
    # # cost = MCL2Hinge

    # bprop = Backpropagation(nn,epochs=3000,max_row_norms=[.2,.2,.2],learning_rate = .01,momentum=.95, batch_size=128,cost=cost)
    # bprop.add_epoch_monitor(lambda e: monitor(e,nn,X_test,y_test))

    # bprop.fit(X_train,y_train)
            
    mlp_params['dev_types'] = [1,1, 2]
    mlp_params['dev_lams']  = [.1,.1, 1]
    NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)
    sgd_params['mlp_type'] = 'sde'
    sgd_params['result_tag'] = 'pretrained_ss_final_{}_{}'.format('_'.join([str(s) for s in mlp_params['layer_sizes']]),sgd_params['mlp_type'])
    sgd_params['start_rate'] = 0.001

    sgd_params['epochs'] = 50
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)
    sgd_params['decay_rate'] = .7
    sgd_params['epochs'] = 25
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)

    IO.pickle(NET,'models/'+sgd_params['result_tag'])

    return 1

def batch_test_ss_mlp_gentle(test_count=10, su_count=1000,train_convnet=False):
    """Run multiple semisupervised learning tests."""
    # Set some reasonable sgd parameters
    sgd_params = {}
    sgd_params['start_rate'] = 0.1
    sgd_params['decay_rate'] = 0.998
    sgd_params['wt_norm_bound'] = 3.5
    sgd_params['epochs'] = 1000
    sgd_params['batch_size'] = 128
    sgd_params['mlp_type'] = 'dev'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [5*5*96, 500,500, 101]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1,  5]
    mlp_params['dev_lams'] = [0.1, 0.2]
    mlp_params['dev_mix_rate'] = 0.
    mlp_params['lam_l2a'] = 1e-2
    mlp_params['use_bias'] = 1

    d = cPickle.load(open('transfer_challenge_data.pkl',"r"))
    X_train, y_train = d['train']
    X_test, y_test = d['test']
    X_unlabel =  d['unlabelled']

    print X_train.shape, X_test.shape

    train_tuple = _shared_dataset((X_train,y_train+1))
    valid_tuple = _shared_dataset((X_test,y_test+1))
    test_tuple  = _shared_dataset((X_test, y_test+1))

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

    datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)

    mlp_params['dev_types'] = [1,1, 5]
    NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)
        # Run test with DEV regularization on unsupervised examples
    sgd_params['result_tag'] = "ss_dev_500x500_s{0:d}_{1:d}".format(su_count,test_num)
    sgd_params['mlp_type'] = 'dev'
    sgd_params['start_rate'] = 0.01

    sgd_params['epochs'] = 5
    NET.set_dev_lams([0.002,0.002, 0.0])

    train_ss_mlp(NET, mlp_params, sgd_params, rng, datasets,su_count)
    
    sgd_params['epochs'] = 10

    NET.set_dev_lams([0.002,0.002, 0.000])
    train_ss_mlp(NET, mlp_params, sgd_params, rng, datasets,su_count)
    sgd_params['epochs'] = 10
    NET.set_dev_lams([0.002,0.002, 0.00])
    train_ss_mlp(NET, mlp_params, sgd_params, rng, datasets,su_count)
    sgd_params['epochs'] = 100
    NET.set_dev_lams([0.002,0.002, 0.00])
    train_ss_mlp(NET, mlp_params, sgd_params, rng, datasets,su_count)
    return 1


def batch_test_ss_mlp_pt(test_count=1, su_count=1000):
    """Setup basic test for semisupervised DEV-regularized MLP."""

    # Set some reasonable sgd parameters
    sgd_params = {}
    sgd_params['start_rate'] = 0.01
    sgd_params['decay_rate'] = 0.998
    sgd_params['wt_norm_bound'] = 3.5
    sgd_params['epochs'] = 1000
    sgd_params['batch_size'] = 100
    sgd_params['mlp_type'] = 'dev'
    sgd_params['result_tag'] = 'xxx'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [5*5*96,500,500,11]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1, 1, 5]
    mlp_params['dev_lams'] = [0.1, 0.1, 0.1]
    mlp_params['dev_mix_rate'] = 0.0
    mlp_params['lam_l2a'] = 1e-3
    mlp_params['use_bias'] = 1


    d = cPickle.load(open('transfer_challenge_data.pkl',"r"))
    X_train, y_train = d['train']
    X_test, y_test = d['test']
    X_unlabel =  d['unlabelled']

    print X_train.shape, X_test.shape

    train_tuple = _shared_dataset((X_train,y_train+1))
    valid_tuple = _shared_dataset((X_test,y_test+1))
    test_tuple  = _shared_dataset((X_test, y_test+1))

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

    datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)

    rng_seed = 666

    rng = np.random.RandomState(rng_seed)

        # Construct the SS_DEV_NET object that we will be training
    x_in = T.matrix('x_in')
    NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)

        # Initialize biases in each net layer (except final layer) to small
        # positive constants (rather than their default zero initialization)
    # for (num, layer) in enumerate(NET.mlp_layers):
    #     b_init = layer.b.get_value(borrow=False)
    #     b_const = np.zeros(b_init.shape, dtype=theano.config.floatX)
    #     if (num < (len(NET.mlp_layers)-1)):
    #         b_const = b_const + 0.05
    #     layer.b.set_value(b_const)

    #     ##########################################
    #     # First, pretrain each layer in the mlp. #
    #     ##########################################
    #     sgd_params['mlp_type'] = 'raw'
    #     sgd_params['batch_size'] = 25
    #     sgd_params['start_rate'] = 0.01
    #     sgd_params['epochs'] = 40
    #     for i in range(len(NET.mlp_layers)-1):
    #             print("==================================================")
    #             print("Pretraining hidden layer {0:d}".format(i+1))
    #             print("==================================================")
    #             train_dae(NET, i, mlp_params, sgd_params,datasets)

    # IO.pickle(NET,'models/pretrained_tc_{}'.format('_'.join([str(s) for s in mlp_params['layer_sizes']])))
    NET = IO.unpickle('models/pretrained_tc_{}'.format('_'.join([str(s) for s in mlp_params['layer_sizes']])))

    mlp_type = 'dev'

        # Run semisupervised training on the given MLP
    sgd_params['top_only'] = True
    sgd_params['mlp_type'] = mlp_type
    sgd_params['epochs'] = 10
    NET.set_dev_lams([0.005, 0.005, 0.005])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)

    sgd_params['top_only'] = False
    sgd_params['mlp_type'] = mlp_type
    sgd_params['epochs'] = 10
    NET.set_dev_lams([0.02, 0.02, 0.02])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)
    sgd_params['mlp_type'] = mlp_type
    sgd_params['epochs'] = 10
    NET.set_dev_lams([0.05, 0.05, 0.05])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)
    sgd_params['mlp_type'] = mlp_type
    sgd_params['epochs'] = 20
    NET.set_dev_lams([0.1, 0.1, 0.1])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)
    sgd_params['result_tag'] = "ss_{}_{}".format( sgd_params['mlp_type'],  '_'.join([str(s) for s in mlp_params['layer_sizes']]) )
    sgd_params['mlp_type'] = mlp_type
    sgd_params['epochs'] = 50
    NET.set_dev_lams([0.1, 0.1, 0.2])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)

    IO.pickle(NET,'models/pretrained_ss_final_tc_{}_{}'.format('_'.join([str(s) for s in mlp_params['layer_sizes']]),sgd_params['mlp_type']))


if __name__ == '__main__':

    # batch_test_ss_mlp_gentle(test_count=1, su_count=40000)
    # optimize_hypers()
    standard_run()
    # batch_test_ss_mlp_pt()
    # preprocess_transfer()
