import numpy as np

from utils import IO
from utils.utils import batch

import preprocessing

@batch(128)
def nn_map(nn,X):
    return nn.output[-3](X)

def complete_to_multiple(rng,X,y,multiple_of = 128):

    rem = multiple_of - len(X) % multiple_of

    indices = rng.choice(len(X), rem)

    X_new, y_new = X[indices], y[indices]

    X = np.vstack((X,X_new))
    y = np.hstack((y,y_new))

    return X,y


def preprocess_transfer_challenge(nn_path):

    rng = np.random.RandomState(666)

    X_train, y_train = IO.unpickle('/home/oalsha1/data/transfer_challenge/transfer_train.pkl')
    X_train = preprocessing.flatten(X_train)
    X_train, y_train = complete_to_multiple(rng,X_train,y_train,128)

    # X_test, y_test = IO.unpickle('/home/oalsha1/data/cifar100/cifar100_test_c.pkl')
    X_test, y_test = IO.unpickle('/home/oalsha1/data/transfer_challenge/transfer_test.pkl')
    X_test = preprocessing.flatten(X_test)
    X_test, y_test = complete_to_multiple(rng,X_test,y_test,128)

    X_unlabel = np.load('/home/oalsha1/data/transfer_challenge/transfer_unlabelled.dat')
    X_unlabel = X_unlabel[:50000-80] # to complete to a multiple of 128
    X_unlabel = preprocessing.flatten(X_unlabel)

    net = IO.unpickle(nn_path)
    X_train = preprocessing.flatten(nn_map(net,X_train))
    X_test = preprocessing.flatten(nn_map(net,X_test))
    X_unlabel = preprocessing.flatten(nn_map(net,X_unlabel))

    X_train = np.array(X_train,dtype='float32')
    X_test = np.array(X_test,dtype='float32')

    print X_train.shape, X_test.shape, X_unlabel.shape

    d = {}
    d['train'] = X_train, y_train
    d['test'] = X_test, y_test
    d['unlabelled'] = X_unlabel

    IO.pickle(d,'transfer_challenge_data.pkl')

if __name__ == '__main__':
    nn_path = 'models/cnn_cifar100_96_3_1k_1_dropout.pkl'
    preprocess_transfer_challenge(nn_path)
