#########################################
# Testing scripts for MNIST experiments #
#########################################

import numpy as np
import theano
import theano.tensor as T
import theano.tensor.shared_randomstreams

from FrankeNet import SS_DEV_NET
from load_data import load_udm, load_udm_ss, load_mnist,_shared_dataset

from utils_phil import *

import NetTrainers as NT
from deepnn.NN import NeuralNetwork
from deepnn.algorithm import Backpropagation
from deepnn.costs import *

import cPickle


def train_ss_mlp(NET, mlp_params, sgd_params, rng, datasets, su_count=1000):
    """Run semisupervised DEV-regularized test."""
 

    # Tell the net that it's semisupervised, which will force it to use only
    # unlabeled examples for computing the DEV regularizer.
    NET.is_semisupervised = 1

    # Run training on the given NET
    NT.train_ss_mlp(NET=NET, \
        mlp_params=mlp_params, \
        sgd_params=sgd_params, \
        datasets=datasets)
    return 1

def monitor(e,model,X,y):
    model.set_test_mode()
    if e % 100 == 0:
        print '{} {:.2f}'.format(e,100*model.score(X,y))
    model.set_train_mode()

def test_transfer_ouais():
    d = cPickle.load(open('transfer_challenge_data.pkl',"r"))
    X_train, y_train = d['train']
    X_test, y_test = d['test']

    nn = NeuralNetwork([1600,100,100],activations=[lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)],drops=[.5,0.5,0.5] )
    nn.add_layer(10, activation=lambda x: x)

    cost = lambda x,y:LargeMarginLoss_2(x,y,10)
    # cost = MCL2Hinge

    bprop = Backpropagation(nn,epochs=3000,max_row_norms=[.2,.2,.2],learning_rate = .01,momentum=.95, batch_size=128,cost=cost)
    bprop.add_epoch_monitor(lambda e: monitor(e,nn,X_test,y_test))

    bprop.fit(X_train,y_train)




def batch_test_ss_mlp_gentle(test_count=10, su_count=1000,train_convnet=False):
    """Run multiple semisupervised learning tests."""
    # Set some reasonable sgd parameters
    sgd_params = {}
    sgd_params['start_rate'] = 0.1
    sgd_params['decay_rate'] = 0.998
    sgd_params['wt_norm_bound'] = 3.5
    sgd_params['epochs'] = 1000
    sgd_params['batch_size'] = 128
    sgd_params['mlp_type'] = 'dev'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [5*5*64, 500,500, 11]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1,  5]
    mlp_params['dev_lams'] = [0.1, 0.2]
    mlp_params['dev_mix_rate'] = 0.
    mlp_params['lam_l2a'] = 1e-2
    mlp_params['use_bias'] = 1

    # Goofy symbolic sacrament to Theano    
    x_in = T.matrix('x_in')

    
    # mlp_params['preprocess_w_net'] = not train_convnet

    # if train_convnet:        

    #     extended_params = []
    #     for j in xrange(len(cnn.layers)-2):
    #         extended_params.extend(cnn.layers[j].params)

    #     output = cnn.layers[2].output
    #     output = output.flatten(2)

    #     mlp_params['extended_params'] = extended_params
    #     mlp_params['preprocessor'] = output

    #     x_in = cnn.x

    rng = np.random.RandomState(666)

    d = cPickle.load(open('transfer_challenge_data.pkl',"r"))
    X_train, y_train = d['train']
    X_test, y_test = d['test']
    X_unlabel =  d['unlabelled']

    train_tuple = _shared_dataset((X_train,y_train+1))
    valid_tuple = _shared_dataset((X_test,y_test+1))
    test_tuple  = _shared_dataset((X_test, y_test+1))

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

    datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)

    # Run tests with different sorts of regularization
    for test_num in range(test_count):
        rng_seed = test_num
        # Initialize a random number generator for this test
        
        # Construct the SS_DEV_NET object that we will be training
        mlp_params['dev_types'] = [1,1, 5]
        NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)
        # Run test with DEV regularization on unsupervised examples
        sgd_params['result_tag'] = "ss_dev_500x500_s{0:d}_{1:d}".format(su_count,test_num)
        sgd_params['mlp_type'] = 'sde'
        sgd_params['start_rate'] = 0.001
        # Train with weak DEV regularization
        sgd_params['epochs'] = 100
        NET.set_dev_lams([0.000, 0.000, 0.0])
        # rng = np.random.RandomState(rng_seed)
        train_ss_mlp(NET, mlp_params, sgd_params, rng, datasets,su_count)
        # Train with more DEV regularization
        # sgd_params['epochs'] = 10
        # NET.set_dev_lams([0.002, 0.002, 0.002])
        # # rng = np.random.RandomState(rng_seed)
        # train_ss_mlp(NET, mlp_params, sgd_params, rng, datasets,su_count)
        # # Train with more DEV regularization
        # sgd_params['epochs'] = 100
        # NET.set_dev_lams([0.005, 0.005, 0.008])
        # # rng = np.random.RandomState(rng_seed)
        # train_ss_mlp(NET, mlp_params, sgd_params, rng, datasets,su_count)
        # Train with most DEV regularization
        # sgd_params['epochs'] = 100
        # NET.set_dev_lams([0.1, 0.1, 0.2])
        # # rng = np.random.RandomState(rng_seed)
        # train_ss_mlp(NET, mlp_params, sgd_params, rng, datasets,su_count)
    return 1


def batch_test_ss_mlp(test_count=10, su_count=1000):
    """Run multiple semisupervised learning tests."""
    # Set some reasonable sgd parameters
    sgd_params = {}
    sgd_params['start_rate'] = 0.01
    sgd_params['decay_rate'] = 0.998
    sgd_params['wt_norm_bound'] = 3.5
    sgd_params['epochs'] = 1000
    sgd_params['batch_size'] = 128
    sgd_params['mlp_type'] = 'dev'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [5*5*64, 1000, 101]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1, 2]
    mlp_params['dev_lams'] = [0.1, 2.0]
    mlp_params['dev_mix_rate'] = 0.
    mlp_params['lam_l2a'] = 1e-3
    mlp_params['use_bias'] = 1


    # Run tests with different sorts of regularization
    # mlp_params['pooling'] = [5,5,5]
    # mlp_params['strides'] = [2,2,2]
    # mlp_params['kernels'] = [64,64,64]
    # mlp_params['is_conv'] = [True,True,True,False,False]
    # mlp_params['kernel_size'] = 5
    # mlp_params['batch_size'] = 128
    # mlp_params['input_size'] = 32

    

    print X_train.shape, X_test.shape, X_unlabel.shape

    extended_params = []
    for j in xrange(len(cnn.layers)-2):
        extended_params.extend(cnn.layers[j].params)

    output = cnn.layers[2].output
    output = output.flatten(2)

    mlp_params['extended_params'] = extended_params
    mlp_params['preprocessor'] = output

    x_in = cnn.x


    for test_num in range(test_count):
        """
        # Run test with no droppish regularization
        sgd_params['result_tag'] = "ss_raw_500x500_{0:d}_s{1:d}".format(test_num,su_count)
        sgd_params['mlp_type'] = 'raw'
        mlp_params['dev_lams'] = [0., 0., 0.]
        # Initialize a random number generator for this test
        rng = np.random.RandomState(test_num)
        # Construct the SS_DEV_NET object that we will be training
        NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)
        rng = np.random.RandomState(test_num)
        train_ss_mlp(NET, mlp_params, sgd_params, rng, su_count)
        # Run test with standard dropout on supervised examples
        sgd_params['result_tag'] = "ss_sde_500x500_{0:d}".format(test_num)
        sgd_params['mlp_type'] = 'sde'
        # Initialize a random number generator for this test
        rng = np.random.RandomState(test_num)
        # Construct the SS_DEV_NET object that we will be training
        NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)
        rng = np.random.RandomState(test_num)
        train_ss_mlp(NET, mlp_params, sgd_params, rng, su_count)
        """
        # Run test with DEV regularization on unsupervised examples
        sgd_params['result_tag'] = "ss_dev_500x500_{0:d}".format(test_num)
        sgd_params['mlp_type'] = 'dev'
        #mlp_params['dev_types'] = [1, 1, 2]
        #mlp_params['dev_lams'] = [0.1, 0.1, 2.0]
        mlp_params['dev_types'] = [1, 6]
        mlp_params['dev_lams'] = [0.1, 1.0]
        # Initialize a random number generator for this test
        rng = np.random.RandomState(test_num)
        # Construct the SS_DEV_NET object that we will be training
        NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)
        rng = np.random.RandomState(test_num)
        train_ss_mlp(NET, mlp_params, sgd_params, rng, su_count)
    return 1



if __name__ == '__main__':
    test_transfer_ouais()
    # batch_test_ss_mlp_gentle(test_count=1, su_count=40000)
