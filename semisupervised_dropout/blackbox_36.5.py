#########################################
# Testing scripts for MNIST experiments #
#########################################

import numpy as np
import theano
import theano.tensor as T
import theano.tensor.shared_randomstreams

from utils.misc import data_dir

from FrankeNet import SS_DEV_NET
from load_data import load_udm, load_udm_ss, load_mnist

from utils_phil import *

from deepnn.dataset import shared_dataset, shared_dataset_X

from load_data import load_udm, load_udm_ss, load_mnist,_shared_dataset

import preprocessing

import NetTrainers as NT

from utils import IO
from utils.utils import batch

import cPickle


def load_blackbox(n_train_samples = 800):

    X_train, y_train = IO.unpickle(data_dir+'/blackbox_challenge/blackbox_train.pkl')
    X_train,y_train = preprocessing.normalize_dataset(X_train,y_train,norm_scale=.1)
    #y_train = preprocessing.normalize_labels(y_train)

    X_kaggle = IO.unpickle(data_dir+'blackbox_challenge/blackbox_test.pkl')
    X_kaggle = preprocessing.normalize_X(X_kaggle,norm_scale=.1)

    X_unlabel = np.load(data_dir+'blackbox_challenge/unlabelled_50k.npy')
    X_unlabel = preprocessing.normalize_X(X_unlabel,norm_scale =.1)    

    X_test = X_train[n_train_samples:,:]
    y_test = y_train[n_train_samples:]

    X_train = X_train[:n_train_samples,:]
    y_train = y_train[:n_train_samples]


    print X_train.shape, X_kaggle.shape, X_unlabel.shape

    Xy_train = X_train, y_train
    Xy_test  =  X_test, y_test

    return Xy_train, Xy_test ,X_kaggle, X_unlabel


def train_ss_mlp(NET, mlp_params, sgd_params, datasets):
    """Run semisupervised DEV-regularized test."""
 

    # Tell the net that it's semisupervised, which will force it to use only
    # unlabeled examples for computing the DEV regularizer.
    NET.is_semisupervised = 1

    # Run training on the given NET
    NT.train_ss_mlp(NET=NET, \
        mlp_params=mlp_params, \
        sgd_params=sgd_params, \
        datasets=datasets)
    return 1


def train_dae(NET, dae_layer, mlp_params, sgd_params,datasets):
    """Run DAE training test."""

    # Run denoising autoencoder training on the given layer of NET
    NT.train_dae(NET=NET, \
        dae_layer=dae_layer, \
        mlp_params=mlp_params, \
        sgd_params=sgd_params, \
        datasets=datasets)
    return 1



def write_predictions_to_file(predictions,outfile):
    predictions += 1

    out_str = "Id,Class\n"
    for i in xrange(len(predictions)):
        out_str += "{},{}\n".format(i+1,predictions[i])

    f = open(outfile, "w")
    f.write(out_str)
    f.close()


def batch_test_ss_mlp_gentle(location):
    """Run multiple semisupervised learning tests."""
    # Set some reasonable sgd parameters
    sgd_params = {}
    sgd_params['start_rate'] = 0.1
    sgd_params['decay_rate'] = 0.998
    sgd_params['wt_norm_bound'] = 4
    sgd_params['epochs'] = 100
    sgd_params['batch_size'] = 128
    sgd_params['mlp_type'] = 'dev'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [1875, 1000, 1000, 10]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1,1,  2]
    mlp_params['dev_lams'] = [0.1,.1, 0.2]
    mlp_params['dev_mix_rate'] = 0.
    mlp_params['lam_l2a'] = 1e-2
    mlp_params['use_bias'] = 1

    x_in = T.matrix('x_in')

    rng = np.random.RandomState(666)

    Xy_train, Xy_test, X_kaggle, X_unlabel = load_blackbox(n_train_samples = 800)

    X_train, y_train = Xy_train
    X_test, y_test = Xy_test 

    train_tuple = _shared_dataset((X_train,y_train+1))
    valid_tuple = _shared_dataset((X_test,y_test+1))
    test_tuple  = _shared_dataset((X_test, y_test+1))

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

    datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)

    # Run tests with different sorts of regularization
    NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)
    sgd_params['result_tag'] = "ss_{}_{}".format( sgd_params['mlp_type'],  '_'.join([str(s) for s in mlp_params['layer_sizes']]) )

    train_ss_mlp(NET, mlp_params, sgd_params, datasets)

    t = NET.predict(X_kaggle)

    write_predictions_to_file(t,location)
    
        
    return 1

def batch_test_ss_mlp_pt(location):
    """Setup basic test for semisupervised DEV-regularized MLP."""

    # Set some reasonable sgd parameters
    rng_seed = 666

    sgd_params = {}
    sgd_params['start_rate'] = 0.1
    sgd_params['decay_rate'] = 0.998
    sgd_params['wt_norm_bound'] = .2
    sgd_params['epochs'] = 1000
    sgd_params['batch_size'] = 100
    sgd_params['mlp_type'] = 'dev'
    sgd_params['result_tag'] = 'xxx'
    # Set some reasonable mlp parameters
    mlp_params = {}
    mlp_params['layer_sizes'] = [1875, 600, 600,10]
    mlp_params['dev_clones'] = 1
    mlp_params['dev_types'] = [1,1, 5]
    mlp_params['dev_lams'] = [0.5,.5, 0.3]
    mlp_params['dev_mix_rate'] = 0.0
    mlp_params['lam_l2a'] = 1e-2
    mlp_params['use_bias'] = 1


    Xy_train, Xy_test, X_kaggle, X_unlabel = load_blackbox(n_train_samples = 800)

    # from sklearn.linear_model import LogisticRegression
    # print Xy_train[0][0].mean()
    # print np.unique(Xy_train[1])

    # model = LogisticRegression()
    # model.fit(Xy_train[0],Xy_train[1])
    # t = model.predict(X_kaggle)
    # write_predictions_to_file(t,location)


    #net = IO.unpickle('models/pretrained_ss_final_1875_1000_1000_11')
    #t = net.predict(X_kaggle)
    #write_predictions_to_file(t,'kaggle/prev_preds.csv')
	

    X_train, y_train = Xy_train
    X_test, y_test = Xy_test 

    train_tuple = _shared_dataset((X_train,y_train+1))
    valid_tuple = _shared_dataset((X_test,y_test+1))
    test_tuple  = _shared_dataset((X_test, y_test+1))

    y_unlabel = np.array([0]*X_unlabel.shape[0],dtype='int32')

    unlabelled_data = _shared_dataset((X_unlabel,y_unlabel))

    datasets = (train_tuple,unlabelled_data, valid_tuple, test_tuple)


    # Initialize a random number generator for this test
    rng = np.random.RandomState(rng_seed)

    # Construct the SS_DEV_NET object that we will be training
    x_in = T.matrix('x_in')
    NET = SS_DEV_NET(rng=rng, input=x_in, params=mlp_params)

    # Initialize biases in each net layer (except final layer) to small
    # positive constants (rather than their default zero initialization)
    for (num, layer) in enumerate(NET.mlp_layers):
        b_init = layer.b.get_value(borrow=False)
        b_const = np.zeros(b_init.shape, dtype=theano.config.floatX)
        if (num < (len(NET.mlp_layers)-1)):
            b_const = b_const + 0.05
        layer.b.set_value(b_const)

    ##########################################
    # First, pretrain each layer in the mlp. #
    ##########################################
    sgd_params['mlp_type'] = 'raw'
    sgd_params['batch_size'] = 100
    sgd_params['start_rate'] = 0.01
    sgd_params['epochs'] = 40
    for i in range(len(NET.mlp_layers)-1):
            print("==================================================")
            print("Pretraining hidden layer {0:d}".format(i+1))
            print("==================================================")
            train_dae(NET, i, mlp_params, sgd_params,datasets)

    IO.pickle(NET,'models/pretrained_{}'.format('_'.join([str(s) for s in mlp_params['layer_sizes']])))

        # Run semisupervised training on the given MLP
    sgd_params['top_only'] = True
    sgd_params['mlp_type'] = 'dev'
    sgd_params['epochs'] = 10
    NET.set_dev_lams([0.005, 0.005, 0.005])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)

    sgd_params['top_only'] = False
    sgd_params['mlp_type'] = 'dev'
    sgd_params['epochs'] = 10
    NET.set_dev_lams([0.02, 0.02, 0.02])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)
    sgd_params['mlp_type'] = 'dev'
    sgd_params['epochs'] = 20
    NET.set_dev_lams([0.05, 0.05, 0.05])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)
    sgd_params['mlp_type'] = 'dev'
    sgd_params['epochs'] = 30
    NET.set_dev_lams([0.1, 0.1, 0.1])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)
    sgd_params['result_tag'] = "ss_{}_{}".format( sgd_params['mlp_type'],  '_'.join([str(s) for s in mlp_params['layer_sizes']]) )
    sgd_params['mlp_type'] = 'dev'
    sgd_params['epochs'] = 250
    NET.set_dev_lams([0.2, 0.2, 0.4])
    rng = np.random.RandomState(rng_seed)
    train_ss_mlp(NET, mlp_params, sgd_params, datasets)

    IO.pickle(NET,'models/pretrained_ss_final_{}'.format('_'.join([str(s) for s in mlp_params['layer_sizes']])))

    t = NET.predict(X_kaggle)
    write_predictions_to_file(t,location)

    return 1

if __name__ == '__main__':
    
    out_file = 'kaggle/predictions.csv'
    # batch_test_ss_mlp_gentle(out_file)
    batch_test_ss_mlp_pt(out_file)
