import numpy as np
import theano
import theano.tensor as T

from deepnn.NN import NeuralNetwork, visualize
from deepnn.algorithm import *
from preprocessing import normalize_dataset2,normalize_dataset

from ldr.samplers import *

from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression

from utils import IO
from utils.utils import downsample
from utils.misc import data_dir

from pylab import *



def test():
	nn = NeuralNetwork([784,100],activation_functions= T.nnet.sigmoid)

	X_train,y_train =IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test, y_test =IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
	X_test, y_test = normalize_dataset(X_test,y_test)

	model = LogisticRegression()
	model.fit(X_train,y_train)

	print 'score: {}'.format(model.score(X_test,y_test))

if __name__== '__main__':
	test()