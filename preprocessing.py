import numpy as np
import cv2
import matplotlib.cm as cm
from matplotlib.pyplot import *
import matplotlib.image as mpimg
from skimage.transform import PiecewiseAffineTransform, warp, swirl
from sklearn.cross_validation import StratifiedKFold
from numpy.random import randint,random
from scipy import ndimage
from sklearn import cluster
import skimage
import skimage.filter
#from skimage.segmentation import clear_border
from skimage.morphology import label, closing, square, disk
from skimage import data
from skimage.filter import threshold_otsu, rank
from skimage.filter import *
from skimage.util import img_as_ubyte
from skimage.segmentation import felzenszwalb, slic, quickshift


#from skimage import io, color

def t_sin_wave(img):
    rows, cols = img.shape[0], img.shape[1]

    src_cols = np.linspace(0, cols, 10)
    src_rows = np.linspace(0, rows, 10)

    src_rows, src_cols = np.meshgrid(src_rows, src_cols)
    src = np.dstack([src_cols.flat, src_rows.flat])[0]

    # add sinusoidal oscillation to row coordinates
    pis = (randint(2,10)*np.pi,randint(2,10)*np.pi)
    freqs = (img.shape[0]/(randint(20,50)),img.shape[1]/(randint(20,50)))
    dst_rows = src[:, 1] - np.sin(np.linspace(0, pis[0], src.shape[0])) * freqs[0]
    dst_cols = src[:, 0] - np.sin(np.linspace(0, pis[1], src.shape[0])) * freqs[1]
    dst = np.vstack([dst_cols, dst_rows]).T


    tform = PiecewiseAffineTransform()
    tform.estimate(src, dst)

    out_rows = img.shape[0]
    out_cols = cols
    out = warp(img, tform, output_shape=(out_rows, out_cols))

    return out


def t_swirl(img):
    swirled = swirl(img, rotation=20, strength=.2, radius=10, order=1)
    return swirled

def t_noise(img):
    return img+random(img.shape)*img.max()/randint(1,30)

def t_gaussian_blur(img):
    return ndimage.gaussian_filter(img, sigma=randint(0,1)*2+1)

def t_random_series(img):
    img = center_sample(img)

    if randint(2) % 2 == 1 and min(img.shape) >= 30:
        img = t_sin_wave(img)   
        
    if randint(2) % 2 == 1: 
        img = t_gaussian_blur(img)
    if randint(2) % 2 == 1:
        img = t_noise(img)
    if randint(2) % 2 == -1:
        if randint(2) % 2 == 1:
            img = prewitt(img)
        else:
            img = sobel(img)
    return img

def rgb2gray(X):
    new_X = []
    n = len(X[0])/3
    size = int(np.sqrt(n))
    for z in X:
        r, g, b = z[:n], z[n:2*n], z[2*n:]
        r = r.reshape((size,size))
        g = g.reshape((size,size))
        b = b.reshape((size,size))

        im = np.dstack((r,g,b))     
        new_X.append(togray(im).flatten())

    new_X = np.array(new_X,dtype='float32')
    return new_X

def deflatten(X):
    new_X = []
    if len(X[0].shape) == 2:
        return X
        
    size = int(np.sqrt(len(X[0])))

    for x in X:
        new_X.append(x.reshape((size,size)))

    new_X = np.array(new_X,dtype=X.dtype)
    return new_X

def normalize_labels(y):
    """
    takes as input a vector of integers, outputs the same vector with elements replaced by their ordinal position
    """
    uniques = np.unique(y)
    uniques.sort()
    uniques = list(uniques)

    mapper = {}
    mapper.update([(x, uniques.index(x)) for x in uniques])

    new_y = np.array([mapper[x] for x in y],dtype='int32')

    return new_y


def normalize_dataset2(X,y,eps=1e-3,flatten=True):
    new_X = []
    X = np.array(X,dtype='float32')

    for i in xrange(len(X)):
        x = X[i]

        if flatten:
            x = x.flatten()
        x -= x.min()
        x /= x.max()+ eps
        new_X.append(x)

    z = zip(new_X,y)
    new_X,y = zip(*z)

    new_X = np.array(new_X,'float32')    
    y = normalize_labels(y)

    return new_X,np.array(y)

def normalize_dataset(X,y,eps=1e-3,flatten=True):
    new_X = []

    X = np.array(X,'float32')

    for i in xrange(len(X)):
        x = X[i]
        
        if flatten:
            x = x.flatten()
        x -= x.mean()
        x /= np.array(x.std()+ eps,dtype='float32')
        new_X.append(x)

    z = zip(new_X,y)
    new_X,y = zip(*z)
    new_X = np.array(new_X,'float32')

    # new_X -= new_X.mean(axis=0)

    y = normalize_labels(y)

    return new_X,np.array(y)


def test():
    path = 'images/1.jpg'
    img=mpimg.imread(path)
    img = togray(img)
    img = center_sample(img)    
    img = t_sin_wave(img)       
    img = t_gaussian_blur(img)
    img = t_noise(img)
    img = prewitt(img)
    imshow(img,origin='lower',cmap = cm.Greys_r)
    show()

class ZCA():

    def __init__(self, regularization=10**-2, copy=False):
        self.regularization = regularization
        self.copy = copy

    def fit(self, X, y=None):

        sigma = np.dot(X.T,X) / X.shape[0]
        U, S, V = np.linalg.svd(sigma)
        tmp = U.dot(np.diag(1/np.sqrt(S+self.regularization)))
        self.components_ = np.dot(tmp, U.T)

        return self

    def transform(self, X):

        X_transformed = X.dot(self.components_.T)
        return X_transformed

def togray(im):
    im = np.float32(im)

    if len(im.shape)==2:
        return im
    # im -= im.mean()
    # im /= np.abs(im).max()
    # return color.rgb2hed(im)[:,:,2]
    return cv2.cvtColor(im,cv2.COLOR_BGR2GRAY) # convert to gray

def resize_line_to_width(line,width):
    x = np.array([line],dtype=float)                  
    x = np.array(np.around(cv2.resize(x,(width,1))),dtype=int)
    return x[0]

def resize_to_width(im,width):
    return cv2.resize(im,(width,im.shape[0]*width/im.shape[1]))

def resize_to_height(im,height):
    return cv2.resize(im,(im.shape[1]*height/im.shape[0],height))

def resize_by_ratio(im,ratio):
    return cv2.resize(im,(int(im.shape[1]*ratio),int(im.shape[0]*ratio))) 


def fft(X):
    X = np.array(map(lambda x : scipy.fft(x),X))
    return X

def __tosample(im,size,flatten):
    if size is not False:
        im = resize_sample(im,size) # resize (could be down or up sampling)
    if flatten:
        im = im.flatten()
    return im

def segment(img,n=3):
    X = img.reshape((-1, 1)) # We need an (n_sample, n_feature) array
    k_means = cluster.KMeans(n_clusters=3)
    k_means.fit(X) 
    values = k_means.cluster_centers_.squeeze()
    labels = k_means.labels_
    z = np.choose(labels, values)
    z.shape = img.shape
    return z


def otsu(im,eps=.001):
    im -= im.mean()
    im /= np.abs(im).max()

    #print im.min(),im.max()
    im = img_as_ubyte(im)

    radius = 10
    selem = disk(radius)
    loc_otsu = rank.otsu(im, selem)
    t_glob_otsu = threshold_otsu(im)
    glob_otsu = im >= t_glob_otsu
    return glob_otsu


def threshold(im):
    
    image = np.array(im,dtype='float32')

    image -=image.mean()
    image /= np.abs(image).max()

    markers = np.zeros(image.shape, dtype=np.uint)
    markers[image < -0.01] = 1
    markers[image >0.01] = 2

    try:
        labels = random_walker(image, markers, beta=10, mode='bf')
    except:
        thresh = threshold_otsu(im)
        labels = closing(im > thresh, square(2))
    image = np.array(labels,dtype='float32')

    return image

def slicer_xrange(img,frame_width):
        return xrange(frame_width/2,img.shape[1]-frame_width/2,frame_width)
        #return xrange(img.shape[1])

def slicer(img,frame_width):        
    for i in slicer_xrange(img,frame_width):
        patch = img[:,max(i-frame_width/2,0):min(i+frame_width/2,img.shape[1])]
        yield patch

def to_frames(img,frame_width):
    return [x for x in slicer(img,frame_width)]

def batch_to_frames(X,frame_width):
    T = []
    for x in X:
        T.extend(to_frames(x,frame_width))
    return T

def resize_sample(im,size):
    return cv2.resize(im,size,interpolation=cv2.INTER_LANCZOS4) 

def normalize_sample(x,eps = 1e-3):
    #x = cv2.equalizeHist(x)
    #x -= x.min()
    #x /= (x.max()+.0001)
    x = np.array(x,dtype='float32')
    x -= x.mean()
    x /= (x.std() + eps) #regularization so we won't divide by zero   
    return x


def center_sample(x):
    x -= x.mean()
    x /= x.max()-x.min()
    return x

def stratify_dataset(X,y,batch_size=128):
    k_fold = StratifiedKFold(y,len(y)/batch_size)
    new_X = np.zeros(X.shape)
    new_y = np.zeros([len(y)],dtype = int)
    y = np.array(y)
    prev = 0
    for i,j in k_fold:
        new_X[prev:prev+len(j)] = X[j]
        new_y[prev:prev+len(j)] = y[j]
        prev += len(j)

    return (new_X,new_y)

def normalize(X):
    X = np.array(map(lambda x : normalize_sample(x),X))
    return X

def flatten(X):
    X = np.array(map(lambda x: x.flatten(),X))
    return X

def toMat(X,size,flatten=False):
    X = np.array(map(lambda x: __tosample(x,size,flatten),X))
    return X

def blacken_words(tagged_image):

    for box in tagged_image.tagged_rectangles:        
        tagged_image.image[box.y:box.y+height,box.x:box.x+box.width] = 0
    return np.array(tagged_image)


class Pipeline():
    
    def __init__(self):        
        self.stages = []

    def add(self,*functions):
        for function in functions:
            self.stages.append(function)

    def apply(self,x):
        newx = x
        for stage in self.stages:
            newx = stage(newx)

        return newx

    def __call__(self,x):
        return self.apply(x)



if __name__ == '__main__':

    path = 'images/2.jpg'
    im = cv2.imread(path,cv2.CV_LOAD_IMAGE_COLOR)
    im = togray(im)
    cv2.imshow("original",im)

    x=806.0
    y=844.0
    width=218.0
    height=141.0

    from datatypes import *

    tr = TaggedRectangle()
    tr.x =x
    tr.y = y
    tr.width = width
    tr.height = height

    im = blockbox(im,tr)
    im = cv2.resize(im,(500,500))

    cv2.imshow("meee",im)
    cv2.waitKey()