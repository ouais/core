from pyflann import *
import time
from numpy import *
from numpy.random import *

from utils import IO

flann = FLANN()

X,y = IO.unpickle('/home/ouais/data/mnist/mnist_train.pkl')
X_test,y_test = IO.unpickle('/home/ouais/data/mnist/mnist_test.pkl')

start = time.time()
params = flann.build_index(X, algorithm="kmeans", target_precision=0.9 , log_level = "info");

print time.time()-start
result, dists = flann.nn_index(X_test,5, checks=params["checks"]);
