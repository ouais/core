import numpy as np
from pylab import *


def test():
	n = 100
	np.random.seed(n)
	I = np.random.random([n,n])
	w = np.random.random(I.shape)
	wI = np.multiply(I,w)
	II = np.zeros(I.shape)

	for i in xrange(I.shape[0]):
		for j in xrange(I.shape[1]):
			if i == 0 and j == 0:
				II[i,j] = wI[i,j]
			if i == 0 and j > 0:
				II[i,j] = II[i,j-1] + wI[i,j]
			if i > 0 and j == 0:
				II[i,j] = II[i-1,j] + wI[i,j]
			if i > 0 and j > 0:
				II[i,j] = II[i,j-1] + II[i-1,j] - II[i-1,j-1] + wI[i,j]


	x1,y1 = (2,10)
	x2,y2 = (30,30)

	z = II[x2,y2] - II[x2,y1-1] - II[x1-1,y2] + II[x1-1,y1-1]

	print z 
	print wI[x1:x2+1,y1:y2+1].sum()


if __name__ =='__main__':
	test()
