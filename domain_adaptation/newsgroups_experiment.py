import numpy as  np
import scipy
import scipy.io
from scipy.sparse import csc_matrix

from utils import IO
from utils.misc import *
from deepnn.NN import NeuralNetwork
from deepnn.algorithm import LDR_Pretrainer, Backpropagation
from deepnn.costs import *
from preprocessing import normalize_dataset
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC

from ldr.experiment_core import execute_test, transform_dataset
from ldr.test_multitask import downsample
from ldr.samplers import *

from pylab import *

def compute_correspondence(Xy_train, Xy_test, train_partitions = ['comp', 'sci', 'rec'],test_partition = ['talk']):

	X, y = Xy_train	

	X = np.array(np.vstack((X, Xy_test[0])),dtype='float32')
	y = np.hstack((y,Xy_test[1]))

	names = ['alt.atheism', 'comp.graphics', 'comp.os.ms-windows.misc', 'comp.sys.ibm.pc.hardware', 'comp.sys.mac.hardware',
	 'comp.windows.x', 'misc.forsale', 'rec.autos', 'rec.motorcycles', 'rec.sport.baseball', 'rec.sport.hockey', 'sci.crypt',
	  'sci.electronics', 'sci.med', 'sci.space', 'soc.religion.christian', 'talk.politics.guns', 'talk.politics.mideast', 'talk.politics.misc', 'talk.religion.misc']

	d = {}
	test_ids  = []
	train_ids = []

	for i in xrange(len(names)):
		if not (names[i].split('.')[0] in d):
			d[ names[i].split('.')[0]] = [i]
		else:
			d[ names[i].split('.')[0]].append(i)


	for z in train_partitions:
		train_ids.extend([x for x in xrange(y.shape[0]) if y[x] in d[z]])

	for z in test_partition:
		test_ids.extend([ x for x in xrange(y.shape[0]) if y[x] in d[z]])

	Xy_train = X[train_ids], y[train_ids]
	Xy_test  = X[test_ids] , y[test_ids]

	train_ids = []

	for z in train_partitions:
		train_ids.append(np.array([x for x in xrange(len(Xy_train[1])) if Xy_train[1][x] in d[z]]))	

	correspondence = np.zeros((len(Xy_train[1]),1),dtype='int32')

	for i in xrange(len(train_partitions)):
		correspondence[train_ids[i]] = i

	return Xy_train, Xy_test, correspondence

def relabel(y):
		d = {}
		counter = Counter(y)
		d.update([(x, counter.keys().index(x)) for x in counter.keys()])

		y_new = np.array([d[x] for x in y],dtype='int32')
		return y_new


def run_newsgroups():
	rng = np.random.RandomState(666)	

	Xy_train = IO.unpickle(data_dir + '/newsgroups/newsgroup_train_lda_transformed.pkl')	
	Xy_test  = IO.unpickle(data_dir +  '/newsgroups/newsgroup_test_lda_transformed.pkl')

	lldr = []
	stl = []

	n_samples = 100
	n_trials = 10

	for trial in xrange(1):		
		Xy_train, Xy_test, correspondence = compute_correspondence(Xy_train,Xy_test)

		X_train, y_train = Xy_train		

		N = X_train.shape[1]

		nn = NeuralNetwork(layers_sizes=[N,1000])
		#note that the default for adding layers is a softmax layer while during construction its a rectified linear layer
		nn.add_layer(len(np.unique(y_train)))		

		sampler = AdaptiveCorrespondenceSampler(rng, y_train,correspondence=correspondence)

		cost = CrossEntropyLoss

		algorithm = LDR_Pretrainer(nn,L1_lambda=0.0, L2_lambda=0.00, learning_rate=.01, batch_size=200, sampler=sampler, max_n_batches=1, cost = cost)
		loss = algorithm.fit(X_train,y_train)	
		plot(loss)
		show()

		bprop = Backpropagation(nn,train_last_layer_only=True, L1_lambda=0.0,L2_lambda=0.0,max_n_batches=200,batch_size=10,momentum=.9,learning_rate=.01,cost = cost)

		X_test, y_test = Xy_test
		y_test = relabel(y_test)

		for i in xrange(n_trials):
			retraining_indices = downsample(rng,y_test,n_samples)

			X_temp_train, y_temp_train = X_test[retraining_indices], y_test[retraining_indices]

			loss = bprop.fit(X_temp_train,y_temp_train)	
			model = LogisticRegression()
			model.fit(X_temp_train,y_temp_train)

			result =  nn.score(X_test,y_test)

			lldr.append(result)
			stl.append(model.score(X_test,y_test))

	print 'LLDR: %f , STL: %f , averaged over %d trials' % (np.mean(lldr),np.mean(stl), n_trials)

if __name__== '__main__':
	run_newsgroups()
	# run_stl()