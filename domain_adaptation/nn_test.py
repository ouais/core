import theano
import theano.tensor as T
import scipy
import scipy.sparse
from deepnn.NN import NeuralNetwork
from deepnn.algorithm import SparseBackpropagation, Backpropagation
from utils import IO
from utils.misc import *
from pylab import *

def test_bow():
	Xy_train = IO.unpickle(data_dir+'newsgroups/newsgroups_train_bow.pkl')
	Xy_test = IO.unpickle(data_dir+'newsgroups/newsgroups_test_bow.pkl')

	Xy_train = Xy_train[0], Xy_train[1]
	N = Xy_train[0].shape[1]
	nn = NeuralNetwork([N,100, 100],activation_functions=[lambda x:  T.maximum(x,0), lambda x:  T.maximum(x,0)],sparse_input=True)
	nn.add_layer(20)	

	bprop = SparseBackpropagation(nn,max_n_batches=1000,batch_size=200,learning_rate = .01, momentum=.9)
	loss = bprop.fit(*Xy_train)
	# plot(loss)
	W = nn.layers[0].W.get_value(borrow=True).T
	print W.shape, W.__class__
	hist(W)
	show()

	print nn.score(*Xy_test)


def test_lda():
	Xy_train = IO.unpickle(data_dir+'newsgroups/newsgroup_train_lda_transformed.pkl')
	Xy_test = IO.unpickle(data_dir+'newsgroups/newsgroup_test_lda_transformed.pkl')

	Xy_train = Xy_train[0], Xy_train[1]
	Xy_test  = np.array(Xy_test[0],dtype='float32'), Xy_test[1]
	N = Xy_train[0].shape[1]
	nn = NeuralNetwork([N,100, 100],activation_functions=[lambda x:  T.maximum(x,0), lambda x:  T.maximum(x,0)])
	nn.add_layer(20)	

	bprop = Backpropagation(nn,max_n_batches=1000,batch_size=200,learning_rate = .01, momentum=.9)
	loss = bprop.fit(*Xy_train)
	plot(loss)
	show()

	print nn.score(*Xy_test)



if __name__ == '__main__':
	test_lda()