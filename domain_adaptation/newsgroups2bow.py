import re

import numpy as np
from scipy.sparse import *


from utils import IO
from utils.misc import *

from collections import Counter
from sklearn.feature_extraction.text import CountVectorizer


def remove_stop_words(documents):
	with open('docs/stopwords.txt') as f:
		stoplist = f.readlines()

	remove = '|'.join([x.strip() for x in stoplist])
	re_stop = re.compile(r'\b('+remove+r')\b', flags=re.IGNORECASE)
	re_bad  = re.compile('[^a-zA-Z \n\t]')

	new_docs = []
	for doc in documents:
 		new_doc = re_stop.sub("", doc)
 		new_doc	= re_bad.sub("", new_doc)
 		new_docs.append(new_doc)

 	return new_docs


def core_converter():
	print 'loading training set...'
	X, y = IO.unpickle(data_dir+'newsgroups/newsgroups_train.pkl')
	print 'removing stop words...'
	X = remove_stop_words(X)
	print 'converting to bag of words...'
	vectorizer = CountVectorizer(min_df=1)	
	X = vectorizer.fit_transform(X)
	X = csc_matrix(X,dtype='float32')
	y = np.array(y,dtype='int32')
	print 'pickling training set...'	
	IO.pickle((X,y), data_dir+'newsgroups/newsgroups_train_bow.pkl')

	print 'loading testing set...'
	X, y = IO.unpickle(data_dir+'newsgroups/newsgroups_test.pkl')
	print 'removing stop words...'
	X = remove_stop_words(X)
	print 'converting to bag of words...'
	X = vectorizer.transform(X)
	X = csc_matrix(X,dtype='float32')
	y = np.array(y,dtype='int32')
	print 'pickling testing set...'
	IO.pickle((X,y), data_dir+'newsgroups/newsgroups_test_bow.pkl')

if __name__ == '__main__':
	core_converter()
