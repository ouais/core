

import numpy as np
import time
from multiprocessing import Process, Value, Array
from multiprocessing.sharedctypes import copy


def test(N,B,K,batches=100):
	
	matrices = generate_matrices(N,B,K)
	result = matrices[0]

	start = time.clock()
	for j in xrange(batches):
		for i in xrange(K+1):
			result = np.dot(result,matrices[i])
	end  = time.clock()

	print end-start

def generate_matrices(N,B,K):
	matrices = [np.random.random((B,N))]
	for k in xrange(K):
		A = np.random.random((N,N))
		matrices.append(A)

	return matrices


def stage(id,matrices,batches):
		
	global queues
	count = 1
	while count < batches:		
		if id == 0:
			queues[0].append(matrices[0])
		else:
			while len(queues[id-1]) == 0:
				time.sleep(0)
			data = queues[id-1].pop()
			queues[id].append(np.dot(data,matrices[id]))
		count += 1
		print len(queues[0])				
		print id,batches



if __name__ == '__main__':

	N = 200
	B = 200
	K = 2
	batches = 2

	global queues
	queues = []
	
	for i in xrange(K):
		queues.append([])

	matrices = generate_matrices(N,B,K)

	for k in xrange(K):
		p = Process(target=stage, args=(k,matrices,batches))
		p.start()

	# test(N,B,K)
