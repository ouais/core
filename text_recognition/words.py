import IO
import cv2
import preprocessing as pp
from preprocessing import *
from pylab import *
import numpy as np
import matplotlib.pyplot as plt


def overlay_with_probs(img,probs):
	mask = np.ones([32,32])
	for i in xrange(img.shape[1]):
		img

def process(img,cls):
	y = np.array([])
	#for i in xrange(3,im.shape[1]/2)
	img_pad = np.pad(img,((0,0),(16,16)),'constant',constant_values=0)

	for j in xrange(img.shape[1]):
			s = img_pad[:,j:j+32]
			s = pp.resize(s,(32,32))
			s = s.flatten()
			y = np.append(y,cls.predict_proba(s)[0][1])

	print img.shape
	fig = figure()
	ax1 = fig.add_subplot(111)
	ax1.imshow(img, cmap = cm.Greys_r)
	ax2 = ax1.twinx()
	ax2.plot(y, 'b-')
	print len(y)
	show()


def row_NMS(M,lengths):
	for i in xrange(M.shape[0]):
		length = lengths[i]
		for j in xrange(len(M[i])):
			if M[i,j] == 0:
				continue
			for k in xrange(-length/2,length/2+1):
				if j+k < 0:
					continue
				if j+k == M.shape[1]:
					break
				if M[i,j+k] > M[i,j]:
					M[i,j] = 0
					break

def NMS(M,lengths):
	#M = row_NMS(M,lengths)
	rows,cols = np.where(M > 0)
	vals = [M[rows[i],cols[i]] for i in xrange(len(rows))]
	t = zip(rows,cols,vals)
	t = [list(x) for x in t]
	t.sort(key=lambda x:x[1])

	for i in xrange(len(t)):
		j = i-1
		col_1 = t[i][1]
		supress = False
		while j > -1 and t[j][1]>= col_1-lengths[t[i][0]]/2 :
			if t[j][2] > t[i][2]:				
				supress = True
			elif t[j][2] < t[i][2]:
				t[j][2] = 0
				
			j -= 1
		j = i+1
		while j < len(t) and t[j][1]>= col_1+lengths[t[i][0]]/2 :
			if t[j][2] > t[i][2]:
				supress = True
			elif t[j][2] < t[i][2]:
				t[j][2] = 0
			j += 1

		if supress:
			t[i][2] = 0

	z = filter(lambda x: x[2] > 0,t)
	return z


def plot_image(im,z,lengths):

	fig = figure()
	ax1 = fig.add_subplot(111)
	ax1.imshow(img, cmap = cm.Greys_r)
	ax2 = ax1.twinx()
	rows,xs,cs = zip(*z)
	ys = [0]*len(xs)
	es = [lengths[i]/2 for i in rows]

	ax2.errorbar(xs,ys,xerr=es,marker=None,fmt=None,ecolor='g')
	ax2.scatter(xs,ys,c=cs,cmap='OrRd')	
	show()

def process2(img,cls):
	y = np.array([])

	n = 3
	step = 4
	m = im.shape[1]/2

	lengths = range(n,m,step)

	M = np.zeros([len(lengths),img.shape[1]])
	img_pad = np.pad(img,((0,0),(m/2,m/2)),'constant',constant_values=0)
	c = 0
	M = IO.unpickle('zzz.pkl')
	# for i in xrange(len(lengths)):
	# 	length = lengths[i]
	# 	for j in xrange(img.shape[1]):

	# 			s = img_pad[:,(m-length)/2+j:(m+length)/2+j]
	# 			s = pp.resize(s,(32,32))
	# 			s = s.flatten()
	# 			M[i,j] = cls.predict_proba(s)[0][1]				
	# 			if M[i,j] < .7:
	# 				M[i,j] = 0

	# IO.pickle('zzz',M)
	z = NMS(M,range(n,m,step))
	
	plot_image(img,z,lengths)


if __name__ == '__main__':

    im_path = 'images/3.jpg'
    cls_path = 'meh.pkl'

    im = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)    
    img = togray(im)
    img = normalize_sample(img)
    #M = IO.unpickle('zzz.pkl')
    cls = IO.unpickle(cls_path)
    #plot_image(img,M)
    #cv2.imshow("original",im)
    process2(img,cls)
    #cv2.waitKey()
