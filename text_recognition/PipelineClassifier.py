import IO
import cv2
import preprocessing as pp
from preprocessing import *
from pylab import *
import numpy as np
import matplotlib.pyplot as plt


class PipelineClassifier():

	def __init__(self):
		self.pipeline = None
		self.classifier= None
		self.degenerate = False

	def preprocess(self, x,batch=False):
		if batch:
			return np.array([self.pipeline.apply(y) for y in x])
		else:
			return self.pipeline.apply(x)


	def classify(self,x,preprocess=True):
		if self.degenrate:
			return self.perm_result

		if preprocess and self.pipeline != None:
			t = self.pipeline.apply(x)

		return self.classifier.predict(t)
		

	def predict(self,x,preprocess=True,batch=False):
		if self.degenerate:
			return np.array([[1.]*len(x)])

		if preprocess and self.pipeline != None:
			x = self.preprocess(x,batch)

		return self.classifier.predict_proba(x)


	def fit(self,X,y):
		unique = np.unique(y)
		if len(unique)==1:
			self.degenerate = True
			self.perm_result = unique
		else:
			self.classifier.fit(X,y)



class CascadeClassifier():
	def __init__(self,c1,c2):
		self.c1 = c1
		self.c2 = c2

	def predict_proba(self,X):
		y1 = self.c1.predict_proba(X)

if __name__ == '__main__':
	pass
