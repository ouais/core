import IO
import numpy as np
import matplotlib as mpl
import preprocessing as pp
from sklearn.metrics import confusion_matrix
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import precision_recall_fscore_support

MODES = ['train_test','cv']

class Experiment:

    def __init__(self):
        self.normalize = False
        self.flatten = True
        self.train_path = None
        self.test_path = None
        self.classifier = None
        self.save = False
        self.mode = 'train_test'        
        self.measure_training_error = False
        self.measure_testing_error = False
        self.zca = False
        self.results = {}
        self.folds = 10
        self.map = None


    def create_classifier(self):
        self.X_train,self.y_train = IO.unpickle(self.train_path)               
        self.X_train = np.array(self.X_train)
        self.y_train = np.array(self.y_train)

        self.map = {}

        if self.zca:
            zca = pp.ZCA()
            zca.fit(self.X_train)

        self.X_train = self.preprocess(self.X_train)  

        if self.mode == 'train_test':
            self.classifier.fit(self.X_train,self.y_train)

            if self.measure_training_error:
                x = self.classifier.score(self.X_train,self.y_train)
                self.results["training score"] = x
        elif self.mode == 'cv':
            self.results['cv'] = self.cv()
            self.results['avg'] = self.results['cv'].mean(axis=0)
            self.classifier.fit(self.X_train,self.y_train) 

        return self.classifier


    def preprocess(self,dataset):
        if self.normalize:
            dataset = pp.normalize(dataset)

        if self.flatten:
            dataset = pp.flatten(dataset)

        if self.zca:            
            dataset = zca.transform(dataset)

        #dataset = pp.fft(dataset)
        return dataset


    def cv(self):  
    
        X = self.X_train
        y = self.y_train

        k_fold = cross_validation.StratifiedKFold(y, self.folds)
        scores = np.zeros([self.folds,3])
        total_pred = []
        total_y = []
        fold = 0

        for train_indices, test_indices in k_fold:                                  

            x_train = X[train_indices]
            y_train = y[train_indices]
            x_test = X[test_indices]
            y_test = y[test_indices]
            
            model = self.classifier.fit(x_train,y_train)     
            score = model.score(x_test,y_test)
            y_pred = model.predict(x_test)

            p, r, f, s = precision_recall_fscore_support(y_test, y_pred)
            p = p.mean()
            r = r.mean()
            f = f.mean()

            scores[fold] = p,r,f
            fold += 1

        return scores;

    def test_classifier(self):
        if (self.mode is not "train_test") or (not self.measure_testing_error):
            return
        self.X_test, self.y_test = IO.unpickle(self.test_path)

        self.X_test = np.array(self.X_test)
        self.y_test = np.array(self.y_test)

        self.X_test = self.preprocess(self.X_test)

        x = self.classifier.score(self.X_test,self.y_test)        
        self.results['testing score'] = x

        return self.results


    def save_model(self,path):
        if self.zca:
            IO.pickle(self.zca,"preprocessor")
        IO.pickle(self.classifier,"classifier")


    def run(self):
        self.create_classifier()
        self.test_classifier()
        #misc.compute_stats(self.X_train,self.y_train,self.X_test,self.y_test)
        return self.results


    def show_confusion_matrix(self):

        y_pred = classifier.predict(self.X_test)
        cm = confusion_matrix(self.y_test, y_pred)

        cm = np.float32(cm)

        cm /= cm.sum(axis=1)[:,np.newaxis]
        IO.pickle(cm,"confusion_matrix.pkl")
        alphabet = misc.ALPHABET
        pl.matshow(cm)
        pl.title('Confusion matrix')
        pl.xticks(range(len(alphabet)), alphabet)
        pl.yticks(range(len(alphabet)), alphabet)
        pl.set_cmap("YlOrBr")
        pl.colorbar()
        pl.show()
