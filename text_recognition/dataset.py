
import IO
from misc import *
from preprocessing import t_random_series
from numpy.random import randint

def extend_dataset(dataset_pkl,new_size):
	X,y = IO.unpickle(dataset_pkl)

	new_X = []
	new_y = []
	new_X.extend(X)
	new_y.extend(y)

	limit = new_size-len(X)
	for i in xrange(new_size): 
		if i % 1000 == 0:
			print 'generated %d samples' % i
		index =randint(len(X))
		sample = X[index]
		transformed = t_random_series(sample)
		new_X.append(transformed)
		new_y.append(y[index])

	return new_X,new_y


def extend_icdar_train():
	nu_dataset = extend_dataset(icdar_train_pkl,20000)
	IO.pickle(nu_dataset,data_dir+'icdar_20k.pkl')

if __name__ == '__main__':
	extend_icdar_train()


