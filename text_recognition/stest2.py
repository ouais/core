import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from pylab import *

from skimage import data
from skimage.filter import threshold_otsu
from skimage.segmentation import clear_border
from skimage.morphology import label, closing, square
from skimage.measure import regionprops
from preprocessing import *
from skimage.segmentation import random_walker
from skimage import exposure
from skimage import io, color





im_path = 'images/s4.jpg'
zword = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)        
lab = color.rgb2hed(zword)
# imshow(lab[:,:,0])
# show()
# imshow(lab[:,:,1])
# show()
# imshow(lab[:,:,2])
# show()
word = togray(zword)

image = np.array(lab[:,:,2],dtype='float32')

thresh = threshold_otsu(image)
bw = closing(image > thresh, square(2))

print bw.max(),bw.min(),bw.mean()


image -= image.mean()
image /= np.abs(image).max()

markers = np.zeros(image.shape, dtype=np.uint)
markers[image < -0.01] = 1
markers[image >.01] = 2

labels = random_walker(image, markers, beta=10, mode='cg_mg')
cleared = bw.copy()
clear_border(cleared)
labels = np.array(labels,dtype='float32')
print labels.min(),labels.max(),labels.mean(),labels.std(),labels.dtype

labels -= labels.mean()
labels /= labels.std()

imshow(labels)
show()


