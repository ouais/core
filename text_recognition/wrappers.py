import numpy as np

from utils import IO
from utils.misc import *
from utils.utils import batch

from sklearn.svm import SVC,LinearSVC
import theano
import theano.tensor as T

from sklearn.metrics import confusion_matrix
import pylab as p
from pylearn2.utils import serial
import preprocessing as pp


class MaxoutWrapper(object):
	def __init__(self,maxout_pkl=None):

		self.x = T.ftensor4('x')
		if maxout_pkl != None:
			model = IO.unpickle(maxout_pkl)						
			self.model = model
			x = self.x
			self.f = theano.function([x],model.fprop(x))		
			self.ful = [0]*len(self.model.layers)
			for i in xrange(1,len(self.model.layers)):				
				self.ful[i] = theano.function([x],self.fprop_until_last(x,i))		
			

	@property
	def yamlf(self):
		return self._yamlf

	@yamlf.setter
	def yamlf(self,x):
		self._yamlf = x
		
	def _reshape(self, X):
		if len(X.shape) == 3:
			X = X.reshape(X.shape+(1,))
		elif len(X.shape) == 2:
			X = X.reshape((1,)+ X.shape+(1,))
		return X

	def fprop_until_last(self, state_below, start=1):

		rval = self.model.layers[0].fprop(state_below)

		rlist = [rval]

		for layer in self.model.layers[1:-start]:
			rval = layer.fprop(rval)
			rlist.append(rval)

		return rval

	@batch(128)
	def fprop_last(self,X):
		X = self._reshape(X)
		X = np.array(X,dtype='float32')
		q = self.ful[2](X)
		if len(q.shape) ==4:
			q = [np.array(q[:,:,:,i]).flatten() for i in xrange(q.shape[3])]
		return q
	
	@batch(128)
	def predict_proba(self,X):
		X = self._reshape(X)
		X = np.array(X,dtype='float32')
		return self.f(X)
	
	def predict(self,X):
		p_hat = self.predict_proba(X)
		y_hat = np.zeros(len(X),dtype=int)
		for i in xrange(len(p_hat)):
			y_hat[i] = p_hat[i].argmax()
		return y_hat
	
	def score(self, X,y):
		y_hat = self.predict(X)
		hits = y == y_hat
		return hits.mean()
		
	def fit(self, X,y):
		IO.pickle((X,y),hmm_maxout_pkl)
		self.train_obj = serial.load_train_file(self.yamlf)		
		self.train_obj.main_loop()
		model =  self.train_obj.model
		self.f = theano.function([self.x],model.fprop(self.x))
		self.model = model


def test_char_recognition():
	X,y = IO.unpickle(icdar_test_pkl)
	print X.shape
	model = MaxoutWrapper('models/char_maxout.pkl')
	
	y_hat = model.predict(X)
	y_hat = np.array([ALPHABETi[x].lower() for x in y_hat])
	y = np.array([ALPHABETi[x].lower() for x in y])

	hits = (y_hat == y)
	nhits = hits.sum()
	misfires = []
	for i in xrange(len(y)):
		if hits[i] == False:
			misfires.append(X[i])
			
	misfires = np.array(misfires)
	# save_dataset(X=misfires)
	# show_confusion(y,y_hat)
	
	print nhits,len(y),hits.mean()
	
	
def show_confusion(y,y_pred):
	cm = confusion_matrix(y, y_pred)
	cm = np.array(cm,'float32')

	cm /= cm.sum(axis=1)[:,np.newaxis]
	IO.pickle(cm,"confusion_matrix.pkl")
	alphabet = ALPHABET
	
	p.matshow(cm)
	p.title('Confusion matrix')
	p.xticks(range(len(alphabet)), alphabet)
	p.yticks(range(len(alphabet)), alphabet)
	p.xlabel('Predicted')
	p.ylabel('Actual')
	p.set_cmap("PuBuGn")
	p.colorbar()	
	p.show()
	#p.savefig('cm.jpg', bbox_inches=0)

def produce():
	model = MaxoutWrapper(maxout_pkl='models/char_maxout.pkl')
	X,y = IO.unpickle(data_dir+'all_train.pkl')

	# y = np.array([ALPHABETi[x].lower() for x in y])
	# y = np.array([iALPHABET[x] for x in y])
	# X = X[:300]
	X =  model.fprop_last(X)
	print 'success'
	print X.shape
	svc = SVC()
	print 'training SVM...'
	svc.fit(X,y)

	print 'testing maxout with SVM...'
	X,y = IO.unpickle(icdar_test_pkl)

	# y = np.array([ALPHABETi[x].lower() for x in y])
	# y = np.array([iALPHABET[x] for x in y])
	
	X =  model.fprop_last(X)
	score = svc.score(X,y)

	print score

def test():
	model = MaxoutWrapper(maxout_pkl='yaml/icdar_best.pkl')
	X = np.random.random([10,32,32])
	r = model.predict_proba(X)
	X = np.random.random([32,32])
	r = model.predict_proba(X)
	X = np.random.random([100,32,32])
	y = np.random.randint(0,8,100)
	IO.pickle((X,y),hmm_maxout_pkl)
	model = MaxoutWrapper(yamlf='yaml/hmm_maxout.yaml')
	model.fit(X,y)

	print "wrapper works."


if __name__ =='__main__':
	#test()
	# produce()
	test_char_recognition()