def make_segments_proper(segments):
	S = []
	length = 0
	for i in xrange(len(segments)-1):
		length += 1
		if segments[i] != segments[i+1]:
			S.append((length,segments[i]))
			length = 0
	length +=1
	S.append((length,segments[-1]))
	return S


if __name__=='__main__':
	s = [0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,1,1,1,0,0]
	t = make_segments_proper(s)

	print t