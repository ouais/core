import IO
import numpy as np

import theano
import theano.tensor as T

from deepnn.dataset import *


X = np.random.random([50,32,32,1])
train_x = shared_dataset_X(X)

index = T.lscalar()
x = T.ftensor4('x')

model = IO.unpickle('yaml/icdar_best.pkl')

f = theano.function([x],model.fprop(x))

f[0]
