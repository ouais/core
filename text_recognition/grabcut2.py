import numpy as np
from pylab import *
from preprocessing import *
import cv2


filename = "images/3.jpg"
im = cv2.imread(filename)

im = resize_to_width(im,200)
print im.shape

h,w = im.shape[:2]

mask = np.zeros((h,w),dtype='uint8')
rect = (2,2,w-5,h-5)
bgdmodel = np.zeros((1,65),np.float64)
fgdmodel = np.zeros((1,65),np.float64)

cv2.grabCut(im,mask,rect,fgdmodel,bgdmodel,10,mode=cv2.GC_INIT_WITH_RECT)

imshow(mask)
show()