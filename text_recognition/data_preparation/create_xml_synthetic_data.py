import numpy as np
import scipy
import scipy.io

from utils import IO
from utils.misc import data_dir

def convert_synthetic():
	mat = scipy.io.loadmat(data_dir + 'icdar/char/train/syntheticData/syntheticData.mat')
	X = mat['e'][0][0][0]
	y = mat['e'][0][0][1]

	IO.pickle((X,y), data_dir + 'icdar/char/train/synth.pkl')


if __name__ == '__main__':
	convert_synthetic()
