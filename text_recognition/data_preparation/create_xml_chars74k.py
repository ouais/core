import os

from utils.misc import data_dir, ALPHABET


def create_xml(path, sub_path):
	folders = os.listdir(path+sub_path)
	folders.sort()
	lines = []
	for i in xrange(len(folders)):
		target_char = ALPHABET[i]
		files = os.listdir(path+sub_path+folders[i])
		files.sort()
		for f in files:
			lines.append((sub_path+folders[i]+'/'+f, target_char))

	output_file = '<?xml version="1.0" encoding="iso-8859-1"?>\n'
	output_file += '<imagelist>\n'
	for line in lines:		
		output_file += '<image file="{}" tag="{}" />\n'.format(line[0],line[1])

	output_file += '</imagelist>'

	return output_file


if __name__ == '__main__':
	train_xml = create_xml(data_dir+'/icdar/char/train/','GoodImg/Bmp/')
	with open(data_dir+'icdar/char/train/chars74k_goodimage.xml','w+') as f:
		f.write(train_xml)

	train_xml = create_xml('/home/oalsha1/data/icdar/char/train/','BadImag/Bmp/')
	with open(data_dir+'icdar/char/train/chars74k_badimage.xml','w+') as f:
		f.write(train_xml)

	train_xml = create_xml('/home/oalsha1/data/icdar/char/train/','Fnt/')
	with open(data_dir+'icdar/char/train/chars74k_fonts.xml','w+') as f:
		f.write(train_xml)
