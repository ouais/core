import IO
import matplotlib.pyplot as plt
import numpy as np
import cv2
from pygco import cut_simple, cut_from_graph
from  preprocessing import *

def example_segmentation():
    im_path = 'images/s3.jpg'
    cic = IO.unpickle('cic_recognizer_ExtraTreesClassifier.pkl')
    word = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)        
    word = cv2.cvtColor(word,cv2.COLOR_BGR2GRAY) # convert to gray

    word = np.array(word,dtype=float)
    word -= word.mean()
    word /= word.std()

    x = word

    print x.min(),x.max(),x.dtype

    frame_width = 15
    p = Pipeline()
    s1 = lambda x: resize_sample(x,(32,32))
    s2 = lambda x: normalize_sample(x)
    s3 = lambda x: x.flatten()
    p.add(s1,s2,s3)

    img_pad = np.pad(word,((0,0),(frame_width/2,frame_width/2)),'constant',constant_values=0)

    unaries = []
    X = []
    for i in xrange(word.shape[1]):
        X.append(p.apply(img_pad[:,i:i+frame_width]))
        
    unaries = cic.predict_proba(X)[:,1]
    unaries = np.tile(unaries,(word.shape[0],1))
    unaries -= unaries.mean()
    unaries /= unaries.std()

    #unaries  = np.multiply(unaries,word)

    print unaries.min(),unaries.max(),unaries.dtype

    unaries = (np.dstack([unaries, -unaries]).copy("C")).astype(np.int32)
    # create potts pairwise
    pairwise = -1 * np.eye(2, dtype=np.int32)
    
    print unaries.min(),unaries.max(),unaries.dtype
    result = cut_simple(unaries, pairwise)
    print result.min(),result.max()

    inds = np.arange(word.size).reshape(word.shape)
    horz = np.c_[inds[:, :-1].ravel(), inds[:, 1:].ravel()]
    vert = np.c_[inds[:-1, :].ravel(), inds[1:, :].ravel()]
    edges = np.vstack([horz, vert]).astype(np.int32)

    # we flatten the unaries
    result_graph = cut_from_graph(edges, unaries.reshape(-1, 2), pairwise)

    #n_word = np.multiply(word,result)
    #plt.imshow(n_word)

    plt.subplot(231, title="original")
    plt.imshow(word, interpolation='nearest')
    plt.subplot(232, title="rounded to integers")
    plt.imshow(np.multiply(word,unaries[:, :, 0]), interpolation='nearest')
    plt.subplot(233, title="cut_simple")
    plt.imshow(result, interpolation='nearest')
    plt.subplot(234, title="cut_from_graph")
    plt.imshow(result_graph.reshape(word.shape), interpolation='nearest')

    plt.show()

    

def example_binary():

    im_path = 'images/s4.jpg'
    word = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)        
    word = cv2.cvtColor(word,cv2.COLOR_BGR2GRAY) # convert to gray

    word = np.array(word,dtype=float)
    word -= word.mean()
    word /= word.std()

    x = word

    print x.min(),x.max(),x.dtype
    x_noisy = x + np.random.normal(0, .001, size=x.shape)
    x_thresh = x_noisy > 0.1

    unaries = x_noisy

    # as we convert to int, we need to multipy to get sensible values
    unaries = (5 * np.dstack([unaries, -unaries]).copy("C")).astype(np.int32)
    # create potts pairwise
    pairwise = -20 * np.eye(2, dtype=np.int32)
    
    print unaries.min(),unaries.max(),unaries.dtype
    # do simple cut
    result = cut_simple(unaries, pairwise)

    print result.min(),result.max()

    # use the gerneral graph algorithm
    # first, we construct the grid graph
    inds = np.arange(x.size).reshape(x.shape)
    horz = np.c_[inds[:, :-1].ravel(), inds[:, 1:].ravel()]
    vert = np.c_[inds[:-1, :].ravel(), inds[1:, :].ravel()]
    edges = np.vstack([horz, vert]).astype(np.int32)

    # we flatten the unaries
    result_graph = cut_from_graph(edges, unaries.reshape(-1, 2), pairwise)

    # plot results
    plt.subplot(231, title="original")
    plt.imshow(x, interpolation='nearest')
    plt.subplot(232, title="noisy version")
    plt.imshow(x_noisy, interpolation='nearest')
    plt.subplot(233, title="rounded to integers")
    plt.imshow(unaries[:, :, 0], interpolation='nearest')
    plt.subplot(234, title="thresholding result")
    plt.imshow(x_thresh, interpolation='nearest')
    plt.subplot(235, title="cut_simple")
    plt.imshow(result, interpolation='nearest')
    plt.subplot(236, title="cut_from_graph")
    plt.imshow(result_graph.reshape(x.shape), interpolation='nearest')

    plt.show()


example_binary()
#example_segmentation()
