from __future__ import unicode_literals
from nltk.corpus import brown
from nltk.probability import LidstoneProbDist
from nltk.model.ngram import NgramModel
import IO
import numpy as np
from nltk.corpus import brown
from nltk.corpus import gutenberg
from nltk.model import NgramModel
from nltk.probability import *
from misc import *

from itertools import chain
from math import log
from nltk.probability import (ConditionalProbDist, ConditionalFreqDist,SimpleGoodTuringProbDist)
from nltk.util import ingrams
from nltk import compat


#corpus =[w for w in gutenberg.words('austen-sense.txt')] 
#corpus = brown.words()

class Trie:
	def __init__(self):
		self.children = {}

	def insert(self,word):
		cur = self
		for i in xrange(len(word)):
			if not (word[i] in cur.children):
				cur.children[word[i]] = Trie()
			cur = cur.children[word[i]]
		cur.children['$'] = '$'

	def build(self,lexicon):
		for word in lexicon:
			self.insert(['']+[c for c in word])

	def logprob(self,char,context):
		cur = self
		context.append(char)
		i = 0
		while i < len(context):
			if context[i] in cur.children:
				cur = cur.children[context[i]]
				i+=1
			else:
				return np.inf
		return 1


def lidstone(gamma):
	return lambda fd, bins: LidstoneProbDist(fd, gamma, bins)

def get_twoway_lexicon(words,n=5):
	l_corpus = words
	l_corpus = [[c for c in w] for w in l_corpus]
	
	r_corpus = [x[::-1] for x in words]
	r_corpus = [[c for c in w] for w in r_corpus]
	
	lms_l = {}
	for i in xrange(2,n+1):
		lms_l[i] = NgramModel(i, l_corpus,estimator=lambda fd, bins: MLEProbDist(fd))
	
	lms_r = {}
	for i in xrange(2,n+1):
		lms_r[i] = NgramModel(i, r_corpus,estimator=lambda fd, bins: MLEProbDist(fd))
		
	return (lms_l,lms_r)

def get_l2r_lexicon(words,n=3):
	corpus = [[c for c in w] for w in words]
	lms = {}
	est = lambda fd, bins: MLEProbDist(fd)
	est = lambda fd, bins: WittenBellProbDist(fd,bins)
	est = lambda fd, bins: LaplaceProbDist(fd)
	for i in xrange(2,n+1):
		lms[i] = NgramModel(i, corpus,estimator=est,pad_right=True)

	return lms

if __name__== '__main__':
	
	pkl = IO.unpickle(synthetic_words_pkl)
	l_corpus = [x[1] for x in pkl]
	l_corpus = [[c for c in w] for w in l_corpus]
	
	r_corpus = [x[1][::-1] for x in pkl]
	r_corpus = [[c for c in w] for w in r_corpus]
	#lm = NgramModel(3, corpus,estimator=lambda fd, bins: MLEProbDist(fd))
	
	lms_l = {}
	for i in xrange(2,6):
		lms_l[i] = NgramModel(i, l_corpus,estimator=lambda fd, bins: MLEProbDist(fd))
	
	
	lms_r = {}
	for i in xrange(2,6):
		lms_r[i] = NgramModel(i, r_corpus,estimator=lambda fd, bins: MLEProbDist(fd))
	
	IO.pickle((lms_l,lms_r),'lmsmall.pkl')
#lm = IO.unpickle('lmtest.pkl')