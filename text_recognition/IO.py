import os
import xml.etree.ElementTree as ET
import cv2
import cPickle
import cloud
from utils.misc import *
from pylab import *
import preprocessing as pp
from datatypes import *

TYPES = ['images','images_with_boxes']

class DatasetReader:

    def __init__(self,pipeline = None,files=['all'],data_type=TYPES[0],tags=ALPHABET,tagfree=False):
        self.files = files
        self.pipeline = pipeline
        self.type = data_type
        self.tags = tags
        self.tagfree = tagfree


    def create_pickle(self, xmlpath, picklename):
        if self.type == 'images':
            res = self.read_image_directory(xmlpath)
        elif self.type == 'images_with_boxes':
            res = self.read_images_with_boxes(xmlpath)           
        res = self.pipeline.apply(res)
        pickle(res,picklename)


    def read_image_directory(self,path):

        dirc = os.listdir(path)
        X = []
        for f in dirc:
            if f.endswith(".xml") and (f in self.files or 'all' in self.files):
                tree = ET.parse(path+f)
                for img in tree.findall("image"):
                    filepath = img.get("file")
                    tag = img.get("tag")
                    if self.tagfree or all((c in self.tags) for c in tag):
                        img = cv2.imread(path+filepath,cv2.CV_LOAD_IMAGE_COLOR)
                        X.append((img,tag))

        return X

    def read_images_with_boxes(self,path):
        dirc = os.listdir(path)
        imgs = []
        for f in dirc:
            if f.endswith(".xml") and (f in self.files or 'all' in self.files):
                tree = ET.parse(path+f)
                for img in tree.findall("image"):
                    filepath = img.find("imageName").text
                    tangles = img.find('taggedRectangles')
                    trecs = []
                    for rect in tangles:
                        tr = TaggedRectangle(tag = rect.find('tag').text,attribs = rect.attrib)
                        trecs.append(tr)

                    scene = cv2.imread(path+filepath)
                    scene = cv2.cvtColor(scene, cv2.COLOR_BGR2RGB)
                    # scene = pp.togray(scene)
                    imgs.append(TaggedImage(image=scene,tagged_rectangles=trecs))
        return imgs
    
    
    def read_images_segmentations(self,words_path,char_path):
        dirc = os.listdir(words_path)
        char_folders = os.listdir(char_path)
        char_folders.sort(key=lambda x: int(x.split('.')[0]))
        char_files = os.listdir(char_path+char_folders[0])
        char_files.sort(key=lambda x: int(x.split('.')[0]))
        file_id = 0
        folder_id = 0
        X = []
        for f in dirc:
            if f.endswith(".xml") and (f in self.files or 'all' in self.files):
                tree = ET.parse(words_path+f)
                for img in tree.findall("image"):
                    filepath = img.get("file")
                    word = img.get("tag")
                    image = cv2.imread(words_path+filepath)
                    
                    chars = []
                    for i in xrange(len(word)):
                        chars.append(cv2.imread(char_path+char_folders[folder_id]+'/'+char_files[file_id]))
                        #print char_path+char_folders[folder_id]+'/'+char_files[file_id]
                        file_id +=1
                        if file_id == len(char_files):
                            folder_id +=1
                            char_files = os.listdir(char_path+char_folders[folder_id])
                            char_files.sort(key=lambda x: int(x.split('.')[0]))
                            file_id = 0
                            
                            
                    X.append((image,word,chars))
        return X

    def read_SVT(self,path):
        X = []
        tree = ET.parse(path+'test.xml')
        for img in tree.findall("image"):
            filepath = img.find("imageName").text
            tangles = img.find('taggedRectangles')
            lex = img.find('lex').text
            lex = lex.split(',')
            trecs = []
            for rect in tangles:
                tr = TaggedRectangle(tag = rect.find('tag').text,attribs = rect.attrib)
                trecs.append(tr)
            scene = cv2.imread(path+filepath,cv2.CV_LOAD_IMAGE_COLOR)
            scene = pp.togray(scene)
            X.append(TaggedImage(image=scene,tagged_rectangles=trecs,lexicon=lex))
        return X




def pickle(data,outfile):
    f = open(outfile,"w")
    cloud.serialization.cloudpickle.dump(data,f)
    f.close()

def unpickle(path):
    return cPickle.load(open(path,"r"))


def create_ICDAR_scene_pkl():
    path = data_dir+'scenes/test/'

    dio = DatasetReader(data_type=TYPES[1])
    res = dio.read_images_with_boxes(path)
    images = [x.image for x in res] 
    
    pickle(images, data_dir+"ICDAR_color_scenes.pkl")

def create_SVT_scene_pkl():
    dr = DatasetReader()
    X = dr.read_SVT(data_dir+'scenes/svt/')
    pickle(X,SVT_scene_test_pkl)

def create_SVT_word_pkl():
    X = unpickle(SVT_scene_test_pkl)
    words = []
    lexicons = []
    for x in X:
        for r in x.tagged_rectangles:
            image = x.image[r.xmin:r.xmax,r.ymin:r.ymax]
            
            words.append(WordImage(image=image,word=r.tag))
            lexicons.append(x.lexicon)

    pickle(words,SVT_test_words_pkl)
    pickle(lexicons,SVT_test_words_lexicons_pkl)

def create_natural_word_pkl():
    dr = DatasetReader()
    X = dr.read_images_segmentations(data_dir+'words/train/',data_dir+'chars/train/icdar/')
    new_X = []

    for i in xrange(len(X)):
        image,word,chars = X[i]
        ratio =  200./image.shape[1]
        for i in xrange(len(chars)):
            chars[i] = pp.resize_by_ratio(chars[i],ratio)     

        recon = np.hstack(chars)
        recon = pp.togray(recon)
        recon = pp.normalize_sample(recon)
        widths = [x.shape[1] for x in chars]
        segments = get_segments(widths,.1)
        s = WordImage(image=recon,word=word,segments=segments,widths=widths)
        new_X.append(s)
        
    pickle(new_X,natural_words_pkl)


        
def get_segments(widths,offset):
    cumsum = np.cumsum(widths)
    t = []
    prev = 0
    for i in xrange(len(cumsum)-1):
        a = cumsum[i]-max(int(offset*widths[i]),1)
        b = cumsum[i]+max(int(offset*widths[i+1]),1)
        t.append((a-prev,1))
        t.append((b-a,0))
        prev = b
    t.append((cumsum[-1]-prev,1))
    return t


def clean_large_lexicon():
    f = open(data_dir+'large_lexicon_original.txt')
    X = f.readlines()
    new_X = []
    for x in X:
        for i in xrange(len(x)):
            if x[i] == '/' or x[i] == '\n':
                break
        x = x[:i]
        new_X.append(x)

    new_X = filter(lambda y: all((c in ALPHABET) for c in y) and len(y) >=3,new_X)

    print len(new_X)    
    pickle(new_X,large_lexicon)
    
if __name__=='__main__':
    #create_natural_word_pkl()
    # clean_large_lexicon()
    create_ICDAR_scene_pkl()
    # create_SVT_scene_pkl()
    # create_SVT_word_pkl()
    

