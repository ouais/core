
from Levenshtein import opcodes,distance

def weighted_distance(s1, s2):
	d = {}
	lenstr1 = len(s1)
	lenstr2 = len(s2)
	for i in xrange(-1,lenstr1+1):
		d[(i,-1)] = i+1
	for j in xrange(-1,lenstr2+1):
		d[(-1,j)] = j+1
 
	for i in xrange(lenstr1):
		for j in xrange(lenstr2):
			cost = 1
			if s1[i].lower() == s2[j].lower():
				cost = 0

			d[(i,j)] = min(
						   d[(i-1,j)] + 1, # deletion
						   d[(i,j-1)] + 1, # insertion
						   d[(i-1,j-1)] + cost, # substitution
						  )
 
	return d[lenstr1-1,lenstr2-1]


def chopped_distance(w1,w2):
	d = distance(w1,w2)
	codes = opcodes(w1,w2)

	if codes[0][0] == 'insert':
		d -= codes[0][4]-codes[0][3]-1
	if codes[0][0] == 'delete':
		d -= codes[0][2]-codes[0][1]-1
	if codes[-1][0] == 'insert':
		d -= codes[-1][4]-codes[-1][3]-1
	if codes[-1][0] == 'delete':
		d -= codes[-1][2]-codes[-1][1]-1
	return d

if __name__=='__main__':
	print weighted_distance('iyama','IIYAMA')