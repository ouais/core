from sklearn.svm import SVC,LinearSVC
from sklearn.ensemble import RandomForestClassifier,ExtraTreesClassifier,GradientBoostingClassifier 
from learning import Experiment
import preprocessing as pp
from preprocessing import Pipeline

from utils import IO
from tr_utils import tile_raster_images

from IO import DatasetReader

import Image
import numpy as np
from pylab import *
from utils.misc import *
import cv2
#from deepnn.cnn import CNN


def test_icdar_chars():
	e = Experiment()
	e.train_path = data_dir + "icdar_train.pkl"
	e.test_path = data_dir + "icdar_test.pkl"
	#e.classifier = SVC(probability=True,C=2.0)
	#e.classifier = LinearSVC(multi_class='crammer_singer',loss='l2', penalty='l1', dual=False)
	#e.classifier = ExtraTreesClassifier(n_estimators=1)
	e.classifier = GradientBoostingClassifier(n_estimators = 10,max_depth=1)
	#e.classifier = CNN()

	e.measure_testing_error = True
	e.measure_training_error = True
	e.flatten = True

	results = e.run()
	print results
	IO.pickle(e.classifier,'recognizer_'+e.classifier.__class__.__name__+'.pkl')
	

def combine_pkls():
	x1,y1 = IO.unpickle(data_dir + "char74k_train.pkl")
	x2,y2 = IO.unpickle(data_dir + "ref_synthetic_chars.pkl")
	x1 = np.vstack((x1,x2))
	y1 = np.append(y1,y2)
	IO.pickle((x1,y1),data_dir+'char74k_synth.pkl')

def create_icdar_char_recognizer_pickle():
	
	size = 32

	trainpath = data_dir + "icdar/char/train/"
	trainpickle  = data_dir + "char74k_train.pkl"
	
	testpath = data_dir + "icdar/char/test/"
	testpickle = data_dir + "icdar_test.pkl"

	pipeline = Pipeline()	

	s1 = lambda x: zip(*x)		
	s2 = lambda x: zip(*filter(lambda x: x[1] in ALPHABET,zip(x[0],x[1])))
	s3 = lambda x: (x[0],[iALPHABET[t] for t in x[1]])
	s4 = lambda x: ([pp.togray(y) for y in x[0]],x[1])
	#s5 = lambda x: ([pp.threshold(y) for y in x[0]],x[1])
	s6 = lambda x: (pp.toMat(x[0],(size,size),False),x[1])
	s7 = lambda x: (pp.normalize(x[0]),x[1])
	#s8 = lambda x: pp.stratify_dataset(x[0],x[1])
	#s7 = lambda x: (x[0],[alphabet_img[i] for i in x[1]],x[1])

	pipeline.add(s1,s2,s3,s4,s6,s7)

	dio = DatasetReader(pipeline=pipeline, files=['chars74k_badimag.xml','chars74k_fonts.xml','chars74k_goodimag.xml'])
	dio.create_pickle(trainpath,trainpickle)
	#dio.create_pickle(testpath,testpickle)

def show_char():
	im_path = 'images/3.jpg'
	im = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)
	img = pp.resize(im,(100,100))	
	img = pp.togray(img)
	img = pp.normalize_sample(img)
	imshow(img, cmap = cm.Greys_r)
	show()

def classify_image():
	im_path = 'images/3794.jpg'

	detector_pkl = 'detector_svm.pkl'

	im = cv2.imread(im_path,cv2.CV_LOAD_IMAGE_COLOR)        
	img = pp.togray(im)   
	img = pp.resize_sample(img,(32,32))
	imshow(img)
	show()
	img = pp.normalize_sample(img)
	#cv2.imwrite('gray.jpg',img)
	#M = IO.unpickle('zzz.pkl')
	detector = IO.unpickle(detector_pkl)
	r1 = IO.unpickle('recognizer_SVC.pkl')
	r2 = IO.unpickle('recognizer_ExtraTreesClassifier.pkl')
	#imshow(img, cmap = cm.Greys_r)

	#show()
	img = img.flatten()
	np.set_printoptions(precision=3,suppress=True)
	print detector.predict_proba(img)
	v1 = r1.predict_proba(img)
	v2 = r2.predict_proba(img)
	#for i in xrange(len(ALPHABET)):
	#	print ALPHABET[i],"%.3f" % v1[0][i], "%.3f" % v2[0][i]

	print ALPHABET[v1.argmax()],ALPHABET[v2.argmax()],ALPHABET[r2.predict(img)]

def test_external_classifier():
	testpickle = data_dir + "icdar_test.pkl"

	c1 = IO.unpickle('recognizer_SVC.pkl')
	c2 = IO.unpickle('recognizer_ExtraTreesClassifier.pkl')
	X,y = IO.unpickle(testpickle)

	hits = 0
	frame_width = 32
	p = Pipeline()
	s1 = lambda x: pp.resize_sample(x,(32,32))
	s2 = lambda x: pp.normalize_sample(x)
	s3 = lambda x: x.flatten()
	p.add(s1,s2,s3)
	
	mf = []
	for i in xrange(len(y)):
		
		v1 = c1.predict_proba(X[i].flatten())[0]
		if v1.argmax() == y[i]:
			hits +=1
		else:
			mf.append(X[i])
			
	mf = np.array(mf)
	image = Image.fromarray(tile_raster_images(X=mf,
		img_shape=(32, 32), tile_shape=(30, 30),tile_spacing=(1, 1)))
	image.save('misfires.png')
		

	print hits,len(y),hits*1./len(y)



if __name__=='__main__':

	# sys.stdout = open('log/'+str(datetime.now()), 'w')
	
	create_icdar_char_recognizer_pickle()
	# test_icdar_chars()
	#combine_pkls()
	#test_external_classifier()
	#classify_image()
	#show_char()
	#

	#sys.stdout.close()
