import numpy as np
import cv2
import IO
import copy
from preprocessing import resize_sample,normalize_sample,Pipeline,togray,resize_to_width,resize_line_to_width
from pylab import imshow,show
from sklearn.cluster import DBSCAN
from hmms import HMMSegmentor
from itertools import groupby
from IO import pickle,unpickle
from misc import *
from datatypes import *
from wrappers import MaxoutWrapper
from test_word_recognition import *
from make_char_detector import read_lexicons
from Levenshtein import opcodes

def activations(img,detector):
	frame_width = 40
	img_pad = np.pad(img,((0,0),(frame_width/2,frame_width/2)),'constant',constant_values=0)

	A = np.zeros(img.shape[1])
	patches = []
	for i in xrange(img.shape[1]):
		patch = img_pad[:,i:i+frame_width]
		patch = resize_sample(patch,(32,32))
		patch = normalize_sample(patch)
		#patch = patch.flatten()       	
		patches.append(patch)

	patches = np.array(patches)
	A = detector.predict_proba(patches)[:,1]

	return A.mean()

	fig = figure()
	ax1 = fig.add_subplot(111)
	ax1.imshow(img, cmap = cm.Greys_r)
	ax2 = ax1.twinx()
	ax2.plot(A)
	show()  

class Window:
	def __init__(self,xmin,ymin,xmax,ymax):
		self.xmin = max(xmin,0)
		self.xmax = xmax
		self.ymin = max(ymin,0)
		self.ymax = ymax

		self.width = ymax-ymin
		self.height = xmax-xmin

		self.score = 0

	def area(self):
		return (self.xmax-self.xmin)*(self.ymax-self.ymin)

	def __repr__(self):
		return '(%d,%d) ,(%d,%d)' % (self.xmin,self.xmax,self.ymin,self.ymax)

def area(window):
	return (window.xmax-window.xmin)*(window.ymax-window.ymin)

def region2window(region):
	minx = region[:,0].min()
	maxx = region[:,0].max()
	miny = region[:,1].min()
	maxy = region[:,1].max()

	return Window(miny,minx,maxy,maxx)

def iscontained(w1,w2):
	if (w1.xmin > w2.xmin and w1.xmax < w2.xmax and w1.ymin > w2.ymin and w1.ymax < w2.ymax):
		return True

def merge_overlap(w1,w2):
	xmin = min(w1.xmin,w2.xmin)
	xmax = max(w1.xmax,w2.xmax)
	ymin = min(w1.ymin,w2.ymin)
	ymax = max(w1.ymax,w2.ymax)
	w = Window(xmin,ymin,xmax,ymax)

	return w

def merge_overlapping(windows,threshold):
	
	check = False
	while check == False:
		check = True
		for i in xrange(len(windows)):
			for j in xrange(len(windows)):
				if i != j and compute_intersection_area(windows[i],windows[j]) >= threshold:
					check = False
					windows.append(merge_overlap(windows[i],windows[j]))
					a = windows[i]
					b = windows[j]

					windows.remove(a)
					windows.remove(b)
					break
			if check == False:
				break

	return windows

def oascan(X):
	tags = range(len(X))

	check = True
	threshold = 20

	while check:
		check = False
		for i in xrange(len(X)):
			for j in xrange(len(X)):
				if abs(X[i][0]-X[j][0]) <= threshold and abs(X[i][1]-X[j][1]) <= threshold and tags[i] != tags[j]:
					tags[i] = min(tags[i],tags[j])
					tags[j] = min(tags[i],tags[j])
					check=True
	return tags

def make_segments_proper(segments):
	S = []
	length = 0
	for i in xrange(len(segments)-1):
		length += 1
		if segments[i] != segments[i+1]:
			S.append((length,segments[i]))
			length = 0
	length +=1
	S.append((length,segments[-1]))
	return S

def cluster(windows,segmentation=False,eps=10):

	X = np.array([(x.xmin,x.xmax) for x in windows])

	db = DBSCAN(eps=eps, min_samples=2).fit(X)
	labels = db.labels_
	#labels = oascan(X)
	for i in xrange(len(windows)):
		windows[i].label = int(labels[i])

	windows.sort(key=lambda x:x.label)
	clusters = groupby(windows,lambda x:x.label)

	new_windows = []
	for k,g in clusters:
		g = [x for x in g]
		xmin = min(g,key=lambda x:x.xmin).xmin
		xmax = max(g,key=lambda x:x.xmax).xmax
		ymin = min(g,key=lambda x:x.ymin).ymin
		ymax = max(g,key=lambda x:x.ymax).ymax
		w = Window(xmin,ymin,xmax,ymax)

		if segmentation:
			w.segmentation = np.zeros(ymax-ymin,dtype=int)
			for x in g:
				w.segmentation[x.ymin-ymin:x.ymax-ymin] = 1
		#w.color = (np.random.randint(20,255),np.random.randint(20,255),np.random.randint(20,255))
		new_windows.append(w)
	return new_windows,windows

def cluster_lines(windows,eps=20):

	clusters = groupby(windows,lambda x:x.label)
	new_windows = []
	
	for k,g in clusters:
		g = [x for x in g]
		X = np.array([((x.ymin+x.ymax)/2,x.xmax-x.xmin,x.ymax-x.ymin) for x in g])
		db = DBSCAN(eps=eps, min_samples=1).fit(X)		
		labels = db.labels_

		for i in xrange(len(g)):
			g[i].label = int(labels[i])

		g.sort(key=lambda x:x.label)
		line_clusters = groupby(g,lambda x:x.label)

		for t,q in line_clusters:
			q = [x for x in q]
			xmin = min(q,key=lambda x:x.xmin).xmin
			xmax = max(q,key=lambda x:x.xmax).xmax
			ymin = min(q,key=lambda x:x.ymin).ymin
			ymax = max(q,key=lambda x:x.ymax).ymax
			w = Window(xmin,ymin,xmax,ymax)

			new_windows.append(w)

	return new_windows

def remove_overlapping(windows,threshold):
	new_windows = []
	for i in xrange(len(windows)):
		check = True
		for j in xrange(len(windows)):
			if i < j and compute_relative_intersection(windows[i],windows[j]) >= threshold:
				check = False
				break
		if check:
			new_windows.append(windows[i])
	return new_windows

def remove_elongated(windows):
	new_windows = []
	for w in windows:
		if w.xmax-w.xmin < w.ymax-w.ymin:
			new_windows.append(w)
	return new_windows	

def plot_windows(img,windows,gt=None):
	fig = figure()
	ax1 = fig.add_subplot(111)
	ax1.imshow(img, cmap = cm.Greys_r)
	
	if gt != None:
		for window in gt:
			rect = Rectangle((window.ymin, window.xmin),window.width,window.height, facecolor='None',edgecolor="#8800ff")
			#rect = Rectangle(( 10,20),100,200, facecolor='None',edgecolor="#aaffaa")

			text(window.ymax, window.xmin,window.tag, color='k', 
				fontsize=10, fontname='Courier', 
				horizontalalignment='center', bbox = dict(boxstyle="square",ec=(0.5, 0.3, 1),fc=(0.5, .3, 1)))
			ax1.add_patch(rect)

	for window in windows:
		rect = Rectangle((window.ymin, window.xmin),window.width,window.height, facecolor='None',edgecolor="#00ff00")
		# rect = Rectangle(( 10,20),100,200, facecolor='None',edgecolor="#aaffaa")
		text(window.ymin, window.xmax+20,window.recognizer_result[0], color='k', 
			fontsize=10, fontname='Courier', 
			horizontalalignment='center', bbox = dict(boxstyle="square",ec=(.6, 1, .5),fc=(0.6, 1, 0.5)))
		ax1.add_patch(rect)

	show()

def prune(img,windows,classifier,threshold):
	new_windows = []
	ss = []
	for window in windows:
		#print window.xmin,window.xmax,window.ymin,window.ymax
		s = img[window.xmin:window.xmax,window.ymin:window.ymax]
		#print s.shape,img.shape
		s = resize_sample(s,(32,32))
		s = normalize_sample(s)
		ss.append(s)

	ss = np.array(ss)

	detections = classifier.predict_proba(ss)[:,1]

	for i in xrange(len(windows)):
		windows[i].detector_result = detections[i]
		if detections[i] >= threshold:
			new_windows.append(windows[i])

	return new_windows

def mser_show(img):
	mser = cv2.MSER()
	regions = mser.detect(gray, None)
	windows = [region2window(p) for p in regions]
	plot_windows(img,windows)

def create_scene_dataset():
	path = data_dir+'scenes/test/'

	dio = DatasetReader(data_type=IO.TYPES[1])
	res = dio.read_images_with_boxes(path)
	IO.pickle(res,icdar_scene_test_pkl)

def compute_intersection_area(box1,box2):
	x1 = max(box1.xmin,box2.xmin)
	x2 = min(box1.xmax,box2.xmax)

	y1 = max(box1.ymin,box2.ymin)
	y2 = min(box1.ymax,box2.ymax)

	if y2> y1 and x2 > x1:
		intersection_area = (y2-y1)*(x2-x1)
	else:
		intersection_area = 0
	return intersection_area

def compute_relative_intersection(box1,box2):
	area = compute_intersection_area(box1,box2)

	a1 = (box1.ymax-box1.ymin)*(box1.xmax-box1.xmin)
	a2 = (box2.ymax-box2.ymin)*(box2.xmax-box2.xmin)

	return float(area)/max(a1,a2)

def compute_bounding_area(box1,box2):
	x1 = min(box1.xmin,box2.xmin)
	x2 = max(box1.xmax,box2.xmax)

	y1 = min(box1.ymin,box2.ymin)
	y2 = max(box1.ymax,box2.ymax)

	bounding_area = (y2-y1)*(x2-x1)	
	return bounding_area

def compute_overlap_measure(box1,box2):
	
	bounding_area = compute_bounding_area(box1,box2)
	intersection_area = compute_intersection_area(box1,box2)

	return float(intersection_area)/bounding_area

def score(image,windows,detector):
	for window in windows:
		t = image[window.xmin:window.xmax,window.ymin:window.ymax]
		window.score = activations(t,detector)
	return windows

def measure_relative(set_a,set_b):
	s = 0
	for a in set_a:
		overlap = measure_overlap(a,set_b)
		print overlap
		s += overlap
	s /= len(set_a)

	return s

def measure_overlap(box,boxes):
	overlap = 0 
	for gtbox in boxes:
		o = compute_overlap_measure(box,gtbox)
		if o > overlap:
			overlap = o

	return overlap

def iterative_clustering(windows):
	eps1_range = xrange(45,1,-5)

	final_candidates = windows

	d = {}
	for w in windows:
		d[(w.xmin,w.xmax,w.ymin,w.ymax)] = True


	for eps1 in eps1_range:
		line_candidates,c = cluster(windows,eps=eps1)
		
		for x in line_candidates:
			if (x.xmin,x.xmax,x.ymin,x.ymax) in d:
				continue
			d[(x.xmin,x.xmax,x.ymin,x.ymax)] = True
			final_candidates.append(x)

	final_candidates = filter(lambda x : x.xmin < x.xmax and x.ymin < x.ymax,final_candidates)
	return final_candidates

def remove_identical(windows):
	d = {}
	new_windows = []
	for x in windows:
		if (x.xmin,x.xmax,x.ymin,x.ymax) in d:
			continue
		d[(x.xmin,x.xmax,x.ymin,x.ymax)] = True
		new_windows.append(x)
	return new_windows

def nms(windows):
	new_windows = []
	for i in xrange(len(windows)):
		windows[i].suppressed = False

	for i in xrange(len(windows)):
		check = True
		for j in xrange(len(windows)):
			if i ==j:
				continue

			la1 = windows[i].recognizer_result[1]*(len(windows[j].recognizer_result[2][0])-1)
			ra1 = windows[j].recognizer_result[1]*(len(windows[i].recognizer_result[2][0])-1)
			a1 =  la1 > ra1 
			a2 = la1 == ra1 and windows[i].recognizer_result[2][1] > windows[j].recognizer_result[2][1]
			a = a1 or a2
			
			b = compute_relative_intersection(windows[i],windows[j]) > .2

			d = compute_intersection_area(windows[i],windows[j]) >= area(windows[i])
			
			if a and (b or d):
				check = False
				break
		if check:
			new_windows.append(windows[i])
	return new_windows


def windows_from_image(image,char_detector=None,word_detector=None):
	image = np.array(image,dtype='uint8')
	mser = cv2.MSER()
	regions = mser.detect(image, None)
	windows = [region2window(p) for p in regions]
	# windows = prune(image,windows,detector,.1)
	
	# windows = cluster_lines(c)
	
	# windows = prune(image,windows,char_detector,0)
	
	# windows = prune(image,windows,word_detector,.9)
	# windows = nms(windows)
	windows = iterative_clustering(windows)
	# windows = prune(image,windows,word_detector,0)
	# windows = nms(windows)
	# windows = filter(lambda x: x.detector_result > .99,windows)
	# windows = score(image,windows,char_detector)
	# windows = filter(lambda x: x.score > .2,windows)


	#windows,c = cluster(windows)
	#print len(windows)
	return windows

def compute_hits(image,predicted,ground_truth,word_recognizer,lexicon=None):
	hits = 0
	for g in ground_truth:
		g.flag = True

	for window in predicted:
		#window.tag = word_recognizer.recognize(window)
		window_image = image[window.xmin:window.xmax,window.ymin:window.ymax]	
		# print window.recognizer_result

		# imshow(window_image)
		# show()

		t = np.array([compute_overlap_measure(window,x) for x in ground_truth])

		check = False
		k = t.argmax()
		if len(t) > 0 and t[k] > .5 and ground_truth[k].flag and window.recognizer_result[0] == ground_truth[k].tag :
			ground_truth[k].flag = False
			hits += 1

	return hits

def remove_segments(a,threshold):
	new_segments = []
	length = 0
	prev = a[0][1]
	for x in a:
		if x[1] != prev and x[0] > threshold:
			new_segments.append((length,prev))
			prev = x[1]
			length = 0

		length += x[0]

	if length > 0:
		new_segments.append((length,prev))
	return new_segments

def space_cluster(segmentation,image=None):
	if len(segmentation) == 1:
		return [segmentation]

	indices = [i for i in xrange(len(segmentation))]
	zi = zip(segmentation,indices)

	zi.sort(key=lambda x:-x[0][0])
	zi = filter(lambda x: x[0][1] == 0,zi)

	current_segmentation = [[x[0],1] for x in segmentation]

	segmentations = []
	for space in zi:
		current_segmentation[space[1]][1] = 0
		united_segmentation = remove_segments(current_segmentation,-1)
		segmentations.append(united_segmentation)
		# print united_segmentation
		
		# g = []
		# for x in united_segmentation:
		# 	g.extend([x[1]]*x[0])
		
		# z = np.array(g)
		# z *= 100
		# imshow(image)
		# plot(z)
		# show()
	return segmentations



def segment_textline(image,window,segmentor):
	new_windows = [] 
	segmentation = segmentor.segment_raw(image[window.xmin:window.xmax,window.ymin:window.ymax])
	t = image[window.xmin:window.xmax,window.ymin:window.ymax]
	
	# z = np.array(segmentation)
	# z *= 100

	# # 	#print segmentation

	# imshow(t)
	# plot(z)
	# show()
	segmentation = make_segments_proper(segmentation)
	segmentations = space_cluster(segmentation,t)
	#segmentation = remove_segments(segmentation,3)
	
	d = {}

	for segmentation in segmentations:
		cur = 0
		for i in xrange(len(segmentation)):
			if segmentation[i][1] == 1 and segmentation[i][0] > 1:	

				# print textline.shape,cur,segmentation[i]
				# print window,cur,segmentation[i]
				w = Window(window.xmin,window.ymin+cur,window.xmax,window.ymin+cur+segmentation[i][0])
				if (w.xmin,w.xmax,w.ymin,w.ymax) in d:
					continue

				new_windows.append(w)
				d[(w.xmin,w.xmax,w.ymin,w.ymax)] = True
				# imshow(image[w.xmin:w.xmax,w.ymin:w.ymax])
				# show()
			
			cur += segmentation[i][0]

	return new_windows
	
def add_segmented_text_lines(image,windows,segmentor):
	new_windows = []

	for x in windows:		
		words = segment_textline(image,x,segmentor)
		new_windows.extend(words)

	new_windows.extend(windows)

	return new_windows

def test_single_image():
	
	char_detector  = MaxoutWrapper('yaml/char_detector.pkl')
	word_detector  = MaxoutWrapper('yaml/detector.pkl')
	#detector = IO.unpickle('detector.pkl')
	hs = HMMSegmentor('models/MaxoutWrapper_40_lines_hmm_1.pkl')

	filename = "images/s13.jpg"
	im = cv2.imread(filename)
	im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
	gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
	predicted = windows_from_image(gray,char_detector,word_detector)
	# # for window in predicted:
		
	# # 	t = gray[window.xmin:window.xmax,window.ymin:window.ymax]
	# #  # 	segmentation = hs.segment_raw(t)
	
	# #  # 	segmentation = np.array(segmentation)
	# #  # 	segmentation *= 100

	# #  	imshow(t)
	# #  	#plot(segmentation)
	# #  	show()

	
	#
	predicted = add_segmented_text_lines(gray,predicted,	hs)
	# predicted = prune(gray,predicted,word_detector,.8)
	# predicted = nms(predicted)
	#predicted = remove_overlapping(predicted,.5)
	# predicted = remove_elongated(predicted)

	print 'candidate windows %d' % len(predicted)
	# recognize_text(gray,predicted)

	
	plot_windows(im,predicted)


def recognize_text(image,windows):
	segmentation_pkl = 'models/MaxoutWrapper_10_hmm_1.pkl'

	hs = HMMSegmentor(segmentation_pkl)
	
	recognizer = MaxoutWrapper('yaml/icdar_855.pkl')
	corrector = MaxoutWrapper('yaml/segmentation_corrector.pkl')
	detector = MaxoutWrapper('yaml/detector.pkl')

	wr = Segmentation_WR(segmentor=hs,detector=corrector,recognizer=recognizer,selector=qterbi)
	lexicons = read_lexicons('/home/ouais/data/lex/test/lex50/')

	lexicon = lexicons[24]
	print lexicon
	for window in windows:
		t = image[window.xmin:window.xmax,window.ymin:window.ymax]
		wordimage = WordImage(image=t)
		print wr.recognize_lexicon(wordimage,lexicon)

def create_word_segmentation_dataset():
	dataset = IO.unpickle(icdar_scene_train_pkl)
	S = []
	for x in dataset:
		if len(x.tagged_rectangles) == 0:
			continue
		windows,z = cluster(x.tagged_rectangles,segmentation=True)
		windows = remove_contained(windows)		
		for window in windows:
			image = x.image[window.xmin:window.xmax,window.ymin:window.ymax]

			#resized_image = resize_to_width(image,400)
			#print make_segments_proper(segmentation[0])
			#segmentation = resize_line_to_width(window.segmentation,400)
			#resized_segmentation = make_segments_proper(segmentation)

			segmentation = make_segments_proper(window.segmentation)

			S.append(WordImage(image=image,segments=segmentation))
			# imshow(resized_image)
			# plot(segmentation*100)
			# show()
	IO.pickle(S,icdar_line_segmentation_pkl)

def test_sample():
	X = IO.unpickle(icdar_scene_test_pkl)

	hs = HMMSegmentor('models/MaxoutWrapper_40_lines_hmm_1.pkl')
	lexicons = read_lexicons('/home/ouais/data/lex/test/lex50/')

	segmentation_pkl = 'models/MaxoutWrapper_10_hmm_1.pkl'

	hs = HMMSegmentor(segmentation_pkl)
	
	recognizer = MaxoutWrapper('yaml/icdar_855.pkl')
	corrector = MaxoutWrapper('yaml/segmentation_corrector.pkl')
	detector = MaxoutWrapper('yaml/detector.pkl')

	wr = Segmentation_WR(segmentor=hs,detector=corrector,recognizer=recognizer,selector=qterbi)

	x = X[29]
	lexicon = lexicons[29]
	imshow(x.image)
	show()

	predicted = windows_from_image(x.image,detector)
	predicted = add_segmented_text_lines(x.image,predicted,hs)		
	predicted = remove_elongated(predicted)
	predicted = filter(lambda x: x.xmax > x.xmin and x.ymax > x.ymin,predicted)

	ts = []
	for p in predicted:
			image = x.image[p.xmin:p.xmax,p.ymin:p.ymax]
			wordimage = WordImage(image=image)			
			p.recognizer_result = wr.recognize_lexicon(wordimage,lexicon,with_accuracies=True)			

			t = normalize_sample(resize_sample(image,(32,32)))
			ts.append(t)
			#print p.recognizer_result

	ts = np.array(ts)
	rs = detector.predict_proba(ts)
	j = 0 
	for p in predicted:
		p.detector_result = rs[j][1]
		j+=1

	ground_truth = x.tagged_rectangles
	ground_truth = filter(lambda x: len(x.tag) > 2,ground_truth)

	hits = 0
	if len(ground_truth) > 0:
		hits = compute_hits(x.image,predicted,ground_truth,None,None)

	print len(predicted),len(ground_truth),hits

def compute_boxes_on_dataset():
	X = IO.unpickle(SVT_scene_test_pkl)	

	hs = HMMSegmentor('models/MaxoutWrapper_40_lines_hmm_1.pkl')
	detector  = MaxoutWrapper('yaml/detector.pkl')

	new_X = []
	
	for i in xrange(len(X)):

		predicted = windows_from_image(X[i].image,detector)
		predicted = add_segmented_text_lines(X[i].image,predicted,hs)		
		predicted = remove_elongated(predicted)
		predicted = filter(lambda x: x.xmax > x.xmin and x.ymax > x.ymin,predicted)

		new_X.append((X[i],predicted))
		print i

	IO.pickle(new_X,SVT_scene_test_stage1_pkl)

def different_lexicon():
	X = IO.unpickle(icdar_scene_test_stage2_50_pkl)
	# lexicons = read_lexicons('/home/ouais/data/lex/test/lex50/')
	# dataset = IO.unpickle(words_test_pkl)
	# dataset = filter(lambda x: all((c in ALPHABET) for c in x.word) and len(x.word) >=3,dataset)
	# lexicon = [x.word for x in dataset]
	# lexicon.extend(IO.unpickle(large_lexicon))
	# lexicons = [lexicon]*len(X)

	for i in xrange(len(X)):
		print i
		x,predicted = X[i]
		for p in predicted:
			original_results = p.recognizer_result[3]
			# p.recognizer_result = correct_lexicon(original_results,lexicons[i])+ (original_results,)
			word = ''.join(x for x in original_results[0][0])
			p.recognizer_result = (word,original_results[0],) + (original_results,)
	IO.pickle(X,icdar_scene_test_stage2_0_pkl)

def compute_wordrecognition_results():
	X = IO.unpickle(icdar_scene_test_stage1_pkl)
	# lexicons = IO.unpickle(SVT_scene_lexicons_pkl)
	# X = IO.unpickle(icdar_scene_test_stage1_pkl)
	# lexicons = read_lexicons('/home/ouais/data/lex/test/lex50/')
	# lexicons = read_lexicons('/home/ouais/data/lex/test/lex5/')

	segmentation_pkl = 'models/MaxoutWrapper_10_hmm_1.pkl'

	hs = HMMSegmentor(segmentation_pkl)
	
	recognizer = MaxoutWrapper('yaml/icdar_855.pkl')
	corrector = MaxoutWrapper('yaml/segmentation_corrector.pkl')
	detector = MaxoutWrapper('yaml/detector.pkl')


	lexicon = IO.unpickle(large_lexicon)
	lexicon = [x.lower() for x in lexicon]
	lms = get_l2r_lexicon(lexicon,n=4)

	wr = Segmentation_WR(segmentor=hs,detector=corrector,recognizer=recognizer,selector=qterbi)
	wr.lms = lms
	# lexicons = lexicons[:1]
	# print lexicons[0]
	ts = []
	for i in xrange(len(X)):
		# print i
		x,predicted = X[i]
		for p in predicted:
			image = x.image[p.xmin:p.xmax,p.ymin:p.ymax]
			wordimage = WordImage(image=image)
			t = normalize_sample(resize_sample(image,(32,32)))
			ts.append(t)
			#print p.recognizer_result

	ts = np.array(ts)
	rs = detector.predict_proba(ts)
	j = 0 
	for x,predicted in X:
		for p in predicted:
			p.detector_result = rs[j][1]
			j+=1

	for i in xrange(len(X)):
		print i
		x,predicted = X[i]
		for p in predicted:				
				if p.detector_result > .9:
					image = x.image[p.xmin:p.xmax,p.ymin:p.ymax]
					image = resize_to_width(image,200)	
					if image == None:
						image = x.image[p.xmin:p.xmax,p.ymin:p.ymax]
					wordimage = WordImage(image=image)	
					p.recognizer_result = wr.recognize(wordimage)			

	#print X[0][1][0].detector_result
	# IO.pickle(X,icdar_scene_test_stage2_5_pkl)
	IO.pickle(X,icdar_scene_test_stage2_0_pkl)

def extend_relative(window):
	pixels_per_char = (window.ymax-window.ymin)/(len(window.recognizer_result[2][0])-1.)

	w1 = window.recognizer_result[0]
	w2 = ''.join([x for x in window.recognizer_result[2][0]])

	codes = opcodes(w2,w1)

	count = 0 
	for i in xrange(len(codes)):
		x = codes[i]
		if x[0] == 'replace' or x[0] == 'equal':
			break

	before,after = 0,0

	for z in xrange(i+1,len(codes)):
		x = codes[z]
		if x[0] == 'insert':
			after += x[4]-x[3]
		elif x[0] == 'delete':
			after -= x[2]-x[1]

	for z in xrange(0,i):
		x = codes[z]
		if x[0] == 'insert':
			before += x[4]-x[3]
		elif x[0] == 'delete':
			before -= x[2]-x[1]

	window.ymin -= int(before*pixels_per_char)
	window.ymax += int(after*pixels_per_char)
	window.width = window.ymax-window.ymin

	return window

def correct_height(image,predicted):
	image = np.array(image,dtype='uint8')
	mser = cv2.MSER()
	regions = mser.detect(image, None)
	windows = [region2window(p) for p in regions]

	for p in predicted:
		height = [np.inf,0]
		for w in windows:
			if w.ymin >= p.ymin and w.ymax <= p.ymax and w.xmin >= p.xmin and w.xmax <= p.xmax:
				height[0] = min(height[0],w.xmin)
				height[1] = max(height[1],w.xmax)

		print p.xmax,p.xmin,height
		if height[0] == np.inf and height[1] == 0:
			height[0] = p.xmin
			height[1] = p.xmax

		p.xmin = height[0]
		p.xmax = height[1]

		p.height = p.xmax - p.xmin
		print p.xmax,p.xmin

	return predicted

def produce_pr_curve():
	X = IO.unpickle(icdar_scene_test_stage2_large_pkl)
	var = np.arange(0.01,1,.01)
	results = np.zeros((len(var),3))
	# X = X[:1]
	for i in xrange(len(var)):
		results[i] = test_precomputed(X,.95,var[i],3.4,i == 0)

	IO.pickle(results,data_dir+'pr_I_large.pkl')
	print results

def test_precomputed(X=None,t1=None,t2=None,t3=None,extend_boxes=True):
	if X == None:
		X = IO.unpickle(icdar_scene_test_stage2_50_pkl)
		# X = IO.unpickle(SVT_scene_test_stage2_pkl)
		C = IO.unpickle(data_dir+"ICDAR_color_scenes.pkl")

	tp,tg,th = 0,0,0
	if t1 == None and t2 == None and t3 == None:
		t1 = .95
		t2 = .5
		t3 = 5

	for i in xrange(len(X)):
		original, predicted = X[i]
		# for p in predicted:
		# 	print p.recognizer_result
		
		t_predicted = remove_identical(predicted)	
		
		t_predicted = filter(lambda x:x.detector_result > t1,t_predicted)		
		if extend_boxes == True:
			t_predicted = [extend_relative(y) for y in t_predicted]
		t_predicted = filter(lambda x:x.recognizer_result[1] < t2*(len(x.recognizer_result[2][0])-1),t_predicted)
		t_predicted = filter(lambda x:x.recognizer_result[2][1] < t3*(len(x.recognizer_result[2][0])-1),t_predicted)
			
		#opt for I-5, I-20, I-50 is .95,.5,5
		#opt for I-full .95, .3, .3
		#opt for SVT: .995, .4, 5
		t_predicted = nms(t_predicted)
		
		ground_truth = original.tagged_rectangles
		ground_truth = filter(lambda y: all((c in ALPHABET) for c in y.tag) and len(y.tag) >=3,ground_truth)

		hits = 0
		if len(ground_truth) > 0:
			hits = compute_hits(original.image,t_predicted,ground_truth,None,None)
		# print len(predicted),len(ground_truth),hits
		# plot_windows(original.image,t_predicted,ground_truth)		

		# if hits == 0:
		# 	print i
		# 	plot_windows(x.image,predicted)		
		

		tp += len(t_predicted)
		tg += len(ground_truth)
		th += hits

	p = th/float(tp)
	r = th/float(tg)

	f = 1/(.5/p+.5/r)

	print tp,tg,th
	print p,r,f

	return p,r,f


if __name__ == '__main__':
	# produce_pr_curve()
	# compute_boxes_on_dataset()
	compute_wordrecognition_results()
	# different_lexicon()
	# test_precomputed()
	# max_f_measure()
	#create_scene_dataset()
	#create_word_segmentation_dataset()
	#test_sample()
	# test_single_image()

