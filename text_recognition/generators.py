import numpy as np
import IO
import scipy
import scipy.misc
from misc import *
from pylab import *
from preprocessing import normalize_sample,resize_sample, Pipeline
import PIL
from PIL import ImageFont,Image,ImageDraw
from numpy.random import randint,random
from numpy import zeros,ones

import matplotlib.font_manager

from sklearn.feature_extraction import image
from datatypes import *



class PatchGenerator:
	def __init__(self,sizes = None, n_patches=None):
		self.sizes = sizes
		self.n_patches = n_patches
		self.pps = float(n_patches)/len(sizes) # patches per size


	def generate(self,images):

		ppi = int(np.ceil(self.pps / len(images)))

		total_patches = []
		for img in images:
			for size in self.sizes:
				patches = image.extract_patches_2d(img, size,max_patches = ppi,random_state = np.random.randint(500))
				total_patches.extend(patches)
		return total_patches



class WordGenerator:
	def __init__(self):
		self.n_images=1
		self.size_range = (30,50)
		self.fonts = matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
		self.dictionary = IO.unpickle(dictionary_dir)
		self.offset=.1



	def construct_dictionary(self,path):
		f = open(path)
		words = f.readlines()
		words = [''.join(filter(lambda x: x in ALPHABET,word)) for word in words]
		IO.pickle(words,dictionary_dir)
		f.close()


	def generate_plain(self,word,font_name):
		font = ImageFont.truetype(font_name,randint(self.size_range[0],self.size_range[1]))
		img,widths = string_to_image(word,font,noise=False)
		img = PIL2np(img)
		img = normalize_sample(np.array(img,dtype=float))
		return img


	def generate_sample(self):
		font_name = self.fonts[randint(len(self.fonts))]		
		font = ImageFont.truetype(font_name,randint(self.size_range[0],self.size_range[1]))
		word = self.dictionary[randint(len(self.dictionary))]		
		width,height = font.getsize(word)
		if width == 0 or height == 0:
			return self.generate_sample()

		img,widths = string_to_image(word,font)

		print word,font_name.split('/')[-1],widths,np.sum(widths),width

		segments = get_segments(widths,self.offset)		

		img = PIL2np(img)
		img = normalize_sample(np.array(img,dtype=float))

		return LabeledWordImage(image=img,word=word,segments=segments,widths=widths)

		print segments
		prev = 0
		draw = ImageDraw.Draw(img)	
		for seg in segments:
			draw.line((prev,height/2,prev+seg[0],height/2), fill=(1-seg[1])*255)
			prev += seg[0]
			#print prev

		img = PIL2np(img)
		scipy.misc.imsave('temp.png',img)
		img.show()
		#mat =  PIL2np(img)
		# print mat
		# imshow(mat)
		# show()

def get_segments(widths,offset):
	cumsum = np.cumsum(widths)
	t = []
	prev = 0
	for i in xrange(len(cumsum)-1):
		a = cumsum[i]-max(int(offset*widths[i]),1)
		b = cumsum[i]+max(int(offset*widths[i+1]),1)
		t.append((a-prev,1))
		t.append((b-a,0))
		prev = b
	t.append((cumsum[-1]-prev,1))
	return t	


def PIL2np(img):
    return np.array(img.getdata()).reshape(img.size[1], img.size[0])


def string_to_image(word,font,noise=True):	
	height = font.getsize(word)[1]
	margin = 3
	widths = [font.getsize(c)[0] for c in word]
	width = np.sum(widths)

	if noise:
		char_color = randint(2)*220+int(random()*35) 
	else:
		char_color = 0

	img = Image.new('L',(width,height))
	loc = 0
	for i in xrange(len(word)):
		if noise:
			c_img = Image.fromarray(255*random((height,widths[i])))
		else:
			c_img = Image.fromarray(255*ones((height,widths[i])))
		draw = ImageDraw.Draw(c_img)		
		draw.text((0, 0),word[i],char_color,font=font)
		img.paste(c_img,(loc,0))
		loc += widths[i]

	return img,widths


def generate_synthetic_word_pkl():
	wg = WordGenerator()
	n_instances = 100
	dataset = []

	for i in xrange(n_instances):
		dataset.append(wg.generate_sample())

	IO.pickle(dataset,synthetic_words_pkl)


def generate_synthetic_char_pkl():
	wg = WordGenerator()
	wg.dictionary = ALPHABET
	n_instances = 100
	dataset = []
	imgs = []
	chars = []

	for i in xrange(n_instances):
		x,char,c,d = wg.generate_sample()
		x = resize_sample(x,(32,32))
		x = normalize_sample(x)
		imgs.append(x)
		chars.append(char)
		
	dataset = (imgs,chars)
	IO.pickle(dataset,synthetic_chars_pkl)

def generate_char_vs_interchar():
	dataset= IO.unpickle(synthetic_words_pkl)
	X = []
	y = []
	frame_width = 15

	p = Pipeline()
	s1 = lambda x: resize_sample(x,(32,32))
	s2 = lambda x: normalize_sample(x)
	p.add(s1,s2)

	for x in dataset:
		prev = 0
		img_pad = np.pad(x.image,((0,0),(frame_width/2,frame_width/2)),'constant',constant_values=0)
		for i in xrange(x.image.shape[1]):
			X.append(p.apply(img_pad[:,i:i+frame_width]))

		for segment in x.segments:
			y.extend([segment[1]]*segment[0])

			
	IO.pickle((X,y),data_dir + 'cic.pkl')


def generate_alphabet_pkl():
	wg = WordGenerator()
	ab = []
	for x in ALPHABET:
		x = wg.generate_plain(x,'/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-L.ttf')
		x = resize_sample(x,(32,32))
		ab.append(x)
	IO.pickle(ab,alphabet_pkl)


if __name__=='__main__':
	#generate_alphabet_pkl()
	#wg.construct_dictionary('/usr/share/dict/words')
	generate_synthetic_word_pkl()
	#generate_char_vs_interchar()
	# generate_synthetic_char_pkl()
	# wg.generate_sample()
	