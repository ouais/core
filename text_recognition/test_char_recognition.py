import numpy as np
from utils import IO
from utils.misc import *
from tr_utils import batch

from wrappers import MaxoutWrapper

from sklearn.svm import SVC,LinearSVC
from sklearn.metrics import confusion_matrix

import pylab as p

import preprocessing as pp


def test_char_recognition():
	X,y = IO.unpickle(icdar_test_pkl)
	model = MaxoutWrapper('models/char_maxout.pkl')
	
	y_hat = model.predict(X)
	y_hat = np.array([ALPHABETi[x].lower() for x in y_hat])
	y = np.array([ALPHABETi[x].lower() for x in y])

	hits = (y_hat == y)
	nhits = hits.sum()
	misfires = []
	for i in xrange(len(y)):
		if hits[i] == False:
			misfires.append(X[i])
			
	misfires = np.array(misfires)
	# save_dataset(X=misfires)
	# show_confusion(y,y_hat)
	
	print nhits,len(y),hits.mean()
	
	
def produce():
	model = MaxoutWrapper(maxout_pkl='models/char_maxout.pkl')
	X,y = IO.unpickle(data_dir+'all_train.pkl')

	# y = np.array([ALPHABETi[x].lower() for x in y])
	# y = np.array([iALPHABET[x] for x in y])
	# X = X[:300]
	X =  model.fprop_last(X)
	print 'success'
	print X.shape
	svc = SVC()
	print 'training SVM...'
	svc.fit(X,y)

	print 'testing maxout with SVM...'
	X,y = IO.unpickle(icdar_test_pkl)

	# y = np.array([ALPHABETi[x].lower() for x in y])
	# y = np.array([iALPHABET[x] for x in y])
	
	X =  model.fprop_last(X)
	score = svc.score(X,y)

	print score

if __name__ =='__main__':
	#test()
	# produce()
	test_char_recognition()