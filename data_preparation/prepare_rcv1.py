from utils import IO
from utils.misc import data_dir

def compute_depths(edges):
	depths = {}
	queue = ['None']
	depths['None'] = 0

	while len(queue) != 0:
		source = queue[0]
		del queue[0]

		for node in edges[source]:
			queue.append(node)			
			depths[node] = depths[source] + 1

	return depths


def compute_node_depth(depths,edges,source):
	depths[source] = 0

	for node in edges[source]:		
		depths[source] = max(depths[source],compute_node_depth(depths,edges,node)+1)

	return depths[source]

def compute_depths_from_leaves(edges):
	depths = {}
	compute_node_depth(depths,edges,'None')

	return depths

def get_classes_at_depth(rcv_dir, depth=4):

	edges = {}
	with open(rcv_dir+'rcv1.topics.hier.expanded') as f:
		for line in f:
			line = line.strip()
			splits = filter(lambda x: len(x) != 0, line.split(' '))
			parent, child = splits[1], splits[3]
			if not (parent in edges):
				edges[parent] = []	

			if not (child in edges):
				edges[child] = []	

			edges[parent].append(child)

	depths = compute_depths(edges)
	# for key in edges:
	# 	print key, depths[key]
	print len(edges.keys())
	L = filter(lambda x: depths[x] == depth, edges.keys())
	# print len(L)
	return L

def read_documents(dat_dir):
	docs = []
	with open(dat_dir) as f:
		lines = f.readlines()
		single_string = ''.join([x for x in lines])
		splits = single_string.split('\n\n')
		for i in xrange(0,len(splits)-1):

			line_splits = splits[i].split('.W')
			id = line_splits[0].split()[1]
			text = line_splits[1]

			docs.append((text, id))

	return docs

def id_2_class_map(classes):
	m = {}
	with open(directory+'rcv1-v2.topics.qrels') as f:
		lines = f.readlines()
		for line in lines:
			splits = line.split()
			if not (splits[0] in classes):
				continue
			# if splits[1] in m:
			# 	print splits[1]
			m[splits[1]] = splits[0]
	# print len(m)
	return m


def rcv1_2_docpkl(directory): 
	docs = []
	docs.extend(read_documents(directory+'lyrl2004_tokens_test_pt0.dat'))
	docs.extend(read_documents(directory+'lyrl2004_tokens_test_pt1.dat'))
	docs.extend(read_documents(directory+'lyrl2004_tokens_test_pt2.dat'))
	docs.extend(read_documents(directory+'lyrl2004_tokens_test_pt3.dat'))
	docs.extend(read_documents(directory+'lyrl2004_tokens_train.dat'))

	IO.pickle(docs,directory+'rcv1_docs_text.pkl')
	print 'pickled {} documents'.format(len(docs))


if __name__=='__main__':
	directory = data_dir+'/rcv1/'
	# rcv1_2_docpkl(directory)

	classes = get_classes_at_depth(directory,4)
	print len(classes)
	id_2_class_map(classes)