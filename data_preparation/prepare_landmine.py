
from utils.misc import *
from utils import IO
from scipy.io import loadmat


path = data_dir+'/landmine/landminedata.mat'

dictionary = loadmat(path)

tasks = []

for task in xrange(dictionary['label'].shape[1]):

	X = dictionary['feature'][:,task][0]
	y = dictionary['label'][:,task][0]

	tasks.append(zip(X,y))

IO.pickle(tasks,data_dir+'/landmine/landmine.pkl')
