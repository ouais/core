import os
from pylab import *
from utils import IO
import preprocessing
from preprocessing import togray, normalize_sample

def convert_format_cifar100_c01b(source_file,target_file):
	
	d = IO.unpickle(source_file)

	X = d['data']
	y = d['fine_labels']

	t = 0
	new_X = np.zeros((3,32,32,X.shape[0]))

	for z in X:
		r, g, b = z[:1024], z[1024:2048], z[2048:]
		r = normalize_sample(r.reshape((32,32)))
		g = normalize_sample(g.reshape((32,32)))
		b = normalize_sample(b.reshape((32,32)))

		im = np.dstack((r,g,b))
		im = np.array([im[:,:,i] for i in xrange(3)])

		new_X[:,:,:,t] = im

		t += 1

	new_X = np.array(new_X,dtype='float32')
	y = np.array(y,dtype='int32')		

	IO.pickle((new_X,y), target_file)
	print 'X shape: %s' % str(new_X.shape)

def convert_format_cifar100_bc01(source_file,target_file):
	
	d = IO.unpickle(source_file)

	X = d['data']
	y = d['fine_labels']

	t = 0
	new_X = np.zeros((X.shape[0],3,32,32))

	for z in X:
		r, g, b = z[:1024], z[1024:2048], z[2048:]
		r = normalize_sample(r.reshape((32,32)))
		g = normalize_sample(g.reshape((32,32)))
		b = normalize_sample(b.reshape((32,32)))

		im = np.dstack((r,g,b))
		im = np.array([im[:,:,i] for i in xrange(3)])

		new_X[t,:,:,:] = im

		t += 1

	new_X = np.array(new_X,dtype='float32')
	y = np.array(y,dtype='int32')		

	IO.pickle((new_X,y), target_file)
	print 'X shape: %s' % str(new_X.shape)


directory = '/home/oalsha1/data/cifar100/'

convert_format_cifar100_bc01(directory+'cifar-100-python/train',directory+'cifar100_train_c.pkl')
convert_format_cifar100_bc01(directory+'cifar-100-python/test',directory+'cifar100_test_c.pkl')
