
import csv
import numpy as np

from utils import IO
from utils.misc import data_dir

def convert(input_file, output_file):

	csvfile = open(input_file, 'rb')
	reader = csv.reader(csvfile, delimiter=',', quotechar='|')

	X = []
	y = []

	for row in reader:
		check = True
		try:
			float(row[0])			
		except Exception, e:
			check = False

		if check is False:
			continue
			
		label = int(float(row[0]))
		sample = [float(x) for x in row[1:]]

		X.append(sample)
		y.append(label)

	X = np.array(X,dtype='float32')
	y = np.array(y,dtype='int32')

	print X.shape

	IO.pickle((X,y),output_file)

def convert_unlabelled(input_file, output_file):

	csvfile = open(input_file, 'rb')
	reader = csv.reader(csvfile, delimiter=',', quotechar='|')

	X = []

	for row in reader:
		check = True
		try:
			float(row[0])			
		except Exception, e:
			check = False

		if check is False:
			continue
			
		sample = [float(x) for x in row]

		X.append(sample)

	X = np.array(X,dtype='float32')

	print X.shape

	IO.pickle(X,output_file)


train_csv = data_dir+'blackbox_challenge/train.csv'
test_csv = data_dir+'blackbox_challenge/test.csv'
unlabelled_csv = data_dir+'blackbox_challenge/extra_unsupervised_data.csv'


train_pkl = data_dir + 'blackbox_challenge/train.pkl'
test_pkl = data_dir+'blackbox_challenge/test.pkl'
unlabelled_pkl = data_dir+'blackbox_challenge/unlabelled'


convert(train_csv,train_pkl)
convert_unlabelled(test_csv,test_pkl)
# convert_unlabelled(unlabelled_csv,unlabelled_pkl)



