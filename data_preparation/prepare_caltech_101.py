import numpy as np
import os
import cv2
from utils import IO

def convert(input_directory,output_file):
	directory = os.listdir(input_directory)
	X = []
	y = []
	j = 0

	for f in directory:
		if f.endswith('.pkl'):
			continue
		X_i = []
		y_i = []
		images_path = input_directory+f+'/'
		images_names = os.listdir(images_path)

		for image in images_names:
			im = cv2.imread(images_path+image)
			im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
			im = cv2.resize(im,(32,32),interpolation=cv2.INTER_LANCZOS4)

			im = np.array([im[:,:,i] for i in xrange(3)])

			X_i.append(im)
			y_i.append(j)

		X.extend(X_i)
		y.extend(y_i)
		j += 1

	X = np.array(X,dtype='float32')
	y = np.array(y)

	IO.pickle((X,y),output_file)



input_directory = '/home/oalsha1/data/caltech101/'
output_file = 'caltech101.pkl'

convert(input_directory,input_directory+output_file)
