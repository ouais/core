import os
from pylab import *
from utils import IO
import preprocessing
from preprocessing import togray, normalize_dataset


def normalize(x,eps=1e-3):
	x = np.array(x,dtype='float32')
	x -= x.mean()
	x /= np.array(x.std()+ eps,dtype='float32')
	return x

def convert_format_cifar10(source_dir,target_file,word):

	directory = os.listdir(source_dir)
	
	X = np.array([])
	y = np.array([])
	
	for f in directory:
		if word in f:
			d = IO.unpickle(source_dir+f)
			if len(X) == 0 :
				X = d['data']
				y = d['labels']
			else:
				X = np.vstack((X,d['data']))
				y = np.hstack((y,d['labels']))

	new_X = []

	for z in X:
		r, g, b = z[:1024], z[1024:2048], z[2048:]
		r = normalize(r.reshape((32,32)))
		g = normalize(g.reshape((32,32)))
		b = normalize(b.reshape((32,32)))

		im = np.dstack((r,g,b))
		im = np.array([im[:,:,i] for i in xrange(3)])

		# print r.mean(), g.mean(), b.mean()
		# imshow(im)
		# show()
		new_X.append(im)

	new_X = np.array(new_X,dtype='float32')

	y = np.array(y,dtype='int64')		

	IO.pickle((new_X,y), target_file)

	print 'X shape: %s' % str(new_X.shape)
	


cifar10_directory = '/home/oalsha1/data/cifar10/'

convert_format_cifar10(cifar10_directory+'cifar-10-batches-py/',cifar10_directory+'cifar10_train_c.pkl','data')
convert_format_cifar10(cifar10_directory+'cifar-10-batches-py/',cifar10_directory+'cifar10_test_c.pkl','test_batch')

