import numpy as np
import scipy.io
from preprocessing import resize_sample,normalize_sample
from utils import IO

def convert(source_file,target_file):
	d = scipy.io.loadmat(source_file)
	X_train, y_train = d['train_patterns'], d['train_labels']
	X_test , y_test = d['test_patterns'], d['test_labels']

	X_train = np.array([normalize_sample(resize_sample(x,(28,28))) for x in X_train.T])
	X_test  = np.array([normalize_sample(resize_sample(x,(28,28))) for x in X_test.T ])

	y_train = np.array([np.where(x>0)[0][0] for x in y_train.T])
	y_test  = np.array([np.where(x>0)[0][0] for x in y_test.T])

	X = np.vstack((X_train,X_test))
	y = np.hstack((y_train,y_test))

	IO.pickle((X,y),target_file)

source_dir = '/home/oalsha1/data/usps/'
convert(source_dir+'usps_resampled.mat',source_dir+'usps.pkl')
