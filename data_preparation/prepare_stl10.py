
import h5py
import numpy as np
import scipy
import scipy.io
from utils.misc import data_dir
from preprocessing import normalize_sample,resize_sample, normalize_labels

import cPickle
from pylab import *


def extract_indices(stl10_training_file,out_file):
	d = scipy.io.loadmat(stl10_training_file)

	indices_list = []

	for fold in xrange(len(d['fold_indices'][0])):
		indices_list.append(d['fold_indices'][0][fold].squeeze())
	
	with open(out_file,'wb') as f:
		cPickle.dump(indices_list,f)

def convert_format_stl10(source_file,target_file,special = False):
	y = None
	if special:
		f = h5py.File(source_file,'r')
		X = f['X'].value.T

	else:
		d = scipy.io.loadmat(source_file)
		X = np.array(d['X'],dtype='float32')
		y = d['y']
		y = np.array(y,dtype='int32').squeeze()
		y = normalize_labels(y)


	t = 0
	new_X = np.zeros((X.shape[0],3,32,32))

	for z in X:

		z = np.array(z,dtype='float32')
		r, g, b = z[:96*96], z[96*96:96*96*2], z[96*96*2:]

		r = normalize_sample(resize_sample(r.reshape((96,96)),(32,32))).T
		g = normalize_sample(resize_sample(g.reshape((96,96)),(32,32))).T
		b = normalize_sample(resize_sample(b.reshape((96,96)),(32,32))).T

		im = np.dstack((r,g,b))
		im = np.array([im[:,:,i] for i in xrange(3)])

		new_X[t,:,:,:] = im

		t += 1

	new_X = np.array(new_X,dtype='float32')
		
	if y != None:
		with open(target_file,'wb') as f:
			cPickle.dump((new_X,y), f)
	else:
		with open(target_file,'wb') as f:
			np.save(f,new_X)

	print 'X shape: %s' % str(new_X.shape)

# data_dir = ''
train_source = data_dir+'stl10/train.mat'
train_target = data_dir+'stl10/stl10_train.pkl'

test_source = data_dir+'stl10/test.mat'
test_target = data_dir+'stl10/stl10_test.pkl'

unlabelled_source = data_dir+'stl10/unlabeled.mat'
unlabelled_target = data_dir+'stl10/stl10_unlabelled.pkl'

# convert_format_stl10(train_source,train_target)
# convert_format_stl10(test_source,test_target)
# convert_format_stl10(unlabelled_source,unlabelled_target,special=True)
extract_indices(train_source,data_dir+'stl10/stl10_folds.pkl')
