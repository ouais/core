import numpy as np
from utils import IO
from preprocessing import normalize_sample,normalize_labels
from pylab import imshow,show

def convert(data,labels,target):
	X = np.loadtxt(data)
	y = np.loadtxt(labels)
	X = X.T

	new_X = []
	for z in X:
		r, g, b = z[:1024], z[1024:2048], z[2048:]
		r = normalize_sample(r.reshape((32,32)))
		g = normalize_sample(g.reshape((32,32)))
		b = normalize_sample(b.reshape((32,32)))

		im = np.dstack((r,g,b))
		# imshow(im)
		# show()
		im = np.array([im[:,:,i] for i in xrange(3)])
		new_X.append(im)

	
	new_y = normalize_labels(y)

	new_X = np.array(new_X,dtype='float32')
	new_y = np.array(new_y,dtype='int32')

	IO.pickle((new_X,new_y),target)


def convert_unlabelled(data,target):
	X = np.loadtxt(data)
	X = X.T

	new_X = []
	for z in X:
		r, g, b = z[:1024], z[1024:2048], z[2048:]
		r = normalize_sample(r.reshape((32,32)))
		g = normalize_sample(g.reshape((32,32)))
		b = normalize_sample(b.reshape((32,32)))

		im = np.dstack((r,g,b))
		# imshow(im)
		# show()
		im = np.array([im[:,:,i] for i in xrange(3)])
		new_X.append(im)

	new_X = np.array(new_X,dtype='float32')
	IO.pickle(new_X,target)


main_dir = '/home/oalsha1/data/transfer_challenge/'

train_data = 'training-data.dat'
train_labels = 'training-labels.dat'
train_target = 'transfer_train.pkl'

test_data = 'test-data.dat'
test_labels = 'test-labels.dat'
test_target = 'transfer_test.pkl'

unlabelled_data = 'unlabelled_tiny.dat'
unlabelled_target = 'transfer_unlabelled.pkl'

# convert(main_dir+train_data,main_dir+train_labels,main_dir+train_target)
# convert(main_dir+test_data,main_dir+test_labels,main_dir+test_target)
convert_unlabelled(main_dir+unlabelled_data,main_dir+unlabelled_target)	