import numpy as np
from utils import IO
from sklearn.datasets import fetch_mldata
from utils.misc import data_dir
from collections import Counter

rng = np.random.RandomState(666)

mnist = fetch_mldata('MNIST original', data_home=data_dir+'/mnist/')
X, y = mnist['data'][:-10000], mnist['target'][:-10000]
Z = zip(X,y)
rng.shuffle(Z)
Xy_train = zip(*Z)
Xy_test = (mnist['data'][-10000:], mnist['target'][-10000:])

print Counter(Xy_train[1])
print Counter(Xy_test[1])

IO.pickle(Xy_train, data_dir+'/mnist/mnist_train.pkl')
IO.pickle(Xy_test, data_dir+'/mnist/mnist_test.pkl')