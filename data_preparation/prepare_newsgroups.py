import numpy as np
from utils import IO
from sklearn.datasets import fetch_20newsgroups
from utils.misc import data_dir
from collections import Counter

from sklearn.feature_extraction.text import CountVectorizer

rng = np.random.RandomState(666)

n_features = 100


newsgroups_train = fetch_20newsgroups(subset='train')
newsgroups_test = fetch_20newsgroups(subset='test')
X_train, y_train = newsgroups_train['data'], newsgroups_train['target']
X_test, y_test = newsgroups_test['data'], newsgroups_test['target']

IO.pickle((X_train, y_train), data_dir+'/newsgroups/newsgroups_train.pkl')
IO.pickle((X_test, y_test), data_dir+'/newsgroups/newsgroups_test.pkl')

X_train, y_train = IO.unpickle(data_dir+'/newsgroups/newsgroups_train.pkl')
X_test , y_test  = IO.unpickle(data_dir+'/newsgroups/newsgroups_test.pkl')

# stop_words = [x.strip() for x in open(data_dir+'general/stopwords.txt','r').readlines()]
vectorizer = CountVectorizer(min_df=1,stop_words='english')

X_train = vectorizer.fit_transform(X_train)
X_test  = vectorizer.transform(X_test)

features = [(i,x) for i,x in enumerate(np.array(X_train.sum(axis=0))[0])]
features.sort(key=lambda x: -x[1])

indices = np.array([i for i,x in features[:n_features]])

X_train = np.array(X_train[:,indices].todense(),dtype='float32')
X_test  = np.array(X_test[:,indices].todense(),dtype='float32')

print X_train.shape, X_test.shape

Xy_train = X_train,y_train
Xy_test  = X_test,y_test

IO.pickle(Xy_train, data_dir+'/newsgroups/newsgroups_train_bow_{}.pkl'.format(n_features))
IO.pickle(Xy_test, data_dir+'/newsgroups/newsgroups_test_bow_{}.pkl'.format(n_features))