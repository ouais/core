import numpy as np
from pylab import *


def compare_approximation():
	fns = []
	fns.append(lambda x: 1./(1+np.exp(-x)))
	fns.append(lambda x: 1./2 + x/4 - x**3/48 + x**5/480)

	xs = np.arange(-5,5,.01)

	for f in fns:
		ys = np.array([f(x) for x in xs])
		plot(xs,ys)

	show()

if __name__ == '__main__':
	compare_approximation()


