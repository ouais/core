import numpy as np
import theano
import theano.tensor as T

from deepnn.NN import NeuralNetwork, visualize
from deepnn.algorithm import *
from preprocessing import normalize_dataset2,normalize_dataset

from ldr.samplers import *

from sklearn.svm import SVC

from utils import IO
from utils.utils import downsample
from utils.misc import data_dir

from pylab import *



def train():
	nn = NeuralNetwork([784,100],activation_functions= T.nnet.sigmoid)

	X_train,y_train =IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X_train,y_train = normalize_dataset2(X_train,y_train)

	bprop = Backpropagation(nn,train_last_layer_only=True,max_n_batches=100,momentum=.9)

	loss = bprop.fit(X_train)

	for i in xrange(len(nn.layers)):
		visualize(X_train,nn,i,'materials/dae_weights_{}.png'.format(i))

	nn.add_layer(10)

	IO.pickle(nn,'materials/nn.pkl')




if __name__ == '__main__':
	train()