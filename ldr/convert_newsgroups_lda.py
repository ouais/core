import re
import gensim

import numpy as np
from utils import IO
from utils.misc import *

from gensim import corpora, models, similarities
from collections import Counter

import sklearn 
import sklearn.linear_model

def remove_stop_words(documents):
	with open('docs/stop.txt') as f:
		stoplist = f.readlines()

	remove = '|'.join([x.strip() for x in stoplist])
	re_stop = re.compile(r'\b('+remove+r')\b', flags=re.IGNORECASE)
	re_bad  = re.compile('[^a-zA-Z \n\t]')

	new_docs = []
	for doc in documents:
 		new_doc = re_stop.sub("", doc)
 		new_doc	= re_bad.sub("", new_doc)
 		new_docs.append(new_doc.lower().split())

 	return new_docs

	
def core_converter(num_topics):
	# X, y = IO.unpickle(data_dir+'newsgroups/newsgroups_train.pkl')
	# X = remove_stop_words(X)
	# IO.pickle((X,y), data_dir+'newsgroups/newsgroups_train_nostop.pkl')
	# X, y = IO.unpickle(data_dir+'newsgroups/newsgroups_train_nostop.pkl')

	# dictionary = corpora.Dictionary(X)
	# IO.pickle(dictionary, data_dir+'newsgroups/newsgroups_dictionary.pkl')
	dictionary = IO.unpickle(data_dir+'newsgroups/newsgroups_dictionary.pkl')

	# sparse_X = [dictionary.doc2bow(x) for x in X]
	# IO.pickle((sparse_X,y), data_dir+'newsgroups/newsgroups_sparse_docs_nostop.pkl')
	sparse_X,y = IO.unpickle(data_dir+'newsgroups/newsgroups_sparse_docs_nostop.pkl')


	# corpus = [dictionary.doc2bow(text) for text in X]
	# IO.pickle(corpus, data_dir+'newsgroups/newsgroups_corpus.pkl')
	corpus = IO.unpickle(data_dir+'newsgroups/newsgroups_corpus.pkl')

	lda = gensim.models.ldamodel.LdaModel(corpus=corpus, id2word=dictionary, num_topics=num_topics)

	IO.pickle(lda, data_dir+'newsgroups/newsgroups_lda_{}.pkl'.format(num_topics))

	new_X = lda.inference(sparse_X)
	

def transform(X, dictionary, lda):
	"""
	assumes X is a list of words and contains no stop words
	"""
	X = remove_stop_words(X)
	sparse_X = [dictionary.doc2bow(x) for x in X]
	new_X = lda.inference(sparse_X)[0]
	
	return new_X	

def run_to_convert_newsgroups(num_topics=10):
	lda = IO.unpickle(data_dir+'newsgroups/newsgroups_lda_{}.pkl'.format(num_topics))
	X_train, y_train = IO.unpickle(data_dir+'newsgroups/newsgroups_train_nostop.pkl')
	dictionary = IO.unpickle(data_dir+'newsgroups/newsgroups_dictionary.pkl')
	lda = IO.unpickle(data_dir+'newsgroups/newsgroups_lda.pkl')

	sparse_X_train = [dictionary.doc2bow(x) for x in X_train]
	new_X_train = lda.inference(sparse_X_train)[0]
	IO.pickle((new_X_train,y_train), data_dir+'newsgroups/newsgroup_train_lda_transformed_{}.pkl'.format(num_topics))

	X_test, y_test = IO.unpickle(data_dir+'newsgroups/newsgroups_test.pkl')	
	new_X_test = transform(X_test,dictionary, lda)
	IO.pickle((new_X_test, y_test),data_dir+'newsgroups/newsgroup_test_lda_transformed_{}.pkl'.format(num_topics))


def test_classification():
	X_train, y_train = IO.unpickle(data_dir+'newsgroups/newsgroup_train_lda_transformed.pkl')
	X_test, y_test = IO.unpickle(data_dir+'newsgroups/newsgroup_test_lda_transformed.pkl')


	X_train, y_train = np.array(X_train), np.array(y_train)
	X_test, y_test = np.array(X_test), np.array(y_test)

	model = sklearn.linear_model.LogisticRegression()

	model.fit(X_train,y_train)
	print model.score(X_test,y_test)

	# X_train, y_train = IO.unpickle(data_dir+'newsgroups/newsgroups_train.pkl')
	# X_test, y_test = IO.unpickle(data_dir+'newsgroups/newsgroups_test.pkl')

	# X_train, y_train = np.array(X_train), np.array(y_train)
	# X_test, y_test = np.array(X_test), np.array(y_test)

	# model.fit(X_train,y_train)
	# print model.score(X_test,y_test)

if __name__ == '__main__':
	core_converter(num_topics=10)
	run_to_convert_newsgroups(num_topics=10)
	# test_classification()
	


	

	# print lda.print_topics(20)