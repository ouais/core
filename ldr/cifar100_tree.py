from utils import IO
from utils.misc import *
import preprocessing

from deepnn.cnn import *
from deepnn.algorithm import *
from samplers import *
from pylab import *


def test():

	rng = np.random.RandomState(666)

	batch_size = 128

	X_train,y_train =IO.unpickle(data_dir+"/cifar100/cifar100_train_c.pkl")
	X_train = preprocessing.flatten(X_train)

	X_test,y_test = IO.unpickle(data_dir+"/cifar100/cifar100_test_c.pkl")
	X_test = preprocessing.flatten(X_test)

	y_train = IO.unpickle(data_dir+'/cifar100/cifar-100-python/train')['coarse_labels']
	y_test = IO.unpickle(data_dir+'/cifar100/cifar-100-python/test')['coarse_labels']

	nn = CNN(input_size=32,kernel_size=5,batch_size=batch_size, pool_sizes=[5,5,5], pool_strides=[2,2,2], nkerns=[64,64,64],nnodes=[1000])
	nn.add_layer(100)	

	sampler = SupervisedSampler(rng,problem_size=10,y=y_train,batch_size=batch_size)

	algorithm = LDR_Pretrainer(nn,learning_rate=.01,batch_size=batch_size,sampler=sampler,max_n_batches=1000,f_momentum=.2,h_momentum=.9)
	losses = algorithm.fit(X_train,y_train)
	# plot(losses)
	# show()

	IO.pickle(nn,'models/cnn_cifar100_ldr.pkl')

	# print "algorithm: %s score: %.4f" % (str(bprop), bprop.score(X_test,y_test))
	

if __name__=='__main__':
	test()