import numpy as np
import cPickle
from scipy import ndimage as nd

from utils import IO
from utils.misc import *
from skimage.filter import gabor_kernel

from sklearn import linear_model, ensemble
from sklearn.decomposition import RandomizedPCA


def gen_kernels():
	kernels = []
	sigma = 1
	frequency = .05
	for theta in range(4):
		theta = theta / 4. * np.pi
		kernel = np.real(gabor_kernel(frequency, theta=theta,sigma_x=sigma, sigma_y=sigma))
		kernels.append(kernel)
	return kernels


def convert_cifar100():

	kernels = gen_kernels()
	print len(kernels)

	# X_train, y_train = IO.unpickle(data_dir+ 'cifar100/cifar100_train.pkl')
	X_test , y_test  = IO.unpickle(data_dir+ 'cifar100/cifar100_test.pkl')

	# print np.array(compute_feats(np.reshape(x,(32,32)),kernels)).shape
	# X_train_new = [compute_feats(np.reshape(x,(32,32)),kernels) for x in X_train]
	X_test_new  = [compute_feats(np.reshape(x,(32,32)),kernels) for x in X_test]

	# with open(data_dir + 'cifar100/cifar_100_train_gabor.pkl','wb') as fp:
		# cPickle.dump((X_train_new ,y_train ),fp)

	with open(data_dir + 'cifar100/cifar_100_test_gabor.pkl','wb') as fp:
		cPickle.dump((X_test_new ,y_test ),fp)


def reduce_with_pca():
	n_components = 10
	pca = RandomizedPCA(n_components = n_components)
	pca = IO.unpickle(data_dir+'cifar100/cifar100_pca.pkl')

	# X_train, y_train = IO.unpickle(data_dir+ 'cifar100/cifar_100_train_gabor.pkl')
	X_test , y_test  = IO.unpickle(data_dir+ 'cifar100/cifar_100_test_gabor.pkl')

	# X_train = np.array([np.array(x).flatten() for x in X_train])
	X_test  = np.array([np.array(x).flatten() for x in X_test ])

	# pca.fit(X_train)

	# IO.pickle(pca, data_dir+'cifar100/cifar100_pca.pkl')

	# X_train_new = np.array([pca.transform(x) for x in X_train])
	X_test_new  = np.array([pca.transform(x) for x in X_test ])

	# IO.pickle((X_train_new,y_train),data_dir + 'cifar100/cifar_100_pca'+str(n_components)+'_train.pkl')
	IO.pickle((X_test_new ,y_test) ,data_dir + 'cifar100/cifar100_pca'+str(n_components)+'_test.pkl' )

def compute_classification():

	X_train, y_train = IO.unpickle(data_dir+ 'cifar100/cifar_100_pca10_train.pkl')
	X_test , y_test  = IO.unpickle(data_dir+ 'cifar100/cifar_100_pca10_test.pkl')

	X_train = np.array([x.flatten() for x in X_train],dtype = 'float32')
	X_test  = np.array([x.flatten() for x in X_test ],dtype = 'float32')

	IO.pickle((X_train,y_train),data_dir+ 'cifar100/cifar_100_pca10_train.pkl')
	IO.pickle((X_test ,y_test) ,data_dir+ 'cifar100/cifar_100_pca10_test.pkl')

	model = ensemble.RandomForestClassifier()
	model.fit(X_train,y_train)
	print model.score(X_test,y_test)


def compute_feats(image, kernels):
	feats = []

	for k, kernel in enumerate(kernels):

		filtered = nd.convolve(image, kernel, mode='wrap')
		feats.extend(filtered)

	return feats


if __name__ == '__main__':
	# convert_cifar100()
	# reduce_with_pca()
	compute_classification()