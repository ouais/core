import numpy as np
import pylab as plt

def newsgroups_results():
	keys = [1,2,5,10,20,50,100]

	LDR = {1: .4877, 2: .5435, 5: .6067, 10: .6395, 20: .6587, 50: .6773, 100: .6878}
	ELLA = {1: .4195, 2: .4915, 5: .5448, 10: .5663, 20: .5744, 50: .5840, 100: .5893}
	STL  = {1: .4532, 2: .5170, 5: .5790, 10: 0.6158, 20: 0.6551, 50: 0.6926, 100: 0.7068}

	labels = ['LLDR', 'ELLA', 'STL']

	save_graphs(keys,[LDR, ELLA, STL],labels,'materials/newsgroups.pdf')

def cifar100_results():
	keys = [1,2,5,10,20,50,100]

	LDR  = {1: .2995, 2: .3329, 5: .3755, 10: .4053, 20: .4334, 50: .4652, 100: .4833}	
	ELLA = {1: .2490, 2: .2757, 5: .3060, 10: .3244, 20: .3373, 50: .3487, 100: .3538}
	STL  = {1: .2741, 2: .2803, 5: .3002, 10: .3423, 20: .3761, 50: .4093, 100: .4230}

	labels = ['LLDR', 'ELLA', 'STL']

	save_graphs(keys,[LDR, ELLA, STL],labels,'materials/cifar100.pdf')

def save_graphs(keys, results,labels, file_name,scale=1):
	markers = '^x*+'
	
	plt.rc('text', usetex=False)
	plt.rc('font', family='serif',size=6)

	fig = plt.figure(figsize=(4,1.7))
	ax = fig.add_subplot(1,1,1)

	xs = [x for x in keys]
	xs.sort()


	for i in xrange(len(results)):
		results_i = results[i]

		f1 = np.array([np.mean(results_i[x]) for x in xs])
		f1_err = []

		for x in xs:			
			# errs = [np.std(results_i[x][y*20:(y+1)*20]) for y in xrange(len(results_i[x])/20)]			
			errs = np.std(results_i[x])
			f1_err.append(np.mean(errs))

		f1_err = np.array(f1_err)

		scaled_xs = [x*scale for x in xs]
		plt.errorbar(scaled_xs,f1,yerr=f1_err,fmt='-'+markers[i%len(markers)],linewidth=.5,markersize=3, label=labels[i])
		# plt.plot(xs,f1,'-'+markers[i%len(markers)],linewidth=.5,markersize=3, label=labels[i])

	plt.xlabel('number of samples (log)')
	ax.set_xscale('log')
	ax.xaxis.grid(True,'both')
	plt.ylabel('accuracy')
	# get handles
	handles, labels = ax.get_legend_handles_labels()
	# remove the errorbars
	handles = [h[0] for h in handles]

	plt.legend(handles,labels, loc=4)
	

	plt.savefig(file_name,bbox_inches='tight')


if __name__ == '__main__':
	newsgroups_results()
	cifar100_results()