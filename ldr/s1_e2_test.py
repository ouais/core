import numpy as np
from utils import IO
from utils.misc import data_dir
from ldr.experiment_core import load_dataset, execute_test, transform_dataset
from plot_graphs import save_graphs

def s1_e2_core(name):
	nets, results, testing_problems = IO.unpickle("temp/"+name+"_s1_e2.pkl")

	Xy_train = load_dataset(data_dir+"/mnist/mnist_train.pkl",normalize=True)	
	Xy_train = Xy_train[0][-10000:], Xy_train[1][-10000:]
	Xy_test = load_dataset(data_dir+"/mnist/mnist_test.pkl",normalize=True)	

	# n_samples_list = [1,2,5,10,20,50,100, 500, 1000]	
	n_samples_list = [1,2,5,10,20,50, 100]	

	rng = np.random.RandomState(666)

	total_results = {}
	total_results.update([(x,[]) for x in n_samples_list])
	i = 0
	print testing_problems[:10]
	for net in nets:

		transform_dataset(Xy_train, net,'temp/temp_train.pkl')
		transform_dataset(Xy_test, net,'temp/temp_test.pkl')

		Xy_train_new = IO.unpickle('temp/temp_train.pkl')
		Xy_test_new = IO.unpickle('temp/temp_test.pkl')
		
		results = execute_test(rng, Xy_train_new,Xy_test_new, testing_problems = [testing_problems[i]], n_samples_list = n_samples_list, n_estimations = 20)

		for x in results:
			total_results[x].extend(results[x])

		i +=1

	return total_results

def s1_e2_single():
	nets, results, testing_problems = IO.unpickle("temp/LDR_s1_e2.pkl")
	n_samples_list = [1,2,5,10,20,50, 100]
	rng = np.random.RandomState(666)

	Xy_train = load_dataset(data_dir+"/mnist/mnist_train.pkl",normalize=True)	
	Xy_train = Xy_train[0][-10000:], Xy_train[1][-10000:]
	Xy_test = load_dataset(data_dir+"/mnist/mnist_test.pkl",normalize=True)	

	results = execute_test(rng, Xy_train,Xy_test, testing_problems = testing_problems[0:10], n_samples_list = n_samples_list, n_estimations = 10)

	IO.pickle(results,'temp/s1_e2_stl_results.pkl')

	return results

def s1_e2_autoencoder():
	nets, results, testing_problems = IO.unpickle("temp/LDR_s1_e2.pkl")
	n_samples_list = [1,2,5,10,20,50, 100 ,500, 1000]
	rng = np.random.RandomState(666)

	Xy_train = IO.unpickle("data/mnist_train_da.pkl")	
	Xy_train = Xy_train[0][-10000:], Xy_train[1][-10000:]
	Xy_test = IO.unpickle("data/mnist_test_da.pkl")	

	results = execute_test(rng, Xy_train,Xy_test, testing_problems = testing_problems[0:10], n_samples_list = n_samples_list, n_estimations = 1)

	return results

def s1_e2_test():
	results_LDR = s1_e2_core("LDR")
	results_baxter = s1_e2_core("baxter")	
	IO.pickle((results_LDR,results_baxter),'temp/experiment_2_results_exp.pkl')
	LDR,baxter = IO.unpickle('temp/experiment_2_results_exp.pkl')
	STL = IO.unpickle('temp/s1_e2_stl_results.pkl')
	save_graphs(LDR.keys(), [LDR, baxter] ,['LLDR', 'Baxter'],'materials/s1_e2_exp.pdf',scale=4)

if __name__ == '__main__':
	s1_e2_test()
	# print s1_e2_autoencoder()
	# print s1_e2_single()