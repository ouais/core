import numpy as  np
import scipy.io
from utils import IO
from utils.misc import *
from deepnn.NN import NeuralNetwork
from deepnn.cnn import CNN
from deepnn.algorithm import LDR_Pretrainer
from deepnn.costs import *
import preprocessing
from preprocessing import normalize_dataset
from sklearn.decomposition import PCA

from experiment_core import execute_test, transform_dataset
from test_multitask import downsample
from samplers import *


def split_dataset_classwise(Z, class_set):

	Z_1 = filter(lambda x: not (x[1] in class_set),Z)
	Z_2 = filter(lambda x: x[1] in class_set,Z)	

	return Z_1, Z_2

def gen_testing_problems(rng, n_classes, n_problems ,order):
	problems = []
	while len(problems) != n_problems:
		problem = tuple(rng.choice(n_classes, order,replace=False))
		if not (problem in problems):
			problems.append(problem)

	return problems

def save_matlab(X_train, y_train, X_valid, y_valid, X_test, y_test, testing_problems,n_components):
	path = 'matlab/cifar_100_{}.mat'.format(n_components)

	scipy.io.savemat(path, mdict={
		'X_train': X_train,
		'X_valid': X_valid,
		'X_test': X_test, 
		'y_train': y_train,
		'y_valid': y_valid,
		'y_test': y_test,
		'testing_problems': testing_problems})

	print 'saved data to matlab in {}'.format(path)

def run_cifar_ldr(train_path, test_path,n_components = 10, problem_size=5,n_trials=1):

	rng = np.random.RandomState(666)

	X, y = IO.unpickle(train_path)
	X = preprocessing.flatten(X)

	valid_indices = downsample(rng,y,100)
	X_valid, y_valid = X[valid_indices], y[valid_indices]
	train_indices = np.array(list(set(range(len(y))) - set(valid_indices)),dtype='int32')
	X_train, y_train = X[train_indices], y[train_indices]

	X_test, y_test = IO.unpickle(test_path)
	X_test = preprocessing.flatten(X_test)

	testing_problems = gen_testing_problems(rng,n_classes=100,n_problems=100,order=problem_size)

	if n_components != X_train.shape[1]:
		print 'fitting PCA...'
		pca = PCA(n_components = n_components)
		pca.fit(X_train)

		X_train = pca.transform(X_train)
		X_test = pca.transform(X_test)
		X_valid = pca.transform(X_valid)

	X_train, y_train = np.array(X_train,'float32'), np.array(y_train)
	X_valid, y_valid = np.array(X_valid,'float32'), np.array(y_valid)
	X_test, y_test = np.array(X_test,'float32'), np.array(y_test)

	print X_train.shape, X_valid.shape, X_test.shape

	Z_train = zip(X_train,y_train)
	Z_valid = zip(X_valid,y_valid)
	Z_test = zip(X_test,y_test)

	save_matlab(X_train, y_train, X_valid, y_valid, X_test, y_test, testing_problems,n_components)

	n_samples_list = [1,2,5,10,20,50,100]

	total_accs = []

	results = {}
	results.update([(x,[]) for x in n_samples_list])

	for trial in xrange(n_trials):
		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		N = X_train.shape[1]
		nn = NeuralNetwork(layers_sizes=[N,500,500])
		#note that the default for adding layers is a softmax layer while during construction its a rectified linear layer
		nn.add_layer(problem_size)		

		sampler = NotSupervisedSampler(rng,-1,y_train,problems=testing_problems)

		cost = CrossEntropyLoss

		algorithm = LDR_Pretrainer(nn,learning_rate=.01,batch_size=50,sampler=sampler,max_n_batches=1000, cost = cost)
		loss = algorithm.fit(X_train,y_train)	

		total_results = []

		X_valid, y_valid = zip(*Z_valid)

		transform_dataset((X_valid, y_valid), nn,'temp/temp_train.pkl')
		transform_dataset((X_test, y_test), nn,'temp/temp_test.pkl')

		Xy_valid_new = IO.unpickle('temp/temp_train.pkl')
		Xy_test_new = IO.unpickle('temp/temp_test.pkl')

		trial_results = execute_test(rng, Xy_valid_new, Xy_test_new, testing_problems = testing_problems, n_samples_list = n_samples_list, n_estimations = 10)

		for x in trial_results:
			results[x].append(trial_results[x])

	for i in xrange(len(n_samples_list)):
		n_samples = n_samples_list[i]
		print "average score: %.4f with %d samples" % (np.mean(results[n_samples]),n_samples)

	return results


def run_cifar_stl(train_path, test_path,n_components = 10,problem_size=5,n_trials=1):

	rng = np.random.RandomState(666)

	X, y = IO.unpickle(train_path)
	X = preprocessing.flatten(X)

	valid_indices = downsample(rng,y,100)
	X_valid, y_valid = X[valid_indices], y[valid_indices]
	train_indices = np.array(list(set(range(len(y))) - set(valid_indices)),dtype='int32')
	X_train, y_train = X[train_indices], y[train_indices]

	X_test, y_test = IO.unpickle(test_path)
	X_test = preprocessing.flatten(X_test)

	testing_problems = gen_testing_problems(rng,n_classes=100,n_problems=100,order=problem_size)

	pca = PCA(n_components = n_components)
	pca.fit(X_train)

	X_train = pca.transform(X_train)
	X_test = pca.transform(X_test)
	X_valid = pca.transform(X_valid)

	X_train, y_train = np.array(X_train,'float32'), np.array(y_train)
	X_valid, y_valid = np.array(X_valid,'float32'), np.array(y_valid)
	X_test, y_test = np.array(X_test,'float32'), np.array(y_test)

	print X_train.shape, X_valid.shape, X_test.shape

	Z_train = zip(X_train,y_train)
	Z_valid = zip(X_valid,y_valid)
	Z_test = zip(X_test,y_test)

	n_samples_list = [1,2,5,10,20,50,100]

	total_accs = []	

	results = {}
	results.update([(x,[]) for x in n_samples_list])

	for trial in xrange(n_trials):
		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		print X_train.shape

		total_results = []

		X_valid, y_valid = zip(*Z_valid)

		trial_results = execute_test(rng, (X_valid, y_valid), (X_test, y_test), testing_problems = testing_problems, n_samples_list = n_samples_list, n_estimations = 10)

		for x in trial_results:
			results[x].append(trial_results[x])

	for i in xrange(len(n_samples_list)):
		n_samples = n_samples_list[i]
		print "average score: %.4f with %d samples" % (np.mean(results[n_samples]),n_samples)

	return results


def run_cifar_convolutional_ldr(train_path, test_path,n_components = 10, problem_size=5,n_trials=1):

	rng = np.random.RandomState(666)

	X, y = IO.unpickle(train_path)
	X = preprocessing.flatten(X)

	valid_indices = downsample(rng,y,100)
	X_valid, y_valid = X[valid_indices], y[valid_indices]
	train_indices = np.array(list(set(range(len(y))) - set(valid_indices)),dtype='int32')
	X_train, y_train = X[train_indices], y[train_indices]

	X_test, y_test = IO.unpickle(test_path)
	X_test = preprocessing.flatten(X_test)

	testing_problems = gen_testing_problems(rng,n_classes=100,n_problems=100,order=problem_size)

	if n_components != X_train.shape[1]:
		print 'fitting PCA...'
		pca = PCA(n_components = n_components)
		pca.fit(X_train)

		X_train = pca.transform(X_train)
		X_test = pca.transform(X_test)
		X_valid = pca.transform(X_valid)

	X_train, y_train = np.array(X_train,'float32'), np.array(y_train)
	X_valid, y_valid = np.array(X_valid,'float32'), np.array(y_valid)
	X_test, y_test = np.array(X_test,'float32'), np.array(y_test)

	print X_train.shape, X_valid.shape, X_test.shape

	Z_train = zip(X_train,y_train)
	Z_valid = zip(X_valid,y_valid)
	Z_test = zip(X_test,y_test)

	save_matlab(X_train, y_train, X_valid, y_valid, X_test, y_test, testing_problems,n_components)

	n_samples_list = [1,2,5,10,20,50,100]

	total_accs = []

	results = {}
	results.update([(x,[]) for x in n_samples_list])

	for trial in xrange(n_trials):
		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		N = X_train.shape[1]

		sampler = NotSupervisedSampler(rng,-1,y_train,problems=testing_problems,batch_size=128)

		cost = CrossEntropyLoss

		nn = CNN(input_size=32,kernel_size=5,batch_size=128, pool_sizes=[5,5,5], pool_strides=[2,2,2], nkerns=[96,96,96],drops=[0,0,0,.5,.5],nnodes=[1000])
		nn.add_layer(100)	

		algorithm = LDR_Pretrainer(nn,learning_rate=.01,batch_size=128,max_filter_norms=[4,4,4],max_row_norms=[4,4,4],sampler=sampler,K=30,max_n_batches=1000,f_momentum=.2,h_momentum=.9)
		loss = algorithm.fit(X_train,y_train)	

		total_results = []

		X_valid, y_valid = zip(*Z_valid)

		nn.set_test_mode()

		transform_dataset((X_valid, y_valid), nn,'temp/temp_train.pkl')
		transform_dataset((X_test, y_test), nn,'temp/temp_test.pkl')

		Xy_valid_new = IO.unpickle('temp/temp_train.pkl')
		Xy_test_new = IO.unpickle('temp/temp_test.pkl')

		trial_results = execute_test(rng, Xy_valid_new, Xy_test_new, testing_problems = testing_problems, n_samples_list = n_samples_list, n_estimations = 10)

		for x in trial_results:
			results[x].append(trial_results[x])

	for i in xrange(len(n_samples_list)):
		n_samples = n_samples_list[i]
		print "average score: %.4f with %d samples" % (np.mean(results[n_samples]),n_samples)

	return results

def save_results_plot(results_ldr_path,results_stl_path,labels,out_file,means_stds=[]):

	results_ldr = IO.unpickle(results_ldr_path)
	results_stl = IO.unpickle(results_stl_path)

	save_graphs(results_ldr.keys(), [results_ldr, results_stl] ,labels,out_file,means_stds,scale=1)

def save_results_plot_conv(results_ldr_path,results_stl_path,results_conv_path, labels,out_file,means_stds=[]):

	results_ldr = IO.unpickle(results_ldr_path)
	results_stl = IO.unpickle(results_stl_path)
	results_conv = IO.unpickle(results_conv_path)

	save_graphs(results_ldr.keys(), [results_ldr, results_stl,results_conv] ,labels,out_file,means_stds,scale=1)


def run_10_cifar():
	n_components = 10

	# train_path = data_dir+"/cifar100/cifar100_train_c.pkl"
	# test_path  = data_dir+"/cifar100/cifar100_test_c.pkl"

	# results_stl = run_cifar_stl(train_path,test_path,n_components = n_components, problem_size=5,n_trials=1)
	# results_ldr = run_cifar_ldr(train_path,test_path,n_components = n_components, problem_size=5,n_trials=1)	

	# IO.pickle(results_ldr,'results/cifar100_ll_ldr_results_10.pkl')
	# IO.pickle(results_stl,'results/cifar100_ll_stl_results_10.pkl')

	results_ldr_path = 'results/cifar100_ll_ldr_results_10.pkl'
	results_stl_path = 'results/cifar100_ll_stl_results_10.pkl'

	out_file = 'materials/cifar100_ll_10.pdf'

	ELLA_means = [0.2574,0.2924,0.3249,0.3438,0.3606,0.3707,0.3768]
	ELLA_stds  = [.0618, 0.0633, 0.0658, 0.0622, 0.0619, 0.0599, 0.0576]

	means_stds = [(ELLA_means,ELLA_stds)]

	labels = ['Our Algorithm', 'Logistic Regression', 'ELLA']

	save_results_plot(results_ldr_path,results_stl_path,labels, out_file,means_stds)


def run_100_cifar():
	n_components = 100

	# train_path = data_dir+"/cifar100/cifar100_train_c.pkl"
	# test_path  = data_dir+"/cifar100/cifar100_test_c.pkl"

	# results_ldr = run_cifar_ldr(train_path,test_path,n_components = n_components, problem_size=5,n_trials=1)
	# results_stl = run_cifar_stl(train_path,test_path,n_components = n_components, problem_size=5,n_trials=1)

	# IO.pickle(results_ldr,'results/cifar100_ll_ldr_results_100.pkl')
	# IO.pickle(results_stl,'results/cifar100_ll_stl_results_100.pkl')

	results_ldr_path = 'results/cifar100_ll_ldr_results_100.pkl'
	results_stl_path = 'results/cifar100_ll_stl_results_100.pkl'

	out_file = 'materials/cifar100_ll_100.pdf'


	ELLA_means = [0.2726,0.3114,0.3507,0.3739,0.3867,0.3980,0.4016]
	ELLA_stds  = [.0676, 0.0703, 0.0686, 0.0663, 0.0652, 0.0611, 0.0607]

	means_stds = [(ELLA_means,ELLA_stds)]

	labels = ['Our Algorithm', 'Logistic Regression', 'ELLA']

	save_results_plot(results_ldr_path,results_stl_path,labels, out_file,means_stds)


def run_3072_cifar():
	n_components = 3072

	train_path = data_dir+"/cifar100/cifar100_train_c.pkl"
	test_path  = data_dir+"/cifar100/cifar100_test_c.pkl"

	# results_ldr = run_cifar_ldr(train_path,test_path,n_components = n_components, problem_size=5,n_trials=1)
	# results_stl = run_cifar_stl(train_path,test_path,n_components = n_components, problem_size=5,n_trials=1)
	# results_ldr_conv = run_cifar_convolutional_ldr(train_path,test_path,n_components = n_components, problem_size=5,n_trials=1)

	# IO.pickle(results_ldr,'results/cifar100_ll_ldr_results_3072.pkl')
	# IO.pickle(results_stl,'results/cifar100_ll_stl_results_3072.pkl')
	# IO.pickle(results_ldr_conv,'results/cifar100_ll_stl_results_conv_3072.pkl')

	results_ldr_path = 'results/cifar100_ll_ldr_results_3072.pkl'
	results_stl_path = 'results/cifar100_ll_stl_results_3072.pkl'
	results_con_path = 'results/cifar100_ll_stl_results_conv_3072.pkl'

	out_file = 'materials/cifar100_ll_3072.pdf'


	labels = ['Our Algorithm-nn', 'Logistic Regression', 'Our Algorithm-Conv']

	save_results_plot_conv(results_ldr_path,results_stl_path,results_con_path,labels, out_file)

def plot_all():
	results_ldr_10 = IO.unpickle('results/cifar100_ll_ldr_results_10.pkl')
	results_stl_10 = IO.unpickle('results/cifar100_ll_stl_results_10.pkl')
	ELLA_means_10 = [0.2574,0.2924,0.3249,0.3438,0.3606,0.3707,0.3768]
	ELLA_stds_10  = [.0618, 0.0633, 0.0658, 0.0622, 0.0619, 0.0599, 0.0576]

	d_10 = {'LDR':results_ldr_10, 'LR': results_stl_10, 'ELLA': [(ELLA_means_10,ELLA_stds_10)]}

	results_ldr_100 = IO.unpickle('results/cifar100_ll_ldr_results_100.pkl')
	results_stl_100 = IO.unpickle('results/cifar100_ll_stl_results_100.pkl')
	ELLA_means_100 = [0.2726,0.3114,0.3507,0.3739,0.3867,0.3980,0.4016]
	ELLA_stds_100 = [.0676, 0.0703, 0.0686, 0.0663, 0.0652, 0.0611, 0.0607]

	d_100 = {'LDR':results_ldr_100, 'LR': results_stl_100, 'ELLA': [(ELLA_means_100,ELLA_stds_100)]}

	results_ldr_3072 = IO.unpickle('results/cifar100_ll_ldr_results_3072.pkl')
	results_stl_3072 = IO.unpickle('results/cifar100_ll_stl_results_3072.pkl')
	results_con_3072 = IO.unpickle('results/cifar100_ll_stl_results_conv_3072.pkl')

	d_3072 = {'LDR':results_ldr_3072, 'LR': results_stl_3072, 'CONV': results_con_3072}


	results = {'10':d_10, '100': d_100,'3072':d_3072}

	save_graph_with_all_data(results,'materials/cifar_ll_all.pdf')


def save_graph_with_all_data(results,file_name):

	n_graphs = len(results.keys())


	ax_label = {'10': 'low dimensionality (d=10)', '100': 'medium dimensionality (d=100)', '3072': 'high dimensionality (d=3072)'}
	labels  = {'LDR':'LeaDR-ANN', 'ELLA':'ELLA','CONV':'LeaDR-Conv','LR':'Logistic Regression'}
	markers = {'LDR':'^', 'ELLA':'x','CONV':'*','LR':'+'}
	colors  = {'LDR':'b', 'ELLA':'r','CONV':'m','LR':'g'}
	
	plt.rc('text', usetex=False)
	plt.rc('font', family='serif',size=7)

	fig = plt.figure(figsize=(9,1.5))
	
	graph_id = 1
	lines = []
	legend_labels = []
	for key in results:
		result = results[key]
		means_stds = []
		local_results = ['LDR','LR']

		if 'ELLA' in result.keys():
			means_stds = result['ELLA']

		if 'CONV' in result.keys():
			local_results.append('CONV')


		ax = fig.add_subplot(1,n_graphs,graph_id)

		xs = [x for x in result['LDR'].keys()]
		xs.sort()

		z = 0
		for i in xrange(len(local_results)):
			results_i = result[local_results[i]]

			f1 = np.array([np.mean(results_i[x]) for x in xs])
			f1_err = []

			for x in xs:			
				# errs = [np.std(results_i[x][y*20:(y+1)*20]) for y in xrange(len(results_i[x])/20)]			
				f1_err.append(np.std(results_i[x]))

			f1_err = np.array(f1_err)
			
			line = plt.errorbar(
				xs,f1,yerr=f1_err,fmt='-'+markers[local_results[i]],
				color = colors[local_results[i]],
				linewidth=.5,markersize=5, label=labels[local_results[i]])

			if not (labels[local_results[i]] in legend_labels):
				lines.append(line[0])
				legend_labels.append(labels[local_results[i]])

			z += 1
			# plt.plot(xs,f1,'-'+markers[i%len(markers)],linewidth=.5,markersize=3, label=labels[i])

		for i in xrange(len(means_stds)):
			means,stds = means_stds[i]

			line = plt.errorbar(xs,means,yerr=stds,fmt='-x',color='r',linewidth=.5,markersize=3, label='ELLA')

			if not ('ELLA' in legend_labels):
				lines.append(line[0])
				legend_labels.append('ELLA')
			z +=1 

		

		ax.set_title(ax_label[key])
		ax.set_xlabel('samples per class (log)')
		ax.set_xscale('log')
		ax.xaxis.grid(True,'both')

		if graph_id == 1:
			ax.set_ylabel('accuracy per task')

		x1,x2,y1,y2 = ax.axis()
		ax.axis((0.9,x2+ 10,.2,.9))

		graph_id+=1

	leg = fig.legend((lines),
			(legend_labels),
			bbox_to_anchor=[.25, 1.14])

	# get handles
	handles, labels = ax.get_legend_handles_labels()
	# handles, labels = leg.get_legend_handler_map()
	# remove the errorbars
	handles = [h[0] for h in handles]


	# plt.legend(handles,labels, loc=4)
	

	plt.savefig(file_name,bbox_inches='tight')



def save_graphs(keys, results,labels, file_name, means_stds=None,scale=1):
	markers = '^x*+'
	colors  = 'bgm'
	
	plt.rc('text', usetex=False)
	plt.rc('font', family='serif',size=7)

	fig = plt.figure(figsize=(4,1.7))
	ax = fig.add_subplot(1,1,1)

	xs = [x for x in keys]
	xs.sort()

	scaled_xs = [x*scale for x in xs]

	z = 0
	for i in xrange(len(results)):
		results_i = results[i]

		f1 = np.array([np.mean(results_i[x]) for x in xs])
		f1_err = []

		for x in xs:			
			# errs = [np.std(results_i[x][y*20:(y+1)*20]) for y in xrange(len(results_i[x])/20)]			
			f1_err.append(np.std(results_i[x]))

		f1_err = np.array(f1_err)
		
		plt.errorbar(
			scaled_xs,f1,yerr=f1_err,fmt='-'+markers[z%len(markers)],
			color = colors[z%len(colors)],
			linewidth=.5,markersize=3, label=labels[z])
		z += 1
		# plt.plot(xs,f1,'-'+markers[i%len(markers)],linewidth=.5,markersize=3, label=labels[i])

	for i in xrange(len(means_stds)):
		means,stds = means_stds[i]

		plt.errorbar(scaled_xs,means,yerr=stds,fmt='-'+markers[z%len(markers)],linewidth=.5,markersize=3, label=labels[z])
		z +=1 


	plt.xlabel('samples per class (log)')
	ax.set_xscale('log')
	ax.xaxis.grid(True,'both')
	# plt.ylabel('accuracy')

	x1,x2,y1,y2 = ax.axis()
	ax.axis((0.9,x2+ 10,y1,y2))

	# get handles
	handles, labels = ax.get_legend_handles_labels()
	# remove the errorbars
	handles = [h[0] for h in handles]	

	plt.legend(handles,labels, loc=4)
	

	plt.savefig(file_name,bbox_inches='tight')


if __name__== '__main__':
	# run_10_cifar()
	# run_100_cifar()
	# run_3072_cifar()
	plot_all()

	