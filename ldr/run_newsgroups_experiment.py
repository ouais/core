

import numpy
import scipy

from utils.IO import *
from utils.misc import *
from utils.utils import *

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from utils import *
from random import shuffle

from pylab import *

from ldr.samplers import SupervisedSampler
from deepnn.algorithm import Backpropagation, LDR_Pretrainer
from deepnn.NN import *

from test_transfer_challenge import test_transfer_with_nn
from collections import Counter


def relabel_to_coarse(Xy_train, Xy_test):

	X_train, y_train = Xy_train
	X_test , y_test  = Xy_test

	partitions = ['comp', 'sci', 'rec','talk']

	names = ['alt.atheism', 'comp.graphics', 'comp.os.ms-windows.misc', 'comp.sys.ibm.pc.hardware', 'comp.sys.mac.hardware',
	 'comp.windows.x', 'misc.forsale', 'rec.autos', 'rec.motorcycles', 'rec.sport.baseball', 'rec.sport.hockey', 'sci.crypt',
	  'sci.electronics', 'sci.med', 'sci.space', 'soc.religion.christian', 'talk.politics.guns', 'talk.politics.mideast', 'talk.politics.misc', 'talk.religion.misc']

	class_map = {}

	for i in xrange(len(names)):
		if names[i].split('.')[0] in partitions:
			class_map[i] = partitions.index(names[i].split('.')[0])
		else:
			class_map[i] = -1

	y_train = np.array([class_map[x] for x in y_train])
	y_test  = np.array([class_map[x] for x in y_test])

	train_indices = np.where(y_train >= 0 )[0]
	test_indices = np.where(y_test >= 0 )[0]

	X_train, y_train = X_train[train_indices], y_train[train_indices]
	X_test, y_test  = X_test[test_indices], y_test[test_indices]

	Xy_train = (X_train,y_train)
	Xy_test  = (X_test ,y_test)

	return Xy_train, Xy_test

def relabel_to_fine(Xy_train, Xy_test):

	X_train, y_train = Xy_train
	X_test , y_test  = Xy_test

	partitions = ['comp', 'sci', 'rec','talk']

	names = ['alt.atheism', 'comp.graphics', 'comp.os.ms-windows.misc', 'comp.sys.ibm.pc.hardware', 'comp.sys.mac.hardware',
	 'comp.windows.x', 'misc.forsale', 'rec.autos', 'rec.motorcycles', 'rec.sport.baseball', 'rec.sport.hockey', 'sci.crypt',
	  'sci.electronics', 'sci.med', 'sci.space', 'soc.religion.christian', 'talk.politics.guns', 'talk.politics.mideast', 'talk.politics.misc', 'talk.religion.misc']

	class_map = {}

	for i in xrange(len(names)):
		if names[i].split('.')[0] in partitions:
			class_map[i] = partitions.index(names[i].split('.')[0])
		else:
			class_map[i] = -1

	y_train = np.array([class_map[x] for x in y_train])
	y_test  = np.array([class_map[x] for x in y_test])

	train_indices = np.where(y_train >= 0 )[0]
	test_indices = np.where(y_test >= 0 )[0]

	X_train, y_train = X_train[train_indices], y_train[train_indices]
	X_test, y_test  = X_test[test_indices], y_test[test_indices]

	Xy_train = (X_train,y_train)
	Xy_test  = (X_test ,y_test)

	return Xy_train, Xy_test


def monitor(e,model,X,y):
	model.set_test_mode()
	if e % 5 == 0:
		print '{} {:.2f}'.format(e,100*model.score(X,y))
	model.set_train_mode()

def test_newsgroups_once():

	Xy_train = IO.unpickle(data_dir+"/newsgroups/newsgroups_train_bow.pkl") 
	Xy_test = IO.unpickle(data_dir+"/newsgroups/newsgroups_test_bow.pkl")

	Xy_train, Xy_test = relabel_to_coarse(Xy_train,Xy_test)

	X_train, y_train = Xy_train
	X_test , y_test = Xy_test	

	nn = NeuralNetwork([X_train.shape[1],1000,1000],activations=[lambda x: T.maximum(x,0.0), lambda x: T.maximum(x,0.0)],drops=[.8,0.5,.5])
	nn.add_layer(20)

	bprop = Backpropagation(nn,epochs=100,max_row_norms=[2,2,2],learning_rate = .01,momentum=.9, batch_size=200)
	bprop.add_epoch_monitor(lambda e: monitor(e,nn,X_test,y_test))
	losses = bprop.fit(X_train,y_train)

	plot(losses)
	show()

	IO.pickle(nn,'models/nn_newsgroups_bprop.pkl')

	print "train score: %.4f, test score: %.4f" % (bprop.score(X_train,y_train), bprop.score(X_test,y_test))


def train_models(algorithms,Xy_train, Xy_test,batch_size):

	X_train, y_train = Xy_train
	X_test , y_test  = Xy_test

	for algorithm in algorithms:
		nn = NeuralNetwork([X_train.shape[1],1000,1000],activations=[lambda x: T.maximum(x,0.0), lambda x: T.maximum(x,0.0)],drops=[.8,0.5,.5])
		nn.add_layer(20)

		ialgorithm = algorithm(nn)
		losses = ialgorithm.fit(X_train,y_train)
		plot(losses)
		show()

		nn.set_test_mode()

		IO.pickle(nn,'models/nn_newsgroups_coarse_{}.pkl'.format(str(ialgorithm)))


def newsgroups_experiment_train():
	batch_size = 200
	rng = np.random.RandomState(666)

	Xy_train = IO.unpickle(data_dir+"/newsgroups/newsgroups_train_bow.pkl") 
	Xy_test = IO.unpickle(data_dir+"/newsgroups/newsgroups_test_bow.pkl")

	Xy_train, Xy_test = relabel_to_coarse(Xy_train,Xy_test)

	X_train, y_train = Xy_train
	X_test , y_test = Xy_test

	sampler = SupervisedSampler(rng,problem_size=4,y=y_train,batch_size=batch_size)
	LDR = lambda nn: LDR_Pretrainer(nn,learning_rate=.01,batch_size=batch_size,sampler=sampler,max_n_batches=1000, K=50, f_momentum=.2,h_momentum=.9)
	bprop = lambda nn: Backpropagation(nn,learning_rate=.01, epochs=100,momentum=.9,batch_size=batch_size)

	algorithms = [bprop]
	train_models(algorithms, (X_train,y_train),(X_test,y_test),batch_size)

def newsgroups_experiment_test():
	batch_size = 128
	rng = np.random.RandomState(666)
	n_samples = [1,2,5,10,20,50,100,200]
	# n_samples = [500]
	
	output_file = 'materials/coarse_to_fine_newsgroups.pdf'

	nns = [IO.unpickle('models/nn_newsgroups_coarse_LDR.pkl'),IO.unpickle('models/nn_newsgroups_coarse_Backpropagation.pkl')]

	Xy_train = IO.unpickle(data_dir+"/newsgroups/newsgroups_train_bow.pkl") 
	Xy_test = IO.unpickle(data_dir+"/newsgroups/newsgroups_test_bow.pkl")

	Xy_train, Xy_test = relabel_to_fine(Xy_train,Xy_test)

	print Counter(Xy_test[1]), Counter(Xy_train[1])

	test_transfer_with_nn(nns,Xy_train,Xy_test,n_samples,output_file)
	
if __name__ == '__main__':
	# test_newsgroups_once()
	newsgroups_experiment_train()
	# newsgroups_experiment_test()