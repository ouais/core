import numpy as np
from sklearn.decomposition import MiniBatchDictionaryLearning,sparse_encode
from utils import IO
from utils.utils import downsample

from sklearn.linear_model import LogisticRegression

from preprocessing import normalize_dataset
from pylab import *

def create_dictionary(X):
	# d = MiniBatchDictionaryLearning(n_components=100, alpha=1, n_iter=500)
	# V = d.fit(X).components_
	# IO.pickle(d,'data/sc_dict.pkl')

	d = IO.unpickle('data/sc_dict.pkl')
	V = d.components_
	figure(figsize=(4.2, 4))
	for i, comp in enumerate(V[:100]):
		subplot(10, 10, i + 1)
		imshow(comp.reshape((28,28)), cmap=cm.gray_r,interpolation='nearest')
		xticks(())
		yticks(())
	subplots_adjust(0.08, 0.02, 0.92, 0.85, 0.08, 0.23)
	show()

	return V


def run():
	X,y = IO.unpickle('/home/oalsha1/data/mnist/mnist_train.pkl')
	X,y = normalize_dataset(X,y)
	dictionary = create_dictionary(X)
	X_new = sparse_encode(X, dictionary)

	IO.pickle((X_new,y),'data/sc_mnist_train.pkl')

	X,y = IO.unpickle('/home/oalsha1/data/mnist/mnist_test.pkl')
	X,y = normalize_dataset(X,y)

	X_new = sparse_encode(X, dictionary)

	IO.pickle((X_new,y),'data/sc_mnist_test.pkl')



def compare():
	# X_train,y_train = IO.unpickle('data/sc_mnist_train.pkl')
	# X_test ,y_test  = IO.unpickle('data/sc_mnist_test.pkl')

	X_train,y_train = IO.unpickle('/home/oalsha1/data/mnist/mnist_train.pkl')
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test,y_test = IO.unpickle('/home/oalsha1/data/mnist/mnist_test.pkl')
	X_test,y_test = normalize_dataset(X_test,y_test)

	rng = np.random.RandomState(666)
	indices = downsample(rng,y_train,1)
	X_train, y_train = X_train[indices], y_train[indices]

	model = LogisticRegression()
	model.fit(X_train,y_train)

	print model.score(X_test,y_test)
	

if __name__=='__main__':
	compare()
