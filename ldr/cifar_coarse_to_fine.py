import numpy as np
from utils import IO
from utils.misc import *
import preprocessing

from collections import Counter

from deepnn.cnn import *
from deepnn.algorithm import *
from samplers import *
from pylab import *

from test_transfer_challenge import test_transfer_with_nn

from utils.utils import *

@batch(128)
def nn_map(nn,X):
	return nn.output[-2](X)


def train_cnns(algorithms,Xy_train, Xy_test,batch_size):

	X_train, y_train = Xy_train
	X_test , y_test  = Xy_test

	for algorithm in algorithms:
		# nn = CNN(input_size=32,kernel_size=5,batch_size=batch_size, pool_sizes=[5,5,5], pool_strides=[2,2,2], nkerns=[96,96,96],drops=[0,0,0,.5,.5],nnodes=[1000])
		nn = CNN(input_size=32,kernel_size=5,batch_size=batch_size, pool_sizes=[5,5,5], pool_strides=[2,2,2], nkerns=[64,64,64],drops=[0,0,0,0,0],nnodes=[1000])
		nn.add_layer(100)	

		# nn = IO.unpickle('models/cnn_cifar100_coarse_to_fine_{}.pkl'.format(str(ialgorithm)))

		ialgorithm = algorithm(nn)
		losses = ialgorithm.fit(X_train,y_train)
		IO.pickle(nn,'models/cnn_cifar100_coarse_to_fine_{}.pkl'.format(str(ialgorithm)))


def relabel(y):
	d = {}
	counter = Counter(y)
	d.update([(x, counter.keys().index(x)) for x in counter.keys()])

	y_new = np.array([d[x] for x in y],dtype='int32')
	return y_new

def relabel_by_node_removal(classes_in_super_class,y_coarse,y_fine):
	
	for i in xrange(len(y_fine)):
		if y_fine[i] in classes_in_super_class:
			y_fine[i] = 101

	y_fine = relabel(y_fine)
	return y_fine

def get_classes_in_super_class(super_class,y_coarse,y_fine):
	classes_in_super_class = []

	for i in xrange(len(y_fine)):
		if y_coarse[i] == super_class:
			classes_in_super_class.append(y_fine[i])

	return np.unique(classes_in_super_class)


def relabel_to_one_vs_all(one, y_fine):
	ids = np.where(y_fine == one)[0]
	y_new = 0 *y_fine
	y_new[ids] = 1
	return y_new


def remove_all_but_classes_in_super_class(classes_in_super_class, X,y_coarse,y_fine):

	new_X = []
	new_y = []

	for i in xrange(len(X)):
		if not(y_fine[i] in classes_in_super_class):
			new_X.append(X[i,:])
			new_y.append(y_fine[i])

	new_X = np.array(new_X,dtype='float32')
	new_y = np.array(new_y,dtype='int32')

	new_y = relabel(new_y)
	return new_X, new_y


def run_cnn_experiment():
	batch_size = 128
	rng = np.random.RandomState(666)

	X_train,y_train_fine =IO.unpickle(data_dir+"/cifar100/cifar100_train_c.pkl")
	X_train = preprocessing.flatten(X_train)
	y_train_coarse = np.array(IO.unpickle(data_dir+'/cifar100/cifar-100-python/train')['coarse_labels'],dtype='int32')

	super_class = rng.randint(0,len(np.unique(y_train_coarse)))

	X_test,y_test_fine = IO.unpickle(data_dir+"/cifar100/cifar100_test_c.pkl")
	X_test = preprocessing.flatten(X_test)
	y_test_coarse = np.array(IO.unpickle(data_dir+'/cifar100/cifar-100-python/test')['coarse_labels'],dtype='int32')

	y_train = relabel_by_node_removal(super_class,y_train_coarse, y_train_fine)
	y_test  = relabel_by_node_removal(super_class,y_test_coarse, y_test_fine)

	print len(np.unique(y_train))
	print len(np.unique(y_test))

	sampler = SupervisedSampler(rng,problem_size=30,y=y_train,batch_size=batch_size)

	LDR = lambda nn: LDR_Pretrainer(nn,learning_rate=.01,batch_size=batch_size,max_filter_norms=[4,4,4],max_row_norms=[4,4,4],sampler=sampler,K=30,max_n_batches=2000,f_momentum=.9,h_momentum=.9)
	bprop = lambda nn: Backpropagation(nn,learning_rate=.01, max_filter_norms=[4,4,4],max_row_norms=[4,4,4],epochs=100,momentum=.9,batch_size=128)

	algorithms = [bprop]
	train_cnns(algorithms, (X_train,y_train),(X_test,y_test),batch_size)

def run_cnn_experiment_test():
	batch_size = 128
	rng = np.random.RandomState(666) #random state must be the same as in training
	n_samples = [1,2,5,10,20,50,100,200]
	n_samples = [1]

	output_file = 'materials/coarse_to_fine_cifar100.pdf'

	nns = [IO.unpickle('models/cnn_cifar100_coarse_to_fine_Backpropagation.pkl'),IO.unpickle('models/cnn_cifar100_coarse_to_fine_LDR.pkl')]

	X_train,y_train =IO.unpickle(data_dir+"/cifar10/cifar10_train_c.pkl")
	X_train = preprocessing.flatten(X_train)
	# y_train_coarse = np.array(IO.unpickle(data_dir+'/cifar100/cifar-100-python/train')['coarse_labels'],dtype='int32')

	# super_class = rng.randint(0,len(np.unique(y_train_coarse)))
	# classes_in_super_class = get_classes_in_super_class(super_class,y_train_coarse,y_train_fine)

	X_test,y_test = IO.unpickle(data_dir+"/cifar10/cifar10_test_c.pkl")
	X_test = preprocessing.flatten(X_test)
	# y_test_coarse = np.array(IO.unpickle(data_dir+'/cifar100/cifar-100-python/test')['coarse_labels'],dtype='int32')

	# y_train = relabel_to_one_vs_all(classes_in_super_class[1],y_train_fine)
	# y_test  = relabel_to_one_vs_all(classes_in_super_class[1],y_test_fine)

	Xy_train = X_train,y_train
	Xy_test  = X_test ,y_test

	results = test_transfer_with_nn(nns,Xy_train,Xy_test,n_samples,n_estimations=100)

	IO.pickle(results,'cifar100_coarse_to_fine_results_lr_2.pkl')	

	# save_graphs(n_samples, [results[0], results[1]] ,['Backprop','LLDR'],output_file,scale=10)



def complete_to_multiple(rng,X,y,multiple_of = 128):

	rem = multiple_of - len(X) % multiple_of

	indices = rng.choice(len(X), rem)

	X_new, y_new = X[indices], y[indices]

	X = np.vstack((X,X_new))
	y = np.hstack((y,y_new))

	return X,y

def monitor(epoch,model,Xys):
	model.set_test_mode()
	msg = ''.join(['{:.3f} '.format(model.score(X,y)) for X,y in Xys])
	print '{}: '.format(epoch) + msg
	model.set_train_mode()

def post_training_run():
	rng = np.random.RandomState(666)

	target_datasets = [data_dir+"/transfer_challenge/transfer_train.pkl", data_dir+"/transfer_challenge/transfer_test.pkl"]

	X_train,y_train = IO.unpickle(target_datasets[0])
	X_train = preprocessing.flatten(X_train)
	X_train, y_train = complete_to_multiple(rng,X_train,y_train,128)

	X_test,y_test = IO.unpickle(target_datasets[1])
	X_test = preprocessing.flatten(X_test)
	X_test, y_test = complete_to_multiple(rng,X_test,y_test,128)

	cost = lambda x,y: LargeMarginLoss_2(x,y,10)
	# cost = MCL2Hinge

	nn = NeuralNetwork([25*64,1000],activations=[lambda x: T.maximum(x,0)],drops=[0.,0] )
	nn.add_layer(10)

	net = IO.unpickle('models/cnn_cifar100_ldr.pkl')

	X_train = preprocessing.flatten(nn_map(net,X_train))
	X_test = preprocessing.flatten(nn_map(net,X_test))

	bprop = Backpropagation(nn,epochs=1000,Linf_lams=[0.0,0.,0.],learning_rate = .01,momentum=.9, batch_size=200)
	bprop.add_epoch_monitor(lambda x: monitor(x, nn,[(X_train,y_train), (X_test,y_test)]))

	bprop.fit(X_train,y_train)


if __name__=='__main__':
	#run_cnn_experiment()
	run_cnn_experiment_test()
