import numpy as np


from deepnn.NN import NeuralNetwork
from deepnn.algorithm import LDR_Pretrainer, Baxter_Pretrainer
from ldr.test_multitask import *

def problem_selection(Z,test_classes,training_problems_order):
	"""
	In this formulation we allow the training problems to include one class of the testing problems"
	"""

	testing_problems = [test_classes]
	X,y = zip(*Z)
	new_classes = np.unique(y)

	problems = [x for x in combinations(new_classes,training_problems_order)]
	training_problems= []

	for problem in problems:
		threshold = sum([x in test_classes for x in problem])
		if threshold <= 1:
			training_problems.append(problem)

	return training_problems, testing_problems, Z

def s1_e2(experiment_name):
	training_problems_order = 5	
	last_layer_flag = True

	n_trials = 10

	print 'running experiment 1'
	print "method name: %s" % experiment_name
	print "testing trains last layer only: " + str(last_layer_flag)


	batch_size = 200
	rng = np.random.RandomState(666)

	Xy_train = load_dataset(data_dir+"/mnist/mnist_train.pkl", normalize = True)
	X_test,y_test = load_dataset(data_dir+"/mnist/mnist_test.pkl", normalize = True)

	Z_test = zip(X_test,y_test)

	Xy_train = Xy_train[0][:50000], Xy_train[1][:50000]
	X_train, y_train = Xy_train
	Z_train = zip(X_train, y_train)

	classes= np.unique(y_train)
	classes_to_remove = [x for x in combinations(classes,4)]
	rng.shuffle(classes_to_remove)

	neural_nets = []
	test_results = []

	print classes_to_remove[:n_trials]

	for trial in xrange(n_trials):

		N = len(X_train[0])	

		nn = NeuralNetwork(layers_sizes=[N,100,100])
		nn.add_layer(10)

		training_problems, testing_problems, Z = problem_selection(Z_train,classes_to_remove[trial],training_problems_order)
		
		sampler = SupervisedSampler(rng,-1,y_train,problems=training_problems)

		if experiment_name == "LDR":
			algorithm = LDR_Pretrainer(nn,L1_lambda=0.0,L2_lambda=0.0,learning_rate=.1,batch_size=batch_size,sampler=sampler,max_n_batches=1000)
			loss = algorithm.fit(X_train,y_train)	
		elif experiment_name == "baxter":
			algorithm = Baxter_Pretrainer(nn,L1_lambda=0.0,L2_lambda=0.0,learning_rate=.1,batch_size=batch_size,sampler=sampler,max_n_batches=50000,stick=False)
			loss = algorithm.fit(X_train,y_train)	
		

		neural_nets.append(nn)
		test_results.append(nn.score(X_test, y_test))

	IO.pickle((neural_nets,test_results,classes_to_remove),'temp/'+experiment_name+'_s1_e2_exp.pkl')

def train_s1_e2():
	s1_e2("LDR")
	s1_e2("baxter")
	

if __name__=='__main__':
	train_s1_e2()