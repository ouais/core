import numpy as  np
import scipy.io
from utils import IO
from utils.misc import *
from deepnn.NN import NeuralNetwork
from deepnn.algorithm import LDR_Pretrainer
from deepnn.costs import *
from preprocessing import normalize_dataset
from sklearn.decomposition import PCA

from experiment_core import execute_test, transform_dataset
from test_multitask import downsample
from samplers import *

def split_dataset_classwise(Z, class_set):

	Z_1 = filter(lambda x: not (x[1] in class_set),Z)
	Z_2 = filter(lambda x: x[1] in class_set,Z)	

	return Z_1, Z_2

def gen_testing_problems(rng, n_classes, n_problems ,order):
	problems = []
	while len(problems) != n_problems:
		problem = tuple(rng.choice(n_classes, order,replace=False))
		if not (problem in problems):
			problems.append(problem)

	return problems

def save_matlab(X_train, y_train, X_valid, y_valid, X_test, y_test, testing_problems):
	scipy.io.savemat('matlab/newsgroups.mat', mdict={
		'X_train': X_train,
		'X_valid': X_valid,
		'X_test': X_test, 
		'y_train': y_train,
		'y_valid': y_valid,
		'y_test': y_test,
		'testing_problems': testing_problems})

def run_newsgroups():

	rng = np.random.RandomState(666)
	problem_size = 5
	n_components = 10

	X, y = IO.unpickle(data_dir+'newsgroups/newsgroup_train_lda_transformed.pkl')
	

	valid_indices = downsample(rng,y,100)
	X_valid, y_valid = X[valid_indices], y[valid_indices]
	train_indices = np.array(list(set(range(len(y))) - set(valid_indices)),dtype='int32')
	X_train, y_train = X[train_indices], y[train_indices]

	X_test, y_test = IO.unpickle(data_dir+'newsgroups/newsgroup_test_lda_transformed.pkl')

	testing_problems = gen_testing_problems(rng,20,100,problem_size)

	# pca = PCA(n_components = n_components)
	# pca.fit(X_train)

	# X_train = pca.transform(X_train)
	# X_test = pca.transform(X_test)
	# X_valid = pca.transform(X_valid)
	X_train, y_train = np.array(X_train,'float32'), np.array(y_train)
	X_valid, y_valid = np.array(X_valid,'float32'), np.array(y_valid)
	X_test, y_test = np.array(X_test,'float32'), np.array(y_test)

	print X_train.shape, X_valid.shape, X_test.shape


	Z_train = zip(X_train,y_train)
	Z_valid = zip(X_valid,y_valid)
	Z_test = zip(X_test,y_test)

	save_matlab(X_train, y_train, X_valid, y_valid, X_test, y_test, testing_problems)

	n_samples_list = [1,2,5,10,20,50,100]

	total_accs = []

	n_trials = 10

	results = {}
	results.update([(x,[]) for x in n_samples_list])

	for trial in xrange(n_trials):
		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		print X_train.shape

		N = len(X_train[0])
		nn = NeuralNetwork(layers_sizes=[N,100,100])
		#note that the default for adding layers is a softmax layer while during construction its a rectified linear layer
		nn.add_layer(problem_size)		

		sampler = NotSupervisedSampler(rng,-1,y_train,problems=testing_problems)

		cost = CrossEntropyLoss

		algorithm = LDR_Pretrainer(nn,L1_lambda=0.0,L2_lambda=0.00,learning_rate=.01,batch_size=50,sampler=sampler,max_n_batches=1000, cost = cost)
		loss = algorithm.fit(X_train,y_train)	

		total_results = []

		X_valid, y_valid = zip(*Z_valid)

		transform_dataset((X_valid, y_valid), nn,'temp/temp_train.pkl')
		transform_dataset((X_test, y_test), nn,'temp/temp_test.pkl')

		Xy_valid_new = IO.unpickle('temp/temp_train.pkl')
		Xy_test_new = IO.unpickle('temp/temp_test.pkl')

		trial_results = execute_test(rng, Xy_valid_new, Xy_test_new, testing_problems = testing_problems, n_samples_list = n_samples_list, n_estimations = 10)

		for x in trial_results:
			results[x].append(trial_results[x])

	for i in xrange(len(n_samples_list)):
		n_samples = n_samples_list[i]
		print "average score: %.4f with %d samples" % (np.mean(results[n_samples]),n_samples)


def run_stl():

	rng = np.random.RandomState(666)
	problem_size = 5
	n_components = 10

	X, y = IO.unpickle(data_dir+'newsgroups/newsgroup_train_lda_transformed.pkl')
	

	valid_indices = downsample(rng,y,100)
	X_valid, y_valid = X[valid_indices], y[valid_indices]
	train_indices = np.array(list(set(range(len(y))) - set(valid_indices)),dtype='int32')
	X_train, y_train = X[train_indices], y[train_indices]

	X_test, y_test = IO.unpickle(data_dir+'newsgroups/newsgroup_test_lda_transformed.pkl')

	testing_problems = gen_testing_problems(rng,20,100,problem_size)

	X_train, y_train = np.array(X_train,'float32'), np.array(y_train)
	X_valid, y_valid = np.array(X_valid,'float32'), np.array(y_valid)
	X_test, y_test = np.array(X_test,'float32'), np.array(y_test)

	print X_train.shape, X_valid.shape, X_test.shape
	Z_train = zip(X_train,y_train)
	Z_valid = zip(X_valid,y_valid)
	Z_test = zip(X_test,y_test)

	n_samples_list = [1,2,5,10,20,50,100]

	total_accs = []

	n_trials = 1

	results = {}
	results.update([(x,[]) for x in n_samples_list])

	for trial in xrange(n_trials):
		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		print X_train.shape

		total_results = []

		X_valid, y_valid = zip(*Z_valid)

		trial_results = execute_test(rng, (X_valid, y_valid), (X_test, y_test), testing_problems = testing_problems, n_samples_list = n_samples_list, n_estimations = 100)

		for x in trial_results:
			results[x].append(trial_results[x])

	for i in xrange(len(n_samples_list)):
		n_samples = n_samples_list[i]
		print "average score: %.4f with %d samples" % (np.mean(results[n_samples]),n_samples)

if __name__== '__main__':
	# run_newsgroups()
	run_stl()