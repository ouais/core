import numpy as np
from utils import IO
from utils.misc import data_dir
from ldr.experiment_core import load_dataset, execute_test, transform_dataset

from plot_graphs import save_graphs

def experiment_1_core(name):
	nets, results = IO.unpickle("temp/"+name+"_s1_e1.pkl")

	Xy_train = load_dataset(data_dir+"/mnist/mnist_train.pkl",normalize=True)	
	Xy_train = Xy_train[0][-10000:], Xy_train[1][-10000:]
	Xy_test = load_dataset(data_dir+"/mnist/mnist_test.pkl",normalize=True)	

	n_samples_list = [1,2,5,10,20,50,100, 500, 1000]	

	rng = np.random.RandomState(666)

	total_results = {}
	total_results.update([(x,[]) for x in n_samples_list])

	for net in nets:

		transform_dataset(Xy_train, net,'temp/temp_train.pkl')
		transform_dataset(Xy_test, net,'temp/temp_test.pkl')

		Xy_train_new = IO.unpickle('temp/temp_train.pkl')
		Xy_test_new = IO.unpickle('temp/temp_test.pkl')
		
		results = execute_test(rng, Xy_train_new,Xy_test_new, testing_problems = [[0,1,2,3,4,5,6,7,8,9]], n_samples_list = n_samples_list, n_estimations = 10)

		for x in results:
			total_results[x].extend(results[x])

	return total_results

def s1_e1_single():
	nets, results = IO.unpickle("temp/LDR_s1_e1.pkl")
	n_samples_list = [1,2,5,10,20,50]#, 100 ,500, 1000]
	rng = np.random.RandomState(666)

	Xy_train = load_dataset(data_dir+"/mnist/mnist_train.pkl",normalize=True)	
	Xy_train = Xy_train[0][-10000:], Xy_train[1][-10000:]
	Xy_test = load_dataset(data_dir+"/mnist/mnist_test.pkl",normalize=True)	

	results = execute_test(rng, Xy_train,Xy_test, testing_problems = [[0,1,2,3,4,5,6,7,8,9]], n_samples_list = n_samples_list, n_estimations = 1)

	IO.pickle(results,'temp/s1_e1_stl_results.pkl')

	return results

def experiment_1_test():
	results_LDR = experiment_1_core("LDR")
	results_baxter = experiment_1_core("baxter")
	IO.pickle((results_LDR,results_baxter),'temp/experiment_1_results.pkl')
	LDR,baxter = IO.unpickle('temp/experiment_1_results.pkl')
	save_graphs(LDR.keys(), [LDR, baxter] ,['Our method', 'Baxter'],'materials/s1_e1.pdf',scale=10)


if __name__ == '__main__':
	experiment_1_test()
	# s1_e1_single()