import scipy
import numpy as np

import theano
import theano.tensor as T

from itertools import combinations
from collections import Counter

from deepnn.algorithm import compute_updates_grads
from deepnn.NN import safe_softmax
from deepnn.algorithm import shared_dataset_X
from deepnn.costs import CrossEntropyLoss

from utils.misc import data_dir

from ldr.test_multitask import load_dataset, problem_selection_4
from ldr.samplers import SupervisedSampler

from sklearn.linear_model import LogisticRegression

def cost(X, y, D, gamma):		
	D = D.reshape(X.shape[1],gamma.shape[0])
	# print D.max()

	activation = np.dot(X,np.dot(D,gamma))
	# activation = np.dot(X,D)
	# print activation.max(), activation.min(), activation.sum(axis=1).min()

	y_hat = np.exp(activation)		
	y_hat /= y_hat.sum(axis=1)[:,None]
	# print y_hat.max()

	cost = -np.mean(np.log(y_hat[range(y_hat.shape[0]),y]))
	
	return cost

def grad(X, y,D,gamma):	

	# print D.shape
	D = D.reshape(X.shape[1],gamma.shape[0])

	D_hat = np.zeros((X.shape[1],gamma.shape[0]))

	y_hat = np.exp(np.dot(X,np.dot(D,gamma)))	
	y_hat /= y_hat.sum(axis=1)[:,None]

	for i in xrange(X.shape[0]):
		for j in xrange(gamma.shape[1]):
			if j == y[i]:
				D_hat -= np.outer(X[i,:],gamma[:,y[i]]) * (1- y_hat[i,y[i]])
			else:
				D_hat += np.outer(X[i,:],gamma[:,j]) * y_hat[i,j]

	D_hat /= X.shape[0]

	D_hat = D_hat.reshape(X.shape[1]*gamma.shape[0])

	return D_hat



class SCMTL:

	"""
	K: number of bases
	M: iterations per task
	"""

	def __init__(self,K,sampler,M=10,L1_lambda=1,n_iterations=10,batch_size=200,rng=None):

		self.K = K
		self.M = M
		self.sampler = sampler
		self.L1_lambda = L1_lambda		
		self.n_iterations = n_iterations
		self.batch_size = batch_size
		self.learning_rate = .01
		self.momentum = .9
		self.problem_size = sampler.problem_size

		self.rng = rng

		if rng == None:
			self.rng = np.random.RandomState(666)

	def relabel(self,y):
		d = {}
		counter = Counter(y)
		d.update([(x, counter.keys().index(x)) for x in counter.keys()])

		y_new = np.array([d[x] for x in y],dtype='int32')
		return y_new

	def fit(self,X,y):
		d = X.shape[1]

		x_sym = T.fmatrix()
		y_sym = T.ivector()
		v = T.ivector()

		train_set_x = shared_dataset_X(X)

		D_values = np.asarray(self.rng.normal(0,.01,(d,self.K)),dtype='float32')
		D_sym = theano.shared(value=D_values, name='D', borrow=True)

		gamma_values = np.zeros((self.K,self.problem_size))

		gamma_sym = theano.shared(value=gamma_values, name='gamma', borrow=True)

		activation = safe_softmax(T.dot(x_sym,T.dot(D_sym,gamma_sym)))

		cost = CrossEntropyLoss(activation,y_sym)
		updates, pars, trsh1 = compute_updates_grads(cost,[D_sym],learning_rate=self.learning_rate,momentum=.9)

		lasso = LogisticRegression(penalty='l1',C = 1./self.L1_lambda)
		
		losses = []
	

		update_D = theano.function([v,y_sym], cost, updates=updates,givens=	{x_sym: train_set_x[v]})

		for i in xrange(self.n_iterations):

			problem = self.sampler.sample_problem()
			indices = self.sampler.sample_training_indices(problem,self.batch_size)
			y_temp  = self.relabel(y[indices])
			
			D_projected_x = np.dot(X[indices,:],D_sym.get_value(borrow=True))
			gamma = lasso.fit(D_projected_x,y_temp).coef_

			gamma_sym.set_value(gamma)

			for m in xrange(self.M):
				update_D(indices,y_temp)



def test_scmtl():
	rng = np.random.RandomState(666)
	training_problems_order = 5

	X,y = load_dataset(data_dir+"/mnist/mnist_train.pkl", normalize=True)
	X_train, y_train = X[:50000], y[:50000]
	X_valid,y_valid = X[50000:], y[50000:]
	X_test,y_test = load_dataset(data_dir+"/mnist/mnist_test.pkl", normalize=True)

	Z_train = zip(X_train,y_train)

	classes= np.unique(y_train)
	classes_to_remove = [x for x in combinations(classes,4)]
	rng.shuffle(classes_to_remove)

	trial = 0

	training_problems, testing_problems, Z_train = problem_selection_4(Z_train,classes_to_remove[trial],training_problems_order)

	X_train,y_train = zip(*Z_train)
	X_train = np.array(X_train,dtype='float32')
	y_train = np.array(y_train,dtype='int32')

	print "# training problems: %d, # testing problems: %d" %(len(training_problems),len(testing_problems))

	sampler = SupervisedSampler(rng,-1,y_train,problems=training_problems)

	scmtl = SCMTL(K=10,sampler=sampler,L1_lambda=.2)
	scmtl.fit(X_train,y_train)


# def cost_2(X, D, y, gamma):		
# 	D = D.reshape(X.shape[1],gamma.shape[0])
# 	# print D.max()

# 	activation = np.dot(X,np.dot(D,gamma))
# 	# activation = np.dot(X,D)
# 	# print activation.max(), activation.min(), activation.sum(axis=1).min()

# 	y_hat = np.exp(activation)		
# 	y_hat /= y_hat.sum(axis=1)[:,None]
# 	# print y_hat.max()

# 	cost = -np.mean(np.log(y_hat[range(y_hat.shape[0]),y]))
	
# 	return cost

# def grad_2(X, D, y, gamma):		
# 	D = D.reshape(X.shape[1],gamma.shape[0])

# 	D_hat = np.zeros((X.shape[1],gamma.shape[0]))

# 	y_hat = np.exp(np.dot(X,np.dot(D,gamma)))	
# 	y_hat /= y_hat.sum(axis=1)[:,None]

# 	for i in xrange(X.shape[0]):
# 		for j in xrange(gamma.shape[1]):
# 			if j == y[i]:
# 				D_hat -= np.outer(X[i,:],gamma[:,y[i]]) * (1- y_hat[i,y[i]])
# 			else:
# 				D_hat += np.outer(X[i,:],gamma[:,j]) * y_hat[i,j]

# 	D_hat /= X.shape[0]

# 	D_hat = D_hat.reshape(X.shape[1]*gamma.shape[0])

# 	# print D_hat.max(), D_hat.min()
# 	return D_hat


def test_grad():

	rng = np.random.RandomState(123)

	X = rng.normal(size=(200,50))
	D = rng.normal(size=(50,30))
	gamma = rng.normal(size=(30,10))
	y = rng.randint(0,10,200)

	print X.shape, D.shape, gamma.shape

	c = lambda D: cost(X,y,D,gamma)
	g = lambda D: grad(X,y,D,gamma)

	D = D.reshape(X.shape[1]*gamma.shape[0])

	# print c(D)

	# rets = scipy.optimize.fmin_l_bfgs_b(c,D,g)

	# print rets[0],rets[1]


	print scipy.optimize.check_grad(c, g, D)


if __name__ == '__main__':
	# test_grad()
	test_scmtl()