import numpy as np
from utils import IO
from utils.misc import *
import preprocessing

from deepnn.cnn import *
from deepnn.algorithm import *
from samplers import *
from pylab import *

from test_transfer_challenge import test_transfer_with_nn

from utils.utils import *

@batch(128)
def nn_map(nn,X):
	return nn.output[-3](X)

def train_cnns(algorithms,Xy_train, Xy_test,batch_size):

	X_train, y_train = Xy_train
	X_test , y_test  = Xy_test

	for algorithm in algorithms:
		nn = CNN(input_size=32,kernel_size=5,batch_size=batch_size, pool_sizes=[5,5,5], pool_strides=[2,2,2], nkerns=[64,64,64],nnodes=[1000])
		nn.add_layer(100)	

		ialgorithm = algorithm(nn)
		losses = ialgorithm.fit(X_train,y_train)
		IO.pickle(nn,'models/cnn_cifar100_{}.pkl'.format(str(ialgorithm)))


def run_cnn_experiment():
	batch_size = 128
	rng = np.random.RandomState(666)

	X_train,y_train =IO.unpickle(data_dir+"/cifar100/cifar100_train_c.pkl")
	X_train = preprocessing.flatten(X_train)
	y_train = np.array(IO.unpickle(data_dir+'/cifar100/cifar-100-python/train')['coarse_labels'],dtype='int32')

	X_test,y_test = IO.unpickle(data_dir+"/cifar100/cifar100_test_c.pkl")
	X_test = preprocessing.flatten(X_test)	
	y_test = np.array(IO.unpickle(data_dir+'/cifar100/cifar-100-python/test')['coarse_labels'],dtype='int32')

	sampler = SupervisedSampler(rng,problem_size=10,y=y_train,batch_size=batch_size)
	LDR = lambda nn: LDR_Pretrainer(nn,learning_rate=.01,batch_size=batch_size,sampler=sampler,max_n_batches=1000, K=50, f_momentum=.2,h_momentum=.9)
	bprop = lambda nn: Backpropagation(nn,learning_rate=.01, epochs=100,momentum=.9,batch_size=128)

	algorithms = [LDR, bprop]
	train_cnns(algorithms, (X_train,y_train),(X_test,y_test),batch_size)

def run_cnn_experiment_test():
	batch_size = 128
	rng = np.random.RandomState(666)
	n_samples = [1,2,5,10,20,50,100,200]
	# n_samples = [1]

	output_file = 'materials/coarse_to_fine_cifar100.pdf'

	nns = [IO.unpickle('models/cnn_cifar100_Backpropagation.pkl'),IO.unpickle('models/cnn_cifar100_LDR.pkl')]

	X_train,y_train =IO.unpickle(data_dir+"/cifar100/cifar100_train_c.pkl")
	X_train = preprocessing.flatten(X_train)
	Xy_train = X_train, y_train

	X_test,y_test = IO.unpickle(data_dir+"/cifar100/cifar100_test_c.pkl")
	X_test = preprocessing.flatten(X_test)	

	Xy_test = X_test, y_test

	test_transfer_with_nn(nns,Xy_train,Xy_test,n_samples,output_file,n_estimations=20)



def complete_to_multiple(rng,X,y,multiple_of = 128):

	rem = multiple_of - len(X) % multiple_of

	indices = rng.choice(len(X), rem)

	X_new, y_new = X[indices], y[indices]

	X = np.vstack((X,X_new))
	y = np.hstack((y,y_new))

	return X,y

def monitor(epoch,model,Xys):
	model.set_test_mode()
	msg = ''.join(['{:.3f} '.format(model.score(X,y)) for X,y in Xys])
	print '{}: '.format(epoch) + msg
	model.set_train_mode()

def post_training_run():
	rng = np.random.RandomState(666)

	target_datasets = [data_dir+"/transfer_challenge/transfer_train.pkl", data_dir+"/transfer_challenge/transfer_test.pkl"]

	X_train,y_train = IO.unpickle(target_datasets[0])
	X_train = preprocessing.flatten(X_train)
	X_train, y_train = complete_to_multiple(rng,X_train,y_train,128)

	X_test,y_test = IO.unpickle(target_datasets[1])
	X_test = preprocessing.flatten(X_test)
	X_test, y_test = complete_to_multiple(rng,X_test,y_test,128)

	cost = lambda x,y: LargeMarginLoss_2(x,y,10)
	# cost = MCL2Hinge

	nn = NeuralNetwork([25*64,1000],activations=[lambda x: T.maximum(x,0)],drops=[0.,0] )
	nn.add_layer(10)

	net = IO.unpickle('models/cnn_cifar100_ldr.pkl')

	X_train = preprocessing.flatten(nn_map(net,X_train))
	X_test = preprocessing.flatten(nn_map(net,X_test))

	bprop = Backpropagation(nn,epochs=1000,Linf_lams=[0.0,0.,0.],learning_rate = .01,momentum=.9, batch_size=200)
	bprop.add_epoch_monitor(lambda x: monitor(x, nn,[(X_train,y_train), (X_test,y_test)]))

	bprop.fit(X_train,y_train)


if __name__=='__main__':
	# run_cnn_experiment()
	run_cnn_experiment_test()