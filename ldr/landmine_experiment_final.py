
import numpy as np

from utils import IO
from utils.misc import data_dir
from deepnn.NN import NeuralNetwork
from deepnn.algorithm import Backpropagation, LDR_Pretrainer, Baxter_Pretrainer
from deepnn.costs import *

from ldr.samplers import CorrespondenceSampler

from pylab import plot,show

from sklearn.metrics import roc_curve, auc
from sklearn import linear_model
from preprocessing import normalize_dataset, normalize_dataset2
from collections import Counter

import theano
import theano.tensor as T

def generate_train_test_split(rng, tasks):

	for task in tasks:
		rng.shuffle(task)

	Z_train = []
	Z_test = []
	correspondence_train = []
	correspondence_test = []

	for i in xrange(len(tasks)):
		task = tasks[i]

		training = task[:len(task)/2]
		testing  = task[len(task)/2:]

		Z_train.extend(training)
		Z_test.extend(testing)

		correspondence_train.extend([i]*len(training))
		correspondence_test.extend([i]*len(testing))

	correspondence_train = np.array(correspondence_train)
	correspondence_test  = np.array(correspondence_test)

	Z_train = np.array(Z_train)
	Z_test = np.array(Z_test)

	return Z_train,Z_test, correspondence_train, correspondence_test

def run_landmine_test():
	rng = np.random.RandomState(666)	

	tasks = IO.unpickle(data_dir+'/landmine/landmine.pkl')

	total_aucs = []
	total_accs = []

	n_trials = 10

	for trial in xrange(n_trials):
		Z_train, Z_test, correspondence_train, correspondence_test = generate_train_test_split(rng, tasks)

		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		N = len(X_train[0])
		nn = NeuralNetwork(layers_sizes=[N,1000,1000])
		#note that the default for adding layers is a softmax layer while during construction its a rectified linear layer
		nn.add_layer(len(np.unique(y_train)))	

		sampler = CorrespondenceSampler(rng, y_train, n_problems=len(tasks),correspondence=correspondence_train)

		cost = CrossEntropyLoss
		# nn.set_input_dropout(.8)
		# nn.set_dropout(.8)

		algorithm = LDR_Pretrainer(nn,L1_lambda=0.03,L2_lambda=0.00,learning_rate=.01,batch_size= 25, K=50,sampler=sampler,max_n_batches=1000, cost = cost)
		loss = algorithm.fit(X_train,y_train)	
		aucs = []
		accs = []

		# nn.set_input_dropout(1.0)
		# nn.set_dropout(1.0)
		# nn.set_weights_scale(.8)

		for problem in xrange(len(tasks)):

			bprop = Backpropagation(nn,train_last_layer_only=True, L1_lambda=0.00,L2_lambda=0.00,epochs=50, momentum=.9,learning_rate=.01,cost = cost)
			train_indices = np.squeeze(np.where(correspondence_train == problem))
			Z_temp_train = Z_train[train_indices]
			X_temp_train, y_temp_train = zip(*Z_temp_train)

			X_temp_train = np.array(X_temp_train)
			y_temp_train = np.array(np.squeeze(y_temp_train))

			# print Counter(y_temp_train)
			
			loss = bprop.fit(X_temp_train,y_temp_train)

			Z_temp_test = Z_test[np.where(correspondence_test == problem)]
			X_temp_test, y_temp_test = zip(*Z_temp_test)

			X_temp_test = np.array(X_temp_test,dtype='float32')
			y_temp_test = np.array(np.squeeze(y_temp_test))

			probas = nn.predict_proba(X_temp_test)
			predictions = nn.predict(X_temp_test)

			fpr, tpr, thresholds = roc_curve(y_temp_test, probas[:, 1])

			result =  nn.score(X_temp_test,y_temp_test)
			area_under_curve = auc(fpr, tpr)
			accuracy = nn.score(X_temp_test,y_temp_test)

			print problem, nn.score(X_temp_test,y_temp_test), area_under_curve

			aucs.append(area_under_curve)
			accs.append(accuracy)

		total_aucs.append(np.mean(aucs))
		total_accs.append(np.mean(accs))

	print 'final score is: %f AUC, %f Accuracy, averaged over %d trials' % (np.mean(total_aucs), np.mean(total_accs), n_trials)


def run_landmine_stl():
	rng = np.random.RandomState(666)	

	tasks = IO.unpickle(data_dir+'/landmine/landmine.pkl')

	total_aucs = []
	total_accs = []

	n_trials = 10

	for trial in xrange(n_trials):
		Z_train, Z_test, correspondence_train, correspondence_test = generate_train_test_split(rng, tasks)

		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		aucs = []
		accs = []

		for problem in xrange(len(tasks)):

			model = linear_model.LogisticRegression()

			train_indices = np.squeeze(np.where(correspondence_train == problem))
			Z_temp_train = Z_train[train_indices]
			X_temp_train, y_temp_train = zip(*Z_temp_train)

			X_temp_train = np.array(X_temp_train)
			y_temp_train = np.array(np.squeeze(y_temp_train))

			# print Counter(y_temp_train)
			
			loss = model.fit(X_temp_train,y_temp_train)
			# plot(loss)
			# show()

			Z_temp_test = Z_test[np.where(correspondence_test == problem)]
			X_temp_test, y_temp_test = zip(*Z_temp_test)

			X_temp_test = np.array(X_temp_test,dtype='float32')
			y_temp_test = np.array(np.squeeze(y_temp_test))

			probas = model.predict_proba(X_temp_test)
			predictions = model.predict(X_temp_test)

			fpr, tpr, thresholds = roc_curve(y_temp_test, probas[:, 1])

			area_under_curve = auc(fpr, tpr)
			accuracy = model.score(X_temp_test,y_temp_test)

			print problem, model.score(X_temp_test,y_temp_test), area_under_curve

			aucs.append(area_under_curve)
			accs.append(accuracy)

		total_aucs.append(np.mean(aucs))
		total_accs.append(np.mean(accs))

	print 'final score is: %f AUC, %f Accuracy, averaged over %d trials' % (np.mean(total_aucs), np.mean(total_accs), n_trials)


def run_landmine_baxter():
	rng = np.random.RandomState(666)	

	tasks = IO.unpickle(data_dir+'/landmine/landmine.pkl')

	total_aucs = []
	total_accs = []

	n_trials = 10

	for trial in xrange(n_trials):
		Z_train, Z_test, correspondence_train, correspondence_test = generate_train_test_split(rng, tasks)

		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		N = len(X_train[0])
		nn = NeuralNetwork(layers_sizes=[N,1000,1000])
		#note that the default for adding layers is a softmax layer while during construction its a rectified linear layer
		nn.add_layer(len(np.unique(y_train)))	

		sampler = CorrespondenceSampler(rng, y_train, n_problems=len(tasks),correspondence=correspondence_train)

		cost = CrossEntropyLoss

		# algorithm = LDR_Pretrainer(nn,L1_lambda=0.03,L2_lambda=0.00,learning_rate=.01,batch_size= 25, K=50,sampler=sampler,max_n_batches=1000, cost = cost)
		algorithm = Baxter_Pretrainer(nn, learning_rate=.001,batch_size=25,sampler=sampler,max_n_batches=15000,stick=False,momentum=0.9)

		loss = algorithm.fit(X_train,y_train)	
		# plot(loss)
		show()
		aucs = []
		accs = []

		for problem in xrange(len(tasks)):

			Z_temp_test = Z_test[np.where(correspondence_test == problem)]
			X_temp_test, y_temp_test = zip(*Z_temp_test)

			X_temp_test = np.array(X_temp_test,dtype='float32')
			y_temp_test = np.array(np.squeeze(y_temp_test))

			probas = algorithm.predictors[problem](X_temp_test)

			fpr, tpr, thresholds = roc_curve(y_temp_test, probas[:, 1])

			area_under_curve = auc(fpr, tpr)

			y_hat = probas.argmax(axis=1)				
			accuracy = np.array(y_hat==y_temp_test).mean()

			print problem, accuracy, area_under_curve

			aucs.append(area_under_curve)
			accs.append(accuracy)

		total_aucs.append(np.mean(aucs))
		total_accs.append(np.mean(accs))

	print 'final score is: %f AUC, %f Accuracy, averaged over %d trials' % (np.mean(total_aucs), np.mean(total_accs), n_trials)

if __name__ =='__main__':
	# run_landmine_test()
	run_landmine_baxter()
	# run_landmine_stl()