import numpy as np

from utils import IO
from utils.misc import data_dir
import preprocessing

from deepnn.cnn import *
from deepnn.algorithm import *
from samplers import *
from pylab import *

def monitor(batch_id,nn,algorithm):
	print 'batch {}: loss: {}'.format(batch_id,np.mean(algorithm.batch_losses))
	if batch_id % 50 == 0:
		IO.pickle(nn,'models/cnn_cifar100_ldr_96_3_500_1_dropout.pkl')

def test():

	rng = np.random.RandomState(666)

	batch_size = 128

	X_train,y_train =IO.unpickle(data_dir+"/cifar100/cifar100_train_c.pkl")
	X_train = preprocessing.flatten(X_train)

	X_test,y_test = IO.unpickle(data_dir+"/cifar100/cifar100_test_c.pkl")
	X_test = preprocessing.flatten(X_test)

	# y_train = IO.unpickle(data_dir+'/cifar100/cifar-100-python/train')['coarse_labels']
	# y_test = IO.unpickle(data_dir+'/cifar100/cifar-100-python/test')['coarse_labels']

	nn = CNN(input_size=32,kernel_size=5,batch_size=batch_size, pool_sizes=[5,5,5], pool_strides=[2,2,2], nkerns=[96,96,96],drops=[0,0,0,.5,.5],nnodes=[1000])
	nn.add_layer(100)	

	sampler = SupervisedSampler(rng,problem_size=20,y=y_train,batch_size=batch_size)

	algorithm = LDR_Pretrainer(nn,learning_rate=.01,batch_size=batch_size,max_filter_norms=[4,4,4],max_row_norms=[4,4,4],sampler=sampler,K=30,max_n_batches=10000,f_momentum=.2,h_momentum=.9)
	algorithm.add_batch_monitor(lambda x: monitor(x,nn,algorithm))
	losses = algorithm.fit(X_train,y_train)
	# plot(losses)
	# show()

	IO.pickle(nn,'models/cnn_cifar100_ldr_96_3_500_1_dropout.pkl')

	# print "algorithm: %s score: %.4f" % (str(bprop), bprop.score(X_test,y_test))
	

if __name__=='__main__':
	test()