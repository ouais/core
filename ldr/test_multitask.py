
import numpy as np

from itertools import *
from pylab import *

from utils import IO
from utils.utils import *
from utils.misc import *
from preprocessing import normalize_dataset,normalize_dataset2, normalize_labels

from deepnn.NN import NeuralNetwork, visualize

from deepnn.algorithm import *
from samplers import SupervisedSampler, ClusteringSampler, OverlappingSampler, OneVsAllSampler

from sklearn import cluster, svm
from sklearn.linear_model import LogisticRegression

@batch(128)
def nn_map(nn,X):
	return nn.output[-2](X)

def downsample(rng,y,n_samples):
	classes = np.unique(y)
	total_candidates = []

	for c in classes:
		candidates = np.where(y == c)[0]
		rng.shuffle(candidates)
		total_candidates.extend(candidates[:n_samples])

	return np.array(total_candidates)

def load_NN(location,last_layer_flag):
	NN = IO.unpickle(location)
	# NN = NeuralNetwork(layers_sizes=[1024,100,100])
	# NN.add_layer(10)
	bprop = Backpropagation(NN,train_last_layer_only=last_layer_flag,epochs=100,momentum=.9)
	return bprop

def test(testing_problems, load_model, Z_transfer, Z_test):
	scores = []
	for index in xrange(len(testing_problems)):
		problem = testing_problems[index]
		model = load_model()

		mapper = {}
		mapper.update([x for x in zip(problem,range(len(problem)))])

		
		X_temp,y_temp = zip(*filter(lambda x: x[1] in problem,Z_transfer))
		y_temp = np.array([mapper[x] for x in y_temp])
		X_temp = np.array(X_temp)
			
		losses = model.fit(X_temp,y_temp)
		# plot(losses)	
		# show()

		X_test,y_test = zip(*filter(lambda x: x[1] in problem,Z_test))

		y_test = np.array([mapper[x] for x in y_test])
		X_test = np.array(X_test)

		score = model.score(X_test,y_test)

		scores.append(score)

	return scores


def relabel_classes(y,classes):
	mapper = {}
	y = np.array(y)
	n_classes = len(np.unique(y))

	for i in xrange(len(y)):
		if y[i] in classes:
			y[i] = n_classes+1

	new_y = normalize_labels(y)
	return new_y



def remove_classes(Z, classes_to_remove):
	new_Z = filter(lambda x: not x[1] in classes_to_remove,Z)
	return new_Z

def problem_selection_1(Z,test_classes,training_problems_order):
	"""
	This formulation simply removes a subset of classes from the training data
	"""

	new_Z = remove_classes(Z,test_classes)
	testing_problems = [test_classes]
	classes = np.unique([x[1] for x in Z])
	new_classes = filter(lambda x: not x in test_classes,classes)
	training_problems = [x for x in combinations(new_classes,training_problems_order)]

	return training_problems, testing_problems, new_Z



def problem_selection_2(Z,test_classes,training_problems_order):
	"""
	In this formulation we relabel a subset of classes with a new label and
	then provide the new dataset with all (n choose k) problems for training.

	"""
	n_relabelled_classes = len(test_classes)

	X,y = zip(*Z)
	new_y = relabel_classes(y,test_classes)
	new_Z = zip(X,new_y)

	testing_problems = [test_classes]
	new_classes = np.unique(new_y)

	training_problems = [x for x in combinations(new_classes,training_problems_order)]

	return training_problems, testing_problems, new_Z


def problem_selection_3(Z,test_classes,training_problems_order):
	"""
	In this formulation we allow the training problems to include one class of the testing problems"
	"""

	testing_problems = [test_classes]
	X,y = zip(*Z)
	new_classes = np.unique(y)

	problems = [x for x in combinations(new_classes,training_problems_order)]
	training_problems= []

	for problem in problems:
		threshold = sum([x in test_classes for x in problem])
		if threshold <= 1:
			training_problems.append(problem)

	return training_problems, testing_problems, Z


def problem_selection_4(Z,test_classes,training_problems_order):

	training_problems = [(0,1,2,3,4,5,6,7,8,9)]
	testing_problems = [[0,1,2,3,4,5,6,7,8,9]]

	return training_problems, testing_problems, Z

def problem_selection_5(Z,test_classes,training_problems_order):
	"Here we train on a subset of classes and test on all the classes"

	X,y = zip(*Z)

	training_problems = [tuple(set(np.unique(y)) - set(test_classes))]
	testing_problems = [[0,1,2,3,4,5,6,7,8,9]]

	return training_problems, testing_problems, Z



def problem_selection_6(rng, y,n_removed_classes,training_problems_order,prev_problems):
	classes = np.unique(y)
	while tuple(sorted(classes[:n_removed_classes])) in prev_problems:
		rng.shuffle(classes)

	new_y = relabel_classes(y,classes[:n_removed_classes])

	testing_problems = []
	for c in classes[n_removed_classes:]:
		temp = list(classes[:n_removed_classes],)				
		temp.append(c)
		testing_problems.append(temp)

	new_classes = np.unique(new_y)

	training_problems = [x for x in combinations(new_classes,training_problems_order)]

	return training_problems, testing_problems, new_y, classes[:n_removed_classes]


def problem_selection_7(rng, y, n_removed_classes,training_problems_order):
	problems = []
	testing_problems = []
	classes = np.unique(y)

	problems = [x for x in combinations(classes,training_problems_order)]
	pairwise_comparisons = [x for x in combinations(classes,2)]

	rng.shuffle(problems)
	rng.shuffle(pairwise_comparisons)
	rng.shuffle(classes)

	testing_pairs = pairwise_comparisons[:int(p*len(pairwise_comparisons))]
	testing_classes = classes[:n_removed_classes]

	for problem in problems:
		if np.any([np.all([x in problem for x in t]) for t in testing_pairs]) or np.any([x in problem for x in testing_classes]):
			testing_problems.append(problem)

	testing_problems = problems[:2]

	training_problems = list(set(problems) - set(testing_problems))


def load_dataset(path, normalize):
	X,y = IO.unpickle(path)
	if normalize:
		X,y = normalize_dataset(X,y)	
	return X,y


def load_lr():
	return LogisticRegression()

def test_multitask(rng, experiment_name, n_labeled_samples,testing_problems,last_layer_flag, Z_valid=None, Z_test=None,n_trials=1):

	nn = IO.unpickle('materials/'+experiment_name+'_NN.pkl')

	if Z_valid is None:
		Z_valid = load_dataset(data_dir+"/mnist/mnist_valid.pkl", normalize=True)

	if Z_test is None:
		Z_test = load_dataset(data_dir+"/mnist/mnist_test.pkl", normalize=True)

	X_valid, y_valid = zip(*Z_valid)
	X_valid = nn_map(nn,(np.array(X_valid)))
	y_valid = np.array(y_valid)

	X_test, y_test = zip(*Z_test)

	X_test = nn_map(nn,(np.array(X_test)))

	Z_test = zip(X_test,y_test)

	results = {}
	results.update([(x,[]) for x in n_labeled_samples])


	sample_scores = []
	down_rng = np.random.RandomState(666)

	for i in xrange(len(n_labeled_samples)):
		sample_scores = []
		for trial in xrange(n_trials):
			n_samples = n_labeled_samples[i]
			indices = downsample(down_rng,y_valid,n_samples)
			# print indices
			X_transfer, y_transfer = X_valid[indices], y_valid[indices]

			Z_transfer = zip(X_transfer,y_transfer)	
		
			# g = lambda : load_NN('materials/'+experiment_name+'_NN.pkl',last_layer_flag)
			g = lambda : load_lr()

			scores = test(testing_problems, g, Z_transfer, Z_test)
			sample_scores.append(np.mean(scores))

		print 'results for {} is {}'.format(n_labeled_samples[i],np.mean(sample_scores))
		results[n_samples].append(np.mean(sample_scores))


	return results


def book_keeping(X_train, nn, experiment_name, loss):
	plot(range(len(loss)),loss)
	# show()
	savefig('materials/'+experiment_name+'_training_curve.pdf',bbox_inches='tight')

	IO.pickle(nn,'materials/'+experiment_name+'_NN.pkl')
	# visualize(X_train,nn,0,'materials/'+experiment_name+'_weights_0.png')
	# visualize(X_train,nn,1,'materials/'+experiment_name+'_weights_1.png')
	# visualize(X_train,nn,2,'materials/'+experiment_name+'_weights_2.png')



def train_test_multitask():
	# n_labeled_samples = [1,2,3,4, 5, 10, 20, 50, 100, 200, 500]
	n_labeled_samples = [1,2,5,10,20]
	training_problems_order = 5
	p = 0 # % of pairwise comparisons removed

	last_layer_flag = True

	n_trials = 1

	# experiment_name = "LDR"
	experiment_name = "baxter"

	print "method name: %s" % experiment_name
	print "testing trains last layer only: " + str(last_layer_flag)

	results = {}
	results.update([(x,[]) for x in n_labeled_samples])

	batch_size = 200
	rng = np.random.RandomState(666)

	X,y = load_dataset(data_dir+"/mnist/mnist_train.pkl", normalize=True)
	X_train, y_train = X[:50000], y[:50000]
	X_valid,y_valid = X[50000:], y[50000:]
	X_test,y_test = load_dataset(data_dir+"/mnist/mnist_test.pkl", normalize=True)

	Z_train = zip(X_train,y_train)
	Z_test = zip(X_test,y_test)
	Z_valid = zip(X_valid, y_valid)
	
	results = {}
	results.update([(x,[]) for x in n_labeled_samples])

	classes= np.unique(y_train)
	classes_to_remove = [x for x in combinations(classes,4)]
	rng.shuffle(classes_to_remove)

	neural_nets = []

	for trial in xrange(n_trials):

		N = len(X[0])	

		nn = NeuralNetwork(layers_sizes=[N,1000,1000])
		nn.add_layer(10)

		# print size, W.shape


		# training_problems, testing_problems, Z_train = problem_selection_1(Z_train,classes_to_remove[trial],training_problems_order)
		# training_problems, testing_problems, Z_train = problem_selection_2(Z_train,classes_to_remove[trial],training_problems_order)
		# training_problems, testing_problems, Z_train = problem_selection_3(Z_train,classes_to_remove[trial],training_problems_order)
		training_problems, testing_problems, Z_train = problem_selection_4(Z_train,classes_to_remove[trial],training_problems_order)
		# training_problems, testing_problems, Z_train = problem_selection_5(Z_train,classes_to_remove[trial],training_problems_order)

		X_train,y_train = zip(*Z_train)

		print "# training problems: %d, # testing problems: %d" %(len(training_problems),len(testing_problems))

		sampler = SupervisedSampler(rng,-1,y_train,problems=training_problems)
		# sampler = OneVsAllSampler(rng,y_train)
		# sampler = OverlappingSampler(rng,[2,2,2,2,2],y_train,limit = 20)
		# clustering = cluster.MiniBatchKMeans(n_clusters=10,random_state=rng)
		# sampler = ClusteringSampler(rng,200,10,clustering)

		if experiment_name == "LDR":
			algorithm = LDR_Pretrainer(nn,L1_lams=[0,0,0],L2_lams=[0,0,0.0],learning_rate=.01,batch_size=batch_size,sampler=sampler,K=100,max_n_batches=1000,f_momentum=.9,h_momentum=.9)
			loss = algorithm.fit(X_train,y_train)	
		elif experiment_name == "baxter":
			algorithm = Baxter_Pretrainer(nn,L1_lambda=0.0,L2_lambda=0.0, learning_rate=.1,batch_size=batch_size,sampler=sampler,max_n_batches=10000,stick=False,momentum=0.9)
			loss = algorithm.fit(X_train,y_train)	

		plot(loss)
		show()
		
		# sampler = SupervisedSampler(rng,10,y_train)

		
		# classes = np.unique(y_train)
		# testing_problems = [[0,1,2,3,4,5,6,7,8,9]]		

		book_keeping(X_train, nn, experiment_name,loss)

		trial_results = test_multitask(rng, experiment_name, n_labeled_samples,testing_problems,last_layer_flag, Z_valid=Z_valid, Z_test=Z_test,n_trials=20)
		for x in trial_results:
			results[x].append(trial_results[x])

	for i in xrange(len(n_labeled_samples)):
		n_samples = n_labeled_samples[i]
		print "average score: %.4f with %d samples" % (np.mean(results[n_samples]),n_samples)

if __name__ =='__main__':
	train_test_multitask()
