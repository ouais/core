import numpy

from utils import IO
from utils.misc import *
from utils.utils import *

import time
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from preprocessing import normalize_dataset,normalize_dataset2

from deepnn.algorithm import *
from deepnn.NN import NeuralNetwork
from sklearn.linear_model import LogisticRegression

from ldr.samplers import SupervisedSampler

def monitor(model,X,y):
	# model.set_input_dropout(1.0)
	# model.set_dropout(1.0)
	# model.set_weights_scale(.5)
	print model.score(X,y)
	# model.set_input_dropout(.6)
	# model.set_dropout(.5)

def train():
	rng = np.random.RandomState(666)
	batch_size = 128

	nn = NeuralNetwork([784,1000,1000],activations=[lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)] )

	# NN = IO.unpickle('sda_cifar100.pkl')
	nn.add_layer(10)	

	X_train,y_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl") 
	X_train,y_train = normalize_dataset(X_train,y_train)

	X_test,y_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
	X_test,y_test = normalize_dataset(X_test,y_test)	

	Z = zip(X_test,y_test)
	Z.sort(key=lambda x: x[1])
	X_test, y_test = zip(*Z)
	X_test = np.array(X_test)
	y_test = np.array(y_test)

	# nn = IO.unpickle('models/nn_dropout_10.pkl')

	# nn.set_input_dropout(.6)
	# nn.set_dropout(.5)

	sampler = SupervisedSampler(rng,problem_size=10,y=y_train,batch_size=batch_size)

	algorithm = LDR_Pretrainer(nn,learning_rate=.01,batch_size=batch_size,sampler=sampler,max_n_batches=1000)
	algorithm.add_monitor(lambda: monitor(nn,X_test,y_test))

	losses = algorithm.fit(X_train,y_train)

	plot(losses)
	show()

	IO.pickle(nn,'models/nn_mnist_ldr.pkl')

	print "train score: %.4f, test score: %.4f" % (algorithm.score(X_train,y_train), algorithm.score(X_test,y_test))
	
def test():

	nn = IO.unpickle('models/nn_mnist_ldr.pkl')
	rng = np.random.RandomState(222)

	X,y = IO.unpickle(data_dir+"/usps/usps.pkl") 
	X,y = normalize_dataset(X,y)

	X = nn.output[-2](X)

	indices = downsample(rng,y,1)
	X_s = X[indices]
	y_s = y[indices]

	model = LogisticRegression()
	model.fit(X_s,y_s)

	print model.score(X,y)



if __name__ == '__main__':
	test()