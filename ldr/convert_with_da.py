from utils import IO
from utils.misc import *

import theano
import theano.tensor as T

from preprocessing import normalize_dataset2


def convert_mnist():
	Xy_train = IO.unpickle(data_dir+"/mnist/mnist_train.pkl")	
	Xy_train = Xy_train[0][-10000:], Xy_train[1][-10000:]
	Xy_test = IO.unpickle(data_dir+"/mnist/mnist_test.pkl")	

	Xy_train = normalize_dataset2(Xy_train[0],Xy_train[1])
	Xy_test = normalize_dataset2(Xy_test[0],Xy_test[1])

	da = IO.unpickle('materials/mnist_da.pkl')

	x = T.fmatrix()
	transformer = theano.function([x],T.nnet.sigmoid(T.dot(x, da.W) + da.b))

	new_X_train = transformer(Xy_train[0])
	new_X_test = transformer(Xy_test[0])

	IO.pickle((new_X_train,Xy_train[1]), 'data/mnist_train_da.pkl')
	IO.pickle((new_X_test,Xy_test[1]), 'data/mnist_test_da.pkl')


if __name__ == '__main__':
	convert_mnist()