
import numpy as np

from utils import IO
from utils.utils import *
from utils.misc import *

from sklearn.cluster import MiniBatchKMeans
from collections import Counter

from preprocessing import normalize_labels, normalize_dataset

# import warnings
# warnings.simplefilter("ignore")
class OverlappingSampler:
	def __init__(self,numpy_rng,partition,y, batch_size=200,limit=None):
		self.rng = numpy_rng		
		self.y = y
		self.classes = np.unique(y)
		self.ilocs = {}
		self.batch_size = batch_size
		self.problem_size = len(partition)
		self.partition = partition
		self.problems = None

		for c in self.classes:
			self.ilocs[c] = np.array(np.where(y == c)[0],dtype='int32')

		if limit != None:			
			temp_problems = []
			for i in xrange(limit):
				
				problem = self.sample_problem()	
				temp_problems.append(problem)
				
			self.problems = temp_problems
			# print temp_problems

		self.compute_sizes()

	def sample_problem(self):	
		if not (self.problems is None):			
			problem = self.problems[self.rng.randint(len(self.problems))]
		else:
			self.rng.shuffle(self.classes)
			problem = tuple(self.classes[:])
		return problem		

	def compute_sizes(self):		
		rem = self.batch_size - (self.problem_size-1)*self.batch_size/self.problem_size
		self.sizes = [self.batch_size/self.problem_size]*(self.problem_size-1) + [rem]

	def gen_y(self):
		y_temp = []
		for i in xrange(self.problem_size):
			y_temp += [i]*self.sizes[i]

		y_temp = np.array(y_temp,dtype='int32')
		return y_temp

	def sample_training_indices(self,problem,batch_size=None):
		for c in problem:
			self.rng.shuffle(self.ilocs[c])

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		k = 0
		for i in xrange(len(self.partition)):
			for j in xrange(self.partition[i]):
				c = problem[k]
				n_samples = self.sizes[self.partition[i]]/self.partition[i]
				temp = self.ilocs[c][:n_samples]
				indices = np.hstack((indices,temp))
				k += 1

		return indices, self.gen_y()

	def sample_testing_indices(self,problem,batch_size=None):

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		k = 0
		for i in xrange(len(self.partition)):
			for j in xrange(self.partition[i]):
				c = problem[k]			
				n_samples = self.sizes[self.partition[i]]/self.partition[i]
				temp = self.rng.choice(self.ilocs[c][n_samples:],n_samples,replace=True)
				indices = np.hstack((indices,temp))
				k += 1

		return indices, self.gen_y()

	def sample_problem_indices(self):

		problem = self.sample_problem()
		return self.sample_indices(problem)

	def fit(self,X):
		pass

class AdaptiveCorrespondenceSampler:
	def __init__(self,rng,y,correspondence,batch_size=200):		
		self.rng = rng
		self.n_problems = len(np.unique(correspondence))
		self.task_indices = {}
		self.task_class_indices = {}
		self.problem_classes = {}		
		self.problems = range(self.n_problems)
		self.batch_size = batch_size

		for problem in xrange(self.n_problems):
			self.task_indices[problem] = np.array(np.where(np.array(correspondence) == problem)[0],dtype='int32')

			z_task = zip(y[self.task_indices[problem]],self.task_indices[problem])

			self.task_class_indices[problem] = {}

			self.problem_classes[problem] = np.unique(y[self.task_indices[problem]])			
			for c in self.problem_classes[problem]:
				temp = filter(lambda x : x[0] == c, z_task)
				self.task_class_indices[problem][c] = np.array([x[1] for x in temp])


	def sample_problem(self):
		return self.rng.randint(self.n_problems)

	def sample_training_indices(self,problem,batch_size=None):
		if batch_size is None:
			batch_size = self.batch_size

		indices = np.array([],dtype='int32')

		for c in self.problem_classes[problem]: # does not activate during regression
			self.rng.shuffle(self.task_class_indices[problem][c])
			temp = self.task_class_indices[problem][c][:1]
			indices = np.hstack((indices,temp))

		self.rng.shuffle(self.task_indices[problem])
		temp = self.rng.choice(self.task_indices[problem][:batch_size-len(indices)],batch_size-len(indices),replace=True)
		indices = np.hstack((indices,temp))

		return indices

	def sample_testing_indices(self,problem,batch_size=None):

		if batch_size is None:
			batch_size = self.batch_size

		indices = np.array([],dtype='int32')

		for c in self.problem_classes[problem]:
			temp = self.task_class_indices[problem][c][:10]
			indices = np.hstack((indices,temp))

		if len(self.task_indices[problem]) <= batch_size:
			temp = self.rng.choice(self.task_indices[problem],batch_size,replace=True)
		else:
			temp = self.rng.choice(self.task_indices[problem][batch_size-len(indices):],batch_size-len(indices),replace=True)
		indices = np.hstack((indices,temp))
		return indices

	def fit(self,X):
		pass


class CorrespondenceSampler:
	"""
	In this sampler, every problem is an intenger. Problems are mapped to instances through the correspondence parameters. 
	"""
	def __init__(self,rng,y, n_problems,correspondence, regression=False,batch_size=200):		
		self.rng = rng
		self.n_problems = n_problems
		self.task_indices = {}
		self.task_class_indices = {}
		self.classes = np.unique(y)
		self.n_classes = len(self.classes)
		self.problems = range(n_problems)
		self.problem_size = self.n_classes
		self.batch_size = batch_size
		self.regression = regression

		if regression:
			self.n_classes = 0
			self.classes= []
		
		for problem in xrange(n_problems):
			self.task_indices[problem] = np.array(np.where(np.array(correspondence) == problem)[0],dtype='int32')

			if regression:
				self.problem_size = 1
				continue

			z_task = zip(y[self.task_indices[problem]],self.task_indices[problem])

			self.task_class_indices[problem] = {}

			for c in self.classes:
				temp = filter(lambda x : x[0] == c, z_task)
				self.task_class_indices[problem][c] = np.array([x[1] for x in temp])


	def sample_problem(self):
		return self.rng.randint(self.n_problems)

	def sample_training_indices(self,problem,batch_size=None):
		if batch_size is None:
			batch_size = self.batch_size

		indices = np.array([],dtype='int32')

		for c in self.classes: # does not activate during regression
			self.rng.shuffle(self.task_class_indices[problem][c])
			temp = self.task_class_indices[problem][c][:10]
			indices = np.hstack((indices,temp))

		self.rng.shuffle(self.task_indices[problem])
		temp = self.rng.choice(self.task_indices[problem][:batch_size-len(indices)],batch_size-len(indices),replace=True)
		indices = np.hstack((indices,temp))

		return indices

	def sample_testing_indices(self,problem,batch_size=None):

		if batch_size is None:
			batch_size = self.batch_size

		indices = np.array([],dtype='int32')

		for c in self.classes:
			temp = self.task_class_indices[problem][c][:10]
			indices = np.hstack((indices,temp))

		if len(self.task_indices[problem]) <= batch_size:
			temp = self.rng.choice(self.task_indices[problem],batch_size,replace=True)
		else:
			temp = self.rng.choice(self.task_indices[problem][batch_size-len(indices):],batch_size-len(indices),replace=True)
		indices = np.hstack((indices,temp))
		return indices

	def fit(self,X):
		pass



class OneVsAllSampler:
	def __init__(self,numpy_rng,y,batch_size=200):
		self.rng = numpy_rng		
		self.y = y
		self.classes = np.unique(y)
		self.n_classes = len(self.classes)
		self.ilocs = {}
		self.batch_size = batch_size
		self.problem_size = self.n_classes
		self.indices_range = range(len(y))

		self.problems = []
		for c in self.classes:
			self.ilocs[c] = np.array(np.where(y == c)[0],dtype='int32')
			self.problems.append(c)

	def sample_problem(self):	
		return self.rng.randint(len(self.n_classes))	

	def sample_training_indices(self,problem,batch_size=None):
		"""
		here a problem is a single class
		"""
		if batch_size is None:
			batch_size = self.batch_size

		self.rng.shuffle(self.ilocs[problem])
		self.rng.shuffle(self.indices_range)

		at_least_one = self.ilocs[c][:1][:]
		indices = self.indices_range[:batch_size-1]

		indices = np.hstack((indices,at_least_one))

		y = np.array(self.y[indices] == problem,dtype='int32')

		print indices.shape, y.shape

		return indices, y

	def sample_testing_indices(self,problem,batch_size=None):

		if batch_size is None:
			batch_size = self.batch_size

		self.rng.shuffle(self.ilocs[problem])

		at_least_one = self.ilocs[c][:1][:]

		indices = self.rng.choice(self.indices_range[batch_size-1:],batch_size-1,replace=True)

		indices = np.hstack((indices,at_least_one))

		y = np.array(self.y[indices] == problem,dtype='int32')

		return indices, y

	def sample_problem_indices(self):

		problem = self.sample_problem()
		return self.sample_indices(problem)

	def fit(self,X):
		pass


class SupervisedSampler:
	def __init__(self,numpy_rng,problem_size,y,batch_size=200,problems=None):
		self.rng = numpy_rng		
		self.y = y
		self.classes = np.unique(y)
		self.ilocs = {}
		self.batch_size = batch_size
		self.problem_size = problem_size
		self.problems = None
		if problems != None:
			self.problem_size = len(problems[0])
			self.problems = problems

		for c in self.classes:
			self.ilocs[c] = np.array(np.where(y == c)[0],dtype='int32')

		self.compute_sizes()

	def sample_problem(self):	
		if self.problems != None:
			problem = self.problems[self.rng.randint(len(self.problems))]
		else:
			problem = self.rng.choice(len(self.classes), self.problem_size,replace=False)
		return problem		

	def compute_sizes(self):		
		rem = int(self.batch_size - (self.problem_size-1)*np.floor(float(self.batch_size)/self.problem_size))
		self.sizes = [self.batch_size/self.problem_size]*(self.problem_size-1) + [rem]

	def gen_y(self):
		y_temp = []
		for i in xrange(self.problem_size):
			y_temp += [i]*self.sizes[i]

		y_temp = np.array(y_temp,dtype='int32')
		return y_temp

	def sample_training_indices(self,problem,batch_size=None):
		for c in problem:
			self.rng.shuffle(self.ilocs[c])

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		for i in xrange(len(problem)):
			c = problem[i]
			temp = self.ilocs[c][:self.sizes[i]]
			indices = np.hstack((indices,temp))

		return indices

	def sample_testing_indices(self,problem,batch_size=None):

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		for i in xrange(len(problem)):
			c = problem[i]			
			temp = self.rng.choice(self.ilocs[c][self.sizes[i]:],self.sizes[i],replace=True)
			indices = np.hstack((indices,temp))

		return indices

	def sample_problem_indices(self):

		problem = self.sample_problem()
		return self.sample_indices(problem)

	def fit(self,X):
		pass


class RandomizedSampler:
	def __init__(self,numpy_rng,data_size,problem_size,batch_size=200,problems=None):
		self.rng = numpy_rng		
		self.ilocs = {}
		self.batch_size = batch_size
		self.data_size = data_size
		self.problem_size = problem_size
		self.classes = range(problem_size)

		a = range(self.problem_size)
		self.y = y_temp = np.array([x for x in a for a in range(self.data_size/self.problem_size)],dtype='int32')
		self.compute_sizes()

	def relabel_data(self):
		self.rng.shuffle(self.y)

		for c in self.classes:
			self.ilocs[c] = np.array(np.where(self.y == c)[0],dtype='int32')	

	def gen_y(self):
		y_temp = []
		for i in xrange(self.problem_size):
			y_temp += [i]*self.sizes[i]

		y_temp = np.array(y_temp,dtype='int32')
		return y_temp

	def compute_sizes(self):		
		rem = self.batch_size - (self.problem_size-1)*self.batch_size/self.problem_size
		self.sizes = [self.batch_size/self.problem_size]*(self.problem_size-1) + [rem]

	def sample_training_indices(self,batch_size=None):
		for c in self.classes:
			self.rng.shuffle(self.ilocs[c])

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')
		
		for i in xrange(len(self.classes)):
			c = self.classes[i]
			temp = self.ilocs[c][:self.sizes[i]]
			indices = np.hstack((indices,temp))


		return indices, self.gen_y()

	def sample_testing_indices(self,batch_size=None):

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		for i in xrange(len(self.classes)):
			c = self.classes[i]			
			temp = self.rng.choice(self.ilocs[c][self.sizes[i]:],self.sizes[i],replace=True)
			indices = np.hstack((indices,temp))

		return indices, self.gen_y()

	def fit(self,X):
		pass

class NotSupervisedSampler:
	"""
	this sampler is the same as a supervised sampler, except for it doesn't sample problems 
	from the list: problems which is given in the input"""

	def __init__(self,numpy_rng,problem_size,y,batch_size=200,problems=None):
		self.rng = numpy_rng		
		self.y = y
		self.classes = np.unique(y)
		self.ilocs = {}
		self.batch_size = batch_size
		self.problem_size = problem_size
		self.problems = None
		if problems != None:
			self.problem_size = len(problems[0])
			self.problems = problems

		for c in self.classes:
			self.ilocs[c] = np.array(np.where(y == c)[0],dtype='int32')

		self.compute_sizes()
		print self.compute_sizes

	def sample_problem(self):	
		problem = tuple(self.rng.choice(len(self.classes), self.problem_size,replace=False))
		
		while problem in self.problems:
			problem = tuple(self.rng.choice(len(self.classes), self.problem_size,replace=False))
		
		return problem		

	def compute_sizes(self):		
		rem = self.batch_size - (self.problem_size-1)*(self.batch_size/self.problem_size)
		# print rem, (self.problem_size-1)*self.batch_size/self.problem_size, self.batch_size
		self.sizes = [self.batch_size/self.problem_size]*(self.problem_size-1) + [rem]

	def gen_y(self):
		y_temp = []
		for i in xrange(self.problem_size):
			y_temp += [i]*self.sizes[i]

		y_temp = np.array(y_temp,dtype='int32')
		return y_temp

	def sample_training_indices(self,problem,batch_size=None):
		for c in problem:
			self.rng.shuffle(self.ilocs[c])

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		for i in xrange(len(problem)):
			c = problem[i]
			temp = self.ilocs[c][:self.sizes[i]]
			indices = np.hstack((indices,temp))

		return indices

	def sample_testing_indices(self,problem,batch_size=None):

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		for i in xrange(len(problem)):
			c = problem[i]			
			temp = self.rng.choice(self.ilocs[c][self.sizes[i]:],self.sizes[i],replace=True)
			indices = np.hstack((indices,temp))

		return indices

	def sample_problem_indices(self):

		problem = self.sample_problem()
		return self.sample_indices(problem)


class ClusteringSampler:
	def __init__(self,numpy_rng,batch_size,problem_size, algorithm):
		self.rng = numpy_rng		
		self.algorithm = algorithm
		self.batch_size = batch_size
		self.problem_size = problem_size
		self.original_problem_size = problem_size

	def compute_sizes(self):		
		rem = self.batch_size - (self.problem_size-1)*self.batch_size/self.problem_size
		self.sizes = [self.batch_size/self.problem_size]*(self.problem_size-1) + [rem]

	def fit(self,X):
		self.algorithm.fit(X)
		labels = self.algorithm.labels_
		self.n_classes = len(np.unique(labels))
		self.problem_size = self.n_classes

		if self.problem_size != self.original_problem_size:
			labels = normalize_labels(labels)

		self.problem_size = self.n_classes
		self.ilocs = {}

		for c in xrange(self.n_classes):
			self.ilocs[c] = np.array(np.where(labels == c)[0],dtype='int32')

		self.compute_sizes()

	def gen_y(self, batch_size):
		y_temp = []
		for i in xrange(self.problem_size):
			y_temp += [i]*self.sizes[i]

		y_temp = np.array(y_temp,dtype='int32')
		return y_temp

	def sample_problem(self):		
		problem = self.rng.choice(self.n_classes, self.problem_size,replace=False)
		return problem

	def sample_training_indices(self,problem,batch_size=None):
		for c in problem:
			self.rng.shuffle(self.ilocs[c])

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		for i in xrange(len(problem)):
			c = problem[i]
			temp = self.ilocs[c][:self.sizes[i]]
			indices = np.hstack((indices,temp))

		return indices, self.gen_y(batch_size)

	def sample_testing_indices(self,problem,batch_size=None):

		if batch_size is None:
			batch_size = self.batch_size

		self.compute_sizes()

		indices = np.array([],dtype='int32')

		for i in xrange(len(problem)):
			c = problem[i]			
			temp = self.rng.choice(self.ilocs[c][self.sizes[i]:],self.sizes[i],replace=True)
			indices = np.hstack((indices,temp))

		return indices, self.gen_y(batch_size)


class HyperplaneSampler:
	def __init__(self,numpy_rng,problem_size,K=5):
		self.rng = numpy_rng
		self.K = K
		self.problem_size = problem_size		
		self.batch_size = 200
		# self.compute_sizes()

	def __str__(self):
	 return "random hyperplanes"

	def sample_problem(self,X):
		h = self.rng.random((self.dimensions,1))
		return h

	def compute_sizes(self):
		rem = self.batch_size - (self.problem_size-1)*self.batch_size/self.problem_size
		self.sizes = [self.batch_size/self.problem_size]*(self.problem_size-1) + [rem]

	def predict(self,X):
		y_pred = np.array(np.zeros(len(X)),dtype='int32')

		for plane,threshold in self.planes:
			y_pred *= 2
			bools = np.squeeze(np.dot(X,plane) < threshold)
			y_pred[np.where(bools)] = y_pred[np.where(bools)]+1

		return y_pred

	def fit(self,X):
		n_dimensions = len(X[0])         
		self.planes = []
	
		while 2**len(self.planes) < self.problem_size:
			maximum_margin = -1
			n_samples = float(len(X))
			for k in xrange(self.K):
				h = self.rng.normal(0,1,(n_dimensions,1))  

				y_pred =  np.squeeze(np.dot(X,h))
				y_pred.sort()

				margin = -1
				for i in xrange(len(y_pred)-1):
					point = (y_pred[i]+y_pred[i+1])/2.
					if abs(y_pred[i]-point) > margin:
						margin = abs(y_pred[i]-point)
						threshold = point
								
				if margin > maximum_margin:
					maximum_margin = margin
					best_h = h
					best_threshold = threshold
			
			self.planes.append((best_h,best_threshold))

def test_overlapping_sampler():
	rng = np.random.RandomState(666)
	X,y = IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X,y = normalize_dataset(X,y)

	X = X[:1000]
	y = y[:1000]

	sampler =  OverlappingSampler(rng,[2,2,2,2,2],y,50)
	for i in xrange(50):
		problem = sampler.sample_problem()
		sampler.sample_training_indices(problem)
		sampler.sample_testing_indices(problem)

		print problem


def test_onevsallsampler():
	rng = np.random.RandomState(666)
	X,y = IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X,y = normalize_dataset(X,y)

	X = X[:1000]
	y = y[:1000]

	sampler =  OneVsAllSampler(rng,y,50)
	print sampler.problems
	for i in xrange(0):
		problem = sampler.sample_problem()
		indices, ys = sampler.sample_training_indices(problem)
		print indices
		print Counter(y[indices])
		indices, ys = sampler.sample_testing_indices(problem)
		print indices
		print Counter(y[indices])

		print problem



if __name__=='__main__':
	
	# test_overlapping_sampler()
	test_onevsallsampler()
