import numpy as np

from utils import IO
from utils.misc import *

from collections import Counter

import sklearn
import sklearn.ensemble
from sklearn.metrics import roc_curve, auc, mean_squared_error

from ldr.londonschools_experiment import generate_train_test_split
from pylab import *

		
def test_landmine():
	rng = np.random.RandomState(666)
	tasks = IO.unpickle(data_dir+'landmine/landmine.pkl')
	Z_train, Z_test, correspondence_train, correspondence_test = generate_train_test_split(rng, tasks)
	X_train, y_train = zip(*Z_train)
	X_test, y_test = zip(*Z_test)

	X_train = np.array(X_train,dtype='float32')
	X_test  = np.array(X_test ,dtype='float32')
	y_train = np.array(np.squeeze(y_train),dtype='int32')
	y_test  = np.array(np.squeeze(y_test) ,dtype='int32')

	model = sklearn.linear_model.LogisticRegression()
	model.fit(X_train,y_train)

	probas = model.predict_proba(X_test)
	fpr, tpr, thresholds = roc_curve(y_test, probas[:, 1])
	area_under_curve = auc(fpr, tpr)

	print "final score for logistic regression on landmine dataset: %.4f accuracy and %.4f AUC" % (model.score(X_test,y_test), area_under_curve)

def test_londonschools():
	rng = np.random.RandomState(666)
	tasks = IO.unpickle(data_dir+'landmine/landmine.pkl')
	Z_train, Z_test, correspondence_train, correspondence_test = generate_train_test_split(rng, tasks)
	X_train, y_train = zip(*Z_train)
	X_test, y_test = zip(*Z_test)

	X_train = np.array(X_train,dtype='float32')
	X_test  = np.array(X_test ,dtype='float32')
	y_train = np.array(np.squeeze(y_train),dtype='int32')
	y_test  = np.array(np.squeeze(y_test) ,dtype='int32')

	model = sklearn.ensemble.RandomForestRegressor()
	model.fit(X_train, y_train)
	y_hat = model.predict(X_test)

	RMSE = np.sqrt(mean_squared_error(y_hat,y_test))

	print "final score for linear regression on london schools dataset: %.4f" % RMSE


if __name__ == '__main__':
	test_landmine()
	test_londonschools()