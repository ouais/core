import warnings
warnings.simplefilter('ignore')

import numpy as np
from utils.utils import *
from utils import IO
from utils.misc import *
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from deepnn.algorithm import Backpropagation
from deepnn.NN import *
from deepnn.costs import *
from ldr.test_multitask import downsample
from deepnn.costs import *

import preprocessing



@batch(128)
def nn_map(nn,X):
	return nn.output[-2](X)

def test_transfer_with_nn(nns,Xy_train, Xy_test,n_samples,n_estimations = 1):

	X_train,y_train = Xy_train
	X_test, y_test = Xy_test

	results = []

	for nn in nns: 
		rng = np.random.RandomState(666)	
		results_j = {}
		nn.set_test_mode()

		for k in n_samples:
			results_j[k] = []

			for i in range(n_estimations):
		
				indices = downsample(rng,y_train,k)
				X_temp = X_train[indices]
				y_temp = y_train[indices]

				X_train_new = preprocessing.flatten(nn_map(nn,X_temp))
				X_test_new = preprocessing.flatten(nn_map(nn,X_test))

				model = LogisticRegression()

				model.fit(X_train_new,y_temp)

				results_j[k].append(model.score(X_test_new,y_test))

			print "{} {}".format(k, np.mean(results_j[k]))

		results.append(results_j)		

	return results

def monitor(e,model,X,y):
	model.set_test_mode()
	if e % 100 == 0:
		print '{} {:.2f}'.format(e,100*model.score(X,y))
	model.set_train_mode()

def test_transfer_full():

	d = IO.unpickle('data/transfer_challenge_data_wo_unlabelled.pkl')

	X_train, y_train = d['train']
	X_test, y_test = d['test']

	nn = NeuralNetwork([2400,1000,1000],activations=[lambda x: T.maximum(x,0.0),lambda x: T.maximum(x,0.0)],drops=[.5,0.5,0.5] )
	nn.add_layer(10, activation=lambda x: x)

	cost = lambda x,y:LargeMarginLoss_2(x,y,10)

	bprop = Backpropagation(nn,epochs=3000,max_row_norms=[.2,.2,.2],learning_rate = .01,momentum=.9, batch_size=128,cost=cost)
	bprop.add_epoch_monitor(lambda e: monitor(e,nn,X_test,y_test))

	bprop.fit(X_train,y_train)

	nn.set_test_mode()

	msg = 'finished with score {:.2f} on transfer learning challenge test set'.format(100.*nn.score(X_test,y_test))

	print msg


def test_transfer_challenge_samples():
	n_samples = [1,2,3,4,5,10,20]

	nns = [IO.unpickle("models/cnn_cifar100_ldr_96_3_500_1_dropout.pkl"),IO.unpickle("models/cnn_cifar100_96_3_1k_1_dropout.pkl")]
	X_train, y_train = IO.unpickle(data_dir+"/transfer_challenge/transfer_train.pkl")
	X_test, y_test  = IO.unpickle(data_dir+"/transfer_challenge/transfer_test.pkl")
	X_train = preprocessing.flatten(X_train)
	X_test = preprocessing.flatten(X_test)

	Xy_train = X_train,y_train
	Xy_test = X_test,y_test

	results = test_transfer_with_nn(nns,Xy_train,Xy_test,n_samples,n_estimations=20)
	IO.pickle(results,'results/transfer_challenge_results_2.pkl')

	# save_graphs(n_samples, [results[0], results[1]] ,['Backprop','LLDR'],output_file)

def plot_results_graph():
	output_file = 'materials/transfer_challenge_lr_2.pdf'

	results = IO.unpickle('results/transfer_challenge_results_2.pkl')

	lines = (.486)
	line_labels = ['S3C']

	n_samples = results[0].keys()

	save_graphs(n_samples, [results[0], results[1]] ,['LeaDR','ConvNet'],lines,line_labels,output_file)


def save_graphs(keys, results,labels,lines, line_labels, file_name,scale=1):
	markers = '^x*+'
	colors = 'km'
	
	plt.rc('text', usetex=False)
	plt.rc('font', family='serif',size=6)

	fig = plt.figure(figsize=(4,1.7))
	ax = fig.add_subplot(1,1,1)

	xs = [x for x in keys]
	xs.sort()

	for i in xrange(len(results)):
		results_i = results[i]

		f1 = np.array([np.mean(results_i[x]) for x in xs])
		f1_err = []

		for x in xs:			
			errs = np.std(results_i[x])
			f1_err.append(np.mean(errs))

		f1_err = np.array(f1_err)

		scaled_xs = [x*scale for x in xs]
		plt.errorbar(scaled_xs,f1,yerr=f1_err,fmt='-'+markers[i%len(markers)],linewidth=.5,markersize=4, label=labels[i])
		# plt.plot(xs,f1,'-'+markers[i%len(markers)],linewidth=.5,markersize=3, label=labels[i])

	plt.plot([20], [.486], 'r*',markersize=5,label='S3C')
	# for i in xrange(len(lines)):
	# 	line = lines[i]
	# 	ax.axhline(y=line,linestyle='-',color=colors[i],linewidth=0.5,zorder=0,label=line_labels[i])

	plt.xlabel('samples per class (log)')	
	ax.set_xscale('log')
	ax.xaxis.grid(True,'both')
	plt.ylabel('accuracy')

	x1,x2,y1,y2 = ax.axis()
	ax.axis((0.9,30,y1,y2))

	# get handles
	old_handles, labels = ax.get_legend_handles_labels()
	# remove the errorbars

	old_handles = [h for h in old_handles]

	handles = []
	for i in xrange(len(old_handles)):
		h= old_handles[i]
		if isinstance(h,Line2D):
			handles.append(h)
		else:
			handles.append(h[0])

	labels.extend(line_labels)

	plt.legend(handles,labels, loc=4)
	

	plt.savefig(file_name,bbox_inches='tight')

if __name__=='__main__':
	# test_transfer_full()
	# test_transfer_challenge_samples()
	plot_results_graph()
