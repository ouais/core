
import numpy as np

from utils import IO
from utils.misc import data_dir
from deepnn.NN import NeuralNetwork
from deepnn.algorithm import Backpropagation, LDR_Pretrainer, Baxter_Pretrainer
from deepnn.costs import *

from ldr.samplers import CorrespondenceSampler

from pylab import plot,show
from sklearn.metrics import roc_curve, auc, mean_squared_error
from sklearn import linear_model
from sklearn import ensemble
from collections import Counter

import theano
import theano.tensor as T

def generate_train_test_split(rng, tasks):

	for task in tasks:
		rng.shuffle(task)

	Z_train = []
	Z_test = []
	correspondence_train = []
	correspondence_test = []

	for i in xrange(len(tasks)):
		task = tasks[i]

		training = task[:len(task)/2]
		testing  = task[len(task)/2:]

		Z_train.extend(training)
		Z_test.extend(testing)

		correspondence_train.extend([i]*len(training))
		correspondence_test.extend([i]*len(testing))

	correspondence_train = np.array(correspondence_train)
	correspondence_test  = np.array(correspondence_test)

	Z_train = np.array(Z_train)
	Z_test = np.array(Z_test)

	return Z_train,Z_test, correspondence_train, correspondence_test

def run_londonschools_test():
	rng = np.random.RandomState(666)	

	tasks = IO.unpickle(data_dir+'/londonschools/londonschools.pkl')

	trial_results = []
	n_trials = 10

	print '# tasks: %d' % len(tasks)

	for trial in xrange(n_trials):
		Z_train, Z_test, correspondence_train, correspondence_test = generate_train_test_split(rng, tasks)

		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		N = len(X_train[0])
		nn = NeuralNetwork(layers_sizes=[N,1000,1000],activations = [lambda x: T.maximum(x, 0), lambda x: x])
		nn.add_layer(1,activation= lambda x: x)

		sampler = CorrespondenceSampler(rng, y_train, n_problems=len(tasks),correspondence=correspondence_train,regression=True)

		cost = RMSE

		algorithm = LDR_Pretrainer(nn,L1_lams= [0,0,0],L2_lams=[0,0,0],learning_rate=.001,batch_size=100,sampler=sampler,max_n_batches=1000, cost = cost)
		loss = algorithm.fit(X_train,y_train)	
		# plot(loss)
		# show()
		results = []

		for problem in xrange(len(tasks)):

			bprop = Backpropagation(nn,train_last_layer_only=True, batch_size=100, epochs=50,momentum=.9,learning_rate=.01,cost = cost)
			train_indices = np.squeeze(np.where(correspondence_train == problem))
			Z_temp_train = Z_train[train_indices]
			X_temp_train, y_temp_train = zip(*Z_temp_train)

			X_temp_train = np.array(X_temp_train)
			y_temp_train = np.array(np.squeeze(y_temp_train))

			loss = bprop.fit(X_temp_train,y_temp_train)

			Z_temp_test = Z_test[np.where(correspondence_test == problem)]
			X_temp_test, y_temp_test = zip(*Z_temp_test)

			X_temp_test = np.array(X_temp_test,dtype='float32')
			y_temp_test = np.array(np.squeeze(y_temp_test))

			probas = np.squeeze(nn.predict_proba(X_temp_test))

			result = np.sqrt(np.mean((probas-y_temp_test)**2))

			results.append(result)

		trial_results.append(np.mean(results))

	print 'final score is: %f averaged over %d trials' % (np.mean(trial_results),n_trials)

		


def run_londonschools_stl():
	rng = np.random.RandomState(666)	

	tasks = IO.unpickle(data_dir+'/londonschools/londonschools.pkl')

	trial_results = []
	n_trials = 100

	print '# tasks: %d' % len(tasks)
	

	for trial in xrange(n_trials):
		Z_train, Z_test, correspondence_train, correspondence_test = generate_train_test_split(rng, tasks)

		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		results = []

		for problem in xrange(len(tasks)):

			# model = linear_model.LinearRegression()
			model = ensemble.RandomForestRegressor()

			train_indices = np.squeeze(np.where(correspondence_train == problem))
			Z_temp_train = Z_train[train_indices]
			X_temp_train, y_temp_train = zip(*Z_temp_train)

			X_temp_train = np.array(X_temp_train)
			y_temp_train = np.array(np.squeeze(y_temp_train))

			loss = model.fit(X_temp_train,y_temp_train)

			Z_temp_test = Z_test[np.where(correspondence_test == problem)]
			X_temp_test, y_temp_test = zip(*Z_temp_test)

			X_temp_test = np.array(X_temp_test,dtype='float32')
			y_temp_test = np.array(np.squeeze(y_temp_test))

			y_hat = np.squeeze(model.predict(X_temp_test))

			result = np.sqrt(mean_squared_error(y_hat, y_temp_test))

			results.append(result)

		trial_results.append(np.mean(results))
		# print probas

	print 'final score is: %f averaged over %d trials' % (np.mean(trial_results),n_trials)


def run_londonschools_baxter():
	rng = np.random.RandomState(666)	

	tasks = IO.unpickle(data_dir+'/londonschools/londonschools.pkl')
	# tasks = tasks[:10]
	trial_results = []
	n_trials = 1

	print '# tasks: %d' % len(tasks)

	for trial in xrange(n_trials):
		Z_train, Z_test, correspondence_train, correspondence_test = generate_train_test_split(rng, tasks)

		X_train, y_train = zip(*Z_train)
		X_train = np.array(X_train)
		y_train = np.array(np.squeeze(y_train))

		N = len(X_train[0])
		nn = NeuralNetwork(layers_sizes=[N,1000,1000],activations = [lambda x: T.maximum(x, 0), lambda x: x])
		nn.add_layer(1,activation= lambda x: x)

		sampler = CorrespondenceSampler(rng, y_train, n_problems=len(tasks),correspondence=correspondence_train,regression=True)

		cost = RMSE

		algorithm = Baxter_Pretrainer(nn, learning_rate=.001,batch_size=25,sampler=sampler,max_n_batches=5000,stick=False,momentum=0.9,cost=cost)

		loss = algorithm.fit(X_train,y_train)	
		plot(loss)
		show()
		results = []

		for problem in xrange(len(tasks)):

			Z_temp_test = Z_test[np.where(correspondence_test == problem)]
			X_temp_test, y_temp_test = zip(*Z_temp_test)

			X_temp_test = np.array(X_temp_test,dtype='float32')
			y_temp_test = np.array(np.squeeze(y_temp_test))

			y_hat = algorithm.predictors[problem](X_temp_test)
			# print y_hat.shape, y_temp_test.shape

			result = np.sqrt(mean_squared_error(y_hat, y_temp_test))
			print result
			results.append(result)

		trial_results.append(np.mean(results))

	print 'final score is: %f averaged over %d trials' % (np.mean(trial_results),n_trials)

if __name__ =='__main__':
	# run_londonschools_stl()
	# run_londonschools_test()
	run_londonschools_baxter()
