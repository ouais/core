
import numpy as np
import theano
import theano.tensor as T
import pylab as plt

from utils import IO
from utils.misc import *
from utils.utils import batch

from sklearn import svm, linear_model
from test_multitask import downsample, test, load_dataset

from deepnn.NN import NeuralNetwork
from deepnn.algorithm import Backpropagation
from preprocessing import normalize_dataset
from collections import Counter

@batch(128)
def nn_map(nn,X):
    return nn.output[-2](X)

def transform_dataset(Z,neural_net,target_location):
	X,y = Z
	X = np.array(X)
	new_X = nn_map(neural_net,X)
	IO.pickle((new_X,y),target_location)

def transform_datasets():
	LDR_net = IO.unpickle('temp/LDR_NN.pkl')
	baxter_net = IO.unpickle('temp/baxter_NN.pkl')

	Z_valid = load_dataset(data_dir+"/mnist/mnist_valid.pkl")
	Z_test = load_dataset(data_dir+"/mnist/mnist_test.pkl")

	transform_dataset(Z_valid,LDR_net,'temp/LDR_mnist_valid.pkl')
	transform_dataset(Z_test,LDR_net,'temp/LDR_mnist_test.pkl')

	transform_dataset(Z_valid,baxter_net,'temp/baxter_mnist_valid.pkl')
	transform_dataset(Z_test,baxter_net,'temp/baxter_mnist_test.pkl')

def load_neural_network(X,y):
	N = len(X[0])
	classes = np.unique(y)
	nn = NeuralNetwork([N,len(classes)],activation_functions = T.nnet.softmax)
	bprop = Backpropagation(nn,train_last_layer_only=True,max_n_batches=100,momentum=.9)

	return bprop

def load_model():
	# return svm.SVC(kernel='linear')
	return linear_model.LogisticRegression()

# def train_test_split(rng,y, spc_train): 
# 	"""
# 	spc_train : samples per class for the train
# 	"""
# 	classes = np.unique(y)
# 	train_indices = []
# 	test_indices  = []

# 	for c in classes:
# 		candidates = np.where(y == c)[0]
# 		rng.shuffle(candidates)
# 		test_indices.extend(candidates[:spc_train])
# 		train_indices.extend(candidates[spc_train:])

# 	return train_indices, test_indices

def execute_test(rng, Xy_train, Xy_test, testing_problems = [[0,1,2,3,4,5,6,7,8,9]], n_samples_list = [1,2,5], n_estimations = 1):
	

	X_train, y_train = Xy_train
	X_train, y_train = np.array(X_train) , np.array(y_train)

	X_test, y_test = Xy_test
	X_test, y_test = np.array(X_test) , np.array(y_test)

	results = {}
	results.update([(x,[]) for x in n_samples_list])

	for n_samples in n_samples_list:
		print n_samples
		for i in xrange(n_estimations):

			indices = downsample(rng,y_train,n_samples)

			X_transfer, y_transfer = X_train[indices], y_train[indices]

			Z_test  = zip(X_test,y_test)			
			Z_train = zip(X_transfer,y_transfer)
#			p
			result = test(testing_problems, load_model, Z_train, Z_test)
			# t = lambda : load_neural_network(X_train,y_train)
			# result = test(testing_problems, t, Z_train, Z_test)
# 
			results[n_samples].append(np.mean(result))

	return results


def standard_test_core(name):
	Z_train = IO.unpickle('temp/'+name+"_mnist_valid.pkl")
	Z_test  = IO.unpickle('temp/'+name+"_mnist_test.pkl")
	n_samples_list = [1,2,5,10,50,100]
	rng = np.random.RandomState(666)	
	results = execute_test(rng, Z_train, Z_test, testing_problems = [[0,1,2,3,4,5,6,7,8,9]], n_samples_list = n_samples_list, n_estimations = 30)
	print_results(name,n_samples_list,results)
	return results

def print_results(name, n_samples_list,results):
	for n_samples in n_samples_list:
		print "%s scored %.4f with %d samples" % (name, np.mean(results[n_samples]), n_samples)
	return results

def standard_test():
	transform_datasets()
	results_LDR = standard_test_core("LDR")
	results_baxter = standard_test_core("baxter")
	save_graphs(results_LDR, results_baxter,'materials/std_test.pdf')

def save_graphs(results_LDR, results_baxter,file_name):
	
	plt.rc('text', usetex=False)
	plt.rc('font', family='serif',size=6)

	xs = [x for x in results_LDR]
	xs.sort()
	f1 = np.array([np.mean(results_LDR[x]) for x in xs])
	f1_err = np.array([np.std(results_LDR[x]) for x in xs])

	f2 = np.array([np.mean(results_baxter[x]) for x in xs])
	f2_err = np.array([np.std(results_LDR[x]) for x in xs])

	fig = plt.figure(figsize=(4,1.7))
	ax = fig.add_subplot(1,1,1)

	# plt.errorbar(xs, f1, yerr=f1_err)
	# plt.errorbar(xs, f2, yerr=f2_err)

	plt.plot(xs,f1,'-x',linewidth=.5,markersize=3, label="This work")
	plt.plot(xs,f2,'-x',linewidth=.5,markersize=3, label="Baxter")

	plt.legend(bbox_to_anchor=[1.1, 0.71])

	plt.xlabel('samples per class (log)')
	ax.set_xscale('log')
	ax.xaxis.grid(True,'both')
	plt.ylabel('accuracy')
	plt.legend(loc=4)

	plt.savefig(file_name,bbox_inches='tight')



if __name__ == '__main__':
	# standard_test()
	# experiment_1_test()
	pass
