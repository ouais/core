import numpy as np


from deepnn.NN import NeuralNetwork
from deepnn.algorithm import LDR_Pretrainer, Baxter_Pretrainer
from ldr.test_multitask import *



def s1_e1(experiment_name):
	last_layer_flag = True

	n_trials = 1

	print 'running experiment 1'
	print "method: %s" % experiment_name
	print "testing trains last layer only: " + str(last_layer_flag)


	batch_size = 200
	rng = np.random.RandomState(666)

	X,y = load_dataset(data_dir+"/mnist/mnist_train.pkl", normalize = True)
	X_test,y_test = load_dataset(data_dir+"/mnist/mnist_test.pkl", normalize = True)

	X_train, y_train = X[:-10000], y[:-10000]

	Z_test = zip(X_test,y_test)
	Z_train = zip(X_train,y_train)

	classes= np.unique(y_train)

	neural_nets = []
	test_results = []

	for trial in xrange(n_trials):

		N = len(X[0])	

		nn = NeuralNetwork(layers_sizes=[N,100,100],rng=rng)
		nn.add_layer(10)

		training_problems = [(0,1,2,3,4,5,6,7,8,9)]
		testing_problems = [[0,1,2,3,4,5,6,7,8,9]]

		X_train,y_train = zip(*Z_train)

		sampler = SupervisedSampler(rng,-1,y_train,problems=training_problems)

		if experiment_name == "LDR":
			algorithm = LDR_Pretrainer(nn,L1_lams=[0,0,0],L2_lams=[0,0,0],learning_rate=.1,batch_size=batch_size,sampler=sampler,max_n_batches=1000,f_momentum=.9,h_momentum=.9)
			loss = algorithm.fit(X_train,y_train)	
		elif experiment_name == "baxter":
			algorithm = Baxter_Pretrainer(nn,L1_lambda=0.0,L2_lambda=0.001,learning_rate=.1,batch_size=batch_size,sampler=sampler,max_n_batches=5000,stick=True,momentum=.9)
			loss = algorithm.fit(X_train,y_train)	
		

		neural_nets.append(nn)
		test_results.append(nn.score(X_test, y_test))

	IO.pickle((neural_nets,test_results),'temp/'+experiment_name+'_s1_e1.pkl')

def train_s1_e1():
	s1_e1("baxter")
	s1_e1("LDR")

if __name__=='__main__':
	train_s1_e1()