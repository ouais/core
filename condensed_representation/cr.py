import numpy as np
import theano
import theano.tensor as T

from deepnn.NN import NeuralNetwork, visualize
from deepnn.algorithm import *
from preprocessing import normalize_dataset2,normalize_dataset

from ldr.samplers import *

from sklearn.svm import SVC

from utils import IO
from utils.utils import downsample
from utils.misc import data_dir

from pylab import *



def train_da():
	nn = NeuralNetwork([784,100],activation_functions= T.nnet.sigmoid)

	X_train,y_train =IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X_train,y_train = normalize_dataset2(X_train,y_train)

	da = da_Pretrainer(nn,learning_rate=.01,n_batches=3000,corruption_levels=[.3])

	loss = da.fit(X_train)

	for i in xrange(len(nn.layers)):
		visualize(X_train,nn,i,'materials/dae_weights_{}.png'.format(i))

	nn.add_layer(10)

	IO.pickle(nn,'materials/nn.pkl')

def train_lldr():

	nn = NeuralNetwork([784,100])
	nn.add_layer(20)

	X_train,y_train =IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X_train,y_train = normalize_dataset(X_train,y_train)

	sampler = RandomizedSampler(nn.numpy_rng,data_size=len(X_train),problem_size=10,batch_size=200)

	algorithm = Unsupervised_LLDR(nn,L1_lambda=0.0,L2_lambda=0.0,learning_rate=.01,sampler=sampler,batch_size=200,max_n_batches=10000)
	loss = algorithm.fit(X_train)

	for i in xrange(len(nn.layers)):
		visualize(X_train,nn,i,'materials/dae_weights_{}.png'.format(i))

	IO.pickle(nn,'materials/nn.pkl')

	

def test():
	rng = np.random.RandomState(666)

	nn = IO.unpickle('materials/nn.pkl')

	X_train,y_train =IO.unpickle(data_dir+"/mnist/mnist_train.pkl")
	X_train,y_train = normalize_dataset2(X_train,y_train)

	X_test,y_test =IO.unpickle(data_dir+"/mnist/mnist_test.pkl")
	X_test,y_test = normalize_dataset2(X_test,y_test)

	indices = downsample(rng,y_train,1)
	X_transfer, y_transfer = X_train[indices], y_train[indices]

	# nn.add_layer(10)

	bprop = Backpropagation(nn,train_last_layer_only=True,max_n_batches=100,momentum=.9)

	bprop.fit(X_transfer,y_transfer)

	svm = SVC()
	svm.fit(X_transfer,y_transfer)

	print 'classification score: {}'.format(nn.score(X_test,y_test))
	print 'svm score: {}'.format(svm.score(X_test,y_test))

def test_sparse_coding():
	dictionary = sklearn.decomposition.MiniBatchDictionaryLearning(n_components=20)
	scoding = sklearn.decomposition.sparse_encode(X, dictionary)



if __name__ == '__main__':
	train_da()
	test()

