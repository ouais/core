
import sys
import pylab
import numpy as np
sys.path.append("..")

import IO
from misc import *
from pylab import *



def normalize_dataset(path,output_pkl):
	f = open(path)
	dataset = f.readlines()
	X = []
	y = []
	for i in xrange(2,len(dataset)):
		instance = dataset[i]
		z = instance.split(' ')
		X.append(np.array([float(x) for x in z[1:-1]]))
		y.append(int(z[-1]))

	X = np.array(X)
	y = np.array(y)
	
	IO.pickle((X,y),output_pkl)


if __name__=='__main__':
	normalize_dataset(data_dir+'faces/train/svm.train.normgrey','cbcl_faces_train.pkl')
	normalize_dataset(data_dir+'faces/test/svm.test.normgrey','cbcl_faces_test.pkl')