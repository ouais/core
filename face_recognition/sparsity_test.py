import numpy as np
import scipy
import time
from scipy.sparse import *
from pylab import *

def random_matrix(N,M,p):
	a = np.random.random((N,M))
	b = np.random.random((N,M)) > p
	c = a*b
	return coo_matrix(c)

def test_1():
	N = 400
	K = 30
	p = .5

	n_trials = 1000
	times = []																															

	for p in np.arange(0,.05,.01):
		total_time = 0
		for i in xrange(n_trials):
			a = random_matrix(K,N,p)
			b = random_matrix(N,K,p)
			start = time.time()
			c = a.dot(b)
			end = time.time()

			total_time += end-start

		times.append(total_time/n_trials)

	plot(np.array(times))
	show()
	# print 'time: %f ' % (total_time/n_trials)

if __name__=='__main__':
	test_1()